<?php

// CRON
//add_action('get_crypto_coins_info', 'get_crypto_coins_info_cron_function');
//add_action('init', 'get_crypto_coins_info_cron_function');
function get_crypto_coins_info_cron_function() {
	global $wpdb;

	// get top 150 coins
	$crypto_datas = file_get_contents( 'https://pro-api.coinmarketcap.com/v1/cryptocurrency/listings/latest?CMC_PRO_API_KEY='. CMC_PRO_API_KEY .'&limit=150&convert=USD' );
	$crypto_datas = json_decode($crypto_datas, true);

	$crypto_datas_btc = file_get_contents( 'https://pro-api.coinmarketcap.com/v1/cryptocurrency/listings/latest?CMC_PRO_API_KEY='. CMC_PRO_API_KEY .'&limit=150&convert=BTC' );
	$crypto_datas_btc = json_decode($crypto_datas_btc, true);


	// Merge USD & BTC data
	if ( isset($crypto_datas['data'][0]['id']) && isset($crypto_datas_btc['data'][0]['id']) ) {
		$crypto_datas = $crypto_datas['data'];
		$crypto_datas_btc = $crypto_datas_btc['data'];

		foreach ($crypto_datas_btc as $key => $val) {
			if ( isset($crypto_datas_btc[$key]['quote']['BTC']) ) {
				if ( $crypto_datas[$key]['symbol'] == $crypto_datas_btc[$key]['symbol'] ) {
					$crypto_datas[$key]['quote']['BTC'] = $crypto_datas_btc[$key]['quote']['BTC'];
				}
			}
		}
	} else {
		$crypto_datas = "";
	}
	unset($crypto_datas_btc);


	// Get all mineable currency mining info
	$mining_datas = get_mining_datas();

	// BTC infok lekérése (pl: dominance)
	add_btc_movement_info();


	if ( !empty($crypto_datas) ) {


		// --- Set new shitcoins ---

		$last_top_150_coins = array();
		foreach ($crypto_datas as $key => $value) {
			if ( check_coin_is_banned($value['symbol']) === FALSE ) {
				$last_top_150_coins []= $value['symbol'];
			}
		}

		$last_150_coin_from_db = $wpdb->get_results( "SELECT `coin_ID`
																									FROM `{$wpdb->prefix}crypto_coinmarketcap_data`
																									ORDER BY `datetime` DESC LIMIT 150", ARRAY_A );

		foreach ($last_150_coin_from_db as $key => $value) {
			$coinname = get_coin_name_by_id($value['coin_ID']);
			if ( !empty($coinname) && !in_array($coinname, $last_top_150_coins) ) {
				if ( check_coin_is_banned($coinname) === FALSE ) {

					$wpdb->insert(
						$wpdb->prefix .'crypto_shitcoins',
						array('shitcoin_name' => $coinname),
						array('%s')
					);

				}
			}
		}


		foreach ($crypto_datas as $key => $coin_data) {

			if ( isset($coin_data['symbol']) && !empty($coin_data['symbol']) && ( check_coin_is_banned($coin_data['symbol']) === FALSE ) ) {

				$symbol = esc_attr($coin_data['symbol']);
				$rank 	= (int) $coin_data['cmc_rank'];


					$coin_id = get_coin_id_by_name($symbol);
					if ( $coin_id > 0 ) {  }
					else {

						$wpdb->insert(
								$wpdb->prefix .'crypto_coin_names',
								array(
									'coin_name' => $symbol,
									'coin_full_name' => "",
								),
								array('%s','%s')
						);
						$coin_id = $wpdb->insert_id;
					}


					add_bittrex_coin_prices_v2($symbol);


					$price_usd = "";
					$price_btc = "";
					$market_cap_usd = "";
					$percent_change_1h = "";
					$percent_change_24h = "";
					$percent_change_7d = "";
					$volume_usd_24h = "";


					if ( isset($coin_data['quote']['USD']['price']) ) {
						$price_usd = round($coin_data['quote']['USD']['price'], 8);
					}
					if ( isset($coin_data['quote']['BTC']['price']) ) {
						$price_btc = round($coin_data['quote']['BTC']['price'], 8);
					}
					if ( isset($coin_data['quote']['USD']['market_cap']) ) {
						$market_cap_usd = round($coin_data['quote']['USD']['market_cap'], 1);
					}
					if ( isset($coin_data['quote']['USD']['percent_change_1h']) ) {
						$percent_change_1h = round($coin_data['quote']['USD']['percent_change_1h'], 2);
					}
					if ( isset($coin_data['quote']['USD']['percent_change_24h']) ) {
						$percent_change_24h = round($coin_data['quote']['USD']['percent_change_24h'], 2);
					}
					if ( isset($coin_data['quote']['USD']['percent_change_7d']) ) {
						$percent_change_7d = round($coin_data['quote']['USD']['percent_change_7d'], 2);
					}
					if ( isset($coin_data['quote']['USD']['volume_24h']) ) {
						$volume_usd_24h = round($coin_data['quote']['USD']['volume_24h'], 3);
					}


					$wpdb->insert(
							$wpdb->prefix .'crypto_coinmarketcap_data',
							array(
								'coin_ID' => $coin_id,
								'rank' => $rank,
								'price_usd' => $price_usd,
								'price_btc' => $price_btc,
								'market_cap_usd' => $market_cap_usd,
								'percent_change_1h' => $percent_change_1h,
								'percent_change_24h' => $percent_change_24h,
								'percent_change_7d' => $percent_change_7d,
								'volume_usd_24h' => $volume_usd_24h,
							),
							array('%d','%d','%s','%s','%s','%s','%s','%s','%s')
					);


					if ( date('G') == 4 ) {
						set_markets_volument_data($coin_id);
					}


					if ( is_array($mining_datas[$symbol]) && !empty($mining_datas[$symbol]) ) {

						$mining_block_time = "";
						$mining_difficult = "";
						$mining_difficult24 = "";
						$mining_nethash = "";
						$mining_estimated_rewards = "";
						$mining_estimated_rewards24 = "";
						$mining_btc_revenue = "";
						$mining_btc_revenue24 = "";
						$mining_profitability = "";
						$mining_profitability24 = "";

						if ( isset($mining_datas[$symbol]['block_time']) ) {
							$mining_block_time = $mining_datas[$symbol]['block_time'];
						}
						if ( isset($mining_datas[$symbol]['diff']) ) {
							$mining_difficult = $mining_datas[$symbol]['diff'];
						}
						if ( isset($mining_datas[$symbol]['diff24']) ) {
							$mining_difficult24 = $mining_datas[$symbol]['diff24'];
						}
						if ( isset($mining_datas[$symbol]['nethash']) ) {
							$mining_nethash = $mining_datas[$symbol]['nethash'];
						}
						if ( isset($mining_datas[$symbol]['estimated_rewards']) ) {
							$mining_estimated_rewards = $mining_datas[$symbol]['estimated_rewards'];
						}
						if ( isset($mining_datas[$symbol]['estimated_rewards24']) ) {
							$mining_estimated_rewards24 = $mining_datas[$symbol]['estimated_rewards24'];
						}
						if ( isset($mining_datas[$symbol]['btc_revenue']) ) {
							$mining_btc_revenue = $mining_datas[$symbol]['btc_revenue'];
						}
						if ( isset($mining_datas[$symbol]['btc_revenue24']) ) {
							$mining_btc_revenue24 = $mining_datas[$symbol]['btc_revenue24'];
						}
						if ( isset($mining_datas[$symbol]['profitability']) ) {
							$mining_profitability = $mining_datas[$symbol]['profitability'];
						}
						if ( isset($mining_datas[$symbol]['profitability24']) ) {
							$mining_profitability24 = $mining_datas[$symbol]['profitability24'];
						}

						$wpdb->insert(
								$wpdb->prefix .'crypto_mining_data',
								array(
									'coin_ID' => $coin_id,
									'block_time' => $mining_block_time,
									'difficult' => $mining_difficult,
									'difficult24' => $mining_difficult24,
									'nethash' => $mining_nethash,
									'estimated_rewards' => $mining_estimated_rewards,
									'estimated_rewards24' => $mining_estimated_rewards24,
									'btc_revenue' => $mining_btc_revenue,
									'btc_revenue24' => $mining_btc_revenue24,
									'profitability' => $mining_profitability,
									'profitability24' => $mining_profitability24,
								),
								array('%d','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')
						);
					}


			}
		}
	}
}
