<?php

// CRON ---> 5 percenként lefut és összerakja az adatokat
//add_action('create_crypto_statistic', 'create_crypto_statistic_function');
function create_crypto_statistic_function($manual_update = "") {
	global $wpdb;
	$egyszerre_kiertekelendo_coinok = EGYSZERRE_ERTEKELENDO_COINOK;


	// Remove old datas
	$wpdb->query(
		$wpdb->prepare(
			"DELETE FROM `{$wpdb->prefix}crypto_compilation`
				WHERE `datetime` < %s
			",
				date('Y-m-d H:i:s', REMOVE_OLD_COMPILATION_DATAS_TIMESTAMP)
			)
	);
	// ---

	// Force update (all)
	if ( $manual_update == "manual_update" ) {
		update_option( 'crypto_compilation_in_progress', "" );
		$egyszerre_kiertekelendo_coinok = 1000;
	}

	// Force update (one)
	if ( isset($_GET['update_coin_by_id']) && (intval($_GET['update_coin_by_id']) > 0) ) {
		update_option( 'crypto_compilation_in_progress', "" );
		$egyszerre_kiertekelendo_coinok = 1;
	}


	$compilation_in_progress = get_option('crypto_compilation_in_progress');

	if ( empty($compilation_in_progress) ) {
		update_option( 'crypto_compilation_in_progress', "" );

		// ----- Get Bittrex coins ID -----

		$bittrex_coins_temp = array();
		$bittrex_coins = $wpdb->get_results( "SELECT DISTINCT `coin_ID` FROM `{$wpdb->prefix}crypto_bittrex_data`", ARRAY_A );
		foreach ($bittrex_coins as $key => $value) {
			$bittrex_coins_temp []= $value['coin_ID'];
		}
		$bittrex_coins_temp []= BCH_ID;
		$bittrex_coins = $bittrex_coins_temp;
		unset($bittrex_coins_temp);


		// Force update
		if ( isset($_GET['update_coin_by_id']) && (intval($_GET['update_coin_by_id']) > 0) ) {
			$bittrex_coins = array( intval($_GET['update_coin_by_id']) );
		}


		// ----- Get last 150 coin -----

		$last_150_coin = $wpdb->get_results( "SELECT `ID`, `coin_ID`, `rank`, `price_btc`, `datetime`
																					FROM `{$wpdb->prefix}crypto_coinmarketcap_data`
																					WHERE `coin_ID` != ". BTC_ID ."
																					ORDER BY `datetime` DESC LIMIT 150", ARRAY_A );

		// csak Bittrexen lévő coinok bekerülése
		$compilation_in_progress = array();
		foreach ($last_150_coin as $key => $value) {

			if ( isset($value['coin_ID']) && !empty($value['coin_ID']) && in_array(strval($value['coin_ID']), $bittrex_coins) ) {
				if ( check_coin_is_banned(get_coin_name_by_id($value['coin_ID'])) === FALSE ) {
					if ( check_coin_compilation_have_to_run(intval($value['coin_ID'])) ) {
						$compilation_in_progress []= $value;
					}
				}
			}
		}

		$compilation_in_progress = array_chunk(array_values($compilation_in_progress), $egyszerre_kiertekelendo_coinok);
		update_option( 'crypto_compilation_in_progress', $compilation_in_progress );
	}

	$compilation_in_progress_temp = $compilation_in_progress[0];
	unset($compilation_in_progress[0]);
	update_option( 'crypto_compilation_in_progress', array_values($compilation_in_progress) );


	$coins_array = array();

	if ( !empty($compilation_in_progress_temp) ) {
		foreach ($compilation_in_progress_temp as $key => $coin_data) {
			if ( isset($coin_data['coin_ID']) && !empty($coin_data['coin_ID']) && ($key < 1000) ) {

				$get_cmc_average_btc_price_range 			= get_cmc_avg_price_range( $coin_data['coin_ID'], 'BTC', 12, TIME_INTERVAL_TIMESTAMP );
				$get_bittrex_average_btc_price_range 	= get_bittrex_average_btc_price_range($coin_data['coin_ID']);
				$coin_name 														= get_coin_name_by_id($coin_data['coin_ID']);

				$bittrex_total_sells 									= get_bittrex_total_sells_and_buys($coin_data['coin_ID'], 'sell');
				$bittrex_total_buys 									= get_bittrex_total_sells_and_buys($coin_data['coin_ID'], 'buy');

				$bitfinex_total_sells 								= get_bitfinex_total_sells_and_buys($coin_data['coin_ID'], 'sell');
				$bitfinex_total_buys 									= get_bitfinex_total_sells_and_buys($coin_data['coin_ID'], 'buy');

				$hitbtc_total_sells 									= get_hitbtc_total_sells_and_buys($coin_data['coin_ID'], 'sell');
				$hitbtc_total_buys 										= get_hitbtc_total_sells_and_buys($coin_data['coin_ID'], 'buy');

				$poloniex_total_sells 								= get_poloniex_total_sells_and_buys($coin_data['coin_ID'], 'sell');
				$poloniex_total_buys 									= get_poloniex_total_sells_and_buys($coin_data['coin_ID'], 'buy');

				$bithumb_total_sells 									= get_bithumb_total_sells_and_buys($coin_data['coin_ID'], 'sell');
				$bithumb_total_buys 									= get_bithumb_total_sells_and_buys($coin_data['coin_ID'], 'buy');

				$kraken_total_sells 									= get_kraken_total_sells_and_buys($coin_data['coin_ID'], 'sell');
				$kraken_total_buys 										= get_kraken_total_sells_and_buys($coin_data['coin_ID'], 'buy');

				$binance_total_sells 									= get_binance_total_sells_and_buys($coin_data['coin_ID'], 'sell');
				$binance_total_buys 									= get_binance_total_sells_and_buys($coin_data['coin_ID'], 'buy');


				$all_buys_datas_evaluation = array();
				if ( isset($bittrex_total_buys['percent_change']) ) 	{ $all_buys_datas_evaluation []= $bittrex_total_buys['percent_change']; }
				if ( isset($bitfinex_total_buys['percent_change']) ) 	{ $all_buys_datas_evaluation []= $bitfinex_total_buys['percent_change']; }
				if ( isset($hitbtc_total_buys['percent_change']) ) 		{ $all_buys_datas_evaluation []= $hitbtc_total_buys['percent_change']; }
				if ( isset($poloniex_total_buys['percent_change']) ) 	{ $all_buys_datas_evaluation []= $poloniex_total_buys['percent_change']; }
				if ( isset($bithumb_total_buys['percent_change']) ) 	{ $all_buys_datas_evaluation []= $bithumb_total_buys['percent_change']; }
				if ( isset($kraken_total_buys['percent_change']) ) 		{ $all_buys_datas_evaluation []= $kraken_total_buys['percent_change']; }
				if ( isset($binance_total_buys['percent_change']) ) 	{ $all_buys_datas_evaluation []= $binance_total_buys['percent_change']; }
				$all_buys_datas_evaluation = array_sum(array_filter($all_buys_datas_evaluation));

				$all_sells_datas_evaluation = array();
				if ( isset($bittrex_total_sells['percent_change']) ) 	{ $all_sells_datas_evaluation []= $bittrex_total_sells['percent_change']; }
				if ( isset($bitfinex_total_sells['percent_change']) ) { $all_sells_datas_evaluation []= $bitfinex_total_sells['percent_change']; }
				if ( isset($hitbtc_total_sells['percent_change']) ) 	{ $all_sells_datas_evaluation []= $hitbtc_total_sells['percent_change']; }
				if ( isset($poloniex_total_sells['percent_change']) ) { $all_sells_datas_evaluation []= $poloniex_total_sells['percent_change']; }
				if ( isset($bithumb_total_sells['percent_change']) ) 	{ $all_sells_datas_evaluation []= $bithumb_total_sells['percent_change']; }
				if ( isset($kraken_total_sells['percent_change']) ) 	{ $all_sells_datas_evaluation []= $kraken_total_sells['percent_change']; }
				if ( isset($binance_total_sells['percent_change']) ) 	{ $all_sells_datas_evaluation []= $binance_total_sells['percent_change']; }
				$all_sells_datas_evaluation = array_sum(array_filter($all_sells_datas_evaluation));

				$all_sells_and_buys_datas_evaluation = get_all_sells_and_buys_datas_evaluation($all_buys_datas_evaluation, $all_sells_datas_evaluation);


				add_bittrex_coin_prices_v2($coin_name);

				$coins_array[$coin_name] =
				array(
						'coin_ID' 		=> $coin_data['coin_ID'],
						'actual_rank' => $coin_data['rank'],
						'last_price' 	=> $coin_data['price_btc'], // aktuális ár
						'timestamp' 	=> get_latest_cmc_data_date($coin_data['coin_ID']), // utolsó frissítés időpontja

						// Számolgatós/hozzáadandó értékek
						'logo_img' 								=> get_coin_logo_img($coin_data['coin_ID']),
						'link' 										=> get_coin_link($coin_data['coin_ID']),
						'price_change_in_percent' => get_price_change_in_percent($coin_data['price_btc'], $get_cmc_average_btc_price_range['tartomany_end']),
						'av_btc_price_range' 			=> $get_cmc_average_btc_price_range['html'],
						'av_btc_price' 						=> get_cmc_average_btc_price($coin_data['coin_ID']),
						'volume' 									=> get_cmc_coin_volume($coin_data['coin_ID']),
						'ranks' 									=> get_ranks($coin_data['coin_ID']),
						'rating' 									=> get_rating_info($coin_data['coin_ID'], 'cmc', $coin_data['price_btc'], $get_cmc_average_btc_price_range['tartomany_start'], $get_cmc_average_btc_price_range['tartomany_end']),
						'oldest_data' 						=> get_oldest_cmc_data($coin_data['coin_ID']),
						'evaluated_data' 					=> get_evaluated_data($coin_data['coin_ID']), // kiértékelt adatok (darab)
						'creation_date' 					=> get_creation_date($coin_data['coin_ID']), // Bittrexre bejegyezve ekkor

						// Bittrex adatok
						'bittrex_last_price' 																	=> get_bittrex_last_price($coin_data['coin_ID']),
						'bittrex_price_change_in_percent'											=> get_price_change_in_percent(get_bittrex_last_price($coin_data['coin_ID']), $get_bittrex_average_btc_price_range['tartomany_end']),
						'bittrex_av_btc_price_range' 													=> $get_bittrex_average_btc_price_range['html'],
						'bittrex_av_btc_price' 																=> get_bittrex_av_btc_price($coin_data['coin_ID']),
						'bittrex_evaluated_data'															=> get_bittrex_evaluated_data($coin_data['coin_ID']), // kiértékelt adatok (darab)
						'bittrex_rating' 																			=> get_rating_info($coin_data['coin_ID'], 'bittrex', get_bittrex_last_price($coin_data['coin_ID']), $get_bittrex_average_btc_price_range['tartomany_start'], $get_bittrex_average_btc_price_range['tartomany_end']),
						'bittrex_total_sells'																	=> $bittrex_total_sells,
						'bittrex_total_buys'																	=> $bittrex_total_buys,
						'bittrex_sells_and_buys_datas_evaluation' 						=> get_sells_and_buys_datas_evaluation($bittrex_total_buys, $bittrex_total_sells),
						'bittrex_percentage_distribution_of_the_total_price' 	=> $get_bittrex_average_btc_price_range['percentage_distribution_of_the_total_price'],

						// Bitfinex
						'bitfinex_total_sells'											=> $bitfinex_total_sells,
						'bitfinex_total_buys'												=> $bitfinex_total_buys,
						'bitfinex_sells_and_buys_datas_evaluation' 	=> get_sells_and_buys_datas_evaluation($bitfinex_total_buys, $bitfinex_total_sells),

						// HitBTC
						'hitbtc_total_sells'											=> $hitbtc_total_sells,
						'hitbtc_total_buys'												=> $hitbtc_total_buys,
						'hitbtc_sells_and_buys_datas_evaluation' 	=> get_sells_and_buys_datas_evaluation($hitbtc_total_buys, $hitbtc_total_sells),

						// Poloniex
						'poloniex_total_sells'											=> $poloniex_total_sells,
						'poloniex_total_buys'												=> $poloniex_total_buys,
						'poloniex_sells_and_buys_datas_evaluation' 	=> get_sells_and_buys_datas_evaluation($poloniex_total_buys, $poloniex_total_sells),

						// Bithumb
						'bithumb_total_sells'												=> $bithumb_total_sells,
						'bithumb_total_buys'												=> $bithumb_total_buys,
						'bithumb_sells_and_buys_datas_evaluation' 	=> get_sells_and_buys_datas_evaluation($bithumb_total_buys, $bithumb_total_sells),

						// Kraken
						'kraken_total_sells'												=> $kraken_total_sells,
						'kraken_total_buys'													=> $kraken_total_buys,
						'kraken_sells_and_buys_datas_evaluation' 		=> get_sells_and_buys_datas_evaluation($kraken_total_buys, $kraken_total_sells),

						// Binance
						'binance_total_sells'												=> $binance_total_sells,
						'binance_total_buys'												=> $binance_total_buys,
						'binance_sells_and_buys_datas_evaluation' 	=> get_sells_and_buys_datas_evaluation($binance_total_buys, $binance_total_sells),


						// All exchanges data
						'all_sells_and_buys_datas_evaluation'				=> $all_sells_and_buys_datas_evaluation,

						// Bányász adatok
						'mining_block_time' 				=> get_mining_data($coin_data['coin_ID'], 'block_time', ""),
						'mining_difficult' 					=> get_mining_data($coin_data['coin_ID'], 'difficult24', 'difficult'),
						'mining_nethash' 						=> get_mining_data($coin_data['coin_ID'], 'nethash', ""),
						'mining_estimated_rewards' 	=> get_mining_data($coin_data['coin_ID'], 'estimated_rewards24', 'estimated_rewards'),
						'mining_btc_revenue' 				=> get_mining_data($coin_data['coin_ID'], 'btc_revenue24', 'btc_revenue'),
						'mining_profitability' 			=> get_mining_data($coin_data['coin_ID'], 'profitability24', 'profitability'),
						'mining_rating' 						=> get_mining_rating_info($coin_data['coin_ID']),
					);


				// --- insert or update ---

				$coin_id = (int) $coin_data['coin_ID'];
				$crypto_compilation = $wpdb->get_results( "SELECT `ID` FROM `{$wpdb->prefix}crypto_compilation`
																										WHERE `coin_ID` = '{$coin_id}' LIMIT 1", ARRAY_A );

				$inserted_data = array(
													'coin_ID' 		=> $coins_array[$coin_name]['coin_ID'],
													'actual_rank' => $coins_array[$coin_name]['actual_rank'],
													'last_price' 	=> $coins_array[$coin_name]['last_price'],
													'timestamp' 	=> $coins_array[$coin_name]['timestamp'],

													// Számolgatós/hozzáadandó értékek
													'logo_img' 								=> $coins_array[$coin_name]['logo_img'],
													'link' 										=> $coins_array[$coin_name]['link'],
													'price_change_in_percent' => $coins_array[$coin_name]['price_change_in_percent'],
													'av_btc_price_range' 			=> $coins_array[$coin_name]['av_btc_price_range'],
													'av_btc_price' 						=> $coins_array[$coin_name]['av_btc_price'],
													'volume' 									=> json_encode($coins_array[$coin_name]['volume']),
													'ranks' 									=> json_encode($coins_array[$coin_name]['ranks']),
													'rating' 									=> json_encode($coins_array[$coin_name]['rating']),
													'oldest_data' 						=> json_encode($coins_array[$coin_name]['oldest_data']),
													'evaluated_data' 					=> $coins_array[$coin_name]['evaluated_data'],
													'creation_date' 					=> $coins_array[$coin_name]['creation_date'],

													// Bittrex adatok
													'bittrex_last_price' 																	=> $coins_array[$coin_name]['bittrex_last_price'],
													'bittrex_price_change_in_percent'											=> $coins_array[$coin_name]['bittrex_price_change_in_percent'],
													'bittrex_av_btc_price_range' 													=> $coins_array[$coin_name]['bittrex_av_btc_price_range'],
													'bittrex_av_btc_price' 																=> $coins_array[$coin_name]['bittrex_av_btc_price'],
													'bittrex_evaluated_data'															=> $coins_array[$coin_name]['bittrex_evaluated_data'],
													'bittrex_rating' 																			=> json_encode($coins_array[$coin_name]['bittrex_rating']),
													'bittrex_total_sells'																	=> json_encode($coins_array[$coin_name]['bittrex_total_sells']),
													'bittrex_total_buys'																	=> json_encode($coins_array[$coin_name]['bittrex_total_buys']),
													'bittrex_sells_and_buys_datas_evaluation'							=> $coins_array[$coin_name]['bittrex_sells_and_buys_datas_evaluation'],
													'bittrex_percentage_distribution_of_the_total_price' 	=> json_encode($coins_array[$coin_name]['bittrex_percentage_distribution_of_the_total_price']),

													// Bittfinex
													'bitfinex_total_sells'											=> json_encode($coins_array[$coin_name]['bitfinex_total_sells']),
													'bitfinex_total_buys'												=> json_encode($coins_array[$coin_name]['bitfinex_total_buys']),
													'bitfinex_sells_and_buys_datas_evaluation'	=> $coins_array[$coin_name]['bitfinex_sells_and_buys_datas_evaluation'],

													// HitBTC
													'hitbtc_total_sells'											=> json_encode($coins_array[$coin_name]['hitbtc_total_sells']),
													'hitbtc_total_buys'												=> json_encode($coins_array[$coin_name]['hitbtc_total_buys']),
													'hitbtc_sells_and_buys_datas_evaluation'	=> $coins_array[$coin_name]['hitbtc_sells_and_buys_datas_evaluation'],

													// Poloniex
													'poloniex_total_sells'											=> json_encode($coins_array[$coin_name]['poloniex_total_sells']),
													'poloniex_total_buys'												=> json_encode($coins_array[$coin_name]['poloniex_total_buys']),
													'poloniex_sells_and_buys_datas_evaluation'	=> $coins_array[$coin_name]['poloniex_sells_and_buys_datas_evaluation'],

													// Bithumb
													'bithumb_total_sells'											=> json_encode($coins_array[$coin_name]['bithumb_total_sells']),
													'bithumb_total_buys'											=> json_encode($coins_array[$coin_name]['bithumb_total_buys']),
													'bithumb_sells_and_buys_datas_evaluation'	=> $coins_array[$coin_name]['bithumb_sells_and_buys_datas_evaluation'],

													// Kraken
													'kraken_total_sells'											=> json_encode($coins_array[$coin_name]['kraken_total_sells']),
													'kraken_total_buys'												=> json_encode($coins_array[$coin_name]['kraken_total_buys']),
													'kraken_sells_and_buys_datas_evaluation'	=> $coins_array[$coin_name]['kraken_sells_and_buys_datas_evaluation'],

													// Binance
													'binance_total_sells'											=> json_encode($coins_array[$coin_name]['binance_total_sells']),
													'binance_total_buys'											=> json_encode($coins_array[$coin_name]['binance_total_buys']),
													'binance_sells_and_buys_datas_evaluation'	=> $coins_array[$coin_name]['binance_sells_and_buys_datas_evaluation'],

													'all_sells_and_buys_datas_evaluation'			=> json_encode($coins_array[$coin_name]['all_sells_and_buys_datas_evaluation']),

													// Bányász adatok
													'mining_block_time' 				=> json_encode($coins_array[$coin_name]['mining_block_time']),
													'mining_difficult' 					=> json_encode($coins_array[$coin_name]['mining_difficult']),
													'mining_nethash' 						=> json_encode($coins_array[$coin_name]['mining_nethash']),
													'mining_estimated_rewards' 	=> json_encode($coins_array[$coin_name]['mining_estimated_rewards']),
													'mining_btc_revenue' 				=> json_encode($coins_array[$coin_name]['mining_btc_revenue']),
													'mining_profitability' 			=> json_encode($coins_array[$coin_name]['mining_profitability']),
													'mining_rating' 						=> json_encode($coins_array[$coin_name]['mining_rating']),

													'datetime' 									=> current_time('mysql'),
												);


				$cc_id = (int) $crypto_compilation[0]['ID'];
				if ( isset($cc_id) && !empty($cc_id) && ( $cc_id > 0 ) ) {

					$wpdb->update(
						$wpdb->prefix .'crypto_compilation',
						$inserted_data,
						array( 'coin_ID' => $coin_id ),
						array(
							'%d','%d','%s','%s','%s','%s','%s','%s','%s',
							'%s','%s','%s','%s','%s','%s','%s','%s','%s',
							'%s','%s','%s','%s','%s','%s','%s','%s','%s',
							'%s','%s','%s','%s','%s','%s','%s','%s','%s',
							'%s','%s','%s','%s','%s','%s','%s','%s','%s',
							'%s','%s','%s','%s','%s','%s','%s',
						),
						array( '%d' )
					);

				} else {

					$new_row_id = $wpdb->insert(
						$wpdb->prefix .'crypto_compilation',
						$inserted_data,
						array(
							'%d','%d','%s','%s','%s','%s','%s','%s','%s',
							'%s','%s','%s','%s','%s','%s','%s','%s','%s',
							'%s','%s','%s','%s','%s','%s','%s','%s','%s',
							'%s','%s','%s','%s','%s','%s','%s','%s','%s',
							'%s','%s','%s','%s','%s','%s','%s','%s','%s',
							'%s','%s','%s','%s','%s','%s','%s',
						)
					);
				}

				// For update
				get_cmc_avg_price_range($coin_id, 'BTC', 12, strtotime("-3 day"));
				get_cmc_avg_price_range($coin_id, 'BTC', 12, strtotime("-2 week"));
				get_cmc_avg_price_range($coin_id, 'BTC', 12, strtotime("-1 month"));
				get_cmc_avg_price_range($coin_id, 'BTC', 12, strtotime("-2 month"));
				get_cmc_avg_price_range($coin_id, 'BTC', 12, strtotime("-3 month"));
				get_cmc_avg_price_range($coin_id, 'BTC', 10, strtotime("-6 month"));
				get_cmc_avg_price_range($coin_id, 'BTC', 10, strtotime("-12 month"));

				get_cmc_avg_price_range($coin_id, 'USD', 12, strtotime("-3 day"));
				get_cmc_avg_price_range($coin_id, 'USD', 12, strtotime("-2 week"));
				get_cmc_avg_price_range($coin_id, 'USD', 12, strtotime("-1 month"));
				get_cmc_avg_price_range($coin_id, 'USD', 12, strtotime("-2 month"));
				get_cmc_avg_price_range($coin_id, 'USD', 12, strtotime("-3 month"));
				get_cmc_avg_price_range($coin_id, 'USD', 10, strtotime("-6 month"));
				get_cmc_avg_price_range($coin_id, 'USD', 10, strtotime("-12 month"));

				unset($inserted_data);
			}
		}
	}


	// Force update
	if ( isset($_GET['update_coin_by_id']) && (intval($_GET['update_coin_by_id']) > 0) ) {
		$redirect_url = get_permalink( PAGE_OSSZEALLITASOK ) .'#'. strtolower(get_coin_name_by_id(intval($_GET['update_coin_by_id'])));
		wp_redirect( $redirect_url, 301 );
		echo '<script type="text/javascript">window.location.href = "'. $redirect_url .'";</script>';
		exit;
	}

	if ( $manual_update == "manual_update" ) {
		$redirect_url = get_permalink( PAGE_OSSZEALLITASOK );
		wp_redirect( $redirect_url, 301 );
		echo '<script type="text/javascript">window.location.href = "'. $redirect_url .'";</script>';
		exit;
	}

	return $coins_array;
}
