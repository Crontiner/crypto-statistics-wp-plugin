<?php

function get_logo_url_by_coin_id($coin_ID = "") {
	if ( empty($coin_ID) ) { return ""; }
	global $wpdb;

	$logo_url = $wpdb->get_results( "SELECT `logo_url`
																		FROM `{$wpdb->prefix}crypto_coin_names`
																		WHERE `ID` = $coin_ID LIMIT 1", ARRAY_A );

	if ( isset($logo_url[0]['logo_url']) && !empty($logo_url[0]['logo_url']) ) {
		return $logo_url[0]['logo_url'];
	}
	return "";
}


function last_month_minings_diagrams() {
	global $wpdb;

	$sql = "SELECT `mining_data`, `timestamp` FROM `{$wpdb->prefix}crypto_statistic`
						WHERE `mining_data` != ''
						AND `timestamp` >= '". date('Y-m-d H:i:s', strtotime('-30 days') ) ."'
						ORDER BY `timestamp` ASC";
	$mining_datas = $wpdb->get_results( $sql, ARRAY_A );

	$days_data = array();
	foreach ($mining_datas as $key => $mining_data) {
		$datetime 					= $mining_data['timestamp'];
		$days_data []= date('Y-m-d', strtotime($datetime));
	}
	$days_data = array_values(array_filter(array_unique($days_data)));
	$days_data = array_flip($days_data);

	foreach ($mining_datas as $key => $mining_data) {
		$datetime = $mining_data['timestamp'];
		$mining_data_array 	= json_decode($mining_data['mining_data'], true);

		if ( !isset($days_data [date('Y-m-d', strtotime($datetime))]['block_time']) ) {
			$days_data [date('Y-m-d', strtotime($datetime))] = "";
		}

		$block_time = $mining_data_array['block_time'];
		if (strpos(strtolower(strval($block_time)), 'e') !== false) {
			$block_time = floatval(substr( strval($block_time), 0, strpos(strtolower(strval($latest_value)), 'e') ));
		} else { $block_time = floatval($block_time); }

		$days_data [date('Y-m-d', strtotime($datetime))]['block_time'][]= floatval($mining_data_array['block_time']) / 2;
		$days_data [date('Y-m-d', strtotime($datetime))]['diff'][]= floatval($mining_data_array['diff24']) / 2;
		$days_data [date('Y-m-d', strtotime($datetime))]['nethash'][]= floatval($mining_data_array['nethash']) / 2;
		$days_data [date('Y-m-d', strtotime($datetime))]['estimated_rewards'][]= floatval($mining_data_array['estimated_rewards24']) / 2;
		$days_data [date('Y-m-d', strtotime($datetime))]['btc_revenue'][]= floatval($mining_data_array['btc_revenue24']) / 2;
		$days_data [date('Y-m-d', strtotime($datetime))]['profitability'][]= floatval($mining_data_array['profitability24']) / 10;
	}


	$days = array();
	foreach ($days_data as $datetime => $value) {
		$days_data[$datetime]['block_time'] = float_number_cleaner(array_sum($days_data[$datetime]['block_time']));
		$days_data[$datetime]['diff'] = float_number_cleaner(array_sum($days_data[$datetime]['diff']));
		$days_data[$datetime]['nethash'] = float_number_cleaner(array_sum($days_data[$datetime]['nethash']));
		$days_data[$datetime]['estimated_rewards'] = float_number_cleaner(array_sum($days_data[$datetime]['estimated_rewards']));
		$days_data[$datetime]['btc_revenue'] = float_number_cleaner(array_sum($days_data[$datetime]['btc_revenue']));
		$days_data[$datetime]['profitability'] = float_number_cleaner(array_sum($days_data[$datetime]['profitability']));

		$days []= '"'. date('d', strtotime( $datetime .' 00:00:00' )) .'"';
	}

	$block_time = array();
	$diff = array();
	$nethash = array();
	$estimated_rewards = array();
	$btc_revenue = array();
	$profitability = array();

	foreach ($days_data as $key => $val) {
		$block_time 				[]= '"'. round($val['block_time'], 3) .'"';
		$diff 							[]= '"'. round($val['diff'], 3) .'"';
		$nethash 						[]= '"'. round($val['nethash'], 3) .'"';
		$estimated_rewards 	[]= '"'. round($val['estimated_rewards'], 3) .'"';
		$btc_revenue 				[]= '"'. round($val['btc_revenue'], 3) .'"';
		$profitability 			[]= '"'. round($val['profitability'], 3) .'"';
	}
	unset($days_data);

	return
		'<div class="last_month_minings_diagrams">
			<div class="col-sm-4">
				<div class="row">
					<canvas id="block_time_diagram"></canvas>
				</div>
			</div>
			<div class="col-sm-4">
				<div class="row">
					<canvas id="diff_diagram"></canvas>
				</div>
			</div>
			<div class="col-sm-4">
				<div class="row">
					<canvas id="nethash_diagram"></canvas>
				</div>
			</div>
			<div class="col-sm-4">
				<div class="row">
					<canvas id="estimated_rewards_diagram"></canvas>
				</div>
			</div>
			<div class="col-sm-4">
				<div class="row">
					<canvas id="btc_revenue_diagram"></canvas>
				</div>
			</div>
			<div class="col-sm-4">
				<div class="row">
					<canvas id="profitability_diagram"></canvas>
				</div>
			</div>
		</div>

		<script type="text/javascript">
			jQuery(function($) {
				$(window).load(function(){
					var ctx = document.getElementById("block_time_diagram").getContext("2d");
					var chart = new Chart(ctx, {
					    type: "line",
					    data: {
					        labels: ['. implode(',', $days) .'],
									datasets: [{
											steppedLine: false,
			                label: "Block time",
			                backgroundColor: "rgba(184, 184, 184, 1)",
			                data: ['. implode(',', $block_time) .']
			            }]
					    },
					    options: { }
					});

					var ctx = document.getElementById("diff_diagram").getContext("2d");
					var chart = new Chart(ctx, {
					    type: "line",
					    data: {
					        labels: ['. implode(',', $days) .'],
									datasets: [{
											steppedLine: false,
			                label: "Difficult",
			                backgroundColor: "rgba(184, 184, 184, 1)",
			                data: ['. implode(',', $diff) .']
			            }]
					    },
					    options: { }
					});

					var ctx = document.getElementById("nethash_diagram").getContext("2d");
					var chart = new Chart(ctx, {
					    type: "line",
					    data: {
					        labels: ['. implode(',', $days) .'],
									datasets: [{
											steppedLine: false,
			                label: "Nethash",
			                backgroundColor: "rgba(184, 184, 184, 1)",
			                data: ['. implode(',', $nethash) .']
			            }]
					    },
					    options: { }
					});

					var ctx = document.getElementById("estimated_rewards_diagram").getContext("2d");
					var chart = new Chart(ctx, {
					    type: "line",
					    data: {
					        labels: ['. implode(',', $days) .'],
									datasets: [{
											steppedLine: false,
			                label: "Estimated rewards",
			                backgroundColor: "rgba(184, 184, 184, 1)",
			                data: ['. implode(',', $estimated_rewards) .']
			            }]
					    },
					    options: { }
					});

					var ctx = document.getElementById("btc_revenue_diagram").getContext("2d");
					var chart = new Chart(ctx, {
					    type: "line",
					    data: {
					        labels: ['. implode(',', $days) .'],
									datasets: [{
											steppedLine: false,
			                label: "BTC Revenue",
			                backgroundColor: "rgba(184, 184, 184, 1)",
			                data: ['. implode(',', $btc_revenue) .']
			            }]
					    },
					    options: { }
					});

					var ctx = document.getElementById("profitability_diagram").getContext("2d");
					var chart = new Chart(ctx, {
					    type: "line",
					    data: {
					        labels: ['. implode(',', $days) .'],
									datasets: [{
											steppedLine: false,
			                label: "Profitability",
			                backgroundColor: "rgba(184, 184, 184, 1)",
			                data: ['. implode(',', $profitability) .']
			            }]
					    },
					    options: { }
					});
				});
			});
		</script>';
}


function float_number_cleaner($num = "") {
	if ( empty($num) ) { return $num; }

	$num = strval($num);
	$num = str_split($num);
	$talalat = 0;

	foreach ($num as $key => $val) {
		if (strpos('e', strtolower($val)) !== false) {
			$talalat = $key;
			break;
		}
	}
	if ( $talalat > 0 ) {
		$num = array_slice($num, 0, $talalat);
	}

	return floatval(round(join($num), 4));
}

function if_empty_add_hidden_class($string = "") {
	if ( empty($string) ) { return "hidden"; }
	return "";
}

function calculating_mining_data($mining_data_array = "", $latest_value = "") {
	if (empty($mining_data_array)) { return $mining_data_array; }
	$result_html = "";
	$mining_data_array = array_values(array_filter($mining_data_array));

	$latest_num = "";
	if ( !empty($latest_value) && is_array($latest_value) ) {
		$latest_value = array_values(array_filter($latest_value));

		if (strpos(strtolower(end($latest_value)), 'e') !== false) {
			$latest_num = substr( end($latest_value), 0, strpos(strtolower(end($latest_value)), 'e') );
			$latest_num = floatval($latest_num);
		} else {
			$latest_num = floatval($latest_num);
		}
	}

	// clean values
	$mining_data_array_temp = array();

	foreach ($mining_data_array as $key => $val) {
		$num = strval($val);

		if (strpos(strtolower($num), 'e') !== false) {
			$num = substr( $num, 0, strpos(strtolower($num), 'e') );
			$mining_data_array_temp []= floatval($num);
		} else {
			$mining_data_array_temp []= floatval($num);
		}
	}
	$mining_data_array = $mining_data_array_temp;
	unset($mining_data_array_temp);


	// átlagszámítás & kiértékelés

	if ( !empty($mining_data_array) ) {
		$mining_data_array_temp = array();
		$calculated_average = array_sum($mining_data_array) / count($mining_data_array);

		if ( !empty($latest_num) && ( $latest_num > $calculated_average ) ) {

				$result_html = '<span class="glyphicon glyphicon-plus plus"></span>
												<div class="calculated_average">'. substr($calculated_average, 0, 6) .' → </div>
												<div class="latest_num">'. substr($latest_num, 0, 6) .'</div>';

		} else if ( !empty($latest_num) && ( $latest_num < $calculated_average ) ) {

				$result_html = '<span class="glyphicon glyphicon-minus minus"></span>
												<div class="calculated_average">'. substr($calculated_average, 0, 6) .' → </div>
												<div class="latest_num">'. substr($latest_num, 0, 6) .'</div>';

		} else if ( !empty($latest_num) && ( $latest_num == $calculated_average ) ) {

				$result_html = '<span class="equal">=</span>
												<div class="calculated_average">'. substr($calculated_average, 0, 6) .' → </div>
												<div class="latest_num">'. substr($latest_num, 0, 6) .'</div>';

		} else {

			$latest_num = end($mining_data_array);

			if ( !empty($latest_num) && ( $latest_num > $calculated_average ) ) {

					$result_html = '<span class="glyphicon glyphicon-plus plus"></span>
													<div class="calculated_average">'. substr($calculated_average, 0, 6) .' → </div>
													<div class="latest_num">'. substr($latest_num, 0, 6) .'</div>';

			} else if ( !empty($latest_num) && ( $latest_num < $calculated_average ) ) {

					$result_html = '<span class="glyphicon glyphicon-minus minus"></span>
													<div class="calculated_average">'. substr($calculated_average, 0, 6) .' → </div>
													<div class="latest_num">'. substr($latest_num, 0, 6) .'</div>';

			} else if ( !empty($latest_num) && ( $latest_num == $calculated_average ) ) {

					$result_html = '<span class="equal">=</span>
													<div class="calculated_average">'. substr($calculated_average, 0, 6) .' → </div>
													<div class="latest_num">'. substr($latest_num, 0, 6) .'</div>';
			}

		}
	}

	if ( empty($result_html) ) {
		return "";
	}

	return '<div class="calculating_mining_data">'. $result_html .'</div>';
}


// Get all mineable currency mining info
function get_mining_datas() {
	$mining_raw_datas = file_get_contents( 'https://whattomine.com/coins.json' );
	$mining_raw_datas = json_decode($mining_raw_datas, true);

	$mining_raw_datas2 = file_get_contents( 'https://whattomine.com/asic.json' );
	$mining_raw_datas2 = json_decode($mining_raw_datas2, true);

	$mining_raw_datas = array_merge($mining_raw_datas['coins'], $mining_raw_datas2['coins']);

	$mining_datas = array();
	if ( isset($mining_raw_datas) ) {
		foreach ($mining_raw_datas as $coin_full_name => $coin_data) {
			if ( isset($coin_data['tag']) && !empty($coin_data['tag']) ) {
				$mining_datas [$coin_data['tag']]= array(
							'block_time' 					=> $coin_data['block_time'],
							'diff' 								=> $coin_data['difficulty'],
							'diff24' 							=> $coin_data['difficulty24'],
							'nethash' 						=> $coin_data['nethash'],
							'estimated_rewards' 	=> $coin_data['estimated_rewards'],
							'estimated_rewards24' => $coin_data['estimated_rewards24'],
							'btc_revenue' 				=> $coin_data['btc_revenue'],
							'btc_revenue24' 			=> $coin_data['btc_revenue24'],
							'profitability' 			=> $coin_data['profitability'],
							'profitability24' 		=> $coin_data['profitability24'],
							'timestamp' 					=> $coin_data['timestamp'],
						);
			}
		}
	}
	unset($mining_raw_datas);
	return $mining_datas;
}


function remove_same_values($ranks_array = "") {
	if (empty($ranks_array)) { return $ranks_array; }

	$ranks_array_temp = array();
	for ($i=0; $i < count($ranks_array); $i++) {
		if ( $ranks_array[$i] != end($ranks_array_temp) ) {
			$ranks_array_temp []= $ranks_array[$i];
		}
	}
	$ranks_array = $ranks_array_temp;
	unset($ranks_array_temp);
	return $ranks_array;
}

function percent_calc($percentage = "", $totalWidth = "") {
	if ( empty($percentage) ) { return ""; }
	if ( empty($totalWidth) ) { return ""; }

	return round((intval($percentage) * 100) / intval($totalWidth));
}

function napi_ertekeles($ertekelesek_array = "") {
	$ertekelesek_array = array_count_values($ertekelesek_array);

	return
		'Expensive: '. ($ertekelesek_array['expensive'] + $ertekelesek_array['decline expensive']) .
		' | Must Buy: '. ($ertekelesek_array['must_buy'] + $ertekelesek_array['decline must_buy']) .
		' | Good Price: '. ($ertekelesek_array['good_price'] + $ertekelesek_array['decline good_price']);
}

// felesleges coinok törlése a cryp_crypto_compilation táblából
function delete_unnecessary_coins($except_coins_array = "") {
	global $wpdb;
	if ( empty($except_coins_array) ) { return ""; }

	$useless_crypto_compilation = $wpdb->get_results( "SELECT `coin_name` FROM `cryp_crypto_compilation` ", ARRAY_A );

	foreach ($useless_crypto_compilation as $key => $value) {
		if ( !in_array($value['coin_name'], $except_coins_array) ) {

			$wpdb->delete( $wpdb->prefix .'crypto_compilation',
										 array( 'coin_name' => $value['coin_name'] ),
										 array( '%s' )
									 );
		}
	}
}


// $coins_array átsorrendezése, hogy a cryp_crypto_compilation tábla
// szerint a legrégebben frissültek legyenek elől
function crypto_compilation_atsorrendezes_regen_frissultek_elol($coins_array = "") {
	global $wpdb;
	$coins_array_temp = array();
	$coins_array_temp2 = array();
	$for_resort_coins = $wpdb->get_results( "SELECT `coin_name` FROM `cryp_crypto_compilation` ORDER BY `timestamp` ASC", ARRAY_A );

	// régi coinok
	foreach ($for_resort_coins as $key => $value) {
		if ( in_array($value['coin_name'], $coins_array) ) {
			$coins_array_temp []= $value['coin_name'];
		}
	}
	// új coinok
	foreach ($coins_array as $key => $coin_name) {
		if ( !in_array($coin_name, $coins_array_temp) ) {
			$coins_array_temp2 []= $coin_name;
		}
	}
	$coins_array = array_merge($coins_array_temp2,$coins_array_temp);
	$coins_array = array_values(array_filter(array_unique($coins_array)));
	unset($coins_array_temp, $coins_array_temp2);

	return $coins_array;
}


function get_daily_percent_change_diagram($months_array = "") {
	$positive_array = array();
	$negative_array = array();
	$days = array();

	foreach ($months_array as $key => $p) {
		foreach ($p as $key2 => $v) {
			$negative_daily_percent_change = 0;
			$positive_daily_percent_change = 0;

			if ( $v['val'] == '-' ) {
				$negative_daily_percent_change = $v['daily_percent_change'];
			} else {
				$positive_daily_percent_change = $v['daily_percent_change'];
			}

			$positive_array []= str_replace('%', '', $positive_daily_percent_change);
			$negative_array []= str_replace('%', '', $negative_daily_percent_change);
			$days []= $v['date'];
		}
	}

	// utolsó 30 nap megjelenítése
	$positive_array = array_slice($positive_array, -30);
	$negative_array = array_slice($negative_array, -30);
	$days 					= array_slice($days, -30);

	$days_html = "";
	foreach ($days as $key => $value) {
		$t = strtotime($value);
		$days_html .= '"'. date('m-d', $t). ' ('.date_i18n('D', $t).')' .'", ';
	}

	$html_result =
		'<canvas id="mklcads"></canvas>
		<script type="text/javascript">
		jQuery(function($) {
			$(window).load(function(){
				var ctx = document.getElementById("mklcads").getContext("2d");
				var chart = new Chart(ctx, {
				    type: "line",
				    data: {
				        labels: ['. $days_html .'],
								datasets: [{
										steppedLine: false,
		                label: "Eladásnap",
		                backgroundColor: "rgba(72, 194, 113, 1)",
		                data: ['. implode(',', $positive_array) .']
		            }, {
										steppedLine: false,
		                label: "Vételnap",
		                backgroundColor: "rgba(181, 76, 51, 1)",
		                data: ['. implode(',', $negative_array) .']
		            }]
				    },
				    options: { }
				});

			});
		});
		</script>';

	return $html_result;
}


// összeállítja a napokat, hogy melyik napon volt érdrmes vásárolni vagy eladni
// nincs használva!
function daily_sales_and_purchase_conditions_v2() {
	$return_html = "";
	global $wpdb;

	$positive = array();
	$negative = array();

	$sql = "SELECT `data`, `timestamp` FROM `cryp_crypto_statistic`
						WHERE `data` != ''
						AND `rank` <= 100
						AND `rank` >= 1
						ORDER BY `timestamp` ASC";
	$crypto_statistic = $wpdb->get_results( $sql, ARRAY_A );

	//  felosztás napokra
	$days_array = array();
	$days_percent_change_array = array();

	// értékek beillesztése a napokba
	foreach ($crypto_statistic as $key => $coin_data) {
		$coin_data_array = json_decode($coin_data['data'], true);

		$days_percent_change_array[date( 'Y-m-d', strtotime($coin_data['timestamp']))] []= floatval($coin_data_array['percent_change_24h']);

		if ( isset($coin_data['timestamp']) && !empty($coin_data['timestamp']) ) {
			if (strpos($coin_data_array['percent_change_24h'], '-') !== false) {
				$days_array[date( 'Y-m-d', strtotime($coin_data['timestamp']))] []= '-';
			} else {
				$days_array[date( 'Y-m-d', strtotime($coin_data['timestamp']))] []= '+';
			}
		}
	}

	// napi százalékos értékváltozások átlagszámításban
	$days_percent_change_array_temp = array();
	foreach ($days_percent_change_array as $timestamp => $percent_change) {
		$daily_percent_change = strval((array_sum($percent_change) / count($percent_change)));
		$days_percent_change_array_temp[$timestamp]= floatval(substr($daily_percent_change, 0, 4));
	}
	$days_percent_change_array = $days_percent_change_array_temp;
	unset($days_percent_change_array_temp);


	$months_array = array();
	$months_array2 = array();
	foreach ($days_array as $date => $val) {
		$val_raw = array_count_values($val);

		if ( $val_raw['+'] > $val_raw['-'] ) { $val = "+"; }
		else if ( $val_raw['+'] < $val_raw['-'] ) { $val = "-"; }
		else { $val = "="; }

		$daily_percent_change = "";
		if ( isset($days_percent_change_array[ date('Y-m-d', strtotime($date .' 00:00:00')) ]) ) {
			$daily_percent_change = $days_percent_change_array[ date('Y-m-d', strtotime($date .' 00:00:00')) ] .'%';
		}

		$days_html = '<li data-day="'. $val .'">
										<span class="val">'. $val .'</span>
										<span class="date">'. date('m-d', strtotime($date .' 00:00:00')) .' <small>'. date_i18n('D', strtotime($date .' 00:00:00')) .'</small></span>
										<span class="daily_percent_change">'. $daily_percent_change .'</span>
										<span class="positive_negative">
											<span class="positive">'. percent_calc($val_raw['+'], $val_raw['+'] + $val_raw['-']) .' /</span>
											<span class="negative">'. percent_calc($val_raw['-'], $val_raw['+'] + $val_raw['-']) .'</span>
										</span>
									</li>';
		$months_array[date('Y-m', strtotime($date .' 00:00:00'))] []= $days_html;

		$months_array2[date('Y-m', strtotime($date .' 00:00:00'))] []= array(
																																					'val' => $val,
																																					'date' => date('Y-m-d', strtotime($date .' 00:00:00')),
																																					'daily_percent_change' => $daily_percent_change,
																																				);
	}

	$months_html = "";
	foreach ($months_array as $key => $value) {
			$months_html .= join($value);
			$months_html .= '<div class="new_month"></div>';
	}

	unset($days_array);
	$return_html = $months_html;

	return
				'<div class="col-md-12">'.
					'<div class="row">'.
						 '<ul class="daily_sales_and_purchase_conditions">'.
								$return_html .
						 		'<div class="clearfix"></div>
						 	</ul>'.
						 '<button type="button" class="btn-primary btn-sm daily_sales_and_purchase_conditions_btn">Bővebben</button>'.
					 '</div>'.
				 '</div>
				 <div class="clearfix"></div><br>'.

				 '<div class="col-md-6">
						<div class="row">
							'. get_daily_percent_change_diagram($months_array2) .'
						</div>
				 </div>

				 <div class="col-md-6">
						<div class="row">
							'. get_today_exchange_rate_change_chart() .'
						</div>
				 </div>';
}


function get_today_exchange_rate_change_chart() {
	global $wpdb;

	$sql = "SELECT `data`, `timestamp` FROM `cryp_crypto_statistic`
						WHERE
						`rank` <= 100 AND
						`rank` >= 1 AND
						`timestamp` >= '". date('Y-m-d H:i:s', strtotime('-49 hour') ) ."'
						ORDER BY `timestamp` ASC";
	$crypto_statistic = $wpdb->get_results( $sql, ARRAY_A );

	$result_data = array();
	$hours = array();

	foreach ($crypto_statistic as $key => $coin_data) {
		$data = json_decode($coin_data['data'], true);

		if ( isset($data['percent_change_24h']) ) {
			$hours []= '"'. date_i18n('D H:00', strtotime($coin_data['timestamp']) ) .'"';
			$result_data [ date('Y-m-d H:00', strtotime($coin_data['timestamp']) ) ][]= floatval($data['percent_change_24h']);
		}
	}
	$hours = array_values(array_filter(array_unique($hours)));

	// átlagszámítás
	$result_data_temp = array();
	foreach ($result_data as $datetime => $value) {
		$result_data_temp [$datetime] = array_sum($value) / count($value);
	}
	$result_data = $result_data_temp;
	unset($result_data_temp);


	$positive_array = array();
	$negative_array = array();

	foreach ($result_data as $datetime => $percent) {
		$positive_percent_change = 0;
		$negative_percent_change = 0;

		if (strpos(strval($percent), '-') !== false) {
			$negative_percent_change = $percent;
		} else {
			$positive_percent_change = $percent;
		}

		$positive_array [$datetime]= strval($positive_percent_change);
		$negative_array [$datetime]= strval($negative_percent_change);
	}


	$html_result =
		'<canvas id="cncjkda"></canvas>
		<script type="text/javascript">
		jQuery(function($) {
			$(window).load(function(){
				var ctx = document.getElementById("cncjkda").getContext("2d");
				var chart = new Chart(ctx, {
				    type: "line",
				    data: {
				        labels: ['. implode(',', $hours) .'],
								datasets: [{
										steppedLine: false,
		                label: "Eladás óra",
		                backgroundColor: "rgba(72, 194, 113, 1)",
		                data: ['. implode(',', $positive_array) .']
		            }, {
										steppedLine: false,
		                label: "Vétel óra",
		                backgroundColor: "rgba(181, 76, 51, 1)",
		                data: ['. implode(',', $negative_array) .']
		            }]
				    },
				    options: { }
				});

			});
		});
		</script>';

	return $html_result;
}


function daily_sales_and_purchase_conditions() {
	$return_html = "";
	global $wpdb;

	$buy_or_sell = get_option( 'crypto_buy_or_sell_'. date('Y-m') );
	$last_time = $buy_or_sell[0][1];

	// --

	$positive = array();
	$negative = array();

	$sql = "SELECT `data`, `timestamp` FROM `cryp_crypto_statistic`
						WHERE `data` != ''
						AND `rank` <= 100
						AND `rank` >= 1
						AND `timestamp` < '".date('Y-m-d 00:00:00', $buy_or_sell[0][1])."'
						ORDER BY `timestamp` ASC";
	$crypto_statistic = $wpdb->get_results( $sql, ARRAY_A );

	//  felosztás napokra
	$days_array = array();

	// értékek beillesztése a napokba
	foreach ($crypto_statistic as $key => $coin_data) {
		$coin_data_array = json_decode($coin_data['data'], true);

		if ( isset($coin_data['timestamp']) && !empty($coin_data['timestamp']) ) {
			if (strpos($coin_data_array['percent_change_24h'], '-') !== false) {
				$days_array[date( 'Y-m-d', strtotime($coin_data['timestamp']))] []= '-';
			} else {
				$days_array[date( 'Y-m-d', strtotime($coin_data['timestamp']))] []= '+';
			}
		}
	}


	foreach ($buy_or_sell as $key => $value) {
		$days_array[date( 'Y-m-d', $value[1])] []= $value[0];
	}

	unset($buy_or_sell, $crypto_statistic);

	$months_array = array();
	foreach ($days_array as $date => $val) {
		$val = array_count_values($val);

		if ( $val['+'] > $val['-'] ) { $val = "+"; }
		else if ( $val['+'] < $val['-'] ) { $val = "-"; }
		else { $val = "="; }

		$days_html = '<li data-day="'. $val .'">
										<span class="val">'. $val .'</span>
										<span class="date">'. date('m-d', strtotime($date .' 00:00:00')) .' <small>'. date_i18n('D', strtotime($date .' 00:00:00')) .'</small></span>
									</li>';
		$months_array[date('Y-m', strtotime($date .' 00:00:00'))] []= $days_html;
	}

	$months_html = "";
	foreach ($months_array as $key => $value) {
			$months_html .= join($value);
			$months_html .= '<div class="new_month"></div>';
	}

	unset($days_array);
	$return_html = $months_html;

	return '<ul class="daily_sales_and_purchase_conditions">'. $return_html .'</ul>';
}

// lekéri a top 100 coint és megnézi, hogy vétel vagy eladási idő van -e éppen
function check_buy_or_sell($update_command = "") {
	$return_html = "";

	$buy_or_sell = get_option( 'crypto_buy_or_sell_'. date('Y-m') );
	if ( empty($buy_or_sell) ) { $buy_or_sell = array(); }

	if ( $update_command == "update" ) {
		$positive = array();
		$negative = array();

		$crypto_datas = file_get_contents( 'https://api.coinmarketcap.com/v1/ticker/' );
		$crypto_datas = json_decode($crypto_datas);

		if ( !empty($crypto_datas) ) {
			foreach ($crypto_datas as $key => $coin_data) {
				if ( isset($coin_data->symbol) && !empty($coin_data->symbol) ) {
					if (strpos($coin_data->percent_change_24h, '-') !== false) {
						$negative []= $coin_data->percent_change_24h;
					} else {
						$positive []= $coin_data->percent_change_24h;
					}
				}
			}
		}

		if ( count($negative) > count($positive) ) {
			$buy_or_sell []= array( '-', strtotime(current_time('mysql')), 'pos' => count($positive), 'neg' => count($negative) );
		} else if ( count($negative) < count($positive) ) {
			$buy_or_sell []= array( '+', strtotime(current_time('mysql')), 'pos' => count($positive), 'neg' => count($negative) );
		} else {
			$buy_or_sell []= array( '=', strtotime(current_time('mysql')), 'pos' => count($positive), 'neg' => count($negative) );
		}

		update_option( 'crypto_buy_or_sell_'. date('Y-m'), $buy_or_sell, false );
		return "";
	}

	$positive_negative = '<div class="positive_negative">'. $buy_or_sell[ count($buy_or_sell) - 1 ]['pos'] .'/'. $buy_or_sell[ count($buy_or_sell) - 1 ]['neg'] .'</div>';

	if ( $buy_or_sell[ count($buy_or_sell) - 1 ][0] == '+' ) {
		$return_html = '<div class="sell"><span>Eladás!</span> <span>'. timeAgo($buy_or_sell[ count($buy_or_sell) - 1 ][1]) .'</span>'. $positive_negative .'</div>';
	} else if ( $buy_or_sell[ count($buy_or_sell) - 1 ][0] == '-' ) {
		$return_html = '<div class="buy"><span>Vásárlás!</span> <span>'. timeAgo($buy_or_sell[ count($buy_or_sell) - 1 ][1]) .'</span>'. $positive_negative .'</div>';
	} else {
		$return_html = "";
	}

	unset($buy_or_sell);
	return '<div class="check_buy_or_sell">'. $return_html .'</div>';
}


function timeAgo($time_ago = "") {
	if ( empty($time_ago) ) { return ""; }

	$cur_time   = strtotime(current_time('mysql'));
	$time_elapsed   = $cur_time - $time_ago;
	$seconds    = $time_elapsed ;
	$minutes    = round($time_elapsed / 60 );
	$hours      = round($time_elapsed / 3600);
	$days       = round($time_elapsed / 86400 );
	$weeks      = round($time_elapsed / 604800);
	$months     = round($time_elapsed / 2600640 );
	$years      = round($time_elapsed / 31207680 );
	// Seconds
	if($seconds <= 60){
	    return "$seconds seconds ago";
	}
	//Minutes
	else if($minutes <=60){
	    if($minutes==1){
	        return "one minute ago";
	    }
	    else{
	        return "$minutes minutes ago";
	    }
	}
	//Hours
	else if($hours <=24){
	    if($hours==1){
	        return "an hour ago";
	    }else{
	        return "$hours hours ago";
	    }
	}
	//Days
	else if($days <= 7){
	    if($days==1){
	        return "yesterday";
	    }else{
	        return "$days days ago";
	    }
	}
	//Weeks
	else if($weeks <= 4.3){
	    if($weeks==1){
	        return "a week ago";
	    }else{
	        return "$weeks weeks ago";
	    }
	}
	//Months
	else if($months <=12){
	    if($months==1){
	        return "a month ago";
	    }else{
	        return "$months months ago";
	    }
	}
	//Years
	else{
	    if($years==1){
	        return "one year ago";
	    }else{
	        return "$years years ago";
	    }
	}
}


function egyforma_hosszu_arak_beallitasa($btc_prices_array = "") {
	if ( empty($btc_prices_array) ) { return $btc_prices_array; }
	$length_array = array();

	foreach ($btc_prices_array as $key => $value) {
		$length_array []= strlen($value);
	}
	asort($length_array);
	$max_long = end($length_array);

	$minta = "";
	foreach ($btc_prices_array as $key => $value) {
		if ( strlen($value) == $max_long ) {
			$minta = $value;
		}
	}

	if ( !empty($minta) ) {
		$btc_prices_array_temp = array();
		foreach ($btc_prices_array as $key => $value) {
			$btc_prices_array_temp []= set_same_price_length($minta, $value);
		}
		$btc_prices_array = $btc_prices_array_temp;
		unset($btc_prices_array_temp);
	}

	return $btc_prices_array;
}

function set_same_price_length($coinmarketcap_price = "", $bittrex_price = "") {
	$val1 = strval($coinmarketcap_price);
	$val2 = strval($bittrex_price);

	if ( strlen($val1) == strlen($val2) ) {
		return $val2;
	} else {
		if ( strlen($val1) < strlen($val2) ) {
			$val2 = substr($val2, 0, strlen($val1));
		} else {
			$val2 = $val2 .str_repeat(0, strlen($val1) - strlen($val2) );
		}
	}
	return $val2;
}

function remove_null_ranks($ranks = "") {
	if ( empty($ranks) ) { return $ranks; }

	$ranks_temp = array();
	foreach ($ranks as $key => $rank) {
		if ( $rank > 0 ) {
			$ranks_temp []= $rank;
		}
	}
	return $ranks_temp;
}

function add_bittrex_coin_prices($selected_coin = "") {
	global $wpdb;
	if ( empty($selected_coin) ) { return ""; }

	// Bittrex árak lekérdezése és feltöltése
	$last_filled_row = $wpdb->get_results( "SELECT `ID`, `timestamp` FROM `{$wpdb->prefix}crypto_statistic`
																						WHERE `coin_name` = '{$selected_coin}' AND `bittrex_data` != ''
																						ORDER BY `timestamp` DESC LIMIT 1", ARRAY_A );

	$last_inserted_row = $wpdb->get_results( "SELECT `ID`, `timestamp` FROM `{$wpdb->prefix}crypto_statistic`
																				WHERE `coin_name` = '{$selected_coin}'
																				ORDER BY `timestamp` ASC
																				LIMIT 1", ARRAY_A );
	$last_inserted_row_id = intval($last_inserted_row[0]['ID']);

	$bittrex_datas = file_get_contents( 'https://bittrex.com/Api/v2.0/pub/market/GetTicks?marketName=BTC-'. $selected_coin .'&tickInterval=hour&_=1483228800' );
	$bittrex_datas = json_decode($bittrex_datas);

	if ( isset($bittrex_datas->result) && !empty($bittrex_datas->result) ) {
		foreach ($bittrex_datas->result as $key => $hourly_data) {
			$bittrex_timestamp = strtotime($hourly_data->T);
			$year = date('Y', $bittrex_timestamp);
			$month = date('m', $bittrex_timestamp);
			$day = date('d', $bittrex_timestamp);
			$hour = date('H', $bittrex_timestamp);

			// folytatás (ha van már...) - csak UPDATE

			if ( isset($last_filled_row[0]['timestamp']) ) {

				if ( strtotime($last_filled_row[0]['timestamp']) < $bittrex_timestamp ) {

					$updated_rows = $wpdb->get_results( "SELECT `ID` FROM `{$wpdb->prefix}crypto_statistic`
																								WHERE `coin_name` = '{$selected_coin}' AND `bittrex_data` = ''
																											AND `timestamp` BETWEEN '".date('Y-m-d H', $bittrex_timestamp).":00:00' AND '".date('Y-m-d H', $bittrex_timestamp).":59:59'
																								LIMIT 1", ARRAY_A );
					$updated_row_id = intval($updated_rows[0]['ID']);


					if ( $updated_row_id > 0 ) {
						$wpdb->update(
							$wpdb->prefix .'crypto_statistic',
							array(
								'bittrex_data' => json_encode($hourly_data),
							),
							array( 'ID' => $updated_row_id ),
							array( '%s' ),
							array( '%d' )
						);
					}
				}

			} else {

				// Régiek felvitele ( only INSERT )

				if ( $last_inserted_row_id > 0 ) {
					if ( strtotime($last_inserted_row[0]['timestamp']) > $bittrex_timestamp ) {
						if ( ( date('H', $bittrex_timestamp) % 2) == 0 ) {

							$wpdb->insert(
									$wpdb->prefix .'crypto_statistic',
									array(
										'coin_name' => $selected_coin,
										'bittrex_data' => json_encode($hourly_data),
										'rank' => 0,
										'timestamp' => date('Y-m-d H:i:s', $bittrex_timestamp),
									),
									array(
										'%s','%s','%d','%s'
									)
							);

						}
					}
				}
			}
		}
	}
}

function string_compress($string = "") {
	if ( empty($string) ) { return ""; }

	$compressed = gzdeflate(strval($string),  9);
	return strval(gzdeflate($compressed, 9));
}

function string_uncompress($string = "") {
	if ( empty($string) ) { return ""; }

	return gzinflate(gzinflate($string));
}

function ar_visszaallitas($eredeti_ar_mintakent = "", $atalakitando_ar = "") {
	$eredeti_ar_mintakent = strval($eredeti_ar_mintakent);
	$atalakitando_ar = strval($atalakitando_ar);

	if ( strlen($eredeti_ar_mintakent) == strlen('0.'. $atalakitando_ar) ) {
		$atalakitando_ar = '0.'. $atalakitando_ar;
	} else if ( strlen($eredeti_ar_mintakent) == strlen('0.0'. $atalakitando_ar) ) {
		$atalakitando_ar = '0.0'. $atalakitando_ar;
	} else if ( strlen($eredeti_ar_mintakent) == strlen('0.00'. $atalakitando_ar) ) {
		$atalakitando_ar = '0.00'. $atalakitando_ar;
	} else if ( strlen($eredeti_ar_mintakent) == strlen('0.000'. $atalakitando_ar) ) {
		$atalakitando_ar = '0.000'. $atalakitando_ar;
	} else if ( strlen($eredeti_ar_mintakent) == strlen('0.0000'. $atalakitando_ar) ) {
		$atalakitando_ar = '0.0000'. $atalakitando_ar;
	} else if ( strlen($eredeti_ar_mintakent) == strlen('0.00000'. $atalakitando_ar) ) {
		$atalakitando_ar = '0.00000'. $atalakitando_ar;
	} else if ( strlen($eredeti_ar_mintakent) == strlen('0.000000'. $atalakitando_ar) ) {
		$atalakitando_ar = '0.000000'. $atalakitando_ar;
	} else if ( strlen($eredeti_ar_mintakent) == strlen('0.0000000'. $atalakitando_ar) ) {
		$atalakitando_ar = '0.0000000'. $atalakitando_ar;
	} else if ( strlen($eredeti_ar_mintakent) == strlen('0.00000000'. $atalakitando_ar) ) {
		$atalakitando_ar = '0.00000000'. $atalakitando_ar;
	}

	return $atalakitando_ar;
}


function atlagos_ar_tartomany($prices_array = "") {
	$prices_array_temp = array();
	//echo "<pre>"; var_dump($prices_array); echo "</pre>";

	foreach ($prices_array as $key => $value) {
		$prices_array_temp []= (int) substr($value, 2);
	}
	$prices_array = $prices_array_temp;
	unset($prices_array_temp);
	asort($prices_array);
	$prices_array = array_values(array_filter(array_unique($prices_array)));

	$min = $prices_array[0];
	$max = end($prices_array);

	$atlag = array_sum($prices_array) / count($prices_array);
	$atlag = (int) $atlag;


	// álltalános tartomány 10 részre felosztás
	$tartomanyok = array();

	$tartomanyok[] = array(
														'start' => $min,
														'end' => intval($min + (( ($max - $min) / 10) * 1) ),
													);
	$tartomanyok[] = array(
														'start' => $tartomanyok[0]['end'] + 1,
														'end' => intval($min + (( ($max - $min) / 10) * 2) ),
													);
	$tartomanyok[] = array(
														'start' => $tartomanyok[1]['end'] + 1,
														'end' => intval($min + (( ($max - $min) / 10) * 3) ),
													);
	$tartomanyok[] = array(
														'start' => $tartomanyok[2]['end'] + 1,
														'end' => intval($min + (( ($max - $min) / 10) * 4) ),
													);
	$tartomanyok[] = array(
														'start' => $tartomanyok[3]['end'] + 1,
														'end' => intval($min + (( ($max - $min) / 10) * 5) ),
													);
	$tartomanyok[] = array(
														'start' => $tartomanyok[4]['end'] + 1,
														'end' => intval($min + (( ($max - $min) / 10) * 6) ),
													);
	$tartomanyok[] = array(
														'start' => $tartomanyok[5]['end'] + 1,
														'end' => intval($min + (( ($max - $min) / 10) * 7) ),
													);
	$tartomanyok[] = array(
														'start' => $tartomanyok[6]['end'] + 1,
														'end' => intval($min + (( ($max - $min) / 10) * 8) ),
													);
	$tartomanyok[] = array(
														'start' => $tartomanyok[7]['end'] + 1,
														'end' => intval($min + (( ($max - $min) / 10) * 9) ),
													);
	$tartomanyok[] = array(
														'start' => $tartomanyok[8]['end'] + 1,
														'end' => $max,
													);

	// ---

	foreach ($prices_array as $key => $price) {
		foreach ($tartomanyok as $k => $v) {
			$start = $v['start'];
			$end = $v['end'];

			if ( ($price <= $end) && ($price >= $start) ) {
				$tartomanyok[$k]['prices'][] = $price;
				break;
			}
		}
	}

	$prices_count_array = array();
	foreach ($tartomanyok as $key => $val) {
		if ( isset($val['prices']) ) {
			$prices_count = count($val['prices']);
		} else {
			$prices_count = 0;
		}

		$tartomanyok[$key]['prices_count'] = $prices_count;
		$prices_count_array []= $prices_count;
	}
	sort($prices_count_array, SORT_NUMERIC);
	$prices_count_array = array_reverse($prices_count_array);

	//echo "<pre>"; var_dump($tartomanyok); echo "</pre>";

	$tartomanyok_temp = array();
	foreach ($tartomanyok as $key => $val) {
		if ( $val['prices_count'] == $prices_count_array[0] ) {
			$tartomanyok_temp []= $val;
		}
	}
	$tartomanyok = $tartomanyok_temp;
	unset($tartomanyok_temp);

	$arak_array = array();
	foreach ($tartomanyok as $key => $value) {
		$arak_array []= $value['start'];
		$arak_array []= $value['end'];
	}
	sort($arak_array, SORT_NUMERIC);

	//echo "<pre>"; var_dump($arak_array); echo "</pre>";

	return array(
								'atlag' => $atlag,
								'tartomany_start' => $arak_array[0],
								'tartomany_end' => end($arak_array),
							);
}


function hexToRgb($hex = "", $alpha = false) {
   $hex      = str_replace('#', '', $hex);
   $length   = strlen($hex);
   $rgb['r'] = hexdec($length == 6 ? substr($hex, 0, 2) : ($length == 3 ? str_repeat(substr($hex, 0, 1), 2) : 0));
   $rgb['g'] = hexdec($length == 6 ? substr($hex, 2, 2) : ($length == 3 ? str_repeat(substr($hex, 1, 1), 2) : 0));
   $rgb['b'] = hexdec($length == 6 ? substr($hex, 4, 2) : ($length == 3 ? str_repeat(substr($hex, 2, 1), 2) : 0));
   if ( $alpha ) {
      $rgb['a'] = $alpha;
   }
   return $rgb;
}

function get_color($i = "") {
	$colors = array(
		'#21618C', '#a836ab', '#09123b', '#40ca15', '#59207b', '#5aac94', '#816a6a', '#5a2b32', '#18a636', '#554d48', '#0ce0dc', '#dbdd0c', '#195765', '#5844e3', '#586302', '#8e79bb', '#87aaa7', '#4f2d59', '#6718b6', '#afde21', '#a1c71f', '#7a64ce', '#59ff2b', '#e6a024', '#e2b4f3', '#907f6a', '#a4708b', '#b208fb', '#5cab0e', '#074419', '#10283d', '#6ba143', '#c1e888', '#2bcf45', '#802e42', '#c47bc0', '#77211d', '#be5c97', '#612f99', '#a70fbb', '#408973', '#fc1376', '#0ce141', '#d4a237', '#b2e077', '#b29a77', '#74bd96', '#233f39', '#6f825f', '#6354fb', '#ee416d', '#5d8c21', '#19726f', '#c769af', '#9c307a', '#16127e', '#ba2ced', '#907539', '#2b1ab5', '#f2f504', '#c9c49a', '#c3087d', '#792779', '#4dbe01', '#8f3c55', '#8ae8df', '#f1432e', '#139fa2', '#d9e1bd', '#154644', '#a2873f', '#c6e15c', '#852480', '#5134fa', '#1fdff5', '#c7a220', '#4211e3', '#c0bb8d', '#5d487c', '#ca6c19', '#fb47fa', '#c3cf77', '#fe5fd4', '#108372', '#095232', '#6abca6', '#14cd1f', '#b8b6d4', '#47fab0', '#877c53', '#b70ca5', '#dde83b', '#ba0d14', '#0217d5', '#1e7744', '#3ce1e6', '#0fff49', '#8857aa', '#9e9cbe', '#186e9d', '#2c2bad', '#b8e0c8', '#a9ad50', '#e3bb3d', '#4f796f', '#240eea', '#ebbea7', '#6f955a', '#6ad545', '#204d3b', '#247860', '#0d01e1', '#2b4fea', '#c855b4', '#5c4ac5', '#b5eb5f', '#8cb33a', '#f5eb78', '#4e8931', '#4b02b4', '#61ee65', '#f572fe', '#b93513', '#dfa8ef', '#7db3ff', '#08a0f5', '#e687f4', '#68a6fc', '#42142e', '#e252c0', '#c8fac2', '#2dc43d', '#9d81de', '#0a5bd4', '#43249c', '#031f22', '#5dd907', '#43b43f', '#c539a9', '#a85d4a', '#cb7813', '#1c2540', '#ea6b92', '#d5db90', '#df5546', '#97db83', '#8a7075', '#11173f', '#290aa0', '#b49ac4', '#703634', '#b3e052', '#c4b99d', '#a9701a', '#e6d868', '#e41ca6', '#c68dc2', '#d70655', '#db0c35', '#9ca48e', '#2ad4ac', '#e6fc82', '#817460', '#b2e176', '#daf5fc', '#96d73d', '#ce9ab5', '#10f59e', '#dbfaf7', '#88bc70', '#0bbfae', '#b5837a', '#a302a5', '#b70c0f', '#1c433e', '#8b68b9', '#c1bcb1', '#65ac77', '#bf83a2', '#1e3614', '#de54bc', '#4d97be', '#2a2475', '#66ea15', '#b3ca00', '#99ead4', '#1b4cd5', '#dde315', '#10e219', '#bef1a2', '#91cdd4', '#bfffd8', '#4b8891', '#c5aa31', '#c8b796', '#3e6fe7', '#cc3c78', '#27666d', '#09202b', '#a4e6d6',
  );

	/*$colors = array(
		'#943126', '#21618C', '#27AE60', '#5B2C6F', '#797D7F', '#D98880', '#F1C40F', '#2980B9', '#9A7D0A', '#F8C471', '#CCD1D1', '#935116', '#9B59B6', '#76D7C4', '#1ABC9C', '#95A5A6', '#BB8FCE', '#873600', '#EDBB99', '#0E6655', '#C0392B', '#5F6A6A', '#AEB6BF', '#E67E22', '#1D8348', '#34495E', '#7DCEA0',
  );*/

	$rgb = hexToRgb($colors[$i]);

	return 'rgb('. $rgb['r'] .', '. $rgb['g'] .', '. $rgb['b'] .')';
}
