<?php

add_shortcode('coindar_events_list', 'coindar_events_list_sc_function');
function coindar_events_list_sc_function($manual_update = "") {
	if ( !is_user_logged_in() ) { return ""; }
	global $wpdb;
	$result_html = "";
	$table_body = array();
	$coins_array = array();

	$coindar_events = $wpdb->get_results( "SELECT *
																	FROM `{$wpdb->prefix}crypto_coindar_events`
																	WHERE `start_date` >= '". date('Y-m-d H:i:s', strtotime('today midnight')) ."'
																	ORDER BY `start_date` ASC", ARRAY_A );

	$last_coindar_update = $wpdb->get_results( "SELECT `datetime`
																	FROM `{$wpdb->prefix}crypto_coindar_events`
																	ORDER BY `datetime` DESC LIMIT 1", ARRAY_A );

	foreach ($coindar_events as $key => $event_data) {
		$coin_name = get_coin_name_by_id($event_data['coin_ID']);

		if ( !empty($coin_name) && check_coin_is_banned($coin_name) === FALSE ) {
			$coins_array []= $coin_name;

			$start_date = "";
			if ( $event_data['start_date'] != '0000-00-00 00:00:00' ) {
				if ( date('H', strtotime($event_data['start_date'])) != '00' ) {
					$start_date = date('Y-m-d H:i', strtotime($event_data['start_date']));
				} else {
					$start_date = date('Y-m-d', strtotime($event_data['start_date']));
				}
			}
			$end_date = "";
			if ( $event_data['end_date'] != '0000-00-00 00:00:00' ) {
				if ( date('H', strtotime($event_data['end_date'])) != '00' ) {
					$end_date = date('Y-m-d H:i', strtotime($event_data['end_date']));
				} else {
					$end_date = date('Y-m-d', strtotime($event_data['end_date']));
				}
			}

			$proof_link = "#";
			if ( !empty($event_data['proof']) ) {
				$proof_link = $event_data['proof'];
			}

			$today_class = "";
			if ( date('Y-m-d', strtotime($event_data['start_date'])) == date('Y-m-d') ) {
				$today_class = "today";
			}
			$tomorrow_class = "";
			if ( date('Y-m-d', strtotime($event_data['start_date'])) == date('Y-m-d', strtotime('tomorrow')) ) {
				$tomorrow_class = "tomorrow";
			}

			$table_body []=
				'<tr class="'. $today_class .' '. $tomorrow_class .' ">
					<td><a href="'. get_permalink( PAGE_OSSZEALLITASOK ) .'#'. strtolower($coin_name) .'" target="_blank" >'. $coin_name .'</a></td>
					<td><a href="'. $proof_link .'" target="_blank">'. $event_data['caption'] .'</a></td>
					<td>'. $start_date .'</td>
					<td>'. $end_date .'</td>
				</tr>';
		}
	}

	$table_body = array_values(array_filter(array_unique($table_body)));

	$result_html =
		'<table class="table table-bordered">
			<tr>
				<th>Coin Name</th>
				<th>Caption</th>
				<th>Start date</th>
				<th>End date</th>
			</tr>
			'. implode('', $table_body) .'
		</table>';


	asort($coins_array);
	$coins_array = array_count_values($coins_array);

	$coins = array();
	foreach ($coins_array as $coin_name => $num) {
		$coins []= $coin_name .': ('. $num .')';
	}

	arsort($coins_array);
	$coins2 = array();
	foreach ($coins_array as $coin_name => $num) {
		$coins2 []= $coin_name .': ('. $num .')';
	}

	return '<div class="coindar_events_list_sc">
						<h4 class="sc_title">Events <small>(Last updated: <b>'. timeAgo(strtotime($last_coindar_update[0]['datetime'])) .'</b>)</small></h4>
						<div class="clearfix"></div>

						'. implode(', ', $coins) .'
						<div class="clearfix"></div><br>

						'. implode(', ', $coins2) .'
						<div class="clearfix"></div><br>

						<div class="table-wrapper">'. $result_html .'</div>'.
					'</div>';
}
