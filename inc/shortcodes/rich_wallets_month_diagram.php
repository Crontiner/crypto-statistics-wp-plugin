<?php

add_shortcode('rich_wallets_month_diagram', 'rich_wallets_month_diagram_function');
function rich_wallets_month_diagram_function($atts, $content = null) {
	extract(shortcode_atts(array(
		'coin_symbol' => ''
	), $atts));
	if ( empty($coin_symbol) ) { return ""; }
	$coin_ID = get_coin_id_by_name($coin_symbol);
	if ( empty($coin_ID) ) { return ""; }
	global $wpdb;

	$result_html = "";
	$canvas_id = 'c'. md5(wp_generate_password(15, false, false));
	if ( !is_user_logged_in() ) { return ""; }


	// --- Get last updated datetime ---
	$last_updated_datetime = $wpdb->get_results( "SELECT `last_updated_datetime`
																								FROM `{$wpdb->prefix}crypto_bitinfocharts_rich_wallets`
																								ORDER BY `last_updated_datetime` DESC
																								LIMIT 1", ARRAY_A );
	if ( isset($last_updated_datetime[0]['last_updated_datetime']) ) {
		$last_updated_datetime = strtotime($last_updated_datetime[0]['last_updated_datetime']);
	} else {
		$last_updated_datetime = strtotime(current_time('mysql'));
	}
	// ---


	// --- CACHE ---

	$sell_buy_month_cache = get_option('rich_wallets_month_diagram_cache_'. $coin_symbol);

	if ( !empty($sell_buy_month_cache) && isset($sell_buy_month_cache['timestamp']) &&
			($last_updated_datetime <= $sell_buy_month_cache['timestamp'])
	 ) {
		return $sell_buy_month_cache['html'];
	}

	// --- /CACHE ---


	$percent_changes_array = array();
	$days_array = array();
	$column_name = 'changed_amount';

	for($i=0; $i >= -(45 * 24); $i-=24) {
		$start = strval($i .' hours');
		$end = strval(($i - 24) .' hours');

		$start 	= date('Y-m-d H:i:s', strtotime( $start ) + 3600 );
		$end 		= date('Y-m-d H:i:s', strtotime( $end ) + 3600 );

		if ( $i == 0 ) { $start = date('Y-m-d H:i:s', strtotime('+1 day')); }

		$cmc_data = $wpdb->get_results( "SELECT AVG(`{$wpdb->prefix}crypto_bitinfocharts_rich_wallets_datas`.`{$column_name}`) AS `{$column_name}`
																		FROM `{$wpdb->prefix}crypto_bitinfocharts_rich_wallets_datas`
																		LEFT JOIN `{$wpdb->prefix}crypto_bitinfocharts_rich_wallets` ON
																			`{$wpdb->prefix}crypto_bitinfocharts_rich_wallets`.`ID` = `{$wpdb->prefix}crypto_bitinfocharts_rich_wallets_datas`.`wallet_ID`
																		WHERE `{$wpdb->prefix}crypto_bitinfocharts_rich_wallets_datas`.`datetime` <= '". $start ."'
																			AND `{$wpdb->prefix}crypto_bitinfocharts_rich_wallets_datas`.`datetime` > '". $end ."'

																			AND `{$wpdb->prefix}crypto_bitinfocharts_rich_wallets`.`coin_ID` = '". $coin_ID ."'
																			AND `{$wpdb->prefix}crypto_bitinfocharts_rich_wallets_datas`.`changed_amount` IS NOT NULL
																			AND `{$wpdb->prefix}crypto_bitinfocharts_rich_wallets_datas`.`changed_amount` != ''
																			AND `{$wpdb->prefix}crypto_bitinfocharts_rich_wallets`.`last_updated_datetime` >= '". date('Y-m-d H:i:s', strtotime("-4 days")) ."'
																			ORDER BY `{$wpdb->prefix}crypto_bitinfocharts_rich_wallets_datas`.`datetime` DESC", ARRAY_A );


		$cmc_datetime = $wpdb->get_results( "SELECT `{$wpdb->prefix}crypto_bitinfocharts_rich_wallets_datas`.`datetime` AS `datetime`
																		FROM `{$wpdb->prefix}crypto_bitinfocharts_rich_wallets_datas`
																		LEFT JOIN `{$wpdb->prefix}crypto_bitinfocharts_rich_wallets` ON
																			`{$wpdb->prefix}crypto_bitinfocharts_rich_wallets`.`ID` = `{$wpdb->prefix}crypto_bitinfocharts_rich_wallets_datas`.`wallet_ID`
																		WHERE (`{$wpdb->prefix}crypto_bitinfocharts_rich_wallets_datas`.`datetime` <= '". $start ."'
																			AND `{$wpdb->prefix}crypto_bitinfocharts_rich_wallets_datas`.`datetime` > '". $end ."')

																			AND `{$wpdb->prefix}crypto_bitinfocharts_rich_wallets`.`coin_ID` = '". $coin_ID ."'
																			AND `{$wpdb->prefix}crypto_bitinfocharts_rich_wallets_datas`.`changed_amount` IS NOT NULL
																			AND `{$wpdb->prefix}crypto_bitinfocharts_rich_wallets_datas`.`changed_amount` != ''
																			AND `{$wpdb->prefix}crypto_bitinfocharts_rich_wallets`.`last_updated_datetime` >= '". date('Y-m-d H:i:s', strtotime("-4 days")) ."'
																			ORDER BY 	`{$wpdb->prefix}crypto_bitinfocharts_rich_wallets_datas`.`datetime` DESC
																			LIMIT 1", ARRAY_A );

		if (
				isset($cmc_data[0][$column_name]) && !empty($cmc_data[0][$column_name]) &&
				isset($cmc_datetime[0]['datetime']) && !empty($cmc_datetime[0]['datetime'])
	 		 ) {

			$average_val  = $cmc_data[0][$column_name];
			$datetime 		= strtotime($cmc_datetime[0]['datetime']);
			$t = $datetime;

			$days_array[] = '"'. date('m-d', $t). ' ('.date_i18n('D', $t).')' .'", ';
			$percent_changes_array[]= $average_val;
		}
	}

	$days_array = array_reverse($days_array);


	$positives = array();
	$negatives = array();


	foreach ($percent_changes_array as $key => $percent_change) {
		$p = 0;
		$n = 0;

		if (strpos($percent_change, '-') !== false) { $n = $percent_change; }
		else { $p = $percent_change; }

		$positives []= $p;
		$negatives []= $n;
	}


	$avg_price = (array_sum($positives) + array_sum($negatives)) / (count($positives) + count($negatives));
	$avg_price = number_format($avg_price,2);

	$sum_price = array_sum($positives) + array_sum($negatives);
	$sum_price = number_format($sum_price,2);

	$result_html =
		'<h4 class="sc_title">'. $coin_symbol .' napi %-os árváltozások <small>(utolsó 45 nap) | SUM: '. $sum_price .'</small></h4>'.
		'<canvas id="'. $canvas_id .'"></canvas>
		<script type="text/javascript">
		jQuery(function($) {
			$(window).load(function(){
				var ctx = document.getElementById("'. $canvas_id .'").getContext("2d");
				var chart = new Chart(ctx, {
				    type: "line",
				    data: {
				        labels: ['. implode('', $days_array) .'],
								datasets: [{
										steppedLine: false,
		                label: "Vásárlások",
		                backgroundColor: "rgba(72, 194, 113, 1)",
		                data: ['. implode(',', array_reverse($positives)) .']
		            }, {
										steppedLine: false,
		                label: "Eladások",
		                backgroundColor: "rgba(181, 76, 51, 1)",
		                data: ['. implode(',', array_reverse($negatives)) .']
		            }]
				    },
				    options: { }
				});

			});
		});
		</script>';

	update_option('rich_wallets_month_diagram_cache_'. $coin_symbol, array('html' => $result_html, 'timestamp' => strtotime(current_time('mysql'))), false);
	return $result_html;
}
