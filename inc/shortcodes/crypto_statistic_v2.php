<?php
add_shortcode('crypto_statistic_v2', 'crypto_statistic_v2_function');
function crypto_statistic_v2_function() {
	global $wpdb;
	$result_html = "";
	if ( !is_user_logged_in() ) { return ""; }

	// Feldolgozatlan adatok lekérése
	//$coins_array = create_crypto_statistic_function();

	$coins_array = $wpdb->get_results( "SELECT * FROM `{$wpdb->prefix}crypto_compilation`
																			ORDER BY `actual_rank` ASC", ARRAY_A );

	$cmc_latest_update = $wpdb->get_results( "SELECT `datetime` FROM `{$wpdb->prefix}crypto_coinmarketcap_data`
																						ORDER BY `datetime` DESC LIMIT 1", ARRAY_A );

	$latest_compilation_update = $wpdb->get_results( "SELECT `datetime` FROM `{$wpdb->prefix}crypto_compilation`
																										ORDER BY `datetime` DESC LIMIT 1", ARRAY_A );


	foreach ($coins_array as $key => $data) {
		$coins_array[$key]['ranks'] = json_decode($data['ranks'], true);
		$coins_array[$key]['volume'] = json_decode($data['volume'], true);
		$coins_array[$key]['rating'] = json_decode($data['rating'], true);
		$coins_array[$key]['oldest_data'] = json_decode($data['oldest_data'], true);

		if ( $coins_array[$key]['volume']['positive_negative'] == '>' ) { $coins_array[$key]['volume']['positive_negative'] = '&#62;'; }
		if ( $coins_array[$key]['volume']['positive_negative'] == '<' ) { $coins_array[$key]['volume']['positive_negative'] = '&#60;'; }

		$coins_array[$key]['bittrex_total_buys'] = json_decode($data['bittrex_total_buys'], true);
		$coins_array[$key]['bittrex_total_sells'] = json_decode($data['bittrex_total_sells'], true);
		$coins_array[$key]['bittrex_rating'] = json_decode($data['bittrex_rating'], true);

		$coins_array[$key]['bitfinex_total_buys'] = json_decode($data['bitfinex_total_buys'], true);
		$coins_array[$key]['bitfinex_total_sells'] = json_decode($data['bitfinex_total_sells'], true);

		$coins_array[$key]['hitbtc_total_buys'] = json_decode($data['hitbtc_total_buys'], true);
		$coins_array[$key]['hitbtc_total_sells'] = json_decode($data['hitbtc_total_sells'], true);

		$coins_array[$key]['poloniex_total_buys'] = json_decode($data['poloniex_total_buys'], true);
		$coins_array[$key]['poloniex_total_sells'] = json_decode($data['poloniex_total_sells'], true);

		$coins_array[$key]['bithumb_total_buys'] = json_decode($data['bithumb_total_buys'], true);
		$coins_array[$key]['bithumb_total_sells'] = json_decode($data['bithumb_total_sells'], true);

		$coins_array[$key]['kraken_total_buys'] = json_decode($data['kraken_total_buys'], true);
		$coins_array[$key]['kraken_total_sells'] = json_decode($data['kraken_total_sells'], true);

		$coins_array[$key]['binance_total_buys'] = json_decode($data['binance_total_buys'], true);
		$coins_array[$key]['binance_total_sells'] = json_decode($data['binance_total_sells'], true);

		$coins_array[$key]['all_sells_and_buys_datas_evaluation'] = json_decode($data['all_sells_and_buys_datas_evaluation'], true);

		$coins_array[$key]['mining_block_time'] = json_decode($data['mining_block_time'], true);
		$coins_array[$key]['mining_difficult'] = json_decode($data['mining_difficult'], true);
		$coins_array[$key]['mining_nethash'] = json_decode($data['mining_nethash'], true);
		$coins_array[$key]['mining_estimated_rewards'] = json_decode($data['mining_estimated_rewards'], true);
		$coins_array[$key]['mining_btc_revenue'] = json_decode($data['mining_btc_revenue'], true);
		$coins_array[$key]['mining_profitability'] = json_decode($data['mining_profitability'], true);
		$coins_array[$key]['mining_rating'] = json_decode($data['mining_rating'], true);
	}

	// sorbarendezés rank szerint
	//array_sort_by_column($coins_array, 'actual_rank');


	$result_html .=
		'<h4 class="sc_title">Összeállítások <small>- utolsó összeállítás: <b>'. timeAgo(strtotime($latest_compilation_update[0]['datetime'])) .'</b> | utolsó lekérés: <b>'. timeAgo(strtotime($cmc_latest_update[0]['datetime'])) .'</b></small> <a href="'. get_permalink(PAGE_OSSZEALLITASOK) .'?crypto_statistic_manual_update=true" class="btn-primary btn-sm crypto_statistic_manual_update_btn">Manual update</a></h4>
		<input type="hidden" id="last_compilation_update" value="'. strtotime($latest_compilation_update[0]['datetime']) .'" />

		<table id="crypto_statistic_v2_sc" class="table table-bordered">
		   <tbody>
		      <tr>
		         <th>Rank</th>
		         <th>Logó</th>
		         <th>Coin name</th>
		         <th>Last price in BTC</th>
						 <th>Árváltozás %-ban</th>
		         <th>Átlag ár tartomány</th>
		         <th>Átlag BTC ár</th>
						 <th>Volume</th>
		         <th>Ranks</th>
		         <th>Értékelés</th>
		         <th>Lekérve</th>
		         <th>Összeállítva</th>
		         <th>Kiértékelt adatok</th>
		         <th>Legrégebbi adat</th>
		         <th>Létrehozva</th>
						 <th>Update</th>
		      </tr>';

	$tr = array();

	foreach ($coins_array as $coin_name => $coin_data) {
		if ( is_numeric($coin_name) ) { $coin_name = get_coin_name_by_id($coin_data['coin_ID']); }


		// árváltozásokkal kapcsolatos class-ok lekérése
		if ( !empty($coin_data['bittrex_rating']['status']) ) { $rating_status_classes = $coin_data['bittrex_rating']['status']; }
		else { $rating_status_classes = $coin_data['rating']['status']; }

		// rankesés class
		if ( $coin_data['rating']['decline'] == "true" ) { $rating_status_classes .= " decline"; }


		$veteran_coin_class = "";
		if ( !empty($coin_data['creation_date']) ) {
			if ( (strtotime("now") - $coin_data['creation_date']) >= 63113851 ) { // older than 2 year
				$veteran_coin_class = "veteran_coin";
			}
		}

		$shitcoin_class = "";
		if ( check_is_shitcoin_by_coin_name($coin_name) ) { $shitcoin_class = "shitcoin"; }

		$logo_img = "";
		$logo_url = get_logo_url_by_coin_id($coin_data['coin_ID']);

		if ( !empty($logo_url) ) {
			if ( strpos($logo_url, 'https') !== false ) {
				$logo_img = $coin_data['logo_img'];
			} else {
				$mthumb_img_size = '&amp;w=90&amp;h=90';
				$logo_img = '<img class="logo" src="'. MTHUMB_URL . esc_attr($logo_url) . $mthumb_img_size .'" alt="" />';
			}
		}


		$coin_full_name = get_coin_full_name_by_id($coin_data['coin_ID']);
		if ( !empty($coin_full_name) && ( strtolower($coin_name) != strtolower($coin_full_name) ) ) {
			$coin_full_name = '<small>('. $coin_full_name .')</small>';
		} else { $coin_full_name = ""; }


		$technical_analysis = get_crypto_technical_analysis($coin_data['coin_ID']);


		// --- avg_price_range_table ---
		$cmc_avg_price_range_table = "";
		$cmc_average_btc_price_range_3D = get_cmc_avg_price_range($coin_data['coin_ID'], 'BTC', 12, strtotime("-3 day"));

		if ( isset($cmc_average_btc_price_range_3D['html']) ) {

			$cmc_average_btc_price_range_2W = get_cmc_avg_price_range($coin_data['coin_ID'], 'BTC', 12, strtotime("-2 week"));
			$cmc_average_btc_price_range_1M = get_cmc_avg_price_range($coin_data['coin_ID'], 'BTC', 12, strtotime("-1 month"));
			$cmc_average_btc_price_range_2M = get_cmc_avg_price_range($coin_data['coin_ID'], 'BTC', 12, strtotime("-2 month"));
			$cmc_average_btc_price_range_3M = get_cmc_avg_price_range($coin_data['coin_ID'], 'BTC', 12, strtotime("-3 month"));
			$cmc_average_btc_price_range_6M = get_cmc_avg_price_range($coin_data['coin_ID'], 'BTC', 10, strtotime("-6 month"));
			$cmc_average_btc_price_range_1Y = get_cmc_avg_price_range($coin_data['coin_ID'], 'BTC', 10, strtotime("-12 month"));

			$cmc_avg_price_range_table =
				'<table class="cmc_avg_price_range_table">'.
					'<tr>'.
						'<th>3D</th>'.
						'<th>2W</th>'.
						'<th>1M</th>'.
						'<th>2M</th>'.
						'<th>3M</th>'.
						'<th>6M</th>'.
						'<th>1Y</th>'.
					'</tr>'.
					'<tr>'.
						'<td class="av_usd_price_range">'. $cmc_average_btc_price_range_3D['html'] .'<span>'. $cmc_average_btc_price_range_3D['compare_current_price'] .'</span></td>'.
						'<td class="av_usd_price_range">'. $cmc_average_btc_price_range_2W['html'] .'<span>'. $cmc_average_btc_price_range_2W['compare_current_price'] .'</span></td>'.
						'<td class="av_usd_price_range">'. $cmc_average_btc_price_range_1M['html'] .'<span>'. $cmc_average_btc_price_range_1M['compare_current_price'] .'</span></td>'.
						'<td class="av_usd_price_range">'. $cmc_average_btc_price_range_2M['html'] .'<span>'. $cmc_average_btc_price_range_2M['compare_current_price'] .'</span></td>'.
						'<td class="av_usd_price_range">'. $cmc_average_btc_price_range_3M['html'] .'<span>'. $cmc_average_btc_price_range_3M['compare_current_price'] .'</span></td>'.
						'<td class="av_usd_price_range">'. $cmc_average_btc_price_range_6M['html'] .'<span>'. $cmc_average_btc_price_range_6M['compare_current_price'] .'</span></td>'.
						'<td class="av_usd_price_range">'. $cmc_average_btc_price_range_1Y['html'] .'<span>'. $cmc_average_btc_price_range_1Y['compare_current_price'] .'</span></td>'.
					'</tr>'.
				'</table>';
			}

		// --- /avg_price_range_table ---


		$tr []=
		      '<tr id="'. strtolower($coin_name) .'" data-coin_name="'. $coin_name .'" data-coin_id="'. $coin_data['coin_ID'] .'" class=" '. $rating_status_classes .' '. $veteran_coin_class .' '. $shitcoin_class .' ">
		         <td>'. $coin_data['actual_rank'] .'</td>
		         <td>'. $logo_img .'</td>
		         <td><a target="_blank" href="'. $coin_data['link'] .'">'. $coin_name . $coin_full_name .'</a></td>
		         <td class="coinmarketcap">'. $coin_data['last_price'] .'</td>
						 <td>'. $coin_data['price_change_in_percent'] .'</td>
		         <td class="av_btc_price_range">'. $coin_data['av_btc_price_range'] .'</td>
		         <td>'. $coin_data['av_btc_price'] .'</td>
						 <td data-volume_change="'. $coin_data['volume']['positive_negative'] .'">'. $coin_data['volume']['html'] .'</td>
						 <td>'. $coin_data['ranks'] .'</td>
		         <td>'. $coin_data['rating']['msg'] .'</td>
		         <td>'. timeAgo($coin_data['timestamp']) .'</td>
		         <td>'. timeAgo(strtotime($coin_data['datetime'])) .'</td>
		         <td>'. $coin_data['evaluated_data'] .' db</td>
		         <td class="oldest_data">'. date('Y-m-d', $coin_data['oldest_data']) .' <br><small><b>'. timeAgo($coin_data['oldest_data']) .'</b></small></td>
		         <td class="created_date">'. date('Y-m', $coin_data['creation_date']) .' <br><small><b>'. timeAgo($coin_data['creation_date']) .'</b></small></td>
						 <td class="update_coin"><a href="'. get_permalink(PAGE_OSSZEALLITASOK) .'?crypto_statistic_manual_update=true&update_coin_by_id='. $coin_data['coin_ID'] .'"><span class="glyphicon glyphicon-repeat" aria-hidden="true"></span></a></td>
					</tr>

		      <tr class="more_info '. $rating_status_classes .' '. $shitcoin_class .'">
		         <td colspan="16">

								<div class="col-xs-12">
									'. $cmc_avg_price_range_table .'
								</div>

								<div class="clearfix"></div>

		            <div class="col-sm-8 bittrex-datas">
		               <div class="">
		                  <table class="table table-bordered">
		                     <tbody>
		                        <tr>
		                           <th colspan="6">Bittrex adatok</th>
		                        </tr>
		                        <tr>
		                           <th>Last price in BTC</th>
															 <th>Árváltozás %-ban</th>
		                           <th>Átlag ár tartomány <br><small>(last 2 month)</small></th>
		                           <th>Átlag BTC ár <br><small>(last 2 month)</small></th>
 		                           <th>Átlag BTC ár <br><small>(last 6 month)</small></th>
															 <th>Kiértékelt adatok</th>
		                        </tr>
		                        <tr>
		                           <td>'. $coin_data['bittrex_last_price'] .'</td>
															 <td>'. $coin_data['bittrex_price_change_in_percent'] .'</td>
		                           <td class="bittrex_av_btc_price_range">'. $coin_data['bittrex_av_btc_price_range'] .'</td>
		                           <td>'. $coin_data['bittrex_av_btc_price'] .'</td>
															 <td>'. get_bittrex_av_btc_price( $coin_data['coin_ID'], strtotime("-6 month") ) .'</td>
															 <td>'. $coin_data['bittrex_evaluated_data'] .' db</td>
		                        </tr>
														<tr class=" '. $coin_data['bittrex_rating']['status'] .' ">
															<td colspan="6">'. $coin_data['bittrex_rating']['msg'] .'</td>
														</tr>
		                     </tbody>
		                  </table>
		               </div>
		            </div>

								<div class="col-sm-4 technical-analysis">
		               <div class="">
		                  <table class="table table-bordered">
		                     <tbody>
		                        <tr>
		                           <th colspan="5">Technical Analysis</th>
		                        </tr>
		                        <tr>
		                           <th>Type</th>
		                           <th>T.I. BUYS</th>
		                           <th>T.I. SELLS</th>
 		                           <th>M.A. BUYS</th>
															 <th>M.A. SELLS</th>
		                        </tr>
		                        <tr>
															 <td><a href="https://www.investing.com/crypto/'. sanitize_title($coin_full_name) .'/'. sanitize_title($coin_name) .'-btc-technical" target="_blank">BTC</a></td>
															 <td class="buy-type '. $technical_analysis['btc_data']['technical_indic_buy_class'] .'">'. $technical_analysis['btc_data']['technical_indic_buy'] .'</td>
		                           <td class="sell-type '. $technical_analysis['btc_data']['technical_indic_sell_class'] .'">'. $technical_analysis['btc_data']['technical_indic_sell'] .'</td>
		                           <td class="buy-type '. $technical_analysis['btc_data']['moving_avg_buy_class'] .'">'. $technical_analysis['btc_data']['moving_avg_buy'] .'</td>
															 <td class="sell-type '. $technical_analysis['btc_data']['moving_avg_sell_class'] .'">'. $technical_analysis['btc_data']['moving_avg_sell'] .'</td>
		                        </tr>
														<tr>
															 <td><a href="https://www.investing.com/crypto/'. sanitize_title($coin_full_name) .'/'. sanitize_title($coin_name) .'-usd-technical" target="_blank">USD</a></td>
															 <td class="buy-type '. $technical_analysis['usd_data']['technical_indic_buy_class'] .'">'. $technical_analysis['usd_data']['technical_indic_buy'] .'</td>
		                           <td class="sell-type '. $technical_analysis['usd_data']['technical_indic_sell_class'] .'">'. $technical_analysis['usd_data']['technical_indic_sell'] .'</td>
		                           <td class="buy-type '. $technical_analysis['usd_data']['moving_avg_buy_class'] .'">'. $technical_analysis['usd_data']['moving_avg_buy'] .'</td>
															 <td class="sell-type '. $technical_analysis['usd_data']['moving_avg_sell_class'] .'">'. $technical_analysis['usd_data']['moving_avg_sell'] .'</td>
		                        </tr>
														<tr class="'. $technical_analysis['btc_data']['technical_indic_summary_class'] .'">
															<td colspan="5">
																'. $technical_analysis['btc_data']['technical_indic_summary'] .'
																<br>'. get_technical_indicator_how_long(intval($coin_data['coin_ID'])) .'
															</td>
														</tr>
		                     </tbody>
		                  </table>
		               </div>
		            </div>

								<div class="clearfix"></div>

								<div class="col-sm-7 actual_buys_and_sells">
		               <div class="">
		                  <table class="table table-bordered">
		                     <tbody>
		                        <tr>
		                           <th colspan="7">Buys & Sells</th>
		                        </tr>
		                        <tr>
															<th>Binance</th>
															<th>Bitfinex</th>
															<th>Bithumb</th>
															<th>Bittrex</th>
															<th>HitBTC</th>
															<th>Kraken</th>
															<th>Poloniex</th>
		                        </tr>
		                        <tr>
															<td data-binance_total_sells_and_buys_res="'. $coin_data['binance_sells_and_buys_datas_evaluation'] .'">
																 <div class="row">
																	 <div class="col-xs-6"><div class="row">'. $coin_data['binance_total_buys']['html'] .'</div></div>
																	 <div class="col-xs-6"><div class="row">'. $coin_data['binance_total_sells']['html'] .'</div></div>
																 </div>
															</td>
															<td data-bitfinex_total_sells_and_buys_res="'. $coin_data['bitfinex_sells_and_buys_datas_evaluation'] .'">
																 <div class="row">
																	 <div class="col-xs-6"><div class="row">'. $coin_data['bitfinex_total_buys']['html'] .'</div></div>
																	 <div class="col-xs-6"><div class="row">'. $coin_data['bitfinex_total_sells']['html'] .'</div></div>
																 </div>
															</td>
															<td data-bithumb_total_sells_and_buys_res="'. $coin_data['bithumb_sells_and_buys_datas_evaluation'] .'">
																 <div class="row">
																	 <div class="col-xs-6"><div class="row">'. $coin_data['bithumb_total_buys']['html'] .'</div></div>
																	 <div class="col-xs-6"><div class="row">'. $coin_data['bithumb_total_sells']['html'] .'</div></div>
																 </div>
															</td>
															<td data-bittrex_total_sells_and_buys_res="'. $coin_data['bittrex_sells_and_buys_datas_evaluation'] .'">
																 <div class="row">
																	 <div class="col-xs-6"><div class="row">'. $coin_data['bittrex_total_buys']['html'] .'</div></div>
																	 <div class="col-xs-6"><div class="row">'. $coin_data['bittrex_total_sells']['html'] .'</div></div>
																 </div>
															</td>
															<td data-hitbtc_total_sells_and_buys_res="'. $coin_data['hitbtc_sells_and_buys_datas_evaluation'] .'">
																 <div class="row">
																	 <div class="col-xs-6"><div class="row">'. $coin_data['hitbtc_total_buys']['html'] .'</div></div>
																	 <div class="col-xs-6"><div class="row">'. $coin_data['hitbtc_total_sells']['html'] .'</div></div>
																 </div>
															</td>
															<td data-kraken_total_sells_and_buys_res="'. $coin_data['kraken_sells_and_buys_datas_evaluation'] .'">
																 <div class="row">
																	 <div class="col-xs-6"><div class="row">'. $coin_data['kraken_total_buys']['html'] .'</div></div>
																	 <div class="col-xs-6"><div class="row">'. $coin_data['kraken_total_sells']['html'] .'</div></div>
																 </div>
															</td>
															<td data-poloniex_total_sells_and_buys_res="'. $coin_data['poloniex_sells_and_buys_datas_evaluation'] .'">
																 <div class="row">
																	 <div class="col-xs-6"><div class="row">'. $coin_data['poloniex_total_buys']['html'] .'</div></div>
																	 <div class="col-xs-6"><div class="row">'. $coin_data['poloniex_total_sells']['html'] .'</div></div>
																 </div>
															</td>
		                        </tr>
														<tr>
		                           <th colspan="7">
															 	'. get_markets_volument_data($coin_data['coin_ID']) .'
		                           </th>
		                        </tr>
														<tr>
		                           <th colspan="7">
		                           	'. $coin_data['all_sells_and_buys_datas_evaluation']['html'] .'
		                           </th>
		                        </tr>
		                     </tbody>
		                  </table>
		               </div>
		            </div>

		            <div class="col-sm-5 miner-datas">
		               <div class="">
		                  <table class="table table-bordered">
		                     <tbody>
		                        <tr>
		                           <th colspan="6">Bányászati adatok</th>
		                        </tr>
		                        <tr>
		                           <th>Block time</th>
		                           <th>Diff</th>
		                           <th>Nethash</th>
		                           <th>Estimated rewards</th>
		                           <th>BTC revenue</th>
		                           <th>Profitability</th>
		                        </tr>
		                        <tr>
		                           <td>'. $coin_data['mining_block_time']['html'] .'</td>
		                           <td>'. $coin_data['mining_difficult']['html'] .'</td>
		                           <td>'. $coin_data['mining_nethash']['html'] .'</td>
		                           <td>'. $coin_data['mining_estimated_rewards']['html'] .'</td>
		                           <td>'. $coin_data['mining_btc_revenue']['html'] .'</td>
		                           <td>'. $coin_data['mining_profitability']['html'] .'</td>
		                        </tr>
														<tr>
		                           <td colspan="6">'. $coin_data['mining_rating']['html'] .'</td>
		                        </tr>
		                     </tbody>
		                  </table>
		               </div>
		            </div>

								<div class="clearfix"></div><br>

								<div class="col-sm-6">
		               <div class="">
									 		'. get_bittrex_percentage_distribution_of_the_total_price_diagram($coin_data['coin_ID'], $coin_data['bittrex_last_price']) .'
		               </div>
		            </div>

								<div class="col-sm-6">
		               <div class="">
									 		'. get_cryp_markets_book_orders_diagram($coin_name) .'
		               </div>
		            </div>

								<div class="clearfix"></div><br>

		         </td>
		      </tr>';
	}

	// compress content
	foreach ($tr as $key => $tr_content) {
		$tr_temp = TinyMinify::html($tr_content, $options = [
									'collapse_whitespace' => false,
									'collapse_json_lt' => false,
								]);
		if ( !empty($tr_temp) ) { $tr[$key] = $tr_temp; }
	}
	unset($tr_temp);


	$result_html .= implode('', $tr);
	$result_html .=
	   '</tbody>
	</table>';

	unset($tr, $coins_array, $coin_data);
	return $result_html;
}
