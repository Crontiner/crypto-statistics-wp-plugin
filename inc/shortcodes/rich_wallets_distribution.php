<?php

add_shortcode('rich_wallets_distribution_diagram', 'rich_wallets_distribution_diagram_sc_function');
function rich_wallets_distribution_diagram_sc_function($atts, $content = null) {
	extract(shortcode_atts(array(
		'coin_name' => "",
	), $atts));
	global $wpdb;
	$html_result = "";
	$coins_array = array();


	$sql = "SELECT DISTINCT `coin_ID`
					FROM {$wpdb->prefix}crypto_rich_wallets_distribution
					ORDER BY `coin_ID` ASC";
	$coins_temp = $wpdb->get_results( $sql, ARRAY_A );

	$coins = array();
	foreach ($coins_temp as $key => $coin) { $coins []= (int) $coin['coin_ID']; }


	if ( !empty($coins) ) {
		foreach ($coins as $key => $coin_ID) {
			if ( intval($coin_ID) > 0 ) {

				$sql = "SELECT *
								FROM `{$wpdb->prefix}crypto_rich_wallets_distribution`
								WHERE `coin_ID` = {$coin_ID} AND
								`datetime` >= '". date('Y-m-d H:i:s', strtotime('-20 days')) ."'
								ORDER BY `datetime` DESC";
				$coin_data = $wpdb->get_results( $sql, ARRAY_A );


				if ( !empty($coin_data) ) {
					foreach ($coin_data as $key2 => $data) {
						$coins_array[$coin_ID][$data['balance_type']]['addresses'][] = $data['addresses'];
						//$coins_array[$coin_ID][$data['balance_type']]['addresses_in_percent'][] = $data['addresses_in_percent'];
						$coins_array[$coin_ID][$data['balance_type']]['coins'][] = $data['coins'];
						//$coins_array[$coin_ID][$data['balance_type']]['coins_in_percent'][] = $data['coins_in_percent'];
						$coins_array[$coin_ID][$data['balance_type']]['datetime'][] = date_i18n('D H', strtotime($data['datetime']));
					}
				}
			}
		}
	}


	$coins_array_temp = array();
	$all_array_counts = array();


	if ( !empty($coins_array) ) {
		$balance_types = get_option('crypto_rich_wallets_distribution_balance_types');

		foreach ($coins_array as $coin_ID => $coins_data) {

			foreach ($balance_types as $balance_type_ID => $balance_type) {
				if ( isset($coins_data[$balance_type_ID]) && !empty($coins_data[$balance_type_ID]) ) {
					$coin_data = $coins_data[$balance_type_ID];


					$dates = $coin_data['datetime'];
					$addresses = $coin_data['addresses'];
					$coins = $coin_data['coins'];
					$datetime = $coin_data['datetime'];


					// Percentage Calculations: address
					$addresses_in_percent = array();
					$addresses_temp = $addresses;
					arsort($addresses_temp);
					$max = $addresses_temp[0];
					foreach ($addresses_temp as $key => $address) {
						$percent = round(($address * 100) / $max, 2);

						if ( $percent >= 100 ) { $addresses_in_percent []= 100; }
						else { $addresses_in_percent []= $percent; }
					}
					unset($addresses_temp);


					// Percentage Calculations: coins
					$coins_in_percent = array();
					$coins_temp = $coins;
					arsort($coins_temp);
					$max = $coins_temp[0];
					foreach ($coins_temp as $key => $coin) {
						$percent = round(($coin * 100) / $max, 2);

						if ( $percent >= 100 ) { $coins_in_percent []= 100; }
						else { $coins_in_percent []= $percent; }
					}
					unset($coins_temp);

					$all_array_counts[]= count($dates);
					$all_array_counts[]= count($addresses_in_percent);
					$all_array_counts[]= count($coins_in_percent);

					$coins_array_temp[$coin_ID][$balance_type] = array(
																														'dates' => $dates,
																														'addresses_in_percent' => $addresses_in_percent,
																														'coins_in_percent' => $coins_in_percent,
																												);
				}
			}
		}
	}

	$coins_array = $coins_array_temp;
	unset($coins_array_temp);
	asort($all_array_counts);
	$max_available_data = $all_array_counts[0];


	if ( $max_available_data > 0 ) {

		foreach ($coins_array as $coin_ID => $wallets_datas) {
			$html_result_temp = "";
			$datasets = array();


			$dates = "";
			foreach ($wallets_datas as $key => $value) { $dates = $value['dates']; break; }
			$global_labels = array_slice($dates, 0, $max_available_data);

			$global_labels_temp = array();
			foreach ($global_labels as $key => $val) { $global_labels_temp []= '"'. $val .'"'; }
			$global_labels = $global_labels_temp;


			foreach ($wallets_datas as $balance_type => $wallets_data) {

				$balance_type_ID = 0;
				foreach ($balance_types as $key => $val) {
					if ( $balance_type == $val ) { $balance_type_ID = $key; } break;
				}


				$addresses_in_percent = array();
				foreach ($wallets_data["addresses_in_percent"] as $key => $val) { $addresses_in_percent []= '"'. $val .'"'; }

				$coins_in_percent = array();
				foreach ($wallets_data["coins_in_percent"] as $key => $val) { $coins_in_percent []= '"'. $val .'"'; }


				$addresses_label = "Address: ". $balance_type;
				$coins_label = "Coin: ". $balance_type;
				$addresses_data = implode(', ', array_reverse(array_slice($addresses_in_percent, 0, $max_available_data)));
				$coins_data = implode(', ', array_reverse(array_slice($coins_in_percent, 0, $max_available_data)));


				$datasets['addresses'][]= '{
																		steppedLine: false,
																		label: "'. $addresses_label .'",
																		fill: false,
																		backgroundColor: "rgba(200, 200, 200, 1)",
																		borderColor: "rgba(200, 200, 200, 1)",
																		data: ['. $addresses_data .'],
																		lineTension: 0,
																	}';

				$datasets['coins'][]= '{
																		steppedLine: false,
																		label: "'. $coins_label .'",
																		fill: false,
																		backgroundColor: "rgba(0, 153, 153, 1)",
																		borderColor: "rgba(0, 153, 153, 1)",
																		data: ['. $coins_data .'],
																		lineTension: 0,
																	}';
			}


			$canvas_id = 'cid'. md5($coin_ID) .rand(1,9999);

			$html_result_temp .=
				'<div class="clearfix"></div>'.
				'<h4 class="sc_title"><b>'. strtoupper(get_coin_name_by_id($coin_ID)) .'</b></h4>';

			$html_result_temp .=
				'<div class="coin_wallets_data">'.
					'<canvas id="'. $canvas_id .'"></canvas>
					<script type="text/javascript">
					jQuery(function($) {
						$(window).load(function(){
							var ctx = document.getElementById("'. $canvas_id .'").getContext("2d");
							var chart = new Chart(ctx, {
									type: "line",
									data: {
											labels: ['. implode(', ', array_reverse($global_labels)) .'],
											datasets: ['. implode(', ', $datasets['addresses']) .', '. implode(', ', $datasets['coins']) .']
									},
									options: {
										responsive: true,
										legend: {
											display: true,
										},
									}
							});

						});
					});
					</script>
					<br>
				</div>';

				$html_result .= $html_result_temp;
		}
	}

	return '<div class="rich_wallets_distribution_diagram_sc">'. $html_result .'</div>';
}
