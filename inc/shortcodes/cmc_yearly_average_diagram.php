<?php

// Lekér néhány évnyi árlistát és összehasonlítja a hónapok értékeinek változásait
add_shortcode('cmc_yearly_average_diagram', 'cmc_yearly_average_diagram_sc_function');
function cmc_yearly_average_diagram_sc_function($atts, $content = null) {
	extract(shortcode_atts(array(
		'coin_name' => "",
	), $atts));
	$html_result = "";
	$canvas_id = 'c'. md5(wp_generate_password(15, false, false));
	$start_date = "-6 years";
	$results_array = get_coin_cmc_historical_data($coin_name, $start_date);
	$current_usd_price = (float) get_cmc_usd_price(get_coin_id_by_full_name($coin_name));
	$current_part_of_month = 0;
	$results_array_temp = array();


	if ( !empty($results_array) && is_array($results_array) ) {
		array_sort_by_column($results_array, 'date');

		foreach ($results_array as $key => $day_datas) {
			$year = (int) date('Y', $day_datas['date']);
			$month = (int) date('m', $day_datas['date']);
			$day = (int) date('d', $day_datas['date']);

			$days_in_month = (int) cal_days_in_month(CAL_GREGORIAN, $month, $year);
			$group1 = (int) round($days_in_month / 4);
			$group2 = (int) round($group1 * 2);
			$group3 = (int) round($group1 * 3);
			$group4 = (int) $days_in_month;

			$part_of_month = "";
			if ( (0 < $day) && ($group1 >= $day) ) { $part_of_month = 1; }
			else if ( ($group1 < $day) && ($group2 >= $day) ) { $part_of_month = 2; }
			else if ( ($group2 < $day) && ($group3 >= $day) ) { $part_of_month = 3; }
			else if ( ($group3 < $day) && ($group4 >= $day) ) { $part_of_month = 4; }

			if ( $group4 >= date('d') ) { $current_part_of_month = 4; }
			if ( $group3 >= date('d') ) { $current_part_of_month = 3; }
			if ( $group2 >= date('d') ) { $current_part_of_month = 2; }
			if ( $group1 >= date('d') ) { $current_part_of_month = 1; }

			$results_array_temp[$year][$month][$part_of_month]['usd_price'][] = $day_datas['usd_price'];
			$results_array_temp[$year][$month][$part_of_month]['volume'][] = $day_datas['volume'];
			$results_array_temp[$year][$month][$part_of_month]['market_cap'][] = $day_datas['market_cap'];
		}


		if ( !empty($results_array_temp) ) {

			foreach ($results_array_temp as $year => $months_array) {
				// Year

				// Find max values
				$max_usd_price = array();
				$max_volume = array();
				$max_market_cap = array();


				foreach ($results_array_temp[$year] as $month => $part_of_month_array) {
					// Month

					foreach ($results_array_temp[$year][$month] as $part_of_month => $day_array) {
						// Part of month

						if ( isset($day_array['usd_price']) ) {
							$usd_prices = array();
							foreach ($day_array['usd_price'] as $key => $value) { $usd_prices []= $value; }
							$results_array_temp[$year][$month][$part_of_month]['usd_price'] = array_sum($usd_prices) / count($usd_prices);
							$max_usd_price[]= $results_array_temp[$year][$month][$part_of_month]['usd_price'];
						}
						if ( isset($day_array['volume']) ) {
							$volumes = array();
							foreach ($day_array['volume'] as $key => $value) { $volumes []= $value; }
							$results_array_temp[$year][$month][$part_of_month]['volume'] = array_sum($volumes) / count($volumes);
							$max_volume[]= $results_array_temp[$year][$month][$part_of_month]['volume'];
						}
						if ( isset($day_array['market_cap']) ) {
							$market_caps = array();
							foreach ($day_array['market_cap'] as $key => $value) { $market_caps []= $value; }
							$results_array_temp[$year][$month][$part_of_month]['market_cap'] = array_sum($market_caps) / count($market_caps);
							$max_market_cap[]= $results_array_temp[$year][$month][$part_of_month]['market_cap'];
						}

					}
				}


				// Find max values
				if ( !empty($max_usd_price) ) {
					arsort($max_usd_price);
					$max_usd_price = array_values($max_usd_price);
					$max_usd_price = $max_usd_price[0];
				}
				if ( !empty($max_volume) ) {
					arsort($max_volume);
					$max_volume = array_values($max_volume);
					$max_volume = $max_volume[0];
				}
				if ( !empty($max_market_cap) ) {
					arsort($max_market_cap);
					$max_market_cap = array_values($max_market_cap);
					$max_market_cap = $max_market_cap[0];
				}


				// Change values to percents
				foreach ($results_array_temp[$year] as $month => $part_of_month_array) {
					// Month

					foreach ($results_array_temp[$year][$month] as $part_of_month => $day_array) {
						// Part of month

						if ( isset($day_array['usd_price']) ) {
							$results_array_temp[$year][$month][$part_of_month]['usd_price'] = round( ($day_array['usd_price'] * 100) / $max_usd_price ,2);
						}
						if ( isset($day_array['volume']) ) {
							$results_array_temp[$year][$month][$part_of_month]['volume'] = round( ($day_array['volume'] * 100) / $max_volume ,2);
						}
						if ( isset($day_array['market_cap']) ) {
							$results_array_temp[$year][$month][$part_of_month]['market_cap'] = round( ($day_array['market_cap'] * 100) / $max_market_cap ,2);
						}
					}
				}


			}


			// Separate values by months
			$results_array = array();

			// Separate values by years
			$annual_datas_array = array();


			foreach ($results_array_temp as $year => $months_array) {
				// Year

				foreach ($results_array_temp[$year] as $month => $part_of_month_array) {
					// Month

					foreach ($results_array_temp[$year][$month] as $part_of_month => $day_array) {
						// Part of month

						$results_array[$month][$part_of_month]['usd_price'][] = $day_array['usd_price'];
						$results_array[$month][$part_of_month]['volume'][] = $day_array['volume'];
						$results_array[$month][$part_of_month]['market_cap'][] = $day_array['market_cap'];

						$annual_datas_array[$year][$month][$part_of_month]['usd_price'] = $day_array['usd_price'];
						$annual_datas_array[$year][$month][$part_of_month]['volume'] = $day_array['volume'];
						$annual_datas_array[$year][$month][$part_of_month]['market_cap'] = $day_array['market_cap'];
					}
				}
			}
			unset($results_array_temp);


			ksort($annual_datas_array);
			foreach ($annual_datas_array as $year => $months_array) {
				// Year
				ksort($annual_datas_array[$year]);

				foreach ($annual_datas_array[$year] as $month => $part_of_month_array) {
					// Month
					ksort($annual_datas_array[$year][$month]);
				}
			}


			// Calculate average

			foreach ($results_array as $month => $part_of_month_array) {
				// Month

				foreach ($results_array[$month] as $part_of_month => $days_array) {
					// Part of month

					$results_array[$month][$part_of_month]['usd_price'] = round(array_sum($days_array['usd_price']) / count($days_array['usd_price']), 2);
					$results_array[$month][$part_of_month]['volume'] = round(array_sum($days_array['volume']) / count($days_array['volume']), 2);
					$results_array[$month][$part_of_month]['market_cap'] = round(array_sum($days_array['market_cap']) / count($days_array['market_cap']), 2);
				}
			}


			$labels = array();
			$usd_prices = array();
			$volumes = array();
			$market_caps = array();


			for ($i=1; $i <= 12; $i++) {
				$calculated_price_p1 = $calculated_price_p2 = $calculated_price_p3 = $calculated_price_p4 = "";


				/*
				if ( $current_usd_price > 0 ) {

					$current_part_percent = $results_array[intval(date('m'))][$current_part_of_month]['usd_price'];
					$top_hight_usd_price = $current_usd_price + (( (100 - $current_part_percent) / 100) * $current_usd_price);


					$calculated_price_p1 = ($results_array[$i][1]['usd_price'] / 100) * $top_hight_usd_price;
					$calculated_price_p2 = ($results_array[$i][2]['usd_price'] / 100) * $top_hight_usd_price;
					$calculated_price_p3 = ($results_array[$i][3]['usd_price'] / 100) * $top_hight_usd_price;
					$calculated_price_p4 = ($results_array[$i][4]['usd_price'] / 100) * $top_hight_usd_price;

					if ( $i == intval(date('m')) ) {
						if ( $current_part_of_month == 1 ) { $calculated_price_p1 = $current_usd_price; }
						if ( $current_part_of_month == 2 ) { $calculated_price_p2 = $current_usd_price; }
						if ( $current_part_of_month == 3 ) { $calculated_price_p3 = $current_usd_price; }
						if ( $current_part_of_month == 4 ) { $calculated_price_p4 = $current_usd_price; }
					}

					$calculated_price_p1 = round($calculated_price_p1);
					$calculated_price_p2 = round($calculated_price_p2);
					$calculated_price_p3 = round($calculated_price_p3);
					$calculated_price_p4 = round($calculated_price_p4);
				}*/


				$labels[] = '"'. date('M', strtotime(date('Y-'. $i .'-01 00:00:00')) ) .' - p1 - '. $calculated_price_p1 .'"';
				$labels[] = '"'. date('M', strtotime(date('Y-'. $i .'-01 00:00:00')) ) .' - p2 - '. $calculated_price_p2 .'"';
				$labels[] = '"'. date('M', strtotime(date('Y-'. $i .'-01 00:00:00')) ) .' - p3 - '. $calculated_price_p3 .'"';
				$labels[] = '"'. date('M', strtotime(date('Y-'. $i .'-01 00:00:00')) ) .' - p4 - '. $calculated_price_p4 .'"';

				$usd_prices []= '"'. $results_array[$i][1]['usd_price'] .'"';
				$usd_prices []= '"'. $results_array[$i][2]['usd_price'] .'"';
				$usd_prices []= '"'. $results_array[$i][3]['usd_price'] .'"';
				$usd_prices []= '"'. $results_array[$i][4]['usd_price'] .'"';

				$volumes []= '"'. $results_array[$i][1]['volume'] .'"';
				$volumes []= '"'. $results_array[$i][2]['volume'] .'"';
				$volumes []= '"'. $results_array[$i][3]['volume'] .'"';
				$volumes []= '"'. $results_array[$i][4]['volume'] .'"';

				$market_caps []= '"'. $results_array[$i][1]['market_cap'] .'"';
				$market_caps []= '"'. $results_array[$i][2]['market_cap'] .'"';
				$market_caps []= '"'. $results_array[$i][3]['market_cap'] .'"';
				$market_caps []= '"'. $results_array[$i][4]['market_cap'] .'"';
			}
			unset($results_array);


			$annual_datas_html = "";

			foreach ($annual_datas_array as $year => $months_array) {
				// Year

				$labels_temp = array();
				$usd_prices_temp = array();
				$volumes_temp = array();
				$market_caps_temp = array();
				$canvas_id_temp = 'c'. md5(wp_generate_password(15, false, false));


				foreach ($annual_datas_array[$year] as $month => $part_of_month_array) {
					// Month

					foreach ($annual_datas_array[$year][$month] as $part_of_month_key => $part_of_month_data) {
						// A part of the months

						$labels_temp			[]= '"'. date('M', strtotime(date('Y-'. $month .'-01 00:00:00')) ) .' - p'. $part_of_month_key .'"';
						$usd_prices_temp	[]= '"'. $part_of_month_data['usd_price'] .'"';
						$volumes_temp			[]= '"'. $part_of_month_data['volume'] .'"';
						$market_caps_temp	[]= '"'. $part_of_month_data['market_cap'] .'"';
					}
				}

				$annual_datas_html .=
					'<div class="col-md-6">'.
						'<h4 class="sc_title">'. $year .'</h4>'.
						'<canvas id="'. $canvas_id_temp .'"></canvas>
						<script type="text/javascript">
						jQuery(function($) {
							$(window).load(function(){
								var ctx = document.getElementById("'. $canvas_id_temp .'").getContext("2d");
								var chart = new Chart(ctx, {
										type: "line",
										data: {
											labels: ['. implode(', ', $labels_temp) .'],
											datasets: [{
												steppedLine: false,
												label: "USD",
												fill: false,
												backgroundColor: "rgba(255, 0, 0, 1)",
												borderColor: "rgba(255, 0, 0, 1)",
												data: ['. implode(', ', $usd_prices_temp) .'],
												lineTension: 0,
											},{
												steppedLine: false,
												label: "Volume",
												fill: false,
												backgroundColor: "rgba(0, 255, 0, 1)",
												borderColor: "rgba(0, 255, 0, 1)",
												data: ['. implode(', ', $volumes_temp) .'],
												lineTension: 0,
											},{
												steppedLine: false,
												label: "Market cap",
												fill: false,
												backgroundColor: "rgba(0, 0, 255, 1)",
												borderColor: "rgba(0, 0, 255, 1)",
												data: ['. implode(', ', $market_caps_temp) .'],
												lineTension: 0,
											}]
										},
										options: {
											responsive: true,
											legend: {
												display: true,
											},
										}
								});

							});
						});
						</script>'.
					'</div>';
			}


			$html_result =
				'<canvas id="'. $canvas_id .'"></canvas>
				<script type="text/javascript">
				jQuery(function($) {
					$(window).load(function(){
						var ctx = document.getElementById("'. $canvas_id .'").getContext("2d");
						var chart = new Chart(ctx, {
						    type: "line",
						    data: {
						        labels: ['. implode(', ', $labels) .'],
										datasets: [{
												steppedLine: false,
				                label: "USD",
												fill: false,
				                backgroundColor: "rgba(255, 0, 0, 1)",
												borderColor: "rgba(255, 0, 0, 1)",
				                data: ['. implode(', ', $usd_prices) .'],
												lineTension: 0,
				            },{
												steppedLine: false,
				                label: "Volume",
												fill: false,
				                backgroundColor: "rgba(0, 255, 0, 1)",
												borderColor: "rgba(0, 255, 0, 1)",
				                data: ['. implode(', ', $volumes) .'],
												lineTension: 0,
				            },{
												steppedLine: false,
				                label: "Market cap",
												fill: false,
				                backgroundColor: "rgba(0, 0, 255, 1)",
												borderColor: "rgba(0, 0, 255, 1)",
				                data: ['. implode(', ', $market_caps) .'],
												lineTension: 0,
				            }]
						    },
								options: {
									responsive: true,
									legend: {
										display: true,
									},
								}
						});

					});
				});
				</script>';

				$html_result =
					'<div class="coin_data_wrapper">'.

						'<h4 class="sc_title">
							<b>'. strtoupper($coin_name) .'</b>
							values compare - <small>'. timeAgo(strtotime($start_date)) .'</small>
						</h4>'.

						$html_result.

						'<div class="row">'.
							$annual_datas_html.
						'</div>'.

						'<div class="clearfix"></div>'.
					'</div>';
		}
	}

	return '<div class="cmc_yearly_average_diagram_sc">'. $html_result .'</div>';
}
