<?php

add_shortcode('rich_separated_wallets_amount_movements', 'rich_separated_wallets_amount_movements_function');
function rich_separated_wallets_amount_movements_function($atts, $content = null) {
	extract(shortcode_atts(array(
		'coin_symbol' => "",
	), $atts));

	if ( empty($coin_symbol) ) { return ""; }
	$coin_ID = get_coin_id_by_name($coin_symbol);
	if ( empty($coin_ID) ) { return ""; }

	global $wpdb;
	$result_html = "";
	$canvas_id = 'c'. wp_generate_password(6, false, false);
	if ( !is_user_logged_in() ) { return ""; }


	// --- Get last updated datetime ---
	$last_updated_datetime = $wpdb->get_results( "SELECT `last_updated_datetime`
																								FROM `{$wpdb->prefix}crypto_bitinfocharts_rich_wallets`
																								ORDER BY `last_updated_datetime` DESC
																								LIMIT 1", ARRAY_A );
	if ( isset($last_updated_datetime[0]['last_updated_datetime']) ) {
		$last_updated_datetime = strtotime($last_updated_datetime[0]['last_updated_datetime']);
	} else {
		$last_updated_datetime = strtotime(current_time('mysql'));
	}
	// ---

	$days_array = array();
	for ($i=0; $i <= 7; $i++) {
		$days_array []= strtotime(date('Y-m-d 00:00:00', strtotime('-'. $i .' day')));
	}
	$days_array = array_reverse($days_array);


	$days_html = array();
	foreach ($days_array as $key => $timestamp) {
		$days_html []= '"'. date('m-d', $timestamp) .'"';
	}
	$days_html = implode(',', $days_html);



	// Get wallets
	$wallets_array = $wpdb->get_results( "
		SELECT DISTINCT `{$wpdb->prefix}crypto_bitinfocharts_rich_wallets`.`ID`
		FROM `{$wpdb->prefix}crypto_bitinfocharts_rich_wallets_datas`
		LEFT JOIN `{$wpdb->prefix}crypto_bitinfocharts_rich_wallets`
			ON `{$wpdb->prefix}crypto_bitinfocharts_rich_wallets`.`ID` = `{$wpdb->prefix}crypto_bitinfocharts_rich_wallets_datas`.`wallet_ID`

		WHERE `{$wpdb->prefix}crypto_bitinfocharts_rich_wallets`.`coin_ID` = ". $coin_ID ."
			AND `{$wpdb->prefix}crypto_bitinfocharts_rich_wallets_datas`.`datetime` > '". date('Y-m-d 00:00:00', strtotime('-8 day')) ."'
		ORDER BY `{$wpdb->prefix}crypto_bitinfocharts_rich_wallets_datas`.`actual_amount` DESC
		LIMIT 300", ARRAY_A );

	$wallets_array_temp = array();
	foreach ($wallets_array as $key => $value) {
		$wallets_array_temp []= (int) $value['ID'];
	}
	$wallets_array = $wallets_array_temp;
	unset($wallets_array_temp);


	// Get amount / day
	$daily_amounts = array();
	$wallets_last_amount = array();

	foreach ($days_array as $key => $timestamp) {
		$start_interval = date('Y-m-d 00:00:00', $timestamp);
		$end_interval = date('Y-m-d 23:59:59', $timestamp);

		foreach ($wallets_array as $key => $wallet_ID) {

			$daily_amount = $wpdb->get_results( "
				SELECT `cryp_crypto_bitinfocharts_rich_wallets_datas`.`actual_amount`
				FROM `cryp_crypto_bitinfocharts_rich_wallets_datas`
				LEFT JOIN `cryp_crypto_bitinfocharts_rich_wallets`
					ON `cryp_crypto_bitinfocharts_rich_wallets`.`ID` = `cryp_crypto_bitinfocharts_rich_wallets_datas`.`wallet_ID`

				WHERE `cryp_crypto_bitinfocharts_rich_wallets`.`ID` = ". $wallet_ID ."
					AND `cryp_crypto_bitinfocharts_rich_wallets_datas`.`datetime` <= '". $end_interval ."'
					AND `cryp_crypto_bitinfocharts_rich_wallets_datas`.`datetime` > '". $start_interval ."'

				ORDER BY `cryp_crypto_bitinfocharts_rich_wallets_datas`.`actual_amount` DESC
				LIMIT 1", ARRAY_A );

				$daily_amount = (float) $daily_amount[0]['actual_amount'];

				if ( !isset($wallets_last_amount[$wallet_ID]) ) {
					$start_amount = $wpdb->get_results( "
						SELECT `cryp_crypto_bitinfocharts_rich_wallets_datas`.`actual_amount`
						FROM `cryp_crypto_bitinfocharts_rich_wallets_datas`
						LEFT JOIN `cryp_crypto_bitinfocharts_rich_wallets`
							ON `cryp_crypto_bitinfocharts_rich_wallets`.`ID` = `cryp_crypto_bitinfocharts_rich_wallets_datas`.`wallet_ID`

						WHERE `cryp_crypto_bitinfocharts_rich_wallets`.`ID` = ". $wallet_ID ."
							AND `cryp_crypto_bitinfocharts_rich_wallets_datas`.`datetime` < '". $start_interval ."'

						ORDER BY `cryp_crypto_bitinfocharts_rich_wallets_datas`.`actual_amount` DESC
						LIMIT 1", ARRAY_A );

					if ( !empty($start_amount[0]['actual_amount']) ) {
						$wallets_last_amount[$wallet_ID] = (float) $start_amount[0]['actual_amount'];
					} else {
						$wallets_last_amount[$wallet_ID] = 0;
					}
				}

				if ( !empty($daily_amount) ) {
					$wallets_last_amount[$wallet_ID] = $daily_amount;

				} else {
					// Get prev amount
					$daily_amount = $wallets_last_amount[$wallet_ID];
				}

				$daily_amount = number_format($daily_amount, 2);
				$daily_amount = str_replace(',', "", $daily_amount);

				$daily_amounts[date('Y-m-d', $timestamp)][$wallet_ID] = $daily_amount;
		}
	}


	$wallets_array_temp = array();
	foreach ($daily_amounts as $date => $amounts_array) {
		foreach ($amounts_array as $wallet_id => $amount) {
			$wallets_array_temp[$wallet_id][] = $amount;
		}
	}


	$datasets_array = array();
	$p = 0;
	$n = 0;
	foreach ($wallets_array_temp as $wallet_id => $amount) {

		$color = "rgba(".rand(1,255).",".rand(1,255).",".rand(1,255).",1)";

		// remove "not changed wallets"
		$amount_temp = array_values(array_filter(array_unique($amount)));
		$percentChange = percent_between_two_numbers($amount[0], end($amount));

		if ( 	(count($amount_temp) > 1) &&
					($percentChange != 0) &&
					($percentChange >= 15) || ($percentChange <= -15)) {


			if ( end($amount) > $amount[0] ) {
				$color = "rgba(0,".rand(150,255).",0,1)";
				$p += 1;
			} else {
				$color = "rgba(".rand(150,255).",0,0,1)";
				$n += 1;
			}

			$datasets_array []=
				'{
					label: "wID: '. $wallet_id .'",
					borderColor: "'. $color .'",
					backgroundColor: "'. $color .'",
					fill: false,
					data: ['. implode(',', $amount) .'],
					yAxisID: "y-axis-1",
				}';
		}
	}
	unset($wallets_array_temp, $daily_amounts);


	$result_html = '
	<canvas id="canvas"></canvas>

	<script>
			var lineChartData = {
				labels: ['. $days_html .'],
				datasets: ['. implode(',', $datasets_array) .']
			};

			window.onload = function() {
				var ctx = document.getElementById("canvas").getContext("2d");
				window.myLine = Chart.Line(ctx, {
					data: lineChartData,
					options: {
						responsive: true,
						hoverMode: "index",
						stacked: false,
						title: {
							display: false,
							text: "BTC Wallets"
						},
						scales: {
							yAxes: [{
								type: "linear",
								display: true,
								position: "left",
								id: "y-axis-1",
							}],
						}
					}
				});
			};
		</script>

	';


	return '<div class="rich_separated_wallets_amount_movements_sc">
						<h4 class="sc_title">'. $coin_symbol .' wallets with at least 15% movement | <small>- P: '.$p.' N: '.$n.'</small></h4>'.
						$result_html .
					'</div>';
}
