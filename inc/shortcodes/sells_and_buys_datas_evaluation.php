<?php

add_shortcode('sells_and_buys_datas_evaluation', 'sells_and_buys_datas_evaluation_sc_function');
function sells_and_buys_datas_evaluation_sc_function() {
	global $wpdb;
	$result_html = "";
	if ( !is_user_logged_in() ) { return ""; }

	/*
	$bittrex_buys 			= get_bittrex_total_buys_from_compilation();
	$bittrex_sells 			= get_bittrex_total_sells_from_compilation();

	$bitfinex_buys 			= get_bitfinex_total_buys_from_compilation();
	$bitfinex_sells 		= get_bitfinex_total_sells_from_compilation();

	$hitbtc_buys 			= get_hitbtc_total_buys_from_compilation();
	$hitbtc_sells 		= get_hitbtc_total_sells_from_compilation();
	*/

	$bittrex_buys 			= get_total_buys_and_sells_from_market_history('bittrex', 'b');
	$bittrex_sells 			= get_total_buys_and_sells_from_market_history('bittrex', 's');

	$bitfinex_buys 			= get_total_buys_and_sells_from_market_history('bitfinex', 'b');
	$bitfinex_sells 		= get_total_buys_and_sells_from_market_history('bitfinex', 's');

	$hitbtc_buys 				= get_total_buys_and_sells_from_market_history('hitbtc', 'b');
	$hitbtc_sells 			= get_total_buys_and_sells_from_market_history('hitbtc', 's');

	$poloniex_buys 			= get_total_buys_and_sells_from_market_history('poloniex', 'b');
	$poloniex_sells 		= get_total_buys_and_sells_from_market_history('poloniex', 's');

	$bithumb_buys 			= get_total_buys_and_sells_from_market_history('bithumb', 'b');
	$bithumb_sells 			= get_total_buys_and_sells_from_market_history('bithumb', 's');

	$kraken_buys 				= get_total_buys_and_sells_from_market_history('kraken', 'b');
	$kraken_sells 			= get_total_buys_and_sells_from_market_history('kraken', 's');

	$binance_buys 			= get_total_buys_and_sells_from_market_history('binance', 'b');
	$binance_sells 			= get_total_buys_and_sells_from_market_history('binance', 's');

	/*
	$b = ($bittrex_buys + $bitfinex_buys + $hitbtc_buys + $poloniex_buys + $bithumb_buys + $kraken_buys + $binance_buys) / 6;
	$s = ($bittrex_sells + $bitfinex_sells + $hitbtc_sells + $poloniex_sells + $bithumb_sells + $kraken_sells + $binance_sells) / 6;
	*/
	$b = $bittrex_buys + $bitfinex_buys + $hitbtc_buys + $poloniex_buys + $bithumb_buys + $kraken_buys + $binance_buys;
	$s = $bittrex_sells + $bitfinex_sells + $hitbtc_sells + $poloniex_sells + $bithumb_sells + $kraken_sells + $binance_sells;


	$evaluation_result = get_sells_and_buys_datas_evaluation(array('percent_change' => $b), array('percent_change' => $s));


	$bittrex_buys 	= round($bittrex_buys,2);
	$bittrex_sells 	= round($bittrex_sells,2);

	$bitfinex_buys 	= round($bitfinex_buys,2);
	$bitfinex_sells = round($bitfinex_sells,2);

	$hitbtc_buys 		= round($hitbtc_buys,2);
	$hitbtc_sells 	= round($hitbtc_sells,2);

	$poloniex_buys 	= round($poloniex_buys,2);
	$poloniex_sells = round($poloniex_sells,2);

	$bithumb_buys 	= round($bithumb_buys,2);
	$bithumb_sells 	= round($bithumb_sells,2);

	$kraken_buys 		= round($kraken_buys,2);
	$kraken_sells 	= round($kraken_sells,2);

	$binance_buys 	= round($binance_buys,2);
	$binance_sells 	= round($binance_sells,2);

	$b 	= round($b,2);
	$s 	= round($s,2);


	$datasets = array();

	$datasets []= '{label: "Binance - Buys",
									backgroundColor: "rgba(0, 204, 104, 1)",
									borderColor: "rgba(0, 204, 104, 1)",
									borderWidth: 1,
									data: ['. $binance_buys .'],}';

	$datasets []= '{label: "Binance - Sells",
									backgroundColor: "rgba(204, 35, 0, 1)",
									borderColor: "rgba(204, 35, 0, 1)",
									borderWidth: 1,
									data: ['. $binance_sells .'],}';


	$datasets []= '{label: "",backgroundColor: "rgba(181, 76, 51, 0)",borderColor: "rgba(181, 76, 51, 0)",borderWidth: 0,data: [0],}';


	$datasets []= '{label: "Bitfinex - Buys",
									backgroundColor: "rgba(0, 204, 104, 1)",
									borderColor: "rgba(0, 204, 104, 1)",
									borderWidth: 1,
									data: ['. $bitfinex_buys .'],}';

	$datasets []= '{label: "Bitfinex - Sells",
									backgroundColor: "rgba(204, 35, 0, 1)",
									borderColor: "rgba(204, 35, 0, 1)",
									borderWidth: 1,
									data: ['. $bitfinex_sells .'],}';


	$datasets []= '{label: "",backgroundColor: "rgba(181, 76, 51, 0)",borderColor: "rgba(181, 76, 51, 0)",borderWidth: 0,data: [0],}';


	$datasets []= '{label: "Bithumb - Buys",
									backgroundColor: "rgba(56, 150, 88, 1)",
									borderColor: "rgba(56, 150, 88, 1)",
									borderWidth: 1,
									data: ['. $bithumb_buys .'],}';

	$datasets []= '{label: "Bithumb - Sells",
									backgroundColor: "rgba(181, 76, 51, 1)",
									borderColor: "rgba(181, 76, 51, 1)",
									borderWidth: 1,
									data: ['. $bithumb_sells .'],}';


	$datasets []= '{label: "",backgroundColor: "rgba(181, 76, 51, 0)",borderColor: "rgba(181, 76, 51, 0)",borderWidth: 0,data: [0],}';


	$datasets []= '{label: "Bittrex - Buys",
									backgroundColor: "rgba(56, 150, 88, 1)",
									borderColor: "rgba(56, 150, 88, 1)",
									borderWidth: 1,
									data: ['. $bittrex_buys .'],}';

	$datasets []= '{label: "Bittrex - Sells",
									backgroundColor: "rgba(181, 76, 51, 1)",
									borderColor: "rgba(181, 76, 51, 1)",
									borderWidth: 1,
									data: ['. $bittrex_sells .'],}';


	$datasets []= '{label: "",backgroundColor: "rgba(181, 76, 51, 0)",borderColor: "rgba(181, 76, 51, 0)",borderWidth: 0,data: [0],}';


	$datasets []= '{label: "HitBTC - Buys",
									backgroundColor: "rgba(56, 150, 88, 1)",
									borderColor: "rgba(56, 150, 88, 1)",
									borderWidth: 1,
									data: ['. $hitbtc_buys .'],}';

	$datasets []= '{label: "HitBTC - Sells",
									backgroundColor: "rgba(181, 76, 51, 1)",
									borderColor: "rgba(181, 76, 51, 1)",
									borderWidth: 1,
									data: ['. $hitbtc_sells .'],}';


	$datasets []= '{label: "",backgroundColor: "rgba(181, 76, 51, 0)",borderColor: "rgba(181, 76, 51, 0)",borderWidth: 0,data: [0],}';


	$datasets []= '{label: "Kraken - Buys",
									backgroundColor: "rgba(0, 204, 104, 1)",
									borderColor: "rgba(0, 204, 104, 1)",
									borderWidth: 1,
									data: ['. $kraken_buys .'],}';

	$datasets []= '{label: "Kraken - Sells",
									backgroundColor: "rgba(204, 35, 0, 1)",
									borderColor: "rgba(204, 35, 0, 1)",
									borderWidth: 1,
									data: ['. $kraken_sells .'],}';


	$datasets []= '{label: "",backgroundColor: "rgba(181, 76, 51, 0)",borderColor: "rgba(181, 76, 51, 0)",borderWidth: 0,data: [0],}';


	$datasets []= '{label: "Poloniex - Buys",
									backgroundColor: "rgba(0, 204, 104, 1)",
									borderColor: "rgba(0, 204, 104, 1)",
									borderWidth: 1,
									data: ['. $poloniex_buys .'],}';

	$datasets []= '{label: "Poloniex - Sells",
									backgroundColor: "rgba(204, 35, 0, 1)",
									borderColor: "rgba(204, 35, 0, 1)",
									borderWidth: 1,
									data: ['. $poloniex_sells .'],}';


	$datasets []= '{label: "",backgroundColor: "rgba(181, 76, 51, 0)",borderColor: "rgba(181, 76, 51, 0)",borderWidth: 0,data: [0],}';
	$datasets []= '{label: "",backgroundColor: "rgba(181, 76, 51, 0)",borderColor: "rgba(181, 76, 51, 0)",borderWidth: 0,data: [0],}';


	$datasets []= '{label: "All - Buys",
									backgroundColor: "rgba(56, 150, 88, 1)",
									borderColor: "rgba(56, 150, 88, 1)",
									borderWidth: 1,
									data: ['. $b .'],}';

	$datasets []= '{label: "All - Sells",
									backgroundColor: "rgba(181, 76, 51, 1)",
									borderColor: "rgba(181, 76, 51, 1)",
									borderWidth: 1,
									data: ['. $s .'],}';


	// Bugfix
	$datasets []= '{label: "",backgroundColor: "rgba(181, 76, 51, 0)",borderColor: "rgba(181, 76, 51, 0)",borderWidth: 0,data: [0],}';


	$result_html =
		'<h4 class="sc_title">Aktuális vásárlások & eladások összesítése | [ '. $evaluation_result .' ]</h4>'.
		'<canvas id="x3v2gh1"></canvas>
		<script type="text/javascript">
				jQuery(function($) {
					$(window).load(function(){
						var ctx = document.getElementById("x3v2gh1").getContext("2d");
						var chart = new Chart(ctx, {
						    type: "bar",
						    data: {
						        labels: [],
										datasets: ['. implode(',', $datasets) .']
						    },
						    options: {
									responsive: true,
									legend: {
										display: false,
									}
								}
						});

					});
				});
				</script>';


	return $result_html;
}
