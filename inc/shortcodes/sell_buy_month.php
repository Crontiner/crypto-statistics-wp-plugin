<?php

add_shortcode('sell_buy_month', 'sell_buy_month_function');
function sell_buy_month_function() {
	global $wpdb;
	$result_html = "";
	if ( !is_user_logged_in() ) { return ""; }


	// --- CACHE ---

	$sell_buy_month_cache = get_option('crypto_sell_buy_month_cache');

	if ( !empty($sell_buy_month_cache) && isset($sell_buy_month_cache['timestamp']) &&
			(strtotime(current_time('mysql')) - $sell_buy_month_cache['timestamp'] <= SELL_BUY_MONTH_CACHE_REFRESH_TIME)
	 ) {
		 return $sell_buy_month_cache['html'];
	}

	// --- /CACHE ---


	$percent_changes_array = array();
	$days_array = array();
	$column_name = 'percent_change_24h';

	for($i=0; $i >= -(45 * 24); $i-=24) {
		$start = strval($i .' hours');
		$end = strval(($i - 24) .' hours');

		$start 	= date('Y-m-d H:i:s', strtotime( $start ) + 3600 );
		$end 		= date('Y-m-d H:i:s', strtotime( $end ) + 3600 );

		if ( $i == 0 ) { $start = date('Y-m-d H:i:s', strtotime('+1 day')); }

		$cmc_data = $wpdb->get_results( "SELECT AVG(`{$column_name}`) AS `{$column_name}`
																		FROM `{$wpdb->prefix}crypto_coinmarketcap_data`
																		WHERE `datetime` <= '". $start ."'
																			AND `datetime` > '". $end ."'
																			ORDER BY `datetime` DESC", ARRAY_A );

		$cmc_datetime = $wpdb->get_results( "SELECT `datetime`
																		FROM `{$wpdb->prefix}crypto_coinmarketcap_data`
																		WHERE `datetime` <= '". $start ."'
																			AND `datetime` > '". $end ."'
																			ORDER BY `datetime` DESC LIMIT 1", ARRAY_A );

		if (
				isset($cmc_data[0][$column_name]) && !empty($cmc_data[0][$column_name]) &&
				isset($cmc_datetime[0]['datetime']) && !empty($cmc_datetime[0]['datetime'])
	 		 ) {

			$average_val  = $cmc_data[0][$column_name];
			$datetime 		= strtotime($cmc_datetime[0]['datetime']);
			$t = $datetime;

			$days_array[] = '"'. date('m-d', $t). ' ('.date_i18n('D', $t).')' .'", ';
			$percent_changes_array[]= number_format($average_val,4);
		}
	}
	$days_array = array_reverse($days_array);

	$positives = array();
	$negatives = array();

	foreach ($percent_changes_array as $key => $percent_change) {
		$p = 0;
		$n = 0;

		if (strpos($percent_change, '-') !== false) { $n = $percent_change; }
		else { $p = $percent_change; }

		$positives []= $p;
		$negatives []= $n;
	}

	// Calculate averages
	$positives_avg = round(array_sum($positives) / count($positives),4);
	$negatives_avg = round(array_sum($negatives) / count($negatives),4);

	$positives_avg_array = array();
	for ($i=0; $i < count($positives); $i++) { $positives_avg_array []= $positives_avg; }

	$negatives_avg_array = array();
	for ($i=0; $i < count($negatives); $i++) { $negatives_avg_array []= $negatives_avg; }
	// ---


	$result_html =
		'<h4 class="sc_title">CMC napi %-os árváltozások <small>(utolsó 45 nap)</small></h4>'.
		'<canvas id="mklcads"></canvas>
		<script type="text/javascript">
		jQuery(function($) {
			$(window).load(function(){
				var ctx = document.getElementById("mklcads").getContext("2d");
				var chart = new Chart(ctx, {
				    type: "line",
				    data: {
				        labels: ['. implode('', $days_array) .'],
								datasets: [{
										steppedLine: false,
		                label: "Eladásnap",
		                backgroundColor: "rgba(72, 194, 113, 1)",
		                data: ['. implode(',', array_reverse($positives)) .']
		            }, {
										steppedLine: false,
		                label: "Eladási átlag",
		                backgroundColor: "rgba(72, 255, 113, 1)",
		                data: ['. implode(',', $positives_avg_array) .']
		            }, {
										steppedLine: false,
		                label: "Vételnap",
		                backgroundColor: "rgba(181, 76, 51, 1)",
		                data: ['. implode(',', array_reverse($negatives)) .']
		            }, {
										steppedLine: false,
		                label: "Vásárlási átlag",
		                backgroundColor: "rgba(255, 76, 51, 1)",
		                data: ['. implode(',', $negatives_avg_array) .']
		            }]
				    },
						options: {
							responsive: true,
							legend: {
								display: false,
							},
						}
				});

			});
		});
		</script>';

	unset($days_array, $positives, $positives_avg_array, $negatives, $negatives_avg_array);

	update_option('crypto_sell_buy_month_cache', array('html' => $result_html, 'timestamp' => strtotime(current_time('mysql'))), false);
	return $result_html;
}
