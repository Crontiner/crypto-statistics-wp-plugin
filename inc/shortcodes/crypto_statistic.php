<?php

/**********************/
/*    Shortcode       */
/**********************/

add_shortcode('crypto_statistic', 'crypto_statistic_function');
function crypto_statistic_function() {
	global $wpdb;
	$result_html = "";
	$crypto_shitcoins_array = get_option('crypto_shitcoins');
	$utolso_osszeallitas = array();
	if ( !is_user_logged_in() ) { return ""; }


	// kényszerfrissítés
	if ( isset($_GET['compilation_update']) ) {
		create_crypto_statistic_function();
	}

	$crypto_compilation = $wpdb->get_results( "SELECT * FROM `cryp_crypto_compilation` ORDER BY `timestamp` DESC LIMIT 150", ARRAY_A );

	$coins_stat_array = array();
	foreach ($crypto_compilation as $key => $coin_data) {
		$coin_data_array = json_decode($coin_data['data'], true);
		$coins_stat_array[$coin_data['coin_name']] = $coin_data_array;
	}


	// sorbarendezés utolsó rank státusz alapján
	$ranks_for_sort = array();
	foreach ($coins_stat_array as $key => $value) {
		$ranks_for_sort []= $value['last_rank'];
	}
	array_multisort($ranks_for_sort, SORT_ASC, $coins_stat_array);

	// echo "<pre>"; var_dump($coins_stat_array); echo "</pre>";

	// lekérdezi, hogy az adott coin létrehozási dátumát és a logót
	$bittrex_getmarkets = file_get_contents( 'https://bittrex.com/api/v1.1/public/getmarkets' );
	$bittrex_getmarkets = json_decode($bittrex_getmarkets);

	$coins_extension_infos_array = array();
	foreach ($bittrex_getmarkets->result as $key => $coin) {
		if ( $coin->BaseCurrency == 'BTC' ) {
			$coins_extension_infos_array[$coin->MarketCurrency]= array(
																																	'created_date' => strtotime($coin->Created),
																																	'logo' => $coin->LogoUrl,
																																	'link' => 'https://coinmarketcap.com/currencies/'. strtolower($coin->MarketCurrencyLong) .'/',
																																);
		}
	}


	$tr = "";
	$tr .=
		'<tr>
			<th>Rank</th>
			<th>Logó</th>
			<th>Coin name</th>
			<th>Last price in BTC</th>
			<th>Átlag BTC ár</th>
			<th>Átlag ár tartomány</th>
			<th>Ranks</th>
			<th>Értékelés</th>
			<th>Lekérve</th>
			<th>Összeállítva</th>
			<th>Kiértékelt adatok</th>
			<th>Legrégebbi adat</th>
			<th>Létrehozva</th>
		</tr>';

	$ertekelesek_array = array();
	foreach ($coins_stat_array as $coin_name => $coin_data) {
		$ertekelesek_array []= $coin_data['ertekeles']['status'];

		$coinname = $coin_name;
		if ( $coin_name == 'BCH' ) { $coinname = 'BCC'; }

		$coin_logo = "";
		if ( isset($coins_extension_infos_array[$coinname]['logo']) ) {
			$coin_logo = $coins_extension_infos_array[$coinname]['logo'];
			$coin_logo = '<img src="'. $coin_logo .'" class="logo" />';
		}

		$created_date = "";
		$created_date_class = "";
		if ( isset($coins_extension_infos_array[$coinname]['created_date']) ) {
			$created_date = date('Y-m', $coins_extension_infos_array[$coinname]['created_date']);

			// 2 évnél régebbi pénzek megjelölése

			if ( (strtotime(current_time('mysql')) - $coins_extension_infos_array[$coinname]['created_date']) >= 63113851 ) {
				$created_date_class = "veteran_coin";
			}
		}

		$link = "#";
		if ( isset($coins_extension_infos_array[$coinname]['link']) ) {
			$link = $coins_extension_infos_array[$coinname]['link'];
		}

		// bugfix
		$crypto_compilation = $wpdb->get_results( "SELECT `coin_name`, `timestamp` FROM `cryp_crypto_compilation` WHERE `coin_name` = '{$coin_name}' ORDER BY `timestamp` DESC LIMIT 1", ARRAY_A );
		$utolso_osszeallitas []= strtotime($crypto_compilation[0]['timestamp']);

		$shitcoin_class = "";
		if ( in_array($coin_name, $crypto_shitcoins_array) ) { $shitcoin_class = "shitcoin"; }

		$tr .=
			'<tr class="'. $coin_data['ertekeles']['status'] .' '. $created_date_class .' '. $shitcoin_class .' ">
				<td>'. $coin_data['last_rank'] .'</td>
				<td>'. $coin_logo .'</td>
				<td><a target="_blank" href="'. $link .'">'. $coin_name .'</a></td>
				<td class="'. $coin_data['price_data_from'] .'">'. $coin_data['last_price_in_btc'] .'</td>
				<td>'. ar_visszaallitas($coin_data['last_price_in_btc'], $coin_data['atlag_btc_ar']) .'</td>
				<td>'. ar_visszaallitas($coin_data['last_price_in_btc'], $coin_data['tartomany_start']) .' &harr; '. ar_visszaallitas($coin_data['last_price_in_btc'], $coin_data['tartomany_end']) .'</td>
				<td><small>'. $coin_data['ranks'] .'</small></td>
				<td>'. $coin_data['ertekeles']['msg'] .'</td>
				<td>'. timeAgo(strtotime($coin_data['last_update'])) .'</td>
				<td>'. timeAgo(strtotime($crypto_compilation[0]['timestamp'])) .'</td>
				<td>'. intval($coin_data['kiertekelt_adatok']) .' db</td>
				<td class="oldest_data">'. $coin_data['oldest_data'] .' <br><small><b>'. timeAgo(strtotime($coin_data['oldest_data'])) .'</b></small></td>
				<td class="created_date">'. $created_date .' <br><small><b>'. timeAgo(strtotime($created_date .'-01 00:00:00')) .'</b></small></td>
			</tr>';

		$tr .=
			'<tr class="more_info '. $coin_data['ertekeles']['status'] .' '. $created_date_class .' '. $shitcoin_class .' '. if_empty_add_hidden_class($coin_data['mining_block_time']) .' ">
				<td colspan="13">

					<div class="col-sm-4 bittrex-datas">
						<div class="row">
							<table class="table table-bordered">
								<tr>
									<th colspan="3">Bittrex adatok</th>
								</tr>
								<tr>
									<th>Last price in BTC</th>
									<th>Átlag BTC ár</th>
									<th>Átlag ár tartomány</th>
								</tr>
								<tr>
									<td>...</td>
									<td>...</td>
									<td>...</td>
								</tr>
							</table>
						</div>
					</div>

					<div class="col-sm-8 miner-datas '. if_empty_add_hidden_class($coin_data['mining_block_time']) .' ">
						<div class="row">
							<table class="table table-bordered">
								<tr>
									<th colspan="6">Bányászati adatok</th>
								</tr>
								<tr>
									<th>Block time</th>
									<th>Diff</th>
									<th>Nethash</th>
									<th>Estimated rewards</th>
									<th>BTC revenue</th>
									<th>Profitability</th>
								</tr>
								<tr>
									<td>'. $coin_data['mining_block_time'] .'</td>
									<td>'. $coin_data['mining_diff'] .'</td>
									<td>'. $coin_data['mining_nethash'] .'</td>
									<td>'. $coin_data['mining_estimated_rewards'] .'</td>
									<td>'. $coin_data['mining_btc_revenue'] .'</td>
									<td>'. $coin_data['mining_profitability'] .'</td>
								</tr>
							</table>
						</div>
					</div>

				</td>
			</tr>';
	}
	asort($utolso_osszeallitas);


	/**************************/
	/*   HTML összeállítás    */
	/**************************/

	$result_html .= '<div class="col-xs-10">'.
										'<div class="row">'.
											'DB Idő: '. date('H:i:s', strtotime(current_time('mysql'))).
											'<div class="clearfix"></div>'.

											'Utolsó összeállítás: '. timeAgo(end($utolso_osszeallitas)).
											'<input type="hidden" value="'. end($utolso_osszeallitas) .'" id="last_compilation_update" />'.
											'<div class="clearfix"></div>'.

											'Értékelések: '. napi_ertekeles($ertekelesek_array).
											'<div class="clearfix"></div><br>'.
											'Shitcoins: <p>'. implode(', ', $crypto_shitcoins_array) .'</p>'.
											'<div class="clearfix"></div><br>'.

										'</div>'.
									'</div>';

	$result_html .= '<div class="col-xs-1 col-xs-offset-1">'.
										'<div class="row">'.
											check_buy_or_sell("").
										'</div>'.
									'</div>';

	$result_html .= daily_sales_and_purchase_conditions_v2();
	$result_html .= '<div class="clearfix"></div><br>';

	$result_html .= last_month_minings_diagrams();
	$result_html .= '<div class="clearfix"></div><br>';

	$result_html .= '<table class="table table-bordered">'. $tr .'</table>';
	$result_html .= '<div class="clearfix"></div><br>';

	//$result_html .= '<a class="btn" style="background: grey !important; border: none;" href="'. $_SERVER['server_name'] .'/?compilation_update=true' .'">Frissítés</a>';
	//$result_html .= '<div class="clearfix"></div><br>';

	return $result_html;
}
