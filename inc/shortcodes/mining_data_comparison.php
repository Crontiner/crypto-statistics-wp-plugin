<?php

add_shortcode('mining_data_comparison', 'mining_data_comparison_sc_function');
function mining_data_comparison_sc_function($update = "") {
	$html_result = "";
	update_option( 'crypto_mining_data_comparison_calculated_ranks', "", false );

	$nethash = get_mining_data_comparison('nethash');
	$difficult = get_mining_data_comparison('difficult');
	$profitability = get_mining_data_comparison('profitability');
	$block_time = get_mining_data_comparison('block_time');
	$estimated_rewards = get_mining_data_comparison('estimated_rewards');
	$btc_revenue = get_mining_data_comparison('btc_revenue');

	$coins_array = array();
	foreach ($nethash as $key => $v) 						{ $coins_array [$v['coin_ID']]['nethash'] = $v; }
	foreach ($difficult as $key => $v) 					{ $coins_array [$v['coin_ID']]['difficult'] = $v; }
	foreach ($profitability as $key => $v) 			{ $coins_array [$v['coin_ID']]['profitability'] = $v; }
	foreach ($block_time as $key => $v) 				{ $coins_array [$v['coin_ID']]['block_time'] = $v; }
	foreach ($estimated_rewards as $key => $v) 	{ $coins_array [$v['coin_ID']]['estimated_rewards'] = $v; }
	foreach ($btc_revenue as $key => $v) 				{ $coins_array [$v['coin_ID']]['btc_revenue'] = $v; }

	unset($nethash, $difficult, $profitability, $block_time, $estimated_rewards, $btc_revenue);
	if ( empty($coins_array) ) { return ""; }

	$tr = "";
	$calculated_ranks = array();
	foreach ($coins_array as $coin_ID => $val) {

		$coin_name = get_coin_name_by_id($coin_ID);
		$coin_full_name = get_coin_full_name_by_id($coin_ID);

		if ( $coin_name != $coin_full_name && !empty($coin_full_name) ) { $coin_name .= ' <small>('. $coin_full_name .')</small>'; }

		//$rank = ($val['nethash']['percent'] + $val['profitability']['percent'] + $val['estimated_rewards']['percent'] + $val['btc_revenue']['percent']) - ($val['difficult']['percent'] + $val['block_time']['percent']);
		$rank = ($val['nethash']['percent'] + $val['profitability']['percent']) - ($val['difficult']['percent']);

		$class = "";
		if ( $rank > 0 ) { $class = "top_coin"; }
		$calculated_ranks[get_coin_name_by_id($coin_ID)]= $rank;

		$tr .= '<tr class="'. $class .'">
							 <td><a href="https://www.coinwarz.com/cryptocurrency/coins/'. sanitize_title(get_coin_full_name_by_id($coin_ID)) .'" target="_blank">'. $coin_name .'</a></td>
							 <td>'. round($rank,1) .'</td>
							 <td>'. round($val['nethash']['percent'],1) .'</td>
							 <td>'. round($val['difficult']['percent'],1) .'</td>
							 <td>'. round($val['profitability']['percent'],1) .'</td>
							 <td>'. round($val['block_time']['percent'],1) .'</td>
							 <td>'. round($val['estimated_rewards']['percent'],1) .'</td>
							 <td>'. round($val['btc_revenue']['percent'],1) .'</td>
						</tr>';
	}
	unset($coins_array);
	arsort($calculated_ranks);

	update_option( 'crypto_mining_data_comparison_calculated_ranks', $calculated_ranks, false );
	if ( strtolower($update) == "update" ) { return ""; }

	$html_result .=
		'<table id="mining_data_comparison_table" class="table">
				 <thead>
						<tr>
							 <th>Coin Name</th>
							 <th data-sort-default="">Calculated Rank</th>
							 <th>Nethash</th>
							 <th>Difficult</th>
							 <th>Profitability</th>
							 <th>Blocktime</th>
							 <th>Estimated Rewards</th>
							 <th>BTC Revenue</th>
						</tr>
				 </thead>
				 <tbody>
						'. $tr .'
				 </tbody>
			</table>';

	return '<div class="mining_data_comparison_sc">'.
						'<h4 class="sc_title">Mining Comparison <small>- (last 7 days in percents)</small></h4>'.
						$html_result.
					'</div>';
}
