<?php

add_shortcode('market_changes_graph', 'market_changes_graph_sc_function');
function market_changes_graph_sc_function() {
	$html_result = "";

	$markets_data = array();

	$buy_or_sell = get_option( 'crypto_buy_or_sell_'. date('Y-m') );
	$buy_or_sell_last_month = get_option( 'crypto_buy_or_sell_'. date('Y-m', strtotime('first day of last month')) );

	if ( !empty($buy_or_sell) ) {
		foreach ($buy_or_sell as $key => $val) { $markets_data [] = $val; }
	}
	if ( !empty($buy_or_sell_last_month) ) {
		foreach ($buy_or_sell_last_month as $key => $val) { $markets_data [] = $val; }
	}
	unset($buy_or_sell, $buy_or_sell_last_month);

	array_sort_by_column($markets_data, '1');

	$positives = array();
	$positives_int = array();
	$dates_array = array();
	$start_timestamp = strtotime('-1 month');

	$positives2 = array();
	$positives2_int = array();
	$dates_array2 = array();
	$start_timestamp2 = strtotime('-7 days');

	foreach ($markets_data as $key => $val) {
		if ( isset($val[1]) && ($val[1] > $start_timestamp) && isset($val['pos']) && ($val['pos'] >= 0) ) {
			$dates_array []= '"'. date('m-d H:i', $val[1]) . ' ('. date_i18n('D', $val[1]) .')' .'"';
			$positives []= '"'. $val['pos'] .'"';
			$positives_int []= $val['pos'];
		}

		if ( isset($val[1]) && ($val[1] > $start_timestamp2) && isset($val['pos']) && ($val['pos'] >= 0) ) {
			$dates_array2 []= '"'. date('m-d H:i', $val[1]) . ' ('. date_i18n('D', $val[1]) .')' .'"';
			$positives2 []= '"'. $val['pos'] .'"';
			$positives2_int []= $val['pos'];
		}
	}


	// Calculate averages
	$positives_avg 	= round(array_sum($positives_int) / count($positives_int),2);
	$positives2_avg = round(array_sum($positives2_int) / count($positives2_int),2);

	$positives_avg_array = array();
	$positives2_avg_array = array();

	for ($i=0; $i < count($positives); $i++) { $positives_avg_array []= $positives_avg; }
	for ($i=0; $i < count($positives2); $i++) { $positives2_avg_array []= $positives2_avg; }
	// ---


	$html_result =
		'<h4 class="sc_title">Market Changes Graph <small>- last 1 month</small></h4>'.
		'<canvas id="vk43h2"></canvas>
		<script type="text/javascript">
		jQuery(function($) {
			$(window).load(function(){
				var ctx = document.getElementById("vk43h2").getContext("2d");
				var chart = new Chart(ctx, {
				    type: "line",
				    data: {
				        labels: ['. implode(',', $dates_array) .'],
								datasets: [{
										steppedLine: false,
		                label: "",
		                backgroundColor: "rgba(72, 194, 113, 1)",
		                data: ['. implode(',', $positives) .']
		            }, {
										steppedLine: false,
		                label: "AVG",
		                backgroundColor: "rgba(72, 255, 113, 1)",
		                data: ['. implode(',', $positives_avg_array) .']
		            },]
				    },
						options: {
							responsive: true,
							legend: {
								display: false,
							}
						}
				});

			});
		});
		</script>';

		$html_result .= '<div class="clearfix"></div>';

		$html_result .=
			'<h4 class="sc_title">Market Changes Graph <small>- last 1 week</small></h4>'.
			'<canvas id="vk43h23"></canvas>
			<script type="text/javascript">
			jQuery(function($) {
				$(window).load(function(){
					var ctx = document.getElementById("vk43h23").getContext("2d");
					var chart = new Chart(ctx, {
					    type: "line",
					    data: {
					        labels: ['. implode(',', $dates_array2) .'],
									datasets: [{
											steppedLine: false,
			                label: "",
			                backgroundColor: "rgba(72, 194, 113, 1)",
			                data: ['. implode(',', $positives2) .']
			            },{
											steppedLine: false,
			                label: "AVG",
			                backgroundColor: "rgba(72, 255, 113, 1)",
			                data: ['. implode(',', $positives2_avg_array) .']
			            },]
					    },
							options: {
								responsive: true,
								legend: {
									display: false,
								}
							}
					});

				});
			});
			</script>';

	return '<div class="market_changes_graph_sc">'. $html_result .'</div>';
}
