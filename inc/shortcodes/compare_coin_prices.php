<?php

add_shortcode('compare_coin_prices', 'compare_coin_prices_sc_function');
function compare_coin_prices_sc_function() {
	global $wpdb;
	$result_html = "";
	if ( !is_user_logged_in() ) { return ""; }

	$bittrex_coins = get_bittrex_coins();

	$coins_array = array();
	foreach ($bittrex_coins as $key => $coin_id) {
		$coin_name = get_coin_name_by_id($coin_id);

		if ( check_coin_is_banned($coin_name) === false ) {
			$coins_array []= array(
															'coin_id' => $coin_id,
															'coin_name' => $coin_name,
															'last_btc_price' => number_format(get_bittrex_last_price($coin_id),8),
															'mineable' => check_coin_is_mineable($coin_id),
														);
		}
	}

	array_sort_by_column($coins_array, 'last_btc_price', SORT_DESC);

	$datasets = array();
	foreach ($coins_array as $key => $coin_data) {
		$n = rand(120,220);
		$rgba = 'rgba('.$n.', '.$n.', '.$n.', 1)';
		$rgba2 = 'rgba('.$n.', '.$n.', '.$n.', 1)';

		if ( $coin_data['mineable'] === true ) {
			$rgba = 'rgba(56,150,88,1)';
			$rgba2 = $rgba;
		}

		$datasets []= '{label: "'. $coin_data['coin_name'] .' - ('. number_format( ($coin_data['last_btc_price'] * 100) / 1 ,2) .'%)",
										backgroundColor: "'.$rgba.'",
										borderColor: "'.$rgba2.'",
										borderWidth: 1,
										data: ['. $coin_data['last_btc_price'] .'],}';
	}


	// Bugfix
	$datasets []= '{label: "",backgroundColor: "rgba(181, 76, 51, 0)",borderColor: "rgba(181, 76, 51, 0)",borderWidth: 0,data: [0],}';


	$result_html =
		'<h4 class="sc_title">Árak összehasonlítása a BTC-hez képest <small>(A zöldek bányászható coinok!)</small></h4>'.
		'<canvas id="b4hk32j"></canvas>
		<script type="text/javascript">
				jQuery(function($) {
					$(window).load(function(){
						var ctx = document.getElementById("b4hk32j").getContext("2d");
						var chart = new Chart(ctx, {
						    type: "bar",
						    data: {
						        labels: [],
										datasets: ['. implode(',', $datasets) .']
						    },
						    options: { responsive: true, }
						});

					});
				});
				</script>';

	return $result_html;
}
