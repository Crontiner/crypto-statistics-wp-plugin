<?php

add_shortcode('must_sell_and_buy_coins_from_compilation', 'must_sell_and_buy_coins_from_compilation_sc_function');
function must_sell_and_buy_coins_from_compilation_sc_function() {
	global $wpdb;
	$return_html = "";
	if ( !is_user_logged_in() ) { return ""; }

	$res = get_must_sell_and_buy_coins_from_compilation();

	$must_buys = array();
	$buys = array();
	$must_sells = array();
	$sells = array();

	if ( !empty($res['must_buys']) ) {
		foreach ($res['must_buys'] as $key => $coin_ID) {
			$coin_name = get_coin_name_by_id($coin_ID);
			$must_buys []= '<li><a href="'. get_permalink( PAGE_OSSZEALLITASOK ) .'#'. strtolower($coin_name) .'">'. $coin_name .'</a></li>';
		}
	}
	if ( !empty($res['buys']) ) {
		foreach ($res['buys'] as $key => $coin_ID) {
			$coin_name = get_coin_name_by_id($coin_ID);
			$buys []= '<li><a href="'. get_permalink( PAGE_OSSZEALLITASOK ) .'#'. strtolower($coin_name) .'">'. $coin_name .'</a></li>';
		}
	}
	if ( !empty($res['must_sells']) ) {
		foreach ($res['must_sells'] as $key => $coin_ID) {
			$coin_name = get_coin_name_by_id($coin_ID);
			$must_sells []= '<li><a href="'. get_permalink( PAGE_OSSZEALLITASOK ) .'#'. strtolower($coin_name) .'">'. $coin_name .'</a></li>';
		}
	}
	if ( !empty($res['sells']) ) {
		foreach ($res['sells'] as $key => $coin_ID) {
			$coin_name = get_coin_name_by_id($coin_ID);
			$sells []= '<li><a href="'. get_permalink( PAGE_OSSZEALLITASOK ) .'#'. strtolower($coin_name) .'">'. $coin_name .'</a></li>';
		}
	}

	$return_html =
			'<h4 class="sc_title">Nagyobb árváltozáson átesett coinok</h4>
			<div class="must_sell_and_buy_coins_from_compilation">

				<div class="conditions">
					<h6>Conditions:</h6>
					<ul>
						<li>min 18 month</li>
						<li>min 2 events</li>
						<li>min 2 exchange</li>
						<li>in top 25 most popular</li>
					</ul>
				</div>
				<div class="clearfix"></div>

				<div class="col-sm-6">';

				if ( !empty($must_buys) ) {
					$return_html .=
									'<h4>Must Buys:</h4>
									 <ul>'. implode('', $must_buys) .'</ul>';
				}

				if ( !empty($buys) ) {
					$return_html .=
									'<h4>Buys:</h4>
									 <ul>'. implode('', $buys) .'</ul>';
				}

	$return_html .= '
				</div>
				<div class="col-sm-6">';

				if ( !empty($must_sells) ) {
					$return_html .=
								'<h4>Must Sells:</h4>
								 <ul>'. implode('', $must_sells) .'</ul>';
				}

				if ( !empty($sells) ) {
					$return_html .=
						'<h4>Sells:</h4>
						 <ul>'. implode('', $sells) .'</ul>';
				}

	$return_html .= '
				</div>
			</div>';


	return $return_html;
}
