<?php

add_shortcode('own_investments_round_diagram', 'own_investments_round_diagram_sc_function');
function own_investments_round_diagram_sc_function() {
	global $wpdb;
	$result_html = "";
	if ( !is_user_logged_in() ) { return ""; }

	$investments_cpt = new WP_Query(array( 'post_type' => 'befektetesek_cpt', 'fields' => 'ids', 'posts_per_page' => -1 ));
	$investments_posts_array = $investments_cpt->posts;
	$investments_array = array();
	$aggregated_investments_array = array(); // separated coins
	$aggregated_investments_array2 = array(); // all coins in one

	foreach ($investments_posts_array as $key => $post_id) {

		$coin_symbol = get_post_meta($post_id, 'coin_symbol_meta', true);
		$amount = get_post_meta($post_id, 'amount_meta', true);

		$coin_id = get_coin_id_by_name($coin_symbol);
		$actual_price = get_actual_cmc_btc_price($coin_id);

		if ( ($coin_id > 0) && !empty($actual_price) ) {
			$amount_in_btc = (float) number_format($actual_price / (1 / $amount),8);

			$investments_array []= array(
																		'post_id' => $post_id,
																		'coin_id' => $coin_id,
																		'coin_symbol' => $coin_symbol,
																		'actual_price' => $actual_price,
																		'amount' => $amount,
																		'amount_in_btc' => $amount_in_btc,
																	);

			$aggregated_investments_array [$coin_symbol][]= $amount_in_btc;
			$aggregated_investments_array2[]= $amount_in_btc;
		}
	}


	$all_investments_summed_val = array_sum($aggregated_investments_array2);

	foreach ($aggregated_investments_array as $coin_name => $amounts_array) {
		$aggregated_investments_array[$coin_name] = array_sum($amounts_array);
	}


	$datas_array = array();
	$colors_array = array();
	$labels_array = array();

	arsort($aggregated_investments_array);

	foreach ($aggregated_investments_array as $coin_name => $amount) {
		$percent = number_format(($amount * 100) / $all_investments_summed_val,2);

		$datas_array 	[]= $amount;
		$colors_array []= '"rgb('.rand(0,255).', '.rand(0,255).', '.rand(0,255).')"';
		$labels_array []= '"'. $percent .'% - '. $coin_name .' - '. change_amount_from_btc($amount, 'HUF') .'"';
	}


	$result_html =
		'<h4 class="sc_title">Saját befektetések <small>- '. count($aggregated_investments_array) .' db | Összesen: '. $all_investments_summed_val .' BTC → '. change_amount_from_btc($all_investments_summed_val, 'HUF') .'</small></h4>'.
		'<canvas id="vcg4b3h2j"></canvas>
		<script type="text/javascript">
				jQuery(function($) {
					$(window).load(function(){

						var config = {
							type: "pie",
							data: {
								datasets: [{
									data: ['. implode(',', $datas_array) .'],
									backgroundColor: ['. implode(',', $colors_array) .'],
									label: "Dataset 1"
								}],
								labels: ['. implode(',', $labels_array) .']
							},
							options: {
								responsive: true,
								legend: {
													position: "right",
												},
							}
						};

						var ctx = document.getElementById("vcg4b3h2j").getContext("2d");
						new Chart(ctx, config);

					});
				});
				</script>';


	return $result_html;
}
