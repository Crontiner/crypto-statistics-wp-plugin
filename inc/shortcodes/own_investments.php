<?php

add_shortcode('own_investments', 'own_investments_sc_function');
function own_investments_sc_function($update = "") {
	global $wpdb;
	$result_html = "";
	if ( !is_user_logged_in() ) { return ""; }

	$investments_cpt = new WP_Query(array( 'post_type' => 'befektetesek_cpt', 'fields' => 'ids', 'posts_per_page' => -1 ));
	$investments_posts_array = $investments_cpt->posts;
	$investments_array = array();
	$profits_in_percent = array();

	foreach ($investments_posts_array as $key => $post_id) {

		$coin_symbol = get_post_meta($post_id, 'coin_symbol_meta', true);
		$btc_exchange_rate = get_post_meta($post_id, 'btc_exchange_rate_meta', true);
		$amount = get_post_meta($post_id, 'amount_meta', true);

		$coin_id = get_coin_id_by_name($coin_symbol);
		$actual_price = get_actual_cmc_btc_price($coin_id);

		if ( ($coin_id > 0) && !empty($actual_price) && ($coin_id != BTC_ID) ) {
			$amount_in_btc = (float) number_format($actual_price / (1 / $amount),8);
			$percent = number_format( (( $actual_price * 100 ) / $btc_exchange_rate) - 100 , 2);

			$investments_array []= array(
																		'post_id' => $post_id,
																		'coin_id' => $coin_id,
																		'coin_symbol' => $coin_symbol,
																		'btc_exchange_rate' => $btc_exchange_rate,
																		'amount' => $amount,
																		'profit_in_percent' => $percent,
																		'actual_price' => $actual_price,
																		'amount_in_btc' => $amount_in_btc,
																	);

			$profits_in_percent []= $percent;
		}
	}

	$profits_in_percent = array_sum($profits_in_percent);
	$investments_array = array_reverse($investments_array);

	$datasets = array();

	if ( !empty($investments_array) ) {
		update_option('crypto_own_investments', json_encode(array( 'investments' => $investments_array, 'timestamp' => strtotime('now') )), false);
	}

	if ( strtolower($update) == 'update' ) { return ""; }

	foreach ($investments_array as $key => $val) {

		$rgba = "rgba(56, 150, 88, 1)";
		if ( $val['profit_in_percent'] < 0 ) { $rgba = "rgba(204, 35, 0, 1)"; }

		$datasets []= '{label: "'. $val['coin_symbol'] .' ( '. $val['profit_in_percent'] .'% )",
										backgroundColor: "'. $rgba .'",
										borderColor: "'. $rgba .'",
										borderWidth: 1,
										data: ['. $val['profit_in_percent'] .'],}';
	}


	// Bugfix
	$datasets []= '{label: "",backgroundColor: "rgba(181, 76, 51, 0)",borderColor: "rgba(181, 76, 51, 0)",borderWidth: 0,data: [0],}';


	$result_html =
		'<h4 class="sc_title">Saját befektetések <small>- '. count($investments_array) .' db | Profitok: '. $profits_in_percent .'%</small></h4>'.
		'<canvas id="v4hj12v"></canvas>
		<script type="text/javascript">
				jQuery(function($) {
					$(window).load(function(){
						var ctx = document.getElementById("v4hj12v").getContext("2d");
						var chart = new Chart(ctx, {
						    type: "bar",
						    data: {
						        labels: [],
										datasets: ['. implode(',', $datasets) .']
						    },
						    options: {
									responsive: true,
									legend: {
														position: "right",
													},
								}
						});

					});
				});
				</script>';

	return $result_html;
}
