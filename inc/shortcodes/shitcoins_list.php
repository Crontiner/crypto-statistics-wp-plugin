<?php
add_shortcode('shitcoins_list', 'shitcoins_list_function');
function shitcoins_list_function() {
	global $wpdb;
	$return_html = "";
	if ( !is_user_logged_in() ) { return ""; }

	$shitcoins = $wpdb->get_results( "SELECT DISTINCT `shitcoin_name`
																		FROM `{$wpdb->prefix}crypto_shitcoins`
																		WHERE `deleted` = 0
																		ORDER BY `shitcoin_name` ASC", ARRAY_A );

	$deleted_shitcoins = $wpdb->get_results( "SELECT DISTINCT `shitcoin_name`
																		FROM `{$wpdb->prefix}crypto_shitcoins`
																		WHERE `deleted` = 1
																		ORDER BY `shitcoin_name` ASC", ARRAY_A );

	$shitcoins_temp = array();
	foreach ($shitcoins as $key => $value) {
		if ( check_coin_is_banned($value['shitcoin_name']) === FALSE ) {
			$shitcoins_temp []= $value['shitcoin_name'];
		}
	}

	$deleted_shitcoins_temp = array();
	foreach ($deleted_shitcoins as $key => $value) { $deleted_shitcoins_temp []= $value['shitcoin_name']; }

	return
			'<div class="row">
				<div class="col-xs-6">
					<div id="shitcoins_list">
					<h4 class="sc_title">New Shitcoins <small>- '. count($shitcoins_temp) .' db</small></h4>
					<textarea readonly class="form-control">'. implode(', ', $shitcoins_temp) .'</textarea>
					</div>
				</div>

				<div class="col-xs-6">
					<div id="shitcoins_list">
					<h4 class="sc_title">Deleted Shitcoins <small>- '. count($deleted_shitcoins_temp) .' db</small></h4>
					<textarea readonly class="form-control">'. implode(', ', $deleted_shitcoins_temp) .'</textarea>
					</div>
				</div>
			</div>';
}
