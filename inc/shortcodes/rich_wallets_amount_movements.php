<?php

add_shortcode('rich_wallets_amount_movements', 'rich_wallets_amount_movements_function');
function rich_wallets_amount_movements_function($atts, $content = null) {
	extract(shortcode_atts(array(
		'coin_symbol' => "",
	), $atts));

	if ( empty($coin_symbol) ) { return ""; }
	$coin_ID = get_coin_id_by_name($coin_symbol);
	if ( empty($coin_ID) ) { return ""; }

	global $wpdb;
	$return_html = "";
	$canvas_id = 'c'. wp_generate_password(6, false, false);
	if ( !is_user_logged_in() ) { return ""; }


	// --- Get last updated datetime ---
	$last_updated_datetime = $wpdb->get_results( "SELECT `last_updated_datetime`
																								FROM `{$wpdb->prefix}crypto_bitinfocharts_rich_wallets`
																								ORDER BY `last_updated_datetime` DESC
																								LIMIT 1", ARRAY_A );
	if ( isset($last_updated_datetime[0]['last_updated_datetime']) ) {
		$last_updated_datetime = strtotime($last_updated_datetime[0]['last_updated_datetime']);
	} else {
		$last_updated_datetime = strtotime(current_time('mysql'));
	}
	// ---


	// --- CACHE ---

	$wallets_amount_movements_cache = get_option('rich_wallets_amount_movements_cache_'. $coin_symbol);

	if ( !empty($wallets_amount_movements_cache) && isset($wallets_amount_movements_cache['timestamp']) &&
			($last_updated_datetime >= $wallets_amount_movements_cache['timestamp'])
	 ) {
		 //return $wallets_amount_movements_cache['html'];
	}

	// --- /CACHE ---


	$sql =
				"SELECT AVG(`{$wpdb->prefix}crypto_bitinfocharts_rich_wallets_datas`.`changed_amount`) AS `changed_amount_avg`,
							COUNT(`{$wpdb->prefix}crypto_bitinfocharts_rich_wallets_datas`.`wallet_ID`) AS `rows_count`,
							`{$wpdb->prefix}crypto_bitinfocharts_rich_wallets_datas`.`wallet_ID`,
							`{$wpdb->prefix}crypto_bitinfocharts_rich_wallets_datas`.`datetime`

				FROM `{$wpdb->prefix}crypto_bitinfocharts_rich_wallets_datas`
				LEFT JOIN `{$wpdb->prefix}crypto_bitinfocharts_rich_wallets`
							ON 	`{$wpdb->prefix}crypto_bitinfocharts_rich_wallets`.`ID` = `{$wpdb->prefix}crypto_bitinfocharts_rich_wallets_datas`.`wallet_ID`

				WHERE `{$wpdb->prefix}crypto_bitinfocharts_rich_wallets`.`coin_ID` = '". $coin_ID ."'
					AND `{$wpdb->prefix}crypto_bitinfocharts_rich_wallets_datas`.`datetime` >= '". date('Y-m-d H:i:s', strtotime('-24 hours')) ."'
					AND `{$wpdb->prefix}crypto_bitinfocharts_rich_wallets_datas`.`changed_amount` IS NOT NULL
					AND `{$wpdb->prefix}crypto_bitinfocharts_rich_wallets_datas`.`changed_amount` != ''
					AND `{$wpdb->prefix}crypto_bitinfocharts_rich_wallets`.`last_updated_datetime` >= '". date('Y-m-d H:i:s', strtotime("-4 days")) ."'

				GROUP BY 	`{$wpdb->prefix}crypto_bitinfocharts_rich_wallets_datas`.`wallet_ID`
				ORDER BY 	`{$wpdb->prefix}crypto_bitinfocharts_rich_wallets_datas`.`actual_amount` DESC
				LIMIT 400";
	$last_changes = $wpdb->get_results( $sql, ARRAY_A );


	if ( !empty($last_changes) ) {
		foreach ($last_changes as $key => $val) {
			if ( intval($val['wallet_ID']) > 0 ) {
				$actual_amount = $wpdb->get_results("SELECT `actual_amount` FROM `{$wpdb->prefix}crypto_bitinfocharts_rich_wallets_datas`
																						WHERE `wallet_ID` = '". $val['wallet_ID'] ."'
																						ORDER BY `datetime` DESC
																						LIMIT 1 ", ARRAY_A );
				$last_changes[$key]['actual_amount'] = $actual_amount[0]['actual_amount'];
			}
		}
	}

	$datasets = array();
	$p = array();
	$n = array();

	array_sort_by_column($last_changes, 'changed_amount_avg');

	foreach ($last_changes as $key => $data) {

		if ( $coin_symbol == 'BTC' ) {
			$changed_amount_avg = floatval(number_format($data['changed_amount_avg'],4));
		} else {
			$changed_amount_avg = floatval($data['changed_amount_avg']);
		}

		if ( $changed_amount_avg <= -14 || $changed_amount_avg >= 14 ) {
			$rows_count = $data['rows_count'];
			$wallet_ID = $data['wallet_ID'];
			$datetime = $data['datetime'];
			$actual_amount = $data['actual_amount'];

			if ( !empty($changed_amount_avg) ) {
				if ( $changed_amount_avg > 0 ) {
					$coin_color = 'rgba(56, 150, 88, 1)';
					$p []= $changed_amount_avg;
				} else {
					$coin_color = 'rgba(181, 76, 51, 1)';
					$n []= $changed_amount_avg;
				}

				$datasets []=
											'{
												label: "wID:'. $wallet_ID .' - (R:'. $rows_count .')",
												backgroundColor: "'. $coin_color .'",
												borderColor: "'. $coin_color .'",
												borderWidth: 3,
												data: ['. $changed_amount_avg .'],
											}';
			}
		}
	}

	$percentChange = percent_between_two_numbers(abs(array_sum($n)), array_sum($p));
	$percentChange = number_format($percentChange,2);

	$result_html =
		'<h4 class="sc_title">'. $coin_symbol .' pénzmozgások az elmúlt 24 órában
				<small>- utolsó összeállítás: <b>'. timeAgo($last_updated_datetime) .'</b> </small>
				<small>- '. count($datasets) .' db</small>
				<small>- Positive: '. count($p) .' db - Sum: '. array_sum($p) .' | Negative: '. count($n) .' db - Sum: '. abs(array_sum($n)) .' | Diff. Percent: '. $percentChange .'%</small>
		</h4>'.
		'<canvas id="'. $canvas_id .'"></canvas>
		<script type="text/javascript">
		jQuery(function($) {

			var barChartData = {
				labels: [],
				datasets: [
					{
						type: "line",
						label: "",
						borderColor: "rgba(255,0,0,1)",
						borderWidth: 3,
						fill: false,
						data: [15]
					},{
						type: "line",
						label: "",
						borderColor: "rgba(255,0,0,1)",
						borderWidth: 3,
						fill: false,
						data: [-15]
					},
					'. implode(', ', array_slice($datasets, 0, 330)) .']
			};

			window.onload = function() {
					var ctx = document.getElementById("'. $canvas_id .'").getContext("2d");
					window.myBar = new Chart(ctx, {
							type: "bar",
							data: barChartData,
							options: {
									responsive: true,
									legend: {
											position: "top",
											display: false,
									},
									title: {
											display: false,
											text: ""
									}
							}
					});
			};

		});
		</script>';

	unset($last_changes, $datasets);
	update_option('rich_wallets_amount_movements_cache_'. $coin_symbol, array('html' => $result_html, 'timestamp' => strtotime(current_time('mysql'))), false);
	return '<div class="rich_wallets_amount_movements_sc">'. $result_html .'</div>';
}
