<?php

add_shortcode('befektetes_kalkulator', 'befektetes_kalkulator_sc_function');
function befektetes_kalkulator_sc_function() {
	$html_result = "";

	if ( isset($_POST['befektetes_kalkulator_submit']) ) {

		$amount = $_POST['amount'];


		$selected_coin = $_POST['selected_coin'];
		$first_warning_percent = $_POST['first_warning_percent'];

		$cmc_data = file_get_contents( 'https://api.coinmarketcap.com/v1/ticker/'. $selected_coin .'/?convert=USD' );
		$cmc_data = json_decode($cmc_data, true);

		$price_usd = (float) $cmc_data[0]['price_usd'];
		$price_btc = (float) $cmc_data[0]['price_btc'];


		


	}



	$html_result = '
		<br><br>

		<div class="col-sm-8">
			<div class="col-sm-4">
				<ol>
					<li>
						<table>
							<tr>
								<th>Árfolyam: </th>
								<td>8555$</td>
							</tr>
							<tr>
								<th>Befizetés: </th>
								<td>20 000 USD</td>
							</tr>
						</table>
					</li>
					<li>
						<table>
							<tr>
								<th>Árfolyam: </th>
								<td>8555$</td>
							</tr>
							<tr>
								<th>Befizetés: </th>
								<td>20 000 USD</td>
							</tr>
						</table>
					</li>
				</ol>
			</div>

			<div class="col-sm-4">
				<h4 class="sc_title">CMC napi %-os árváltozások <small>(utolsó 45 nap)</small></h4>

				Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
			</div>

			<div class="col-sm-4">
				<h4 class="sc_title">CMC napi %-os árváltozások <small>(utolsó 45 nap)</small></h4>

				Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
			</div>
		</div>

		<div class="col-sm-4">
			<form action="" method="POST">
				<div class="form-group">
					<select name="selected_coin" class="form-control">
						<option value="bitcoin">BTC</option>
						<option value="ethereum">ETH</option>
						<option value="litecoin">LTC</option>
					</select>
				</div>

				<div class="form-group">
					<input type="text" class="form-control" value="'. $_POST['amount'] .'" name="amount" placeholder="Befektetendő összeg">
				</div>

				<div class="form-group">
					<label for="">1. jelzés kezdése</label>
					<input type="text" class="form-control" placeholder="" value="5" name="first_warning_percent" /> %
				</div>

				<input type="submit" value="ok" class="btn btn-default" name="befektetes_kalkulator_submit" />
			</form>
		</div>';

	return '<div class="befektetes_kalkulator_sc">'. $html_result .'</div>';
}
