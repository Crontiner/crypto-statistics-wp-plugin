<?php

add_shortcode('rich_wallets_daily_diagram', 'rich_wallets_daily_diagram_function');
function rich_wallets_daily_diagram_function($atts, $content = null) {
	extract(shortcode_atts(array(
		'coin_symbol' => ''
	), $atts));

	if ( empty($coin_symbol) ) { return ""; }
	$coin_ID = get_coin_id_by_name($coin_symbol);
	if ( empty($coin_ID) ) { return ""; }

	global $wpdb;
	$result_html = "";
	$canvas_id = 'c'. md5(wp_generate_password(15, false, false));

	$percent_changes_array = array();
	$days_array = array();
	$column_name = 'changed_amount';

	for($i=0; $i >= -48; $i-=2) {
		$start = strval($i .' hours');
		$end = strval(($i - 2) .' hours');

		$start 	= date('Y-m-d H:i:s', strtotime( $start ) + 3600 );
		$end 		= date('Y-m-d H:i:s', strtotime( $end ) + 3600 );

		if ( $i == 0 ) { $start = date('Y-m-d H:i:s', strtotime('+1 day')); }

		$cmc_data = $wpdb->get_results( "SELECT AVG(`{$wpdb->prefix}crypto_bitinfocharts_rich_wallets_datas`.`{$column_name}`) AS `{$column_name}`
																		FROM `{$wpdb->prefix}crypto_bitinfocharts_rich_wallets_datas`
																		LEFT JOIN `{$wpdb->prefix}crypto_bitinfocharts_rich_wallets` ON
																			`{$wpdb->prefix}crypto_bitinfocharts_rich_wallets`.`ID` = `{$wpdb->prefix}crypto_bitinfocharts_rich_wallets_datas`.`wallet_ID`
																		WHERE `{$wpdb->prefix}crypto_bitinfocharts_rich_wallets_datas`.`datetime` <= '". $start ."'
																			AND `{$wpdb->prefix}crypto_bitinfocharts_rich_wallets_datas`.`datetime` > '". $end ."'

																			AND `{$wpdb->prefix}crypto_bitinfocharts_rich_wallets`.`coin_ID` = '". $coin_ID ."'
																			AND `{$wpdb->prefix}crypto_bitinfocharts_rich_wallets_datas`.`changed_amount` IS NOT NULL
																			AND `{$wpdb->prefix}crypto_bitinfocharts_rich_wallets_datas`.`changed_amount` != ''
																			AND `{$wpdb->prefix}crypto_bitinfocharts_rich_wallets`.`last_updated_datetime` >= '". date('Y-m-d H:i:s', strtotime("-4 days")) ."'
																			ORDER BY `{$wpdb->prefix}crypto_bitinfocharts_rich_wallets_datas`.`ID` DESC", ARRAY_A );

		$cmc_datetime = $wpdb->get_results( "SELECT `{$wpdb->prefix}crypto_bitinfocharts_rich_wallets_datas`.`datetime` AS `datetime`
																		FROM `{$wpdb->prefix}crypto_bitinfocharts_rich_wallets_datas`
																		LEFT JOIN `{$wpdb->prefix}crypto_bitinfocharts_rich_wallets` ON
																			`{$wpdb->prefix}crypto_bitinfocharts_rich_wallets`.`ID` = `{$wpdb->prefix}crypto_bitinfocharts_rich_wallets_datas`.`wallet_ID`
																		WHERE (`{$wpdb->prefix}crypto_bitinfocharts_rich_wallets_datas`.`datetime` <= '". $start ."'
																			AND `{$wpdb->prefix}crypto_bitinfocharts_rich_wallets_datas`.`datetime` > '". $end ."')

																			AND `{$wpdb->prefix}crypto_bitinfocharts_rich_wallets`.`coin_ID` = '". $coin_ID ."'
																			AND `{$wpdb->prefix}crypto_bitinfocharts_rich_wallets_datas`.`changed_amount` IS NOT NULL
																			AND `{$wpdb->prefix}crypto_bitinfocharts_rich_wallets_datas`.`changed_amount` != ''
																			AND `{$wpdb->prefix}crypto_bitinfocharts_rich_wallets`.`last_updated_datetime` >= '". date('Y-m-d H:i:s', strtotime("-4 days")) ."'
																			ORDER BY 	`{$wpdb->prefix}crypto_bitinfocharts_rich_wallets_datas`.`datetime` DESC
																			LIMIT 1", ARRAY_A );

		if (
				isset($cmc_data[0][$column_name]) && !empty($cmc_data[0][$column_name]) &&
				isset($cmc_datetime[0]['datetime']) && !empty($cmc_datetime[0]['datetime'])
	 		 ) {

			$average_val  = $cmc_data[0][$column_name];
			$datetime 		= strtotime($cmc_datetime[0]['datetime']);
			$t = $datetime;

			$days_array[] = '"'. date_i18n('D H:00', $t) .'",';
			$percent_changes_array[]= $average_val;
		}
	}
	$days_array = array_reverse($days_array);

	$positives = array();
	$negatives = array();

	foreach ($percent_changes_array as $key => $percent_change) {
		$p = 0;
		$n = 0;

		if (strpos($percent_change, '-') !== false) { $n = $percent_change; }
		else { $p = $percent_change; }

		$positives []= $p;
		$negatives []= $n;
	}

	$avg_price = (array_sum($positives) + array_sum($negatives)) / (count($positives) + count($negatives));
	$avg_price = number_format($avg_price,2);

	$sum_price = array_sum($positives) + array_sum($negatives);
	$sum_price = number_format($sum_price,2);

	$result_html =
		'<h4 class="sc_title">'. $coin_symbol .' <small>(utolsó 48 óra) | SUM: '. $sum_price .'</small></h4>'.
		'<canvas id="'. $canvas_id .'"></canvas>
		<script type="text/javascript">
		jQuery(function($) {
			$(window).load(function(){
				var ctx = document.getElementById("'. $canvas_id .'").getContext("2d");
				var chart = new Chart(ctx, {
				    type: "line",
				    data: {
				        labels: ['. implode('', $days_array) .'],
								datasets: [{
										steppedLine: false,
		                label: "Vásárlások",
		                backgroundColor: "rgba(72, 194, 113, 1)",
		                data: ['. implode(',', array_reverse($positives)) .']
		            }, {
										steppedLine: false,
		                label: "Eladások",
		                backgroundColor: "rgba(181, 76, 51, 1)",
		                data: ['. implode(',', array_reverse($negatives)) .']
		            }]
				    },
				    options: { }
				});

			});
		});
		</script>';


	// Utolsó emelkedés vagy süllyedés érték elmentése
	/*if ( $manual_update == "update" ) {
		$r = 0;
		$p = floatval(end(array_reverse($positives)));
		$n = floatval(end(array_reverse($negatives)));

		if ( $p != 0 ) { $r = $p; }
		else if ( $n != 0 ) { $r = $n; }

		update_option( 'crypto_last_sell_buy_days_data', $r, false );
		return "";
	}*/


	return $result_html;
}
