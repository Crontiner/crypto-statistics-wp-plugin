<?php


add_shortcode('one_btc_mining_cost_sc', 'one_btc_mining_cost_sc_functions');
function one_btc_mining_cost_sc_functions() {
	$html_result = "";

	$mining_values = one_btc_mining_cost();
	if ( empty($mining_values) ) { return ""; }


	$html_result .=
		'<table>

			<tr>
				<th colspan="2">The most common values</th>
			</tr>
			<tr>
				<th>Income per day:</th>
				<td>
					Min: '. number_format($mining_values['income_per_day']['min'], 2) .' $<br>
					Max: '. number_format($mining_values['income_per_day']['max'], 2) .' $<br>
					AVG: '. number_format($mining_values['income_per_day']['avg'], 2) .' $<br>
				</td>
			</tr>
			<tr>
				<th>Electricity per day:</th>
				<td>
					Min: '. number_format($mining_values['electricity_per_day']['min'], 2) .' $<br>
					Max: '. number_format($mining_values['electricity_per_day']['max'], 2) .' $<br>
					AVG: '. number_format($mining_values['electricity_per_day']['avg'], 2) .' $<br>
				</td>
			</tr>
			<tr>
				<th>Profit per day:</th>
				<td>
					Min: '. number_format($mining_values['profit_per_day']['min'], 2) .' $<br>
					Max: '. number_format($mining_values['profit_per_day']['max'], 2) .' $<br>
					AVG: '. number_format($mining_values['profit_per_day']['avg'], 2) .' $<br>
				</td>
			</tr>
			<tr>
				<th>1 BTC electricity cost:</th>
				<td>
					Min: '. number_format($mining_values['one_btc_electricity_cost']['min'], 2) .' $<br>
					Max: '. number_format($mining_values['one_btc_electricity_cost']['max'], 2) .' $<br>
					AVG: '. number_format($mining_values['one_btc_electricity_cost']['avg'], 2) .' $<br>
				</td>
			</tr>
			<tr>
				<th>1 BTC electricity cost percent:</th>
				<td>
					Min: '. number_format($mining_values['one_btc_electricity_cost_percent']['min'], 2) .' %<br>
					Max: '. number_format($mining_values['one_btc_electricity_cost_percent']['max'], 2) .' %<br>
					AVG: '. number_format($mining_values['one_btc_electricity_cost_percent']['avg'], 2) .' %<br>
				</td>
			</tr>
			<tr>
				<th>1 BTC mining profit:</th>
				<td>
					Min: '. number_format($mining_values['one_btc_mining_profit']['min'], 2) .' $<br>
					Max: '. number_format($mining_values['one_btc_mining_profit']['max'], 2) .' $<br>
					AVG: '. number_format($mining_values['one_btc_mining_profit']['avg'], 2) .' $<br>
				</td>
			</tr>

			<tr>
				<th colspan="2">Average values</th>
			</tr>
			<tr>
				<th>Income per day:</th>
				<td>'. number_format($mining_values['income_per_day_avg'], 2) .' $</td>
			</tr>
			<tr>
				<th>Electricity per day:</th>
				<td>'. number_format($mining_values['electricity_per_day_avg'], 2) .' $</td>
			</tr>
			<tr>
				<th>Profit per day:</th>
				<td>'. number_format($mining_values['profit_per_day_avg'], 2) .' $</td>
			</tr>
			<tr>
				<th>1 BTC electricity cost:</th>
				<td>'. number_format($mining_values['one_btc_electricity_cost_avg'], 2) .' $</td>
			</tr>
			<tr>
				<th>1 BTC electricity cost percent:</th>
				<td>'. number_format($mining_values['one_btc_electricity_cost_percent_avg'], 2) .' %</td>
			</tr>
			<tr>
				<th>1 BTC mining profit:</th>
				<td>'. number_format($mining_values['one_btc_mining_profit_avg'], 2) .' $</td>
			</tr>

		</table>';


	return '<div class="one_btc_mining_cost_sc">'.
						'<h4 class="sc_title">1 BTC mining cost</h4>'.
						$html_result.
					'</div>';
}
