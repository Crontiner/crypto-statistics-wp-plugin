<?php

add_shortcode('coin_values_compare_in_percent', 'coin_values_compare_in_percent_sc_function');
function coin_values_compare_in_percent_sc_function($atts, $content = null) {
	extract(shortcode_atts(array(
		'coin_name' => "",
		'start_date' => "",
	), $atts));
	$html_result = "";
	$canvas_id = 'c'. md5(wp_generate_password(15, false, false));
	$results_array = get_coin_cmc_historical_data($coin_name, $start_date);


	// Separete to weeks
	if ( !empty($results_array) && is_array($results_array) ) {
		$results_array_temp = array();
		foreach ($results_array as $key => $day_data) {
			$monday = strtotime('monday this week', $day_data['date']);
			$results_array_temp[$monday][] = $day_data;
		}
		$results_array = $results_array_temp;
	}
	unset($results_array_temp);


	// Find max values for percent
	$all_usd_prices = array();
	$all_volumes = array();
	$all_market_caps = array();


	// Average values
	if ( !empty($results_array) && is_array($results_array) ) {
		foreach ($results_array as $monday_timestamp => $days_data) {

			$usd_prices = array();
			$volumes = array();
			$market_caps = array();

			foreach ($days_data as $key => $day_data) {
				$usd_prices []= $day_data['usd_price'];
				$volumes []= $day_data['volume'];
				$market_caps []= $day_data['market_cap'];
			}


			$results_array[$monday_timestamp] = array(
																								'usd_price' => array_sum($usd_prices) / count($usd_prices),
																								'volume' => array_sum($volumes) / count($volumes),
																								'market_cap' => array_sum($market_caps) / count($market_caps),
																							);

			$all_usd_prices[]= $results_array[$monday_timestamp]['usd_price'];
			$all_volumes[]= $results_array[$monday_timestamp]['volume'];
			$all_market_caps[]= $results_array[$monday_timestamp]['market_cap'];
		}
	}

	$all_usd_prices_original = $all_usd_prices;
	arsort($all_usd_prices); arsort($all_volumes); arsort($all_market_caps);

	$all_usd_prices = array_values($all_usd_prices);
	$all_volumes = array_values($all_volumes);
	$all_market_caps = array_values($all_market_caps);

	$max_usd_prices = $all_usd_prices[0];
	$max_volumes = $all_volumes[0];
	$max_market_caps = $all_market_caps[0];

	unset($all_usd_prices); unset($all_volumes); unset($all_market_caps);


	$labels_array = array();
	$usd_price_percents = array();
	$volume_percents = array();
	$market_cap_percents = array();

	$usd_volume_differents = array();
	$usd_volume_market_cap_averages = array();


	// Calculate percents
	if ( !empty($results_array) ) {
		foreach ($results_array as $monday_timestamp => $days_avg_datas) {

			$results_array[$monday_timestamp]['usd_price_percent'] = round(($days_avg_datas['usd_price'] * 100) / $max_usd_prices, 2);
			$results_array[$monday_timestamp]['volume_percent'] = round(($days_avg_datas['volume'] * 100) / $max_volumes, 2);
			$results_array[$monday_timestamp]['market_cap_percent'] = round(($days_avg_datas['market_cap'] * 100) / $max_market_caps, 2);

			$labels_array[]= '"'. date('y-m-d', $monday_timestamp) .'"';
			$usd_price_percents[]= '"'. $results_array[$monday_timestamp]['usd_price_percent'] .'"';
			$volume_percents[]= '"'. $results_array[$monday_timestamp]['volume_percent'] .'"';
			$market_cap_percents[]= '"'. $results_array[$monday_timestamp]['market_cap_percent'] .'"';

			//$usd_volume_differents[] = 100 - round(($results_array[$monday_timestamp]['volume_percent'] * 100) / $results_array[$monday_timestamp]['usd_price_percent'],2);
			$usd_volume_differents[] = round($results_array[$monday_timestamp]['usd_price_percent'] - $results_array[$monday_timestamp]['volume_percent'], 2);
			$usd_volume_market_cap_averages[] = round(($results_array[$monday_timestamp]['usd_price_percent'] + $results_array[$monday_timestamp]['volume_percent'] + $results_array[$monday_timestamp]['market_cap_percent']) / 3,2);
		}
	}


	// usd_volume_differents
	$max_usd_volume_differents = $usd_volume_differents;
	arsort($max_usd_volume_differents);
	$max_usd_volume_differents = array_values($max_usd_volume_differents);
	$max_usd_volume_different = $max_usd_volume_differents[0];
	unset($max_usd_volume_differents);

	$usd_volume_differents_temp = array();
	foreach ($usd_volume_differents as $key => $val) {
		$usd_volume_differents_temp[]= round(($val * 100) / $max_usd_volume_different,2);
	}
	$usd_volume_differents = $usd_volume_differents_temp;
	unset($usd_volume_differents_temp);


	// usd_volume_market_cap_averages
	$max_usd_volume_market_cap_averages = $usd_volume_market_cap_averages;
	arsort($max_usd_volume_market_cap_averages);
	$max_usd_volume_market_cap_averages = array_values($max_usd_volume_market_cap_averages);
	$usd_volume_market_cap_average = $max_usd_volume_market_cap_averages[0];
	unset($max_usd_volume_market_cap_averages);

	$usd_volume_market_cap_averages_temp = array();
	foreach ($usd_volume_market_cap_averages as $key => $val) {
		$usd_volume_market_cap_averages_temp[]= round(($val * 100) / $usd_volume_market_cap_average,2);
	}
	$usd_volume_market_cap_averages = $usd_volume_market_cap_averages_temp;
	unset($usd_volume_market_cap_averages_temp);


	$labels_array = array_reverse($labels_array);
	$usd_price_percents = array_reverse($usd_price_percents);
	$volume_percents = array_reverse($volume_percents);
	$market_cap_percents = array_reverse($market_cap_percents);
	$usd_volume_differents = array_reverse($usd_volume_differents);
	$usd_volume_market_cap_averages = array_reverse($usd_volume_market_cap_averages);


	/*
	// Find Realistic price
	$realistic_price = array();
	$uvmc_last_value = "";
	foreach ($usd_volume_market_cap_averages as $key => $value) {
		if ( $key == 0 ) { $uvmc_last_value = $value; }
		else {
			if 	( !empty($uvmc_last_value) &&
						( ($uvmc_last_value + 0.095) > $value ) &&
						( ($uvmc_last_value - 0.095) < $value )
					) {
						$realistic_price []= $all_usd_prices_original[$key];
					}
		}
	}


/*
var_dump($labels_array);
echo "<br><br>";
var_dump($usd_price_percents);
echo "<br><br>";
var_dump($volume_percents);
echo "<br><br>";
var_dump($market_cap_percents);
echo "<br><br>";
var_dump($usd_volume_differents);
echo "<br><br>";
var_dump($usd_volume_market_cap_averages);
die;
*/


	$html_result =
		'<h4 class="sc_title"><b>'. strtoupper($coin_name) .'</b> values compare - <small>'. timeAgo(strtotime($start_date)) .'</small></h4>'.
		'<canvas id="'. $canvas_id .'"></canvas>
		<script type="text/javascript">
		jQuery(function($) {
			$(window).load(function(){
				var ctx = document.getElementById("'. $canvas_id .'").getContext("2d");
				var chart = new Chart(ctx, {
				    type: "line",
				    data: {
				        labels: ['. implode(', ', $labels_array) .'],
								datasets: [{
										steppedLine: false,
		                label: "USD",
										fill: false,
		                backgroundColor: "rgba(255, 0, 0, 1)",
										borderColor: "rgba(255, 0, 0, 1)",
		                data: ['. implode(', ', $usd_price_percents) .'],
										lineTension: 0,
		            },{
										steppedLine: false,
		                label: "Volume",
										fill: false,
		                backgroundColor: "rgba(0, 255, 0, 1)",
										borderColor: "rgba(0, 255, 0, 1)",
		                data: ['. implode(', ', $volume_percents) .'],
										lineTension: 0,
		            },{
										steppedLine: false,
		                label: "Market cap",
										fill: false,
		                backgroundColor: "rgba(0, 0, 255, 1)",
										borderColor: "rgba(0, 0, 255, 1)",
		                data: ['. implode(', ', $market_cap_percents) .'],
										lineTension: 0,
		            },{
										steppedLine: false,
		                label: "USD & Volume Diff",
										fill: false,
		                backgroundColor: "rgba(0, 0, 0, 1)",
										borderColor: "rgba(0, 0, 0, 1)",
		                data: ['. implode(', ', $usd_volume_differents) .'],
										lineTension: 0,
		            },{
										steppedLine: false,
		                label: "USD & Volume & Market cap AVG",
										fill: false,
		                backgroundColor: "rgba(127, 127, 127, 1)",
										borderColor: "rgba(127, 127, 127, 1)",
		                data: ['. implode(', ', $usd_volume_market_cap_averages) .'],
										lineTension: 0,
		            }]
				    },
						options: {
							responsive: true,
							legend: {
								display: true,
							},
						}
				});

			});
		});
		</script>';

	return $html_result;
}
