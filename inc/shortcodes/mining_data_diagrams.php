<?php

function after_processing_of_mining_data($block_times) {
	$block_times_temp = array();
	foreach ($block_times as $key => $array) {
		foreach ($block_times[$key] as $date => $percent) {
			$block_times_temp [$date][]= $percent;
		}
	}

	$block_times = array();
	foreach ($block_times_temp as $date => $percents) {
		$block_times [$date]= number_format(array_sum($percents) / count($percents),2);
	}

	return $block_times;
}

function processing_of_mining_data($time_intervals, $coin_id, $db_column) {
	global $wpdb;

	$return_array = array();

	// Get max value
	$start = $time_intervals[0]['start'];
	$end = $time_intervals[count($time_intervals)-1]['end'];
	$max_val = 0;

	$mining_data = $wpdb->get_results( "SELECT `{$db_column}` AS `max_val`
																			FROM `{$wpdb->prefix}crypto_mining_data`
																			WHERE `datetime` <= '". $start ."'
																				AND `datetime` > '". $end ."'
																				AND `coin_ID` = ". $coin_id ."
																			ORDER BY `{$db_column}` DESC
																			LIMIT 1", ARRAY_A );
	$max_val = $mining_data[0]['max_val'];

	if ( $max_val > 0 ) {

		// Get time interval
		foreach ($time_intervals as $ti_key => $ti_val) {
			$start = $ti_val['start'];
			$end = $ti_val['end'];

			// Get percent
			$sql = "SELECT ROUND((`{$db_column}` * 100) / {$max_val}, 2) AS `percent`, `datetime`
							FROM `{$wpdb->prefix}crypto_mining_data`
							WHERE `datetime` <= '". $start ."'
								AND `datetime` > '". $end ."'
								AND `coin_ID` = ". $coin_id ."
							ORDER BY `datetime` DESC";
			$percents = $wpdb->get_results($sql, ARRAY_A);

			$percent_avg = array();
			foreach ($percents as $percent_k => $percent_v) { $percent_avg []= $percent_v['percent']; }
			$percent_avg = array_sum($percent_avg) / count($percent_avg);


			// Get last datetime

			$sql = "SELECT `datetime`
							FROM `{$wpdb->prefix}crypto_mining_data`
							WHERE `datetime` <= '". $start ."'
								AND `datetime` > '". $end ."'
								AND `coin_ID` = ". $coin_id ."
							ORDER BY `datetime` DESC
							LIMIT 1";
			$last_date = $wpdb->get_results($sql, ARRAY_A);
			$last_date = $last_date[0]['datetime'];

			// ---

			$return_array[date('Y-m-d', strtotime($last_date))]= number_format($percent_avg, 2);
		}
	}
	return $return_array;
}

add_shortcode('mining_data_diagrams', 'mining_data_diagrams_function2');
//add_shortcode('mining_data_diagrams2', 'mining_data_diagrams_function2');
function mining_data_diagrams_function2() {
	global $wpdb;
	$result_html = "";

	if ( !is_user_logged_in() ) { return ""; }


	// --- CACHE ---

	$mining_data_diagrams_cache = get_option('crypto_mining_data_diagrams_cache');

	if ( !empty($mining_data_diagrams_cache) && isset($mining_data_diagrams_cache['timestamp']) &&
			(strtotime(current_time('mysql')) - $mining_data_diagrams_cache['timestamp'] <= MINING_DATA_DIAGRAMS_CACHE_REFRESH_TIME)
	 ) {
		return $mining_data_diagrams_cache['html'];
	}

	// --- /CACHE ---


	$block_times = array();
	$days_array = array();
	$columns = array( 'block_time', 'difficult24', 'nethash', 'estimated_rewards24', 'btc_revenue24', 'profitability24' );


	$coins_array = $wpdb->get_results( "SELECT DISTINCT `coin_ID`
																			FROM `{$wpdb->prefix}crypto_mining_data`
																			ORDER BY `coin_ID` ASC ", ARRAY_A );

	$coins_array_temp = array();
	$counts = array();
	foreach ($coins_array as $key => $value) {
		$coin_id = (int) $value['coin_ID'];

		$count = $wpdb->get_results( "SELECT COUNT(`coin_ID`) AS `count`
																				FROM `{$wpdb->prefix}crypto_mining_data`
																				WHERE `coin_ID` = ". $coin_id, ARRAY_A );
		$count = (int) $count[0]['count'];

		$counts []= $count;
		$coins_array_temp [$coin_id]= array( 'count' => $count );
	}


	$counts = array_count_values($counts);
	krsort($counts);
	$max_count = 0;
	foreach ($counts as $key => $value) { $max_count = (int) $key; break; }
	unset( $counts );


	$time_intervals = array();
	for($i=0; $i >= -(45 * 24); $i-=24) {
		$start = strval($i .' hours');
		$end = strval(($i - 24) .' hours');
		if ( $i == 0 ) { $start = current_time('mysql'); }

		$time_intervals []= array('start' => date('Y-m-d H:i:s', strtotime( $start )), 'end' => date('Y-m-d H:i:s', strtotime( $end )));
	}


	$excluded_coins = $wpdb->get_results( "SELECT DISTINCT `coin_ID`
																					FROM `{$wpdb->prefix}crypto_mining_data`
																					WHERE (`estimated_rewards` = 0.9999999999 OR `estimated_rewards24` = 0.9999999999) AND
																								`datetime` <= '". $$time_intervals[0]['start'] ."' AND
																								`datetime` > '". $$time_intervals[count($time_intervals)-1]['end'] ."'
																					ORDER BY `coin_ID` ASC", ARRAY_A );
	$excluded_coins_temp = array();
	foreach ($excluded_coins as $key => $value) { $excluded_coins_temp []= $value['coin_ID']; }
	$excluded_coins = $excluded_coins_temp;
	unset($excluded_coins_temp);


	// kiszedni a felesleges értékeket a tömbből
	$coins_array = array();
	foreach ($coins_array_temp as $coin_id => $v) {
		if ( ($v['count'] == $max_count) && !in_array($coin_id, $excluded_coins) ) {
			$coins_array [$coin_id] = "";
		}
	}
	unset($coins_array_temp);

	$block_times = array();
	$difficult24 = array();
	$nethash = array();
	$estimated_rewards24 = array();
	$btc_revenue24 = array();
	$profitability24 = array();

	foreach ($coins_array as $coin_id => $v) {
		$block_times []= processing_of_mining_data($time_intervals, $coin_id, 'block_time');
		$difficult24 []= processing_of_mining_data($time_intervals, $coin_id, 'difficult24');
		$nethash []= processing_of_mining_data($time_intervals, $coin_id, 'nethash');
		$estimated_rewards24 []= processing_of_mining_data($time_intervals, $coin_id, 'estimated_rewards24');
		$btc_revenue24 []= processing_of_mining_data($time_intervals, $coin_id, 'btc_revenue24');
		$profitability24 []= processing_of_mining_data($time_intervals, $coin_id, 'profitability24');
	}

	$block_times = array_reverse(after_processing_of_mining_data($block_times));
	$difficult24 = array_reverse(after_processing_of_mining_data($difficult24));
	$nethash = array_reverse(after_processing_of_mining_data($nethash));
	$estimated_rewards24 = array_reverse(after_processing_of_mining_data($estimated_rewards24));
	$btc_revenue24 = array_reverse(after_processing_of_mining_data($btc_revenue24));
	$profitability24 = array_reverse(after_processing_of_mining_data($profitability24));

	$days_array = array();
	foreach ($block_times as $days => $value) { $days_array []= '"'. date('m-d', strtotime($days .' 12:00:00')) .'"'; }

	$result_html =
		'<div class="last_month_minings_diagrams">
			<h4 class="sc_title">Bányászati adatok <small>(utolsó 45 nap)</small></h4>

			<div class="col-sm-4">
				<div class="row">
					<canvas id="block_time_diagram"></canvas>
				</div>
			</div>
			<div class="col-sm-4">
				<div class="row">
					<canvas id="diff_diagram"></canvas>
				</div>
			</div>
			<div class="col-sm-4">
				<div class="row">
					<canvas id="nethash_diagram"></canvas>
				</div>
			</div>
			<div class="col-sm-4">
				<div class="row">
					<canvas id="estimated_rewards_diagram"></canvas>
				</div>
			</div>
			<div class="col-sm-4">
				<div class="row">
					<canvas id="btc_revenue_diagram"></canvas>
				</div>
			</div>
			<div class="col-sm-4">
				<div class="row">
					<canvas id="profitability_diagram"></canvas>
				</div>
			</div>
		</div>

		<script type="text/javascript">
			jQuery(function($) {
				$(window).load(function(){
					var ctx = document.getElementById("block_time_diagram").getContext("2d");
					var chart = new Chart(ctx, {
					    type: "line",
					    data: {
					        labels: ['. implode(',', $days_array) .'],
									datasets: [{
											steppedLine: false,
			                label: "Block time",
			                backgroundColor: "rgba(184, 184, 184, 1)",
			                data: ['. implode(',', $block_times) .']
			            }]
					    },
					    options: { }
					});

					var ctx = document.getElementById("diff_diagram").getContext("2d");
					var chart = new Chart(ctx, {
					    type: "line",
					    data: {
					        labels: ['. implode(',', $days_array) .'],
									datasets: [{
											steppedLine: false,
			                label: "Difficult",
			                backgroundColor: "rgba(184, 184, 184, 1)",
			                data: ['. implode(',', $difficult24) .']
			            }]
					    },
					    options: { }
					});

					var ctx = document.getElementById("nethash_diagram").getContext("2d");
					var chart = new Chart(ctx, {
					    type: "line",
					    data: {
					        labels: ['. implode(',', $days_array) .'],
									datasets: [{
											steppedLine: false,
			                label: "Nethash",
			                backgroundColor: "rgba(184, 184, 184, 1)",
			                data: ['. implode(',', $nethash) .']
			            }]
					    },
					    options: { }
					});

					var ctx = document.getElementById("estimated_rewards_diagram").getContext("2d");
					var chart = new Chart(ctx, {
					    type: "line",
					    data: {
					        labels: ['. implode(',', $days_array) .'],
									datasets: [{
											steppedLine: false,
			                label: "Estimated rewards",
			                backgroundColor: "rgba(184, 184, 184, 1)",
			                data: ['. implode(',', $estimated_rewards24) .']
			            }]
					    },
					    options: { }
					});

					var ctx = document.getElementById("btc_revenue_diagram").getContext("2d");
					var chart = new Chart(ctx, {
					    type: "line",
					    data: {
					        labels: ['. implode(',', $days_array) .'],
									datasets: [{
											steppedLine: false,
			                label: "BTC Revenue",
			                backgroundColor: "rgba(184, 184, 184, 1)",
			                data: ['. implode(',', $btc_revenue24) .']
			            }]
					    },
					    options: { }
					});

					var ctx = document.getElementById("profitability_diagram").getContext("2d");
					var chart = new Chart(ctx, {
					    type: "line",
					    data: {
					        labels: ['. implode(',', $days_array) .'],
									datasets: [{
											steppedLine: false,
			                label: "Profitability",
			                backgroundColor: "rgba(184, 184, 184, 1)",
			                data: ['. implode(',', $profitability24) .']
			            }]
					    },
					    options: { }
					});
				});
			});
		</script>';

		update_option('crypto_mining_data_diagrams_cache', array('html' => $result_html, 'timestamp' => strtotime(current_time('mysql'))), false);
		return $result_html;
}


function mining_data_diagrams_function() {
	global $wpdb;
	$result_html = "";
	return "";
	if ( !is_user_logged_in() ) { return ""; }


	// --- CACHE ---

	$mining_data_diagrams_cache = get_option('crypto_mining_data_diagrams_cache');

	if ( !empty($mining_data_diagrams_cache) && isset($mining_data_diagrams_cache['timestamp']) &&
			(strtotime(current_time('mysql')) - $mining_data_diagrams_cache['timestamp'] <= MINING_DATA_DIAGRAMS_CACHE_REFRESH_TIME)
	 ) {
		 return $mining_data_diagrams_cache['html'];
	}

	// --- /CACHE ---


	$block_times = array();
	$days_array = array();
	$columns = array( 'block_time', 'difficult24', 'nethash', 'estimated_rewards24', 'btc_revenue24', 'profitability24' );


	$coins_array = $wpdb->get_results( "SELECT DISTINCT `coin_ID`
																			FROM `{$wpdb->prefix}crypto_coinmarketcap_data`
																			ORDER BY `coin_ID` ASC ", ARRAY_A );

	// SELECT DISTINCT `coin_ID` FROM `{$wpdb->prefix}crypto_coinmarketcap_data` WHERE `block_time` != '' AND `difficult24` != '' AND `nethash` != '' AND `estimated_rewards24` != '' AND `btc_revenue24` != '' AND `profitability24` != '' ORDER BY `coin_ID` ASC

	$coins_array_temp = array();
	foreach ($coins_array as $key => $value) {
		$coins_array_temp []= (int) $value['coin_ID'];
	}
	$coins_array = $coins_array_temp;
	unset($coins_array_temp);

	$coins_array_temp = array();
	foreach ($coins_array as $key => $coin_id) {
		$coins_array = $wpdb->get_results( "SELECT count(`coin_ID`) AS `count`
																				FROM `{$wpdb->prefix}crypto_coinmarketcap_data`
																				WHERE `coin_ID` = '{$coin_id}' ", ARRAY_A );

		$coins_array_temp [$coin_id]= (int) $coins_array[0]['count'];
	}
	arsort($coins_array_temp);

	$max_num = "";
	foreach ($coins_array_temp as $coin_id => $value) {
		$max_num = $value;
		break;
	}

	unset($coins_array);
	$top_mineable_coins = array();

	foreach ($coins_array_temp as $coin_id => $num) {
		if ( $num == $max_num ) {
			$top_mineable_coins []= (int) $coin_id;
		}
	}

	unset($coins_array_temp);

	// ---

	foreach ($columns as $key => $column_name) {

		$mining_max_value_original = $wpdb->get_results( "SELECT `{$column_name}` AS `max_value`
																								FROM `{$wpdb->prefix}crypto_mining_data`
																								WHERE `datetime` >= '". date('Y-m-d H:i:s', strtotime('-46 days') ) ."'
																								AND `{$column_name}` != ''
																								AND `coin_ID` IN (". implode(',', $top_mineable_coins) .")
																								ORDER BY `{$column_name}` DESC LIMIT 1", ARRAY_A );
		$mining_max_value_original = $mining_max_value_original[0]['max_value']; // this value is a 100%


		for($i=0; $i >= -(45 * 24); $i-=24) {
			$start = strval($i .' hours');
			$end = strval(($i - 24) .' hours');
			if ( $i == 0 ) { $start = current_time('mysql'); }

			$mining_data = $wpdb->get_results( "SELECT AVG(`{$column_name}`) AS `{$column_name}`
																			FROM `{$wpdb->prefix}crypto_mining_data`
																			WHERE `datetime` <= '". date('Y-m-d H:i:s', strtotime( $start )) ."'
																				AND `datetime` > '". date('Y-m-d H:i:s', strtotime( $end )) ."'
																				AND `{$column_name}` != ''
																				AND `coin_ID` IN (". implode(',', $top_mineable_coins) .") ", ARRAY_A );

			$mining_datetime = $wpdb->get_results( "SELECT `datetime`
																			FROM `{$wpdb->prefix}crypto_mining_data`
																			WHERE `datetime` <= '". date('Y-m-d H:i:s', strtotime( $start )) ."'
																				AND `datetime` > '". date('Y-m-d H:i:s', strtotime( $end )) ."'
																				AND `coin_ID` IN (". implode(',', $top_mineable_coins) .")
																				ORDER BY `datetime` DESC LIMIT 1", ARRAY_A );

			if (
					isset($mining_data[0][$column_name]) && !empty($mining_data[0][$column_name]) &&
					isset($mining_datetime[0]['datetime']) && !empty($mining_datetime[0]['datetime'])
		 		) {

							$average_val  							= $mining_data[0][$column_name];
							$datetime 									= strtotime($mining_datetime[0]['datetime']);
							$days_array[$column_name][] = date('d',$datetime);

							if ( $column_name == "estimated_rewards24" ) {

								$mining_max_value = $mining_max_value_original / 6;
								$average_val = $average_val / 6;

								$block_times[$column_name][]= ($average_val * 100) / $mining_max_value;

							} else if ( $column_name == "nethash" ) {

								$mining_max_value = $mining_max_value_original / 6;
								$average_val = $average_val / 6;

								$block_times[$column_name][]= ($average_val * 100) / $mining_max_value;

							} else if ( $column_name == "difficult24" ) {

								$mining_max_value = str_replace(',', '', $mining_max_value_original);
								$mining_max_value = str_replace('.', '', $mining_max_value);
								$mining_max_value = round(substr($mining_max_value, 0, 5) / 4);

								$average_val = str_replace(',', '', $average_val);
								$average_val = str_replace('.', '', $average_val);
								$average_val = round(substr($average_val, 0, 5) / 4);


								$block_times[$column_name][]= ($average_val * 100) / $mining_max_value;

							} else if ( $column_name == "btc_revenue24" ) {
								$mining_max_value = substr($mining_max_value_original, 0, 10);

								$average_val = substr($average_val, 0, 10);


								$block_times[$column_name][]= ($average_val * 100) / $mining_max_value;
							} else {
								$mining_max_value = number_format($mining_max_value_original,3);

								$average_val = number_format($average_val,3);


								$block_times[$column_name][]= ($average_val * 100) / $mining_max_value;
							}

			}
		}
	}


	$result_html =
		'<div class="last_month_minings_diagrams">
			<h4 class="sc_title">Bányászati adatok <small>(utolsó 45 nap)</small></h4>

			<div class="col-sm-4">
				<div class="row">
					<canvas id="block_time_diagram"></canvas>
				</div>
			</div>
			<div class="col-sm-4">
				<div class="row">
					<canvas id="diff_diagram"></canvas>
				</div>
			</div>
			<div class="col-sm-4">
				<div class="row">
					<canvas id="nethash_diagram"></canvas>
				</div>
			</div>
			<div class="col-sm-4">
				<div class="row">
					<canvas id="estimated_rewards_diagram"></canvas>
				</div>
			</div>
			<div class="col-sm-4">
				<div class="row">
					<canvas id="btc_revenue_diagram"></canvas>
				</div>
			</div>
			<div class="col-sm-4">
				<div class="row">
					<canvas id="profitability_diagram"></canvas>
				</div>
			</div>
		</div>

		<script type="text/javascript">
			jQuery(function($) {
				$(window).load(function(){
					var ctx = document.getElementById("block_time_diagram").getContext("2d");
					var chart = new Chart(ctx, {
					    type: "line",
					    data: {
					        labels: ['. implode(',', array_reverse($days_array['block_time'])) .'],
									datasets: [{
											steppedLine: false,
			                label: "Block time",
			                backgroundColor: "rgba(184, 184, 184, 1)",
			                data: ['. implode(',', array_reverse($block_times['block_time'])) .']
			            }]
					    },
					    options: { }
					});

					var ctx = document.getElementById("diff_diagram").getContext("2d");
					var chart = new Chart(ctx, {
					    type: "line",
					    data: {
					        labels: ['. implode(',', array_reverse($days_array['difficult24'])) .'],
									datasets: [{
											steppedLine: false,
			                label: "Difficult",
			                backgroundColor: "rgba(184, 184, 184, 1)",
			                data: ['. implode(',', array_reverse($block_times['difficult24'])) .']
			            }]
					    },
					    options: { }
					});

					var ctx = document.getElementById("nethash_diagram").getContext("2d");
					var chart = new Chart(ctx, {
					    type: "line",
					    data: {
					        labels: ['. implode(',', array_reverse($days_array['nethash'])) .'],
									datasets: [{
											steppedLine: false,
			                label: "Nethash",
			                backgroundColor: "rgba(184, 184, 184, 1)",
			                data: ['. implode(',', array_reverse($block_times['nethash'])) .']
			            }]
					    },
					    options: { }
					});

					var ctx = document.getElementById("estimated_rewards_diagram").getContext("2d");
					var chart = new Chart(ctx, {
					    type: "line",
					    data: {
					        labels: ['. implode(',', array_reverse($days_array['estimated_rewards24'])) .'],
									datasets: [{
											steppedLine: false,
			                label: "Estimated rewards",
			                backgroundColor: "rgba(184, 184, 184, 1)",
			                data: ['. implode(',', array_reverse($block_times['estimated_rewards24'])) .']
			            }]
					    },
					    options: { }
					});

					var ctx = document.getElementById("btc_revenue_diagram").getContext("2d");
					var chart = new Chart(ctx, {
					    type: "line",
					    data: {
					        labels: ['. implode(',', array_reverse($days_array['btc_revenue24'])) .'],
									datasets: [{
											steppedLine: false,
			                label: "BTC Revenue",
			                backgroundColor: "rgba(184, 184, 184, 1)",
			                data: ['. implode(',', array_reverse($block_times['btc_revenue24'])) .']
			            }]
					    },
					    options: { }
					});

					var ctx = document.getElementById("profitability_diagram").getContext("2d");
					var chart = new Chart(ctx, {
					    type: "line",
					    data: {
					        labels: ['. implode(',', array_reverse($days_array['profitability24'])) .'],
									datasets: [{
											steppedLine: false,
			                label: "Profitability",
			                backgroundColor: "rgba(184, 184, 184, 1)",
			                data: ['. implode(',', array_reverse($block_times['profitability24'])) .']
			            }]
					    },
					    options: { }
					});
				});
			});
		</script>';

		update_option('crypto_mining_data_diagrams_cache', array('html' => $result_html, 'timestamp' => strtotime(current_time('mysql'))), false);
		return $result_html;
}
