<?php


function get_cmc_usd_percentage_distribution_of_the_total_price_diagram($coin_ID = "", $last_price = "") {
	if ( empty($coin_ID) ) { return ""; }
	global $wpdb;
	$result_html = "";
	$last_price = round($last_price,4);

	$get_cmc_average_usd_price_range_extended = get_cmc_average_usd_price_range_extended($coin_ID);
	if ( isset($get_cmc_average_usd_price_range_extended['percentage_distribution_of_the_total_price']) ) {
		$percentage_distribution_of_the_total_price = $get_cmc_average_usd_price_range_extended['percentage_distribution_of_the_total_price'];
	} else {
		return "";
	}

	$bpdottp_array = $percentage_distribution_of_the_total_price;

	$bpdottp_array_temp = $bpdottp_array;
	array_sort_by_column($bpdottp_array_temp, 'tartomany_end');
	$bpdottp_array_temp = array_reverse($bpdottp_array_temp);
	$max_tartomany_end = $bpdottp_array_temp[0]['tartomany_end'];
	unset($bpdottp_array_temp);

	array_sort_by_column($bpdottp_array, 'tartomany_start');

	$percents = array();
	$prices = array();
	$actual_price = array();
	$list = array();

	foreach ($bpdottp_array as $key => $value) {
		$percents []= '"'. round($value['how_many_times_in_percent'],4) .'"';
		$prices 	[]= round(($value['tartomany_end'] + $value['tartomany_start']) / 2,4);

		if ( !empty($last_price) && ( $value['tartomany_end'] >= floatval($last_price) ) && ( $value['tartomany_start'] < floatval($last_price) ) ) {
			//$actual_price []= '"'. round($last_price,4) .'"';
		} else {
			$actual_price []= "";
		}

		$list []= '<li>'.
								'<div class="percent">'. $value['how_many_times_in_percent'] .'% - <small>'. $value['how_many_times'] .'</small></div>'.
								'<div class="prices"><span>'. $value['tartomany_end'] .'</span><span>'. $value['tartomany_start'] .'</span></div>'.
							'</li>';
	}


	if ( !empty($percents) && !empty($prices) && !empty($list) ) {

		$result_html =
			'<div class="col-xs-12">
				<canvas id="f3gc21'. $coin_ID .'"></canvas>
				<script type="text/javascript">
				jQuery(function($) {
					$(window).load(function(){
						var ctx = document.getElementById("f3gc21'. $coin_ID .'").getContext("2d");
						var chart = new Chart(ctx, {
								type: "line",
								data: {
										labels: ['. implode(',', $prices) .'],
										datasets: [{
												steppedLine: false,
												fill: false,
												label: "Highest value: '. $max_tartomany_end .'",
												backgroundColor: "rgba(72, 194, 113, 1)",
												borderColor: "rgba(72, 194, 113, 1)",
												data: ['. implode(',', $percents) .']
										},
										{
												steppedLine: false,
												fill: false,
												label: "Last Price: ",
												backgroundColor: "rgba(255, 0, 0, 1)",
												borderColor: "rgba(255, 0, 0, 1)",
												data: ['. implode(',', $actual_price) .']
										}]
								},
								options: { }
						});
					});
				});
				</script>
			</div>
			<div class="col-xs-12 hidden">
				<ul>'. implode("",$list) .'</ul>
			</div>';
	}


	return '<div class="bittrex_percentage_distribution_of_the_total_price_diagram">'. $result_html .'</div>';
}


add_shortcode('osszeallitasok_usd_alapjan', 'osszeallitasok_usd_alapjan_sc_function');
function osszeallitasok_usd_alapjan_sc_function() {
	global $wpdb;
	$result_html = "";
	if ( !is_user_logged_in() ) { return ""; }

	$coins_array = $wpdb->get_results( "SELECT `ID`, `coin_ID`, `rank`, `price_usd`, `market_cap_usd`, `volume_usd_24h`, `datetime`
																			FROM `{$wpdb->prefix}crypto_coinmarketcap_data`
																			ORDER BY `datetime` DESC
																			LIMIT 150", ARRAY_A );

	// sorbarendezés rank szerint
	array_sort_by_column($coins_array, 'rank');
	$coins_array = array_slice($coins_array, 0, 25);


	foreach ($coins_array as $key => $data) {
		$coin_ID = (int) $coins_array[$key]['coin_ID'];

		$cmc_average_usd_price_range_3D = get_cmc_avg_price_range($coin_ID, 'USD', 12, strtotime("-3 day"));
		$cmc_average_usd_price_range_2W = get_cmc_avg_price_range($coin_ID, 'USD', 12, strtotime("-2 week"));
		$cmc_average_usd_price_range_1M = get_cmc_avg_price_range($coin_ID, 'USD', 12, strtotime("-1 month"));
		$cmc_average_usd_price_range_2M = get_cmc_avg_price_range($coin_ID, 'USD', 12, strtotime("-2 month"));
		$cmc_average_usd_price_range_3M = get_cmc_avg_price_range($coin_ID, 'USD', 12, strtotime("-3 month"));
		$cmc_average_usd_price_range_6M = get_cmc_avg_price_range($coin_ID, 'USD', 10, strtotime("-6 month"));
		$cmc_average_usd_price_range_1Y = get_cmc_avg_price_range($coin_ID, 'USD', 10, strtotime("-12 month"));


		$formatter = new NumberFormatter('en_US',  NumberFormatter::CURRENCY);
		$coins_array[$key]['last_price_origin_format'] = $data['price_usd'];
		$coins_array[$key]['last_price'] = $formatter->formatCurrency($data['price_usd'], 'USD');

		$coins_array[$key]['coin_ID'] = $coin_ID;
		$coins_array[$key]['link'] = get_coin_link($coin_ID);
		$coins_array[$key]['logo_img'] = get_coin_logo_img($coin_ID);

		$coins_array[$key]['price_change_in_percent_3D'] 	= $cmc_average_usd_price_range_3D['compare_current_price'];
		$coins_array[$key]['price_range_HTML_3D'] 				= $cmc_average_usd_price_range_3D['html'];
		$coins_array[$key]['price_change_in_percent_2W'] 	= $cmc_average_usd_price_range_2W['compare_current_price'];
		$coins_array[$key]['price_range_HTML_2W'] 				= $cmc_average_usd_price_range_2W['html'];
		$coins_array[$key]['price_change_in_percent_1M'] 	= $cmc_average_usd_price_range_1M['compare_current_price'];
		$coins_array[$key]['price_range_HTML_1M'] 				= $cmc_average_usd_price_range_1M['html'];
		$coins_array[$key]['price_change_in_percent_2M'] 	= $cmc_average_usd_price_range_2M['compare_current_price'];
		$coins_array[$key]['price_range_HTML_2M'] 				= $cmc_average_usd_price_range_2M['html'];
		$coins_array[$key]['price_change_in_percent_3M'] 	= $cmc_average_usd_price_range_3M['compare_current_price'];
		$coins_array[$key]['price_range_HTML_3M'] 				= $cmc_average_usd_price_range_3M['html'];
		$coins_array[$key]['price_change_in_percent_6M'] 	= $cmc_average_usd_price_range_6M['compare_current_price'];
		$coins_array[$key]['price_range_HTML_6M'] 				= $cmc_average_usd_price_range_6M['html'];
		$coins_array[$key]['price_change_in_percent_1Y'] 	= $cmc_average_usd_price_range_1Y['compare_current_price'];
		$coins_array[$key]['price_range_HTML_1Y'] 				= $cmc_average_usd_price_range_1Y['html'];

		$coins_array[$key]['av_btc_price'] = get_cmc_average_usd_price($coin_ID);
		$coins_array[$key]['volume']['positive_negative'] = "todo";
		$coins_array[$key]['volume']['html'] = "todo";
		$coins_array[$key]['rating']['msg'] = "todo";
		$coins_array[$key]['oldest_data'] = "todo";
		$coins_array[$key]['updated'] = strtotime($data['datetime']);
	}


	$result_html .=
		'<h4 class="sc_title">Összeállítások <small>- utolsó összeállítás: <b>'. '...' .'</b> | utolsó lekérés: <b>'. timeAgo(strtotime($cmc_latest_update[0]['datetime'])) .'</b></small> <a href="'. get_permalink(PAGE_OSSZEALLITASOK) .'?crypto_statistic_manual_update=true" class="btn-primary btn-sm crypto_statistic_manual_update_btn">Manual update</a></h4>
		<input type="hidden" id="last_compilation_update" value="'. '' .'" />

		<table id="crypto_statistic_v2_sc" class="table table-bordered">
		   <tbody>
		      <tr>
						<th>Rank</th>
						<th>Logo</th>
						<th>Coin name</th>
						<th>Last price in USD</th>

						<th>3D</th>
						<th>2W</th>
						<th>1M</th>
						<th>2M</th>
						<th>3M</th>
						<th>6M</th>
						<th>1Y</th>

						<th>Average USD price <br>2M</th>
						<th>Volume USD in 24h</th>
						<th>Last updated</th>
		      </tr>';

	$tr = "";

	foreach ($coins_array as $coin_name => $coin_data) {
		if ( is_numeric($coin_name) ) { $coin_name = get_coin_name_by_id($coin_data['coin_ID']); }

		$shitcoin_class = "";
		if ( check_is_shitcoin_by_coin_name($coin_name) ) { $shitcoin_class = "shitcoin"; }


		$coin_full_name = get_coin_full_name_by_id($coin_data['coin_ID']);
		if ( !empty($coin_full_name) && ( strtolower($coin_name) != strtolower($coin_full_name) ) ) {
			$coin_full_name = '<small>('. $coin_full_name .')</small>';
		} else { $coin_full_name = ""; }


		$technical_analysis = get_crypto_technical_analysis($coin_data['coin_ID']);


		$tr .=
		      '<tr id="'. strtolower($coin_name) .'" data-coin_name="'. $coin_name .'" data-coin_id="'. $coin_data['coin_ID'] .'" class="expensive veteran_coin more_info_is_filled">
						<td>'. $coin_data['rank'] .'</td>
						<td>'. $coin_data['logo_img'] .'</td>
						<td><a target="_blank" href="'. $coin_data['link'] .'">'. $coin_name . $coin_full_name .'</a></td>
						<td class="coinmarketcap">'. $coin_data['last_price'] .'</td>

						<td class="av_usd_price_range">'. $coin_data['price_range_HTML_3D'] .'<span>'. $coin_data['price_change_in_percent_3D'] .'</span></td>
						<td class="av_usd_price_range">'. $coin_data['price_range_HTML_2W'] .'<span>'. $coin_data['price_change_in_percent_2W'] .'</span></td>
						<td class="av_usd_price_range">'. $coin_data['price_range_HTML_1M'] .'<span>'. $coin_data['price_change_in_percent_1M'] .'</span></td>
						<td class="av_usd_price_range">'. $coin_data['price_range_HTML_2M'] .'<span>'. $coin_data['price_change_in_percent_2M'] .'</span></td>
						<td class="av_usd_price_range">'. $coin_data['price_range_HTML_3M'] .'<span>'. $coin_data['price_change_in_percent_3M'] .'</span></td>
						<td class="av_usd_price_range">'. $coin_data['price_range_HTML_6M'] .'<span>'. $coin_data['price_change_in_percent_6M'] .'</span></td>
						<td class="av_usd_price_range">'. $coin_data['price_range_HTML_1Y'] .'<span>'. $coin_data['price_change_in_percent_1Y'] .'</span></td>

						<td>'. number_format($coin_data['av_btc_price'],2) .'</td>
						<td data-volume_change="'. $coin_data['volume']['positive_negative'] .'">'. $coin_data['volume']['html'] .'</td>
						<td>'. timeAgo($coin_data['updated']) .'</td>
					</tr>

		      <tr class="more_info expensive">
		         <td colspan="14">

								<div class="col-sm-6 technical-analysis">
		               <div class="">
		                  <table class="table table-bordered">
		                     <tbody>
		                        <tr>
		                           <th colspan="5">Technical Analysis</th>
		                        </tr>
		                        <tr>
		                           <th>Type</th>
		                           <th>T.I. BUYS</th>
		                           <th>T.I. SELLS</th>
 		                           <th>M.A. BUYS</th>
															 <th>M.A. SELLS</th>
		                        </tr>
		                        <tr>
															 <td><a href="https://www.investing.com/crypto/'. sanitize_title($coin_full_name) .'/'. sanitize_title($coin_name) .'-btc-technical" target="_blank">BTC</a></td>
															 <td class="buy-type '. $technical_analysis['btc_data']['technical_indic_buy_class'] .'">'. $technical_analysis['btc_data']['technical_indic_buy'] .'</td>
		                           <td class="sell-type '. $technical_analysis['btc_data']['technical_indic_sell_class'] .'">'. $technical_analysis['btc_data']['technical_indic_sell'] .'</td>
		                           <td class="buy-type '. $technical_analysis['btc_data']['moving_avg_buy_class'] .'">'. $technical_analysis['btc_data']['moving_avg_buy'] .'</td>
															 <td class="sell-type '. $technical_analysis['btc_data']['moving_avg_sell_class'] .'">'. $technical_analysis['btc_data']['moving_avg_sell'] .'</td>
		                        </tr>
														<tr>
															 <td><a href="https://www.investing.com/crypto/'. sanitize_title($coin_full_name) .'/'. sanitize_title($coin_name) .'-usd-technical" target="_blank">USD</a></td>
															 <td class="buy-type '. $technical_analysis['usd_data']['technical_indic_buy_class'] .'">'. $technical_analysis['usd_data']['technical_indic_buy'] .'</td>
		                           <td class="sell-type '. $technical_analysis['usd_data']['technical_indic_sell_class'] .'">'. $technical_analysis['usd_data']['technical_indic_sell'] .'</td>
		                           <td class="buy-type '. $technical_analysis['usd_data']['moving_avg_buy_class'] .'">'. $technical_analysis['usd_data']['moving_avg_buy'] .'</td>
															 <td class="sell-type '. $technical_analysis['usd_data']['moving_avg_sell_class'] .'">'. $technical_analysis['usd_data']['moving_avg_sell'] .'</td>
		                        </tr>
														<tr class="'. $technical_analysis['btc_data']['technical_indic_summary_class'] .'">
															<td colspan="5">
																'. $technical_analysis['btc_data']['technical_indic_summary'] .'
																<br>'. get_technical_indicator_how_long(intval($coin_data['coin_ID'])) .'
															</td>
														</tr>
		                     </tbody>
		                  </table>
		               </div>
		            </div>

								<div class="col-sm-6">
		               <div class="">
									 		'. get_cryp_markets_book_orders_diagram($coin_name) .'
		               </div>
		            </div>

								<div class="clearfix"></div><br>

								<div class="col-sm-6">
		               <div class="">
									 		'. get_cmc_usd_percentage_distribution_of_the_total_price_diagram(intval($coin_data['coin_ID']), $coin_data['last_price_origin_format']) .'
		               </div>
		            </div>

								<div class="clearfix"></div><br>

		         </td>
		      </tr>';
	}

	$result_html .= $tr;
	$result_html .=
	   '</tbody>
	</table>';

	return $result_html;
}
