<?php

add_shortcode('daily_sales_and_purchase', 'daily_sales_and_purchase_function2');
function daily_sales_and_purchase_function2($force_update = "") {
	global $wpdb;
	$return_html = "";
	if ( !is_user_logged_in() ) { return ""; }

	$cmc_days = $wpdb->get_results( "SELECT DISTINCT `datetime`
																		FROM `{$wpdb->prefix}crypto_coinmarketcap_data`
																		WHERE `datetime` >= '". date('Y-m-d H:i:s', DAILY_SALES_AND_PURCHASE_INTERVAL_TIMESTAMP) ."'
																		ORDER BY `datetime` ASC", ARRAY_A );

	$cmc_days_temp = array();
	foreach ($cmc_days as $key => $val) {
		$cmc_days_temp[]= date('Y-m-d', strtotime($val['datetime']));
	}
	$cmc_days = array_values(array_filter(array_unique($cmc_days_temp)));
	unset($cmc_days_temp);

	$days_results = array();
	foreach ($cmc_days as $key => $day) {

		$day_start 	= $day .' 00:00:00';
		$day_end 		= $day .' 23:59:59';

		// Adott nap adatainak lekérése

		$cmc_percent_change_24h = $wpdb->get_results( "SELECT AVG(`percent_change_24h`) AS `percent_change_24h`
																										FROM `{$wpdb->prefix}crypto_coinmarketcap_data`
																										WHERE `datetime` >= '{$day_start}'
																											AND `datetime` < '{$day_end}'
																										ORDER BY `datetime` ASC", ARRAY_A );

		$cmc_positives = $wpdb->get_results( "SELECT COUNT(`percent_change_24h`) AS `positives`
																										FROM `{$wpdb->prefix}crypto_coinmarketcap_data`
																										WHERE `datetime` >= '{$day_start}'
																											AND `datetime` < '{$day_end}'
																											AND `percent_change_24h` NOT LIKE '-%'
																										ORDER BY `datetime` ASC", ARRAY_A );

		$cmc_negatives = $wpdb->get_results( "SELECT COUNT(`percent_change_24h`) AS `negatives`
																										FROM `{$wpdb->prefix}crypto_coinmarketcap_data`
																										WHERE `datetime` >= '{$day_start}'
																											AND `datetime` < '{$day_end}'
																											AND `percent_change_24h` LIKE '-%'
																										ORDER BY `datetime` ASC", ARRAY_A );

		if ( $cmc_positives[0]["positives"] > $cmc_negatives[0]["negatives"] ) { $positive_negative = "+"; }
		else if ( $cmc_positives[0]["positives"] < $cmc_negatives[0]["negatives"] ) { $positive_negative = "-"; }
		else { $positive_negative = "="; }

		$positive = ($cmc_positives[0]["positives"] * 100) / ($cmc_positives[0]["positives"] + $cmc_negatives[0]["negatives"]);
		$negative = ($cmc_negatives[0]["negatives"] * 100) / ($cmc_positives[0]["positives"] + $cmc_negatives[0]["negatives"]);


		$days_results[strtotime($day_start)] = array(
																								'positive_negative' => $positive_negative,
																								'daily_percent_change' => $cmc_percent_change_24h[0]["percent_change_24h"],
																								'positive' => round($positive,1),
																								'negative' => round($negative,1),
																							 );
	}

	$li = "";
	$positive_negative_array = array();
	$weekly_average = array();
	foreach ($days_results as $day_start => $daily_values) {

		if ( date('d', $day_start) == 1 ) {
			$li .= '<div class="new_month"></div>';
		}

		$positive_negative_array []= $daily_values["positive_negative"];
		$weekly_average[date('D', $day_start)][] = $daily_values["positive_negative"];

		$li .=
			'<li data-day="'. $daily_values["positive_negative"] .'">
				 <span class="val">'. $daily_values["positive_negative"] .'</span>
				 <span class="date">'. date('m-d', $day_start) .' <small>'. date_i18n('D', $day_start) .'</small></span>
				 <span class="daily_percent_change">'. round($daily_values["daily_percent_change"],1) .'%</span>
				 <span class="positive_negative">
					 <span class="positive">'. $daily_values["positive"] .' /</span>
					 <span class="negative">'. $daily_values["negative"] .'</span>
				 </span>
			</li>';
	}

	$positive_negative_array = array_count_values($positive_negative_array);
	$positive_negative_array = 'P: '. $positive_negative_array["+"] .' | N: '. $positive_negative_array["-"];


	// weekly_average
	$weekly_average_max_value = array();
	foreach ($weekly_average as $key => $val) {
		$val = array_count_values($val);
		$pos = 0;
		$neg = 0;

		if ( isset($val['+']) ) { $pos = intval($val['+']); }
		if ( isset($val['-']) ) { $neg = intval($val['-']); }
		if ( isset($val['=']) ) { $neg += intval($val['=']); }

		$weekly_average[$key] = round(($pos * 100) / ($pos + $neg));
		$weekly_average_max_value []= $weekly_average[$key];
	}

	arsort($weekly_average_max_value);
	$weekly_average_max_value = array_values(array_filter(array_unique($weekly_average_max_value)));
	$weekly_average_max_value = array_sum($weekly_average_max_value);


	$weekly_average = 'H: '. round($weekly_average['Mon'] * 100 / $weekly_average_max_value) .'%, '.
										'K: '. round($weekly_average['Tue'] * 100 / $weekly_average_max_value) .'%, '.
										'Sz: '. round($weekly_average['Wed'] * 100 / $weekly_average_max_value) .'%, '.
										'Cs: '. round($weekly_average['Thu'] * 100 / $weekly_average_max_value) .'%, '.
										'P: '. round($weekly_average['Fri'] * 100 / $weekly_average_max_value) .'%, '.
										'Sz: '. round($weekly_average['Sat'] * 100 / $weekly_average_max_value) .'%, '.
										'V: '. round($weekly_average['Sun'] * 100 / $weekly_average_max_value) .'%';
	// /weekly_average


	$daily_sales_and_purchase_conditions =
		'<ul class="daily_sales_and_purchase_conditions">
			'. $li .'
			<div class="new_month"></div>
			<div class="clearfix"></div>
		</ul>';



	$days_result_indicator = array();
	$pre_days_result_indicator = array();
	$pre_pre_days_result_indicator = array();
	foreach ($days_results as $day_start => $daily_values) {

		// actual
		if ( $day_start >= strtotime(date('Y-m-d 00:00:01', strtotime( '-21 days' ))) ) {
			$days_result_indicator []= $daily_values["daily_percent_change"];
		}

		// pre days
		if ( 	($day_start >= strtotime(date('Y-m-d 00:00:01', strtotime( '-22 days' )))) &&
					($day_start < strtotime(date('Y-m-d 00:00:00'))) ) {

			$pre_days_result_indicator []= $daily_values["daily_percent_change"];
		}

		// pre pre days
		if ( 	($day_start >= strtotime(date('Y-m-d 00:00:01', strtotime( '-23 days' )))) &&
					($day_start < strtotime(date('Y-m-d 00:00:00', strtotime( '-1 days' ))) )
				) {

			$pre_pre_days_result_indicator []= $daily_values["daily_percent_change"];
		}
	}
	unset($days_results);
	$days_result_indicator = round(array_sum($days_result_indicator),2);

	/*
	echo '1<br>';
	var_dump(date('Y-m-d 00:00:01', strtotime( '-21 days' )));
	echo '<br>';
	var_dump(date('Y-m-d 00:00:01', strtotime( '-22 days' ))) .' - '. var_dump(date('Y-m-d 00:00:00'));
	echo '<br>';
	var_dump(date('Y-m-d 00:00:01', strtotime( '-23 days' ))) .' - '. var_dump(date('Y-m-d 00:00:00', strtotime( '-1 days' )));
	die;*/

	$days_result_indicator_class = "negative";
	if ( $days_result_indicator > 0 ) { $days_result_indicator_class = "positive"; }

	$days_result_indicator =
		'<div data-cdansjk="'. strtotime(date('Y-m-d 00:00:00', strtotime( '-21 days' ))) .'" class="days_result_indicator '. $days_result_indicator_class .' ">
				<div class="line" style="width: calc( '. abs($days_result_indicator) .'% / 2 );">
					<span>'. round(array_sum($pre_pre_days_result_indicator),2) .'% → '. round(array_sum($pre_days_result_indicator),2) .'% → '. $days_result_indicator .'%</span>
				</div>
		 </div>';



	$return_html =
			'<h4 class="sc_title">Napi állapotok</h4>
			<div class="col-xs-6">
				<div class="row">
					'. check_buy_or_sell() .'
					<div class="clearfix"></div>
					'. get_technical_indicator_summary() .'
					<div class="clearfix"></div>
					'. get_technical_indicator_summary(BTC_ID) .'
					<div class="clearfix"></div>
					'. get_technical_indicator_summary(ETH_ID) .'
					<div class="clearfix"></div>
					'. get_technical_indicator_summary(DASH_ID) .'
					<div class="clearfix"></div>
					'. get_technical_indicator_summary(XMR_ID) .'
					<div class="clearfix"></div>
					'. get_technical_indicator_summary(LTC_ID) .'
					<div class="clearfix"></div>
					'. get_technical_indicator_summary(XRP_ID) .'
				</div>
			</div>
			<div class="col-xs-3 col-xs-offset-3">
				<div class="row">
					'. get_btc_movement() .'
				</div>
			</div>

			<div class="clearfix"></div>
			'. $days_result_indicator .'

			<div class="clearfix"></div>
			'. get_daily_sales_and_purchase_conditions_by_coin_id( BTC_ID ) .'

			<div class="clearfix"></div>
			'. $daily_sales_and_purchase_conditions .'

			<div class="clearfix"></div>
			'. $positive_negative_array .'
			<br>
			'. $weekly_average .'
			<div class="clearfix"></div><br>

			<button type="button" class="btn-primary btn-sm daily_sales_and_purchase_conditions_btn">Bővebben</button>';

	unset($cmc_days, $days_result_indicator, $positive_negative_array);
	return $return_html;
}
