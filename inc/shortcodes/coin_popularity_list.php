<?php

add_shortcode('coin_popularity_list', 'coin_popularity_list_sc_function');
function coin_popularity_list_sc_function($force_update = "") {
	global $wpdb;
	$return_html = "";

	$cc_socialstats_coins = $wpdb->get_results( "SELECT DISTINCT `coin_ID`
																							 FROM `{$wpdb->prefix}crypto_cryptocompare_socialstats` ", ARRAY_A );

	$latest_cc_socialstats_coin_date = $wpdb->get_results( "SELECT `datetime`
																										 			FROM `{$wpdb->prefix}crypto_cryptocompare_socialstats`
																													ORDER BY `datetime` DESC LIMIT 1 ", ARRAY_A );
	$latest_cc_socialstats_coin_date = strtotime($latest_cc_socialstats_coin_date[0]['datetime']);

	$bittrex_coins = get_bittrex_coins();

	$coins_array = array();
	foreach ($cc_socialstats_coins as $key => $value) {
		if ( in_array($value['coin_ID'], $bittrex_coins) ) {
			$coins_array []= (int) $value['coin_ID'];
		}
	}


	$cc_socialstats_compilation_array = array();
	foreach ($coins_array as $key => $cc_socialstats_coin_id) {

		$row_data = $wpdb->get_results( "SELECT *
																		 FROM `{$wpdb->prefix}crypto_cryptocompare_socialstats`
																		 WHERE `coin_ID` = '{$cc_socialstats_coin_id}'
																		 ORDER BY `datetime` DESC LIMIT 1", ARRAY_A );
		$row_data = $row_data[0];

		$code_repo_created_at = "";
		$code_repo_created_at_time_ago = "";
		if ( !empty($row_data['code_repo_created_at']) ) {
			$code_repo_created_at = $row_data['code_repo_created_at'];
			$code_repo_created_at_time_ago = timeago($row_data['code_repo_created_at']);

			$coin_created_date = get_coin_created_date_by_id($cc_socialstats_coin_id);
			if ( !empty($coin_created_date) && $coin_created_date != "#" && ( $row_data['code_repo_created_at'] > $coin_created_date ) ) {
				$code_repo_created_at = $coin_created_date;
				$code_repo_created_at_time_ago = timeago($coin_created_date);
			}

		} else {
			$code_repo_created_at = get_coin_created_date_by_id($cc_socialstats_coin_id);
			if ( !empty($code_repo_created_at) && $code_repo_created_at != "#" ) {
				$code_repo_created_at_time_ago = timeago($code_repo_created_at);
			}
		}

		$cc_socialstats_compilation_array []=
				array(
					'coin_id' => $cc_socialstats_coin_id,
					'coin_name' => get_coin_name_by_id($cc_socialstats_coin_id),
					'coin_rank' => get_coin_rank_by_coin_id($cc_socialstats_coin_id),

					'cryptopian_followers' => $row_data['cryptopian_followers'],
					'cryptopian_posts' => $row_data['cryptopian_posts'],
					'cryptopian_comments' => $row_data['cryptopian_comments'],

					'twitter_link' => $row_data['twitter_link'],
					'twitter_followers' => $row_data['twitter_followers'],
					'twitter_favourites' => $row_data['twitter_favourites'],

					'reddit_link' => $row_data['reddit_link'],
					'reddit_posts_per_day' => $row_data['reddit_posts_per_day'],
					'reddit_comments_per_day' => $row_data['reddit_comments_per_day'],
					'reddit_subscribers' => $row_data['reddit_subscribers'],

					'facebook_link' => $row_data['facebook_link'],
					'facebook_likes' => $row_data['facebook_likes'],

					'code_repo_created_at' => $code_repo_created_at,
					'code_repo_created_at_time_ago' => $code_repo_created_at_time_ago,
					'code_repo_last_update' => $row_data['code_repo_last_update'],
					'code_repo_last_update_time_ago' => timeago($row_data['code_repo_last_update']),
					'code_repo_forks' => $row_data['code_repo_forks'],
					'code_repo_url' => $row_data['code_repo_url'],
					'code_repo_closed_issues' => $row_data['code_repo_closed_issues'],
					'code_repo_closed_pull_issues' => $row_data['code_repo_closed_pull_issues'],
					'code_repo_last_push' => $row_data['code_repo_last_push'],
					'code_repo_last_push_time_ago' => timeago($row_data['code_repo_last_push']),
					'code_repo_language' => $row_data['code_repo_language'],
				);
	}

	$winners = array();
	$conditions_for_win = array(
															'cryptopian_followers',
															'cryptopian_posts',
															'cryptopian_comments',
															'twitter_followers',
															'twitter_favourites',
															'reddit_posts_per_day',
															'reddit_comments_per_day',
															'reddit_subscribers',
															'facebook_likes',
															'code_repo_created_at',
															'code_repo_last_update',
															'code_repo_forks',
															'code_repo_closed_issues',
															'code_repo_closed_pull_issues',
															'code_repo_last_push',
														);

	foreach ($conditions_for_win as $key => $keyname) {
		$order_by = $cc_socialstats_compilation_array;

		if ( $keyname == 'code_repo_created_at' ) {
			array_sort_by_column($order_by, $keyname, SORT_ASC);
		} else {
			array_sort_by_column($order_by, $keyname, SORT_DESC);
		}

		foreach ($order_by as $key => $value) {
			foreach ($order_by[$key] as $key2 => $value2) {
				if ( ($key2 == $keyname) && empty($value2) ) {
					unset($order_by[$key]);
					break;
				}
			}
		}

		$order_by = array_slice($order_by, 0, 10);

		$order_by_temp = array();
		foreach ($order_by as $key => $val) {
			$order_by_temp []= $val['coin_name'];
		}
		$winners[$keyname]= $order_by_temp;
		unset($order_by_temp, $order_by);
	}

	$winners_temp = array();
	foreach ($winners as $key => $value) {
		foreach ($winners[$key] as $key2 => $value2) {
			$winners_temp []= $value2;
		}
	}
	$winners = $winners_temp;
	unset($winners_temp);
	$winners = array_count_values($winners);
	arsort($winners);

	$winners_temp = array();
	foreach ($winners as $coin_symbol => $value) {
		$winners_temp []= $coin_symbol;
	}
	$winners = $winners_temp;
	unset($winners_temp);
	$winners = array_values(array_filter(array_unique($winners)));
	$calculated_ranks = array_flip($winners);
	$winners = array_slice($winners, 0, 25);

	update_option( 'crypto_coin_popularity_list_winners', $winners, false );
	if ( $force_update == 'update' ) { return ""; }


	if ( !empty($calculated_ranks) ) {
		$max_calculated_ranks = array();
		foreach ($calculated_ranks as $key => $value) { $max_calculated_ranks []= (int) $value; }
		arsort($max_calculated_ranks);
		foreach ($max_calculated_ranks as $key => $value) { $max_calculated_ranks = $value; break; }

		$i = 1;
		foreach ($calculated_ranks as $coin_name => $num) {
			$calculated_ranks[$coin_name] = $i;
			$i++;
		}
	}


	$tr = array();
	foreach ($cc_socialstats_compilation_array as $key => $val) {

		$top_coin_class = "";
		if ( in_array($val['coin_name'], $winners) ) { $top_coin_class = 'top_coin'; }

		$cr_link1 = "";
		$cr_link2 = "";
		if ( !empty($val['code_repo_url']) ) {
			$cr_link1 = '<a href="'. $val['code_repo_url'] .'" target="_blank">';
			$cr_link2 = '</a>';
		}

		$twitter_link1 = "";
		$twitter_link2 = "";
		if ( !empty($val['twitter_link']) ) {
			$twitter_link1 = '<a href="'. $val['twitter_link'] .'" target="_blank">';
			$twitter_link2 = '</a>';
		}

		$fb_link1 = "";
		$fb_link2 = "";
		if ( !empty($val['facebook_link']) ) {
			$fb_link1 = '<a href="'. $val['facebook_link'] .'" target="_blank">';
			$fb_link2 = '</a>';
		}

		$reddit_link1 = "";
		$reddit_link2 = "";
		if ( !empty($val['reddit_link']) ) {
			$reddit_link1 = '<a href="'. $val['reddit_link'] .'" target="_blank">';
			$reddit_link2 = '</a>';
		}

		$coin_full_name = get_coin_full_name_by_id($val['coin_id']);
		if ( !empty($coin_full_name) && ( strtolower($val['coin_name']) != strtolower($coin_full_name) ) ) {
			$coin_full_name = '<small>('. $coin_full_name .')</small>';
		} else { $coin_full_name = ""; }

		$calculated_ranks_data_value = intval(1000 - $calculated_ranks[$val['coin_name']]);
		if ( empty($calculated_ranks[$val['coin_name']]) ) { $calculated_ranks_data_value = 0; }

		$tr []=
			'<tr class="'. $top_coin_class .'">'.
			 '<td>'. $val['coin_rank'] .'</td>
				<td>'. $cr_link1. $val['coin_name'] .$coin_full_name .$cr_link2 .'</td>
				<td data-sort="'. $calculated_ranks_data_value .'">'. $calculated_ranks[$val['coin_name']] .'</td>

				<td>'. $val['cryptopian_followers'] .'</td>
				<td>'. $val['cryptopian_posts'] .'</td>
				<td>'. $val['cryptopian_comments'] .'</td>

				<td>'. $twitter_link1. $val['twitter_followers'] .$twitter_link2 .'</td>
				<td>'. $twitter_link1. $val['twitter_favourites'] .$twitter_link2 .'</td>

				<td>'. $reddit_link1. $val['reddit_posts_per_day'] .$reddit_link2 .'</td>
				<td>'. $reddit_link1. $val['reddit_comments_per_day'] .$reddit_link2 .'</td>
				<td>'. $reddit_link1. $val['reddit_subscribers'] .$reddit_link2 .'</td>

				<td>'. $fb_link1. $val['facebook_likes'] .$fb_link2 .'</td>

				<td data-sort="'. $val['code_repo_created_at'] .'">'. $val['code_repo_created_at_time_ago'] .'</td>
				<td data-sort="'. $val['code_repo_last_update'] .'">'. $val['code_repo_last_update_time_ago'] .'</td>
				<td>'. $val['code_repo_forks'] .'</td>
				<td>'. $val['code_repo_closed_issues'] .'</td>
				<td>'. $val['code_repo_closed_pull_issues'] .'</td>
				<td data-sort="'. $val['code_repo_last_push'] .'">'. $val['code_repo_last_push_time_ago'] .'</td>
				<td>'. $val['code_repo_language'] .'</td>'.
			'</tr>';
	}


	$return_html = '
	<table id="coin_popularity_table" class="table">
	  <thead>
	    <tr>
				<th>Rank</th>
				<th>Coin Name</th>
				<th data-sort-default="">Calculated Rank</th>

				<th>Cryptopian<br> Followers</th>
				<th>Cryptopian<br> Posts</th>
				<th>Cryptopian<br> Comments</th>

				<th>Twitter<br> Followers</th>
				<th>Twitter<br> Favourites</th>

				<th>Reddit<br> Posts per day</th>
				<th>Reddit<br> Comments per day</th>
				<th>Reddit<br> Subscribers</th>

				<th>Facebook<br> Likes</th>

				<th>Code Repo<br> Created date</th>
				<th>Code Repo<br> Last update</th>
				<th>Code Repo<br> Forks</th>
				<th>Code Repo<br> Closed issues</th>
				<th>Code Repo<br> Pull issues</th>
				<th>Code Repo<br> Last push</th>
				<th>Code Repo<br> Lang</th>
	    </tr>
	  </thead>
	  <tbody>
			'. implode('', $tr) .'
	  </tbody>
	</table>';

	return '<div class="coin_popularity_list_sc">'.
					'<h4 class="sc_title">Népszerűség és fejlesztői adatok <small>- utolsó lekérés: <b>'. timeAgo($latest_cc_socialstats_coin_date) .'</b></small></h4>'.
					'<div class="top_coins">'. implode(', ', $winners) .'</div><br>'.
					$return_html.
				 '</div>';
}
