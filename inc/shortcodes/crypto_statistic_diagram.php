<?php

add_shortcode('crypto_statistic_diagram', 'crypto_statistic_diagram_function');
function crypto_statistic_diagram_function() {
	global $wpdb;
	$return_html = "";
	if ( !is_user_logged_in() ) { return ""; }

	$sql = "SELECT `{$wpdb->prefix}crypto_compilation`.`coin_ID`, `{$wpdb->prefix}crypto_compilation`.`actual_rank` AS `rank`, `{$wpdb->prefix}crypto_compilation`.`price_change_in_percent`, `{$wpdb->prefix}crypto_compilation`.`bittrex_price_change_in_percent`
					FROM `{$wpdb->prefix}crypto_compilation`
					LEFT JOIN `{$wpdb->prefix}crypto_coin_names` ON `{$wpdb->prefix}crypto_compilation`.`coin_ID` = `{$wpdb->prefix}crypto_coin_names`.`ID`
					ORDER BY `{$wpdb->prefix}crypto_compilation`.`actual_rank` ASC";
	$crypto_compilation = $wpdb->get_results( $sql, ARRAY_A );

	$datasets = array();
	foreach ($crypto_compilation as $key => $data) {
		if ( check_coin_is_on_bittrex($data['coin_ID']) ) {
			$coin_name = get_coin_name_by_id($data['coin_ID']);

			if ( !check_is_shitcoin_by_coin_name($coin_name) ) {
				$price_change_in_percent = str_replace('%', '', $data['price_change_in_percent']);
				if ( !empty($data['bittrex_price_change_in_percent']) ) {
					$price_change_in_percent = str_replace('%', '', $data['bittrex_price_change_in_percent']);
				}

				if ( $price_change_in_percent <= -15 ) { $coin_color = 'rgba(181, 76, 51, 1)'; }
				else if ( $price_change_in_percent > -16 && $price_change_in_percent <= 0 ) { $coin_color = 'rgba(195, 116, 99, 1)'; }
				else { $coin_color = 'rgba(56, 150, 88, 1)'; }

				$coin_border_color = $coin_color;
				if ( check_coin_is_mineable($data['coin_ID']) ) {
					$coin_border_color = 'rgba(0,0,0,1)';
				}

				$datasets []=
											'{
												label: "'. $coin_name .' - ('. $data['rank'] .')",
												backgroundColor: "'. $coin_color .'",
												borderColor: "'. $coin_border_color .'",
												borderWidth: 3,
												data: ['. $price_change_in_percent .'],
											}';
			}
		}
	}


	$result_html =
		'<h4 class="sc_title">Árfolyam %-os kilengések <small>- '. count($datasets) .' db</small></h4>'.
		'<canvas id="y3bj21bh"></canvas>
		<script type="text/javascript">
		jQuery(function($) {

			var barChartData = {
				labels: [],
				datasets: [
					{
						type: "line",
						label: "",
						borderColor: "rgba(255,0,0,1)",
						borderWidth: 3,
						fill: false,
						data: [15]
					},{
						type: "line",
						label: "",
						borderColor: "rgba(255,0,0,1)",
						borderWidth: 3,
						fill: false,
						data: [-15]
					},
					'. implode(', ', $datasets) .']
			};

			window.onload = function() {
					var ctx = document.getElementById("y3bj21bh").getContext("2d");
					window.myBar = new Chart(ctx, {
							type: "bar",
							data: barChartData,
							options: {
									responsive: true,
									legend: {
											position: "top",
									},
									title: {
											display: false,
											text: ""
									}
							}
					});
			};

		});
		</script>';

	return $result_html;
}
