<?php

function get_coin_cmc_historical_data($coin_name = "", $start_date = "") {
	$coin_fullname = $coin_name;
	$start_date = date('Ymd', strtotime($start_date));
	$end_date = date('Ymd', strtotime('now'));

	$html_content = file_get_contents( 'https://coinmarketcap.com/currencies/'. $coin_fullname .'/historical-data/?start='. $start_date .'&end='. $end_date );

	if (strpos($html_content, 'id="historical-data"') !== false) {
		$html_content = substr($html_content, strpos($html_content, 'id="historical-data"'));
		$html_content = substr($html_content, strpos($html_content, '<table'));
		$html_content = substr($html_content, 0, strpos($html_content, '</table>'));

		if (strpos($html_content, '</tr>') !== false) {
			$tr_array = explode('</tr>', $html_content);
			$results_array = array();

			foreach ($tr_array as $key => $value) {
				if (strpos($value, '<td') !== false) {
					$td_array = explode('</td>', $value);


					foreach ($td_array as $key => $html) {
						if ( $key == 0 ) {
							$td_array[$key] = substr($td_array[$key], strpos($td_array[$key], 'left">') + 6);
						}
						else if ( $key > 0 ) {
							$attr = parseAttributesFromTag($html);

							if ( isset($attr['value']) && !empty($attr['value']) ) {
								$td_array[$key] = $attr['value'];
							} else {
								$td_array[$key] = "";
							}
						}
					}

					if 	( !empty($td_array[0]) &&
								!empty($td_array[1]) &&
								!empty($td_array[2]) &&
								!empty($td_array[3]) &&
								!empty($td_array[4]) &&
								!empty($td_array[5]) &&
								!empty($td_array[6])
							) {

								$results_array []= array(
																					'date' => strtotime($td_array[0]),
																					'open' => $td_array[1],
																					'high' => $td_array[2],
																					'low' => $td_array[3],
																					'usd_price' => $td_array[4],
																					'volume' => $td_array[5],
																					'market_cap' => $td_array[6]
																				);
							}
				}
			}
			return $results_array;
		}
	}
	return "";
}


function parseAttributesFromTag($tag) {
	//The Regex pattern will match all instances of attribute="value"
	$pattern = '/(\w+)=[\'"]([^\'"]*)/';

	//preg_match_all used with the PREG_SET_ORDER flag will build an array
	//for each attribute-value pair present and put it in $matches. eg:
	/* with tag <answer scale='10' points="23">
	Array (
		[0] =>(
		    [0] => scale='10
		    [1] => scale
		    [2] => 10
		  )
		[1] => (
		    [0] => points="23
		    [1] => points
		    [2] => 23
		)
	)
	*/
	preg_match_all($pattern,$tag,$matches,PREG_SET_ORDER);

	$result = [];
	foreach($matches as $match){
		$attrName = $match[1];

		//parse the string value into an integer if it's numeric,
		// leave it as a string if it's not numeric,
		$attrValue = is_numeric($match[2]) ? floatval($match[2]) : trim($match[2]);
		$result[$attrName] = $attrValue; //add match to results
	}

	return $result;
}


function its_time_to_run($starting_minutes = "", $delayed_minutes = "") {
	if ( !is_array($starting_minutes) || empty($starting_minutes) ) { return FALSE; }
	if ( empty($delayed_minutes) ) { $delayed_minutes = 4 * 60; }
	else { $delayed_minutes = (int) $delayed_minutes * 60; }


	foreach ($starting_minutes as $key => $minute) {
		$minute = str_pad(intval($minute), 2, "0", STR_PAD_LEFT);

		$start_timestamp = strtotime(date('Y-m-d H:'. $minute .':00'));
		$now_timestamp = strtotime('NOW');
		$end_timestamp = $start_timestamp + $delayed_minutes;

		if 	( ($start_timestamp <= $now_timestamp) &&
					($now_timestamp < $end_timestamp)
				) {
					return TRUE;
					break;
				}
	}

	return FALSE;
}


function one_btc_mining_cost() {
	$html_content = file_get_contents( 'https://www.asicminervalue.com/' );

	if (strpos($html_content, 'datatable_profitability') !== false) {
		$html_content = substr($html_content, strpos($html_content, 'datatable_profitability'));
		$html_content = substr($html_content, 0, strpos($html_content, '</table>'));

		if (strpos($html_content, '</tr>') !== false) {
			$tr_array = explode('</tr>', $html_content);
			$results_array = array();
			$btc_usd_price = (float) get_cmc_usd_price(BTC_ID);

			foreach ($tr_array as $key => $value) {
				if (strpos($value, '<td') !== false) {
					$td_array = explode('</td>', $value);
					$td_array = array_values(array_filter(array_unique($td_array)));

					if ( count($td_array) == 7 ) {

						$quantity = $td_array[4];
						$algo = $td_array[5];
						$income_price = $td_array[6]; // /day
						$electricity = $td_array[6]; // /day


						$quantity = substr($quantity, strpos($quantity, 'data-sort="') + 11);
						$quantity = (int) substr($quantity, 0, strpos($quantity, '"'));

						$algo = substr($algo, strpos($algo, 'data-sort="') + 11);
						$algo = substr($algo, 0, strpos($algo, '"'));

						$income_price = substr($income_price, strpos($income_price, 'rentabilitylabel'));
						$income_price = substr($income_price, strpos($income_price, '$') + 1);
						$income_price = (float) substr($income_price, 0, strpos($income_price, '<'));

						$electricity = substr($electricity, strpos($electricity, '-$') + 2);
						$electricity = (float) substr($electricity, 0, strpos($electricity, '<'));

						$profit = $income_price - $electricity;


						if ( $algo == 'SHA-256' ) {
							if ( is_numeric($income_price) && is_numeric($electricity) ) {
								if ( $quantity > 0 ) {
									for ($i=1; $i <= $quantity; $i++) {
										$one_btc_cost = $btc_usd_price / $income_price;
										$one_btc_cost = $one_btc_cost * $electricity;

										if ( 	$one_btc_cost > ((45 / 100) * $btc_usd_price) &&
													$one_btc_cost < ((55 / 100) * $btc_usd_price) + $btc_usd_price ) {

											$results_array[$algo]['income_per_day'][] = $income_price;
											$results_array[$algo]['electricity_per_day'][] = $electricity;
											$results_array[$algo]['profit_per_day'][] = $profit;
											$results_array[$algo]['one_btc_electricity_cost'][] = $one_btc_cost;
											$results_array[$algo]['one_btc_electricity_cost_percent'][] = (($one_btc_cost * 100) / $btc_usd_price) - 100;
											$results_array[$algo]['one_btc_mining_profit'][] = $btc_usd_price - $one_btc_cost;
										}
									}
								}
							}
						}

					}
				}
			}


			// Calculating
			if ( isset($results_array['SHA-256']['income_per_day']) ) {

				//echo "<pre>"; var_dump($results_array['SHA-256']); echo "</pre>"; die;

				return array(
											'income_per_day' => range_of_most_frequently_occurring_values($results_array['SHA-256']['income_per_day']),
											'electricity_per_day' => range_of_most_frequently_occurring_values($results_array['SHA-256']['electricity_per_day']),
											'profit_per_day' => range_of_most_frequently_occurring_values($results_array['SHA-256']['profit_per_day']),
											'one_btc_electricity_cost' => range_of_most_frequently_occurring_values($results_array['SHA-256']['one_btc_electricity_cost']),
											'one_btc_electricity_cost_percent' => range_of_most_frequently_occurring_values($results_array['SHA-256']['one_btc_electricity_cost_percent']),
											'one_btc_mining_profit' => range_of_most_frequently_occurring_values($results_array['SHA-256']['one_btc_mining_profit']),

											'income_per_day_avg' => array_sum($results_array['SHA-256']['income_per_day']) / count($results_array['SHA-256']['income_per_day']),
											'electricity_per_day_avg' => array_sum($results_array['SHA-256']['electricity_per_day']) / count($results_array['SHA-256']['electricity_per_day']),
											'profit_per_day_avg' => array_sum($results_array['SHA-256']['profit_per_day']) / count($results_array['SHA-256']['profit_per_day']),
											'one_btc_electricity_cost_avg' => array_sum($results_array['SHA-256']['one_btc_electricity_cost']) / count($results_array['SHA-256']['one_btc_electricity_cost']),
											'one_btc_electricity_cost_percent_avg' => array_sum($results_array['SHA-256']['one_btc_electricity_cost_percent']) / count($results_array['SHA-256']['one_btc_electricity_cost_percent']),
											'one_btc_mining_profit_avg' => array_sum($results_array['SHA-256']['one_btc_mining_profit']) / count($results_array['SHA-256']['one_btc_mining_profit']),
										);
			}
		}
	}
	return "";
}


// Visszaadja a tömbben leggyakrabban előforduló értékeket
// array( 15.1561, 756.161456, 64456.23132 );
function range_of_most_frequently_occurring_values($values_array = "", $splitting = "") {
	if ( is_array($values_array) && !empty($values_array) ) {  }
	else { return ""; }

	asort($values_array);
	$min_value = array_values($values_array)[0];
	$max_value = array_values($values_array)[count($values_array) - 1];


	// Csoportosítások
	if ( (intval($splitting) > 1) && (intval($splitting) <= 30) ) { $splitting = intval($splitting); }
	else { $splitting = 12; }


	// Felosztás több részre
	$splitting_array = array();
	$one_unit = ($max_value - $min_value) / $splitting;
	$unit_counter = 0;

	for ($i=1; $i <= $splitting; $i++) {
		if ($i == 1) { $unit_counter = $min_value; }
		else { $unit_counter += $one_unit; }

		$min = $unit_counter;
		$max = $unit_counter + $one_unit;

		$quantity = 0;
		$values_array_temp = array();
		foreach ($values_array as $key => $num) {
			if ( ($num > $min) && ($num <= $max) ) {
				$quantity++;
				$values_array_temp []= $num;
				unset($values_array[$key]);
			}
		}

		$splitting_array []= array(
																'min' => $min,
																'max' => $max,
																'quantity' => intval($quantity),
																'values' => $values_array_temp,
															);
	}

	array_sort_by_column($splitting_array, 'quantity');
	$splitting_array = array_reverse($splitting_array);

	//echo "<pre>";var_dump($splitting_array[0]['values']);echo "</pre>";die;

	// ---

	if ( is_array($splitting_array) && isset($splitting_array[0]['min']) ) {
		return array(
									'min' => $splitting_array[0]['min'],
									'max'	=> $splitting_array[0]['max'],
									//'avg' => array_sum($splitting_array[0]['values']) / count($splitting_array[0]['values']),
									'avg' => ($splitting_array[0]['min'] + $splitting_array[0]['max']) / 2,
								);
	}
	return "";
}


function clear_cmc_avg_price_range_cache() {
	global $wpdb;
	$results = $wpdb->get_results( "SELECT `cache_ID`
																	FROM `{$wpdb->prefix}crypto_cmc_avg_price_range_cache`
																	WHERE `datetime` < '". date('Y-m-d H:i:s', strtotime('-3 hours')) ."' ", ARRAY_A );

	if ( !empty($results) ) {
		foreach ($results as $key => $value) {
			if ( isset($value['cache_ID']) && !empty($value['cache_ID']) ) {
				$wpdb->delete( $wpdb->prefix .'crypto_cmc_avg_price_range_cache', array('cache_ID' => $value['cache_ID']), array('%s') );
			}
		}
		return TRUE;
	}
	return FALSE;
}


function get_cmc_avg_price_range_cache($cache_ID = "") {
	if ( empty($cache_ID) ) { return ""; }
	global $wpdb;

	$result = $wpdb->get_row("SELECT `values`
														FROM `{$wpdb->prefix}crypto_cmc_avg_price_range_cache`
														WHERE `cache_ID` = '{$cache_ID}'
															AND `datetime` > '". date('Y-m-d H:i:s', strtotime('-3 hours')) ."'
															ORDER BY `datetime` DESC ", ARRAY_A);

	if ( isset($result['values']) && !empty($result['values']) ) {
		return unserialize(base64_decode($result['values']));
	}
	return "";
}


function set_cmc_avg_price_range_cache($cache_ID = "", $values = "") {
	if ( empty($cache_ID) ) { return FALSE; }
	if ( empty($values) ) { return FALSE; }
	global $wpdb;

	$result = $wpdb->get_row("SELECT `cache_ID`
														FROM `{$wpdb->prefix}crypto_cmc_avg_price_range_cache`
														WHERE `cache_ID` = '{$cache_ID}'
														ORDER BY `datetime` DESC ", ARRAY_A);

	if ( isset($result['cache_ID']) && !empty($result['cache_ID']) ) {
		$cache_ID = $result['cache_ID'];
		$wpdb->delete( $wpdb->prefix .'crypto_cmc_avg_price_range_cache', array( 'cache_ID' => $cache_ID ), array( '%s' ) );
	}

	$wpdb->insert(
		$wpdb->prefix .'crypto_cmc_avg_price_range_cache',
		array(
			'cache_ID' => $cache_ID,
			'values' => base64_encode(serialize($values)),
		),
		array('%s', '%s')
	);

	return TRUE;
}


function get_cmc_avg_price_range($coin_ID = "", $currency_type = "BTC", $splitting = "", $start_date = "") {
	global $wpdb;
	if ( intval($coin_ID) > 0 ) { $coin_ID = intval($coin_ID); }
	else { return ""; }
	$result = "";
	$currency_type_original = strtoupper($currency_type);
	$time_interval = time() - intval($start_date);

	// Rank limit
	if ( intval(get_last_rank_by_coin_id($coin_ID)) > 25 ) { return ""; }


	// Csoportosítások
	if ( (intval($splitting) > 1) && (intval($splitting) <= 30) ) { $splitting = intval($splitting); }
	else { $splitting = 12; }


	// Mennyire régi adatig legyen lekérve
	$start_date = intval($start_date);
	if ( ($start_date < strtotime('-6 hours')) && ($start_date > strtotime('-2 years')) ) {  }
	else { $start_date = TIME_INTERVAL_TIMESTAMP; }


	// Get The Cache
	$cache_ID = md5($coin_ID . $currency_type_original . $splitting . $time_interval);
	$cache_ID = substr($cache_ID, 0, 15);

	$cache_array = get_cmc_avg_price_range_cache($cache_ID);
	if ( !empty($cache_array) ) { return $cache_array; }
	// ---


	// Pénznem
	if ( $currency_type_original == 'USD' ) {
		$currency_type = 'price_usd';
		$actual_coin_price = (float) get_cmc_usd_price($coin_ID);
	}
	else {
		$currency_type = 'price_btc';
		$actual_coin_price = (float) get_cmc_btc_price($coin_ID);
	}


	// Find min & max value
	$min_price = $wpdb->get_results( "SELECT CAST( `{$currency_type}` AS DECIMAL(10,6) ) as `min_price`
																		FROM `{$wpdb->prefix}crypto_coinmarketcap_data`
																		WHERE `coin_ID` = '{$coin_ID}'
																			AND `datetime` >= '". date('Y-m-d H:i:s', $start_date) ."'
																		ORDER BY `min_price` ASC LIMIT 1", ARRAY_A );
	$min_price = (float) $min_price[0]['min_price'];

	$max_price = $wpdb->get_results( "SELECT CAST( `{$currency_type}` AS DECIMAL(10,6) ) as `max_price`
																		FROM `{$wpdb->prefix}crypto_coinmarketcap_data`
																		WHERE `coin_ID` = '{$coin_ID}'
																			AND `datetime` >= '". date('Y-m-d H:i:s', $start_date) ."'
																		ORDER BY `max_price` DESC LIMIT 1", ARRAY_A );
	$max_price = (float) $max_price[0]['max_price'];


	// Felosztás több részre
	$splitting_array = array();
	$one_unit = ($max_price - $min_price) / $splitting;
	$unit_counter = 0;

	for ($i=1; $i <= $splitting; $i++) {
		if ($i == 1) { $unit_counter = $min_price; }
		else { $unit_counter += $one_unit; }

		$min = $unit_counter;
		$max = $unit_counter + $one_unit;

		$quantity = $wpdb->get_results( "	SELECT COUNT(`{$currency_type}`) AS `quantity`
																			FROM `{$wpdb->prefix}crypto_coinmarketcap_data`
																			WHERE `coin_ID` = '{$coin_ID}'
																				AND `datetime` >= '". date('Y-m-d H:i:s', $start_date) ."'
																				AND `{$currency_type}` > {$min}
																				AND `{$currency_type}` <= {$max}
																				ORDER BY `{$currency_type}` DESC ", ARRAY_A);

		$splitting_array []= array(
																'min' => $min,
																'max' => $max,
																'quantity' => intval($quantity[0]['quantity']),
															);
	}

	array_sort_by_column($splitting_array, 'quantity');
	$splitting_array = array_reverse($splitting_array);

	/*
	if ( isset($_GET['devmode']) && $coin_ID == ETH_ID ) {
		var_dump( $wpdb->last_query );
		echo "<br>";
		var_dump( 'min_price: '. $min_price );
		echo "<br>";
		var_dump( 'max_price: '. $max_price );
		echo "<br>";
		echo "<pre>"; var_dump($splitting_array); die;
	}
	*/

	// ---

	if ( is_array($splitting_array) && isset($splitting_array[0]['min']) ) {
		$avg_price = ($splitting_array[0]['min'] + $splitting_array[0]['max']) / 2;

		if ( $currency_type == 'price_btc' ) {

			$result = array(
											'tartomany_start' => (string) number_format($splitting_array[0]['min'],8),
											'tartomany_end' 	=> (string) number_format($splitting_array[0]['max'],8),
											'html' 						=> strval(number_format($splitting_array[0]['max'],8) .'<br>'. number_format($splitting_array[0]['min'],8)),
											'avg'							=> $avg_price,
											'compare_current_price'	=> get_price_change_in_percent( $actual_coin_price, $avg_price ),
											'start_date_timestamp' 	=> $start_date,
											'time_interval' 				=> timeAgo($start_date),
										);
		} else {

			$result = array(
											'tartomany_start' => $splitting_array[0]['min'],
											'tartomany_end' 	=> $splitting_array[0]['max'],
											'html' 						=> number_format($splitting_array[0]['max'],4) .'<br>'. number_format($splitting_array[0]['min'],4),
											'avg'							=> $avg_price,
											'compare_current_price'	=> get_price_change_in_percent( $actual_coin_price, $avg_price ),
											'start_date_timestamp' 	=> $start_date,
											'time_interval' 				=> timeAgo($start_date),
										);
		}
	}

	// Save to cache
	set_cmc_avg_price_range_cache($cache_ID, $result);

	return $result;
}


// Visszaadja az adott coin napi árváltozás átlagát
function get_daily_sales_and_purchase_conditions_by_coin_id($coin_ID = "") {
	if ( intval($coin_ID) > 0 ) {  }
	else { return ""; }
	global $wpdb;

	$cmc_days = $wpdb->get_results( "SELECT DISTINCT `datetime`
																		FROM `{$wpdb->prefix}crypto_coinmarketcap_data`
																		WHERE `datetime` >= '". date('Y-m-d H:i:s', strtotime(date('Y-m-01 00:00:00', strtotime("-2 month"))) ) ."'
																			AND `coin_ID` = '{$coin_ID}'
																		ORDER BY `datetime` ASC", ARRAY_A );

	$cmc_days_temp = array();
	foreach ($cmc_days as $key => $val) {
		$cmc_days_temp[]= date('Y-m-d', strtotime($val['datetime']));
	}
	$cmc_days = array_values(array_filter(array_unique($cmc_days_temp)));
	unset($cmc_days_temp);

	$days_results = array();
	foreach ($cmc_days as $key => $day) {

		$day_start 	= $day .' 00:00:00';
		$day_end 		= $day .' 23:59:59';

		// Adott nap adatainak lekérése

		$cmc_percent_change_24h = $wpdb->get_results( "SELECT AVG(`percent_change_24h`) AS `percent_change_24h`
																										FROM `{$wpdb->prefix}crypto_coinmarketcap_data`
																										WHERE `datetime` >= '{$day_start}'
																											AND `datetime` < '{$day_end}'
																											AND `coin_ID` = '{$coin_ID}'
																										ORDER BY `datetime` ASC", ARRAY_A );

		$cmc_positives = $wpdb->get_results( "SELECT COUNT(`percent_change_24h`) AS `positives`
																										FROM `{$wpdb->prefix}crypto_coinmarketcap_data`
																										WHERE `datetime` >= '{$day_start}'
																											AND `datetime` < '{$day_end}'
																											AND `percent_change_24h` NOT LIKE '-%'
																											AND `coin_ID` = '{$coin_ID}'
																										ORDER BY `datetime` ASC", ARRAY_A );

		$cmc_negatives = $wpdb->get_results( "SELECT COUNT(`percent_change_24h`) AS `negatives`
																										FROM `{$wpdb->prefix}crypto_coinmarketcap_data`
																										WHERE `datetime` >= '{$day_start}'
																											AND `datetime` < '{$day_end}'
																											AND `percent_change_24h` LIKE '-%'
																											AND `coin_ID` = '{$coin_ID}'
																										ORDER BY `datetime` ASC", ARRAY_A );

		if ( $cmc_positives[0]["positives"] > $cmc_negatives[0]["negatives"] ) { $positive_negative = "+"; }
		else if ( $cmc_positives[0]["positives"] < $cmc_negatives[0]["negatives"] ) { $positive_negative = "-"; }
		else { $positive_negative = "="; }

		$positive = ($cmc_positives[0]["positives"] * 100) / ($cmc_positives[0]["positives"] + $cmc_negatives[0]["negatives"]);
		$negative = ($cmc_negatives[0]["negatives"] * 100) / ($cmc_positives[0]["positives"] + $cmc_negatives[0]["negatives"]);


		$days_results[strtotime($day_start)] = array(
																								'positive_negative' => $positive_negative,
																								'daily_percent_change' => $cmc_percent_change_24h[0]["percent_change_24h"],
																								'positive' => round($positive,1),
																								'negative' => round($negative,1),
																							 );
	}

	$li = "";
	$positive_negative_array = array();
	foreach ($days_results as $day_start => $daily_values) {

		if ( date('d', $day_start) == 1 ) {
			$li .= '<div class="new_month"></div>';
		}

		$positive_negative_array []= $daily_values["positive_negative"];

		$li .=
			'<li data-day="'. $daily_values["positive_negative"] .'">
				 <span class="val">'. $daily_values["positive_negative"] .'</span>
				 <span class="date">'. date('m-d', $day_start) .' <small>'. date_i18n('D', $day_start) .'</small></span>
				 <span class="daily_percent_change">'. round($daily_values["daily_percent_change"],1) .'%</span>
				 <span class="positive_negative">
					 <span class="positive">'. $daily_values["positive"] .' /</span>
					 <span class="negative">'. $daily_values["negative"] .'</span>
				 </span>
			</li>';
	}

	$positive_negative_array = array_count_values($positive_negative_array);
	$positive_negative_array = 'P: '. $positive_negative_array["+"] .' | N: '. $positive_negative_array["-"];


	return
		'<ul class="daily_sales_and_purchase_conditions selected_coin">
			'. $li .'
			<div class="new_month"></div>
			<div class="clearfix"></div>
		</ul>';
}


function get_cmc_btc_price($coin_ID = "") {
	if ( empty($coin_ID) ) { return ""; }
	global $wpdb;

	$price_usd = $wpdb->get_results( "SELECT `price_btc`
								FROM `{$wpdb->prefix}crypto_coinmarketcap_data`
								WHERE `coin_ID` = '{$coin_ID}'
								ORDER BY `datetime` DESC LIMIT 1", ARRAY_A );

	if ( isset($price_usd[0]['price_btc']) ) {
		return $price_usd[0]['price_btc'];
	}
	return "";
}


function get_cmc_usd_price($coin_ID = "") {
	if ( empty($coin_ID) ) { return ""; }
	global $wpdb;

	$price_usd = $wpdb->get_results( "SELECT `price_usd`
								FROM `{$wpdb->prefix}crypto_coinmarketcap_data`
								WHERE `coin_ID` = '{$coin_ID}'
								ORDER BY `datetime` DESC LIMIT 1", ARRAY_A );

	if ( isset($price_usd[0]['price_usd']) ) {
		return $price_usd[0]['price_usd'];
	}
	return "";
}

// Get amount in HUF
function change_amount_from_btc($amount_in_btc = "", $to_currency = "") {
	if ( empty($amount_in_btc) ) { return ""; }
	if ( empty($to_currency) ) { return ""; }

	$changed_amount = file_get_contents( 'http://crypto.paulovics.hu/btc_converter/?amount='. $amount_in_btc .'&to_currency='. strtoupper($to_currency) );
	$changed_amount = json_decode($changed_amount, true);
	if ( isset($changed_amount['amount']) ) { $changed_amount = $changed_amount['amount']; }
	else { $changed_amount = ""; }

	return $changed_amount;
}

// Megnézi, hogy a kiértélelés szükséges -e az adott coinnál
function check_coin_compilation_have_to_run($coin_ID = 0) {
	global $wpdb;
	$have_to_run = TRUE;

	if ( $coin_ID > 0 ) {  }
	else { return $have_to_run; }

	$main_query_last_run_timestamp = $wpdb->get_results( "SELECT `datetime`
																	FROM `{$wpdb->prefix}crypto_coinmarketcap_data`
																	WHERE `coin_ID` = '{$coin_ID}'
																	ORDER BY `datetime` DESC LIMIT 1", ARRAY_A );

	$main_query_last_run_timestamp2 = $wpdb->get_results( "SELECT `datetime`
																		FROM `{$wpdb->prefix}crypto_compilation`
																		WHERE `coin_ID` = '{$coin_ID}'
																		ORDER BY `datetime` DESC LIMIT 1", ARRAY_A );

	$date1 = ""; $date2 = "";

	if ( isset($main_query_last_run_timestamp[0]['datetime']) && !empty($main_query_last_run_timestamp[0]['datetime']) ) 		{ $date1 = strtotime($main_query_last_run_timestamp[0]['datetime']); }
	if ( isset($main_query_last_run_timestamp2[0]['datetime']) && !empty($main_query_last_run_timestamp2[0]['datetime']) ) 	{ $date2 = strtotime($main_query_last_run_timestamp2[0]['datetime']); }

	if ( isset($main_query_last_run_timestamp[0]['datetime']) && !empty($main_query_last_run_timestamp[0]['datetime']) && isset($main_query_last_run_timestamp2[0]['datetime']) && !empty($main_query_last_run_timestamp2[0]['datetime']) ) {
		if ( $date1 < $date2 ) { $have_to_run = FALSE; }
	}

	return $have_to_run;
}

// Visszaadja az összes, ezen az oldalon használatban levő coin adatát
function get_usable_coins_from_db() {
	global $wpdb;

	$last_150_coin_from_db = $wpdb->get_results( "SELECT `coin_ID`, `rank`, `datetime`
																								FROM `{$wpdb->prefix}crypto_coinmarketcap_data`
																								ORDER BY `datetime` DESC LIMIT 150", ARRAY_A );

	$usable_coins_array = array();
	foreach ($last_150_coin_from_db as $key => $value) {
		if ( intval($value['coin_ID']) > 0 ) {

			$coin_name 			= get_coin_name_by_id(intval($value['coin_ID']));
			$coin_full_name = get_coin_full_name_by_id(intval($value['coin_ID']));

			if ( check_coin_is_banned($coin_name) === FALSE ) {

				$usable_coins_array[$value['coin_ID']]= array(
																									'coin_id' => intval($value['coin_ID']),
																									'coin_name' => $coin_name,
																									'coin_full_name' => $coin_full_name,
																									'rank' => intval($value['rank']),
																									'timestamp' => strtotime($value['datetime']),
																								);
			}
		}
	}
	array_sort_by_column($usable_coins_array, 'rank');
	return $usable_coins_array;
}

function get_mining_data_comparison($compare_column = "") {
	global $wpdb;
	if ( empty($compare_column) ) { return ""; }
	$mining_datas = array();

	$raw_mining_datas = $wpdb->get_results("SELECT DISTINCT `coin_ID`
																			FROM `{$wpdb->prefix}crypto_mining_data`
																			WHERE `{$compare_column}` != '' AND
																						`datetime` > '". date('Y-m-d H:i:s', strtotime('-1 days')) ."'
																			ORDER BY `datetime` DESC", ARRAY_A );

	if (!empty($raw_mining_datas)) {
		foreach ($raw_mining_datas as $key => $val) {

			$raw_mining_datas1 = $wpdb->get_results("SELECT AVG(`{$compare_column}`) AS '{$compare_column}'
																							FROM `{$wpdb->prefix}crypto_mining_data`
																							WHERE `{$compare_column}` != '' AND
																										`coin_ID` = '". $val['coin_ID'] ."' AND
																										`datetime` > '". date('Y-m-d H:i:s', strtotime('-1 days')) ."'
																							ORDER BY `datetime` DESC", ARRAY_A );

			if ( isset($raw_mining_datas1[0][$compare_column]) ) {
				$mining_datas [$val['coin_ID']]['1']= $raw_mining_datas1[0][$compare_column];
			}

			$raw_mining_datas2 = $wpdb->get_results("SELECT AVG(`{$compare_column}`) AS '{$compare_column}'
																							FROM `{$wpdb->prefix}crypto_mining_data`
																							WHERE `{$compare_column}` != '' AND
																										`coin_ID` = '". $val['coin_ID'] ."' AND
																										`datetime` <= '". date('Y-m-d H:i:s', strtotime('-7 days')) ."' AND
																										`datetime` 	> '". date('Y-m-d H:i:s', strtotime('-8 days')) ."'
																							ORDER BY `datetime` DESC", ARRAY_A );

			if ( isset($raw_mining_datas2[0][$compare_column]) ) {
				$mining_datas [$val['coin_ID']]['2']= $raw_mining_datas2[0][$compare_column];
			}


			if ( count($mining_datas[$val['coin_ID']]) != 2 ) { unset($mining_datas[$val['coin_ID']]); }
			else {
				$mining_datas[$val['coin_ID']]['percent']= percent_between_two_numbers($mining_datas[$val['coin_ID']][2], $mining_datas[$val['coin_ID']][1]);
				$mining_datas[$val['coin_ID']]['coin_ID']= intval($val['coin_ID']);
			}

		}
	}

	if ( empty($mining_datas) ) { return ""; }

	array_sort_by_column($mining_datas, 'percent');
	return array_reverse($mining_datas);
}


// Visszaadja, hogy az adott coin ára mióta csökken vagy nő
function get_technical_indicator_how_long($selected_coin_ID = "", $return_val = "") {
	global $wpdb;
	if ( intval($selected_coin_ID) > 0 ) { }
	else { return ""; }

	$max_date_interval = date('Y-m-d H:i:s', strtotime('-72 hours'));

	$summary_data = $wpdb->get_results(
																			"SELECT `technical_indic_summary`, `datetime`
																			FROM `{$wpdb->prefix}crypto_technical_analysis`
																			WHERE `coin_ID` = ". $selected_coin_ID ." AND
																						`datetime` > '". $max_date_interval ."'
																			ORDER BY `datetime` DESC ", ARRAY_A );

	if ( !empty($summary_data) ) {

		$values_array = array();
		foreach ($summary_data as $key => $val) {
			$values_array []= array( intval($val['technical_indic_summary']), $val['datetime'] );
		}

		if ( isset($values_array[0][0]) ) {

			$first_val = $values_array[0][0];
			$positives = array();
			$negatives = array();

			foreach ($values_array as $key => $val) {

				if ( ($first_val > 0) && ($values_array[$key][0] > 0) ) {

					$positives []= $values_array[$key][1];

				} else if ( ($first_val <= 0) && ($values_array[$key][0] <= 0) ) {

					$negatives []= $values_array[$key][1];

				} else { break; }
			}
		}

		$msg = "";
		if ( !empty($positives) ) {
			$msg = '<div class="technical_indicator_how_long price-increase">Árnövekedés tart: <b>'. timeAgo(strtotime(end($positives))) .'</b></div>';

			if ( $return_val == 'timestamp' ) {
				return strtotime('NOW') - strtotime(end($positives));
			}
		}
		if ( !empty($negatives) ) {
			$msg = '<div class="technical_indicator_how_long price-drop">Árcsökkenés tart: <b>'. timeAgo(strtotime(end($negatives))) .'</b></div>';

			if ( $return_val == 'timestamp' ) {
				return strtotime('NOW') - strtotime(end($negatives));
			}
		}
		unset($positives, $negatives, $values_array, $summary_data);
		return $msg;
	}
	return "";
}

function get_technical_indicator_summary($selected_coin_ID = "") {
	global $wpdb;

	if ( intval($selected_coin_ID) > 0 ) {
		$technical_analysis_data[0]['coin_ID'] = intval($selected_coin_ID);
	} else {
		$technical_analysis_data = $wpdb->get_results(
																				"SELECT DISTINCT `coin_ID`
																				FROM `{$wpdb->prefix}crypto_technical_analysis`
																				ORDER BY `coin_ID` ASC ", ARRAY_A );
	}


	if ( !empty($technical_analysis_data) ) {
		$coins_array = array();
		$last_values_array = array();
		foreach ($technical_analysis_data as $key => $val) {
			$coin_ID = (int) $val['coin_ID'];

			$max_date_interval = date('Y-m-d H:i:s', strtotime('-6 hours'));

			$summary_data = $wpdb->get_results(
																					"SELECT `technical_indic_summary`, `datetime`
																					FROM `{$wpdb->prefix}crypto_technical_analysis`
																					WHERE `coin_ID` = ". $coin_ID ." AND
																								`datetime` > '". $max_date_interval ."'
																					ORDER BY `datetime` DESC ", ARRAY_A );

			// Ha 30 percnél régebbi az adat akkor nem jelenik meg
			if ( (strtotime('NOW') - strtotime($summary_data[0]['datetime'])) > 1800 ) {
				return "";
			}

			if (!empty($summary_data)) {
				foreach ($summary_data as $key2 => $value2) {
					if ( $key2 == 0 ) {
						$last_values_array[] = (int) $value2['technical_indic_summary'];
					} else {
						$coins_array[] = (int) $value2['technical_indic_summary'];
					}
				}
			}
		}


		$coins_array = round(array_sum($coins_array) / count($coins_array),1);
		$last_values_array = round(array_sum($last_values_array) / count($last_values_array),1);

		if ( intval($selected_coin_ID) > 0 ) {

			return '<div class="technical_indicator_summary '. get_crypto_technical_analysis_classes($last_values_array, 'main') .' ">'.
								'<span class="title">
									<a href="https://www.investing.com/crypto/'. sanitize_title(get_coin_full_name_by_id(intval($selected_coin_ID))) .'/'. sanitize_title(get_coin_name_by_id(intval($selected_coin_ID))) .'-btc-technical" target="_blank">'.
										get_coin_name_by_id(intval($selected_coin_ID)) .' Technical Indicator Summary: <br>'.
										get_technical_indicator_how_long(intval($selected_coin_ID)).
									'</a>'.
								'</span>'.
								'<span class="val">'. $coins_array .' → '. $last_values_array .'</span>'.
								'<div class="clearfix"></div>'.
							'</div>';
		} else {
			return '<div class="technical_indicator_summary '. get_crypto_technical_analysis_classes($last_values_array, 'main') .' ">'.
								'<span class="title">Technical Indicator Summary: </span>'.
								'<span class="val">'. $coins_array .' → '. $last_values_array .'</span>'.
								'<div class="clearfix"></div>'.
							'</div>';
		}
	}
	return "";
}

function get_crypto_technical_analysis_classes($last_val = "", $main = "") {
	$last_val = (int) $last_val;
	$result = "";

	if ( $main == 'main' ) {

		if ( $last_val == 2 ) { $result = 'very-high'; }
		else if ( $last_val == 1 ) { $result = 'high'; }
		else if ( $last_val == 0 ) { $result = 'low'; }
		else if ( $last_val == -1 ) { $result = 'low'; }
		else if ( $last_val == -2 ) { $result = 'very-low'; }

	} else {

		if ( $last_val > 5 ) { $result = 'very-high'; }
		else if ( ($last_val <= 5) && ($last_val > 0) ) { $result = 'high'; }
		else if ( ($last_val <= 0) && ($last_val > -6) ) { $result = 'low'; }
		else if ( $last_val <= -6 ) { $result = 'very-low'; }
	}

	return $result;
}


function get_crypto_technical_analysis($coin_ID = "") {
	if ( $coin_ID > 0 ) { }
	else { return ""; }

	global $wpdb;
	$max_date_interval = date('Y-m-d H:i:s', strtotime('-6 hours'));

	$sql = "SELECT *
					FROM `{$wpdb->prefix}crypto_technical_analysis`
					WHERE `coin_ID` = ". $coin_ID ." AND
								`datetime` > '". $max_date_interval ."'
					ORDER BY `datetime` DESC";
	$technical_analysis_data = $wpdb->get_results($sql, ARRAY_A );


	// Ha 30 percnél régebbi az adat akkor nem jelenik meg
	if ( (strtotime('NOW') - strtotime($technical_analysis_data[0]['datetime'])) > 1800 ) {
		unset($technical_analysis_data);
	}


	$btc_data['technical_indic_summary'] = array();
	$btc_data['technical_indic_buy'] = array();
	$btc_data['technical_indic_sell'] = array();
	$btc_data['moving_avg_buy'] = array();
	$btc_data['moving_avg_sell'] = array();

	$usd_data['technical_indic_buy'] = array();
	$usd_data['technical_indic_sell'] = array();
	$usd_data['moving_avg_buy'] = array();
	$usd_data['moving_avg_sell'] = array();

	if ( !empty($technical_analysis_data) ) {
		foreach ($technical_analysis_data as $key => $val) {

			$btc_data['technical_indic_summary'][]= (int) $val['technical_indic_summary'];

			if ( $val['type'] == 'BTC' ) {

				$btc_data['technical_indic_buy'][]= $val['technical_indic_buy'];
				$btc_data['technical_indic_sell'][]= $val['technical_indic_sell'];
				$btc_data['moving_avg_buy'][]= $val['moving_avg_buy'];
				$btc_data['moving_avg_sell'][]= $val['moving_avg_sell'];

			} else if ( $val['type'] == 'USD' ) {

				$usd_data['technical_indic_buy'][]= $val['technical_indic_buy'];
				$usd_data['technical_indic_sell'][]= $val['technical_indic_sell'];
				$usd_data['moving_avg_buy'][]= $val['moving_avg_buy'];
				$usd_data['moving_avg_sell'][]= $val['moving_avg_sell'];

			}
		}

		if ( !empty($btc_data) ) {
			$last = (int) $btc_data['technical_indic_summary'][0];
			unset($btc_data['technical_indic_summary'][0]);

			$avg = round(array_sum($btc_data['technical_indic_summary']) / count($btc_data['technical_indic_summary']),1);
			$btc_data['technical_indic_summary']= $avg .' → '. $last;
			$btc_data['technical_indic_summary_class']= get_crypto_technical_analysis_classes($last, 'main');

			$last = (float) $btc_data['technical_indic_buy'][0];
			unset($btc_data['technical_indic_buy'][0]);
			$avg = round(array_sum($btc_data['technical_indic_buy']) / count($btc_data['technical_indic_buy']),1);
			$btc_data['technical_indic_buy']= $avg .' → '. $last;
			$btc_data['technical_indic_buy_class']= get_crypto_technical_analysis_classes($last);

			$last = (float) $btc_data['technical_indic_sell'][0];
			unset($btc_data['technical_indic_sell'][0]);
			$avg = round(array_sum($btc_data['technical_indic_sell']) / count($btc_data['technical_indic_sell']),1);
			$btc_data['technical_indic_sell']= $avg .' → '. $last;
			$btc_data['technical_indic_sell_class']= get_crypto_technical_analysis_classes($last);

			$last = (float) $btc_data['moving_avg_buy'][0];
			unset($btc_data['moving_avg_buy'][0]);
			$avg = round(array_sum($btc_data['moving_avg_buy']) / count($btc_data['moving_avg_buy']),1);
			$btc_data['moving_avg_buy']= $avg .' → '. $last;
			$btc_data['moving_avg_buy_class']= get_crypto_technical_analysis_classes($last);

			$last = (float) $btc_data['moving_avg_sell'][0];
			unset($btc_data['moving_avg_sell'][0]);
			$avg = round(array_sum($btc_data['moving_avg_sell']) / count($btc_data['moving_avg_sell']),1);
			$btc_data['moving_avg_sell']= $avg .' → '. $last;
			$btc_data['moving_avg_sell_class']= get_crypto_technical_analysis_classes($last);
		}

		if ( !empty($usd_data) ) {
			$last = (float) $usd_data['technical_indic_buy'][0];
			unset($usd_data['technical_indic_buy'][0]);
			$avg = round(array_sum($usd_data['technical_indic_buy']) / count($usd_data['technical_indic_buy']),1);
			$usd_data['technical_indic_buy']= $avg .' → '. $last;
			$usd_data['technical_indic_buy_class']= get_crypto_technical_analysis_classes($last);

			$last = (float) $usd_data['technical_indic_sell'][0];
			unset($usd_data['technical_indic_sell'][0]);
			$avg = round(array_sum($usd_data['technical_indic_sell']) / count($usd_data['technical_indic_sell']),1);
			$usd_data['technical_indic_sell']= $avg .' → '. $last;
			$usd_data['technical_indic_sell_class']= get_crypto_technical_analysis_classes($last);

			$last = (float) $usd_data['moving_avg_buy'][0];
			unset($usd_data['moving_avg_buy'][0]);
			$avg = round(array_sum($usd_data['moving_avg_buy']) / count($usd_data['moving_avg_buy']),1);
			$usd_data['moving_avg_buy']= $avg .' → '. $last;
			$usd_data['moving_avg_buy_class']= get_crypto_technical_analysis_classes($last);

			$last = (float) $usd_data['moving_avg_sell'][0];
			unset($usd_data['moving_avg_sell'][0]);
			$avg = round(array_sum($usd_data['moving_avg_sell']) / count($usd_data['moving_avg_sell']),1);
			$usd_data['moving_avg_sell']= $avg .' → '. $last;
			$usd_data['moving_avg_sell_class']= get_crypto_technical_analysis_classes($last);
		}
	}

	foreach ($btc_data as $key => $value) {
		if ( empty($value) ) {
			$btc_data [$key]= "";
		}
	}

	foreach ($usd_data as $key => $value) {
		if ( empty($value) ) {
			$usd_data [$key]= "";
		}
	}

	return array(	'btc_data' => $btc_data,
								'usd_data' => $usd_data );
}


function string_convertTo_int($string) {
	if (!empty($string)) {
		preg_match_all('!\d+!', $string, $matches);
		return (int) join($matches[0]); // fontos!: Ha megvan adva az (int), akkor a 0-val kezdődő értékeknél kimarad az első 0
	}
}

// Megnézi, hogy van -e ilyen befektetés
function get_investment_id($coin_name = "") {
	if ( empty($coin_name) ) { return ""; }

	$befektetesek_cpt = new WP_Query(array(
																			'post_type' => 'befektetesek_cpt',
																			'fields' => 'ids',
																			'posts_per_page' => 1,
																			'meta_query' => array(
																				array(
																					'key'     => 'coin_symbol_meta',
																					'value'   => $coin_name,
																					'compare' => '=',
																				),
																			),
																		));
	$befektetesek_cpt_posts_array = $befektetesek_cpt->posts;

	if ( intval($befektetesek_cpt_posts_array[0]) > 0 ) {
		return intval($befektetesek_cpt_posts_array[0]);
	} else {
		return FALSE;
	}
}

function get_markets_volument_data($coin_ID = "") {
	global $wpdb;
	if ( empty($coin_ID) ) { return ""; }

	$markets_data = $wpdb->get_results( "SELECT `markets`
																			FROM `{$wpdb->prefix}crypto_coin_names`
																			WHERE `ID` = '{$coin_ID}' LIMIT 1 ", ARRAY_A );

	if ( isset($markets_data[0]['markets']) && !empty($markets_data[0]['markets']) ) {
		$markets_data = json_decode($markets_data[0]['markets']);

		$max_val = 0;
		foreach ($markets_data as $key => $value) {
			$max_val += $value;
		}

		$markets_data_temp = array();
		foreach ($markets_data as $key => $value) {
			$markets_data_temp[$key] = number_format(($value * 100) / $max_val, 1);
		}
		arsort($markets_data_temp);

		$markets_data = array();
		foreach ($markets_data_temp as $key => $value) {
			if ( $value >= 1 ) {
				$markets_data []= '<li>'. $key .': '. $value .'%</li>';
			}
		}

		return '<div class="get_markets_volument_data"><ul>'. implode('', $markets_data) .'</ul></div>';
	}
	return "";
}

function set_markets_volument_data($coin_ID = "") {
	if ( empty($coin_ID) ) { return FALSE; }
	global $wpdb;
	$coin_name = get_coin_name_by_id($coin_ID);
	if ( empty($coin_name) ) { return FALSE; }

	$markets = array();
	//$switches_array = array( 'BTC', 'USD', 'ETH', 'XRP', 'BCH', 'LTC' );
	$switches_array = array( 'BTC' );


	foreach ($switches_array as $skey => $switch) {
		$markets_data = file_get_contents( 'https://api.cryptonator.com/api/full/'. strtolower($coin_name) .'-'. strtolower($switch) );
		$markets_data = json_decode($markets_data, true);

		// set markets
		if ( isset($markets_data['ticker']['markets'][0]['market']) && isset($markets_data['ticker']['markets'][0]['volume']) ) {
			foreach ($markets_data['ticker']['markets'] as $k => $v) {

				$market = $markets_data['ticker']['markets'][$k]['market'];
				$volume = $markets_data['ticker']['markets'][$k]['volume'];

				if ( !empty($volume) ) { $markets [$market][] = (float) $volume; }
			}
		}
	}

	// array sum
	if ( !empty($markets) ) {
		foreach ($markets as $key => $value) {
			$markets[$key] = array_sum($value);
		}
		arsort($markets);
		$markets = json_encode($markets);

		$wpdb->update(
			$wpdb->prefix .'crypto_coin_names',
			array( 'markets' => $markets ),
			array( 'ID' => $coin_ID ),
			array( '%s' ),
			array( '%d' )
		);

		return TRUE;
	}
	return FALSE;
}

function get_actual_cmc_btc_price($coin_id = "") {
	if ( empty($coin_id) ) { return ""; }

	$cmc_data = file_get_contents( 'https://api.coinmarketcap.com/v1/ticker/'. get_coin_full_name_by_id($coin_id) .'/?convert=BTC' );
	$cmc_data = json_decode($cmc_data, true);

	if ( isset($cmc_data[0]['symbol']) && isset($cmc_data[0]['price_btc']) ) {
		return (float) $cmc_data[0]['price_btc'];
	}
	return "";
}

function get_coin_rank_by_coin_id($coin_ID = "") {
	if (empty($coin_ID)) { return ""; }
	global $wpdb;

	$cmc_coin_rank = $wpdb->get_results( "SELECT `rank`
																				FROM `{$wpdb->prefix}crypto_coinmarketcap_data`
																				WHERE `coin_ID` = '{$coin_ID}' ", ARRAY_A );

	if ( isset($cmc_coin_rank[0]['rank']) && !empty($cmc_coin_rank[0]['rank']) ) {
		return (int) $cmc_coin_rank[0]['rank'];
	}
	return "";
}

function get_bittrex_coins() {
	global $wpdb;

	$bittrex_coins = $wpdb->get_results( "SELECT DISTINCT `coin_ID`
																				FROM `{$wpdb->prefix}crypto_bittrex_data` ", ARRAY_A );

	$coins_array = array();
	foreach ($bittrex_coins as $key => $value) {
		if ( check_coin_is_banned(get_coin_name_by_id($value['coin_ID'])) === FALSE ) {
			$coins_array []= (int) $value['coin_ID'];
		}
	}

	if ( !empty($coins_array) ) { return $coins_array; }
	return "";
}


function getTextBetween($start = "", $end = "", $text = "") {
	// explode the start string
	$first_strip= end(explode($start,$text,2));

	// explode the end string
	$final_strip = explode($end,$first_strip)[0];
	return $final_strip;
}


function get_btc_movement() {
	global $wpdb;
	$btc_dominance_html_result = "";
	$total_market_cap_html_result = "";


	// Get BTC dominance

	$btc_dominance_avg = $wpdb->get_results( "SELECT AVG(`bitcoin_percentage_of_market_cap`) AS `btc_dominance_avg`
																	FROM `{$wpdb->prefix}crypto_btc_movement`
																	WHERE `datetime` >= '". date('Y-m-d H:i:s', strtotime('-70 days')) ."'
																	ORDER BY `datetime` DESC", ARRAY_A );

	$last_btc_dominance_val = $wpdb->get_results( "SELECT `bitcoin_percentage_of_market_cap` AS `last_btc_dominance_val`
																	FROM `{$wpdb->prefix}crypto_btc_movement`
																	WHERE `datetime` >= '". date('Y-m-d H:i:s', strtotime('-70 days')) ."'
																	ORDER BY `datetime` DESC LIMIT 1", ARRAY_A );

	$column_name 	= $btc_dominance_avg[0]['btc_dominance_avg'];
	$last_data 		= $last_btc_dominance_val[0]['last_btc_dominance_val'];


	if ( isset($column_name) && !empty($column_name) &&
			 isset($last_data) && !empty($last_data) ) {

			$positive_negative = "";
			if ( $column_name == $last_data ) 			{ $positive_negative = array( '=', '<span class="equal">=</span>' ); }
			else if ( $column_name > $last_data ) 	{ $positive_negative = array( '&#62;', '<span class="glyphicon glyphicon-minus minus"></span>' ); }
			else if ( $column_name < $last_data ) 	{ $positive_negative = array( '&#60;', '<span class="glyphicon glyphicon-plus plus"></span>' ); }

			$percentChange = percent_between_two_numbers($column_name, $last_data);
			$column_name 	= substr($column_name, 0, 6);
			$last_data 		= substr($last_data, 0, 6);


			$btc_dominance_html_result .=
				'<td data-value_change="'. $positive_negative[0] .'">
					'. $positive_negative[1] .'
					<div class="calculated_average hidden">'. $column_name .' → </div>
					<div class="latest_num">'. round($percentChange,1) .'%</div>
				</td>';
	}


	// Get Total market cap

	$total_market_cap_avg = $wpdb->get_results( "SELECT AVG(`total_market_cap_by_available_supply_usd`) AS `total_market_cap_avg`
																	FROM `{$wpdb->prefix}crypto_btc_movement`
																	WHERE `datetime` >= '". date('Y-m-d H:i:s', strtotime('-7 days')) ."'
																	ORDER BY `datetime` DESC", ARRAY_A );

	$last_total_market_cap_val = $wpdb->get_results( "SELECT `total_market_cap_by_available_supply_usd` AS `last_total_market_cap_val`
																	FROM `{$wpdb->prefix}crypto_btc_movement`
																	WHERE `datetime` >= '". date('Y-m-d H:i:s', strtotime('-7 days')) ."'
																	ORDER BY `datetime` DESC LIMIT 1", ARRAY_A );

	$column_name 	= $total_market_cap_avg[0]['total_market_cap_avg'];
	$last_data 		= $last_total_market_cap_val[0]['last_total_market_cap_val'];


	if ( isset($column_name) && !empty($column_name) &&
			 isset($last_data) && !empty($last_data) ) {

			$positive_negative = "";
			if ( $column_name == $last_data ) 			{ $positive_negative = array( '=', '<span class="equal">=</span>' ); }
			else if ( $column_name > $last_data ) 	{ $positive_negative = array( '&#62;', '<span class="glyphicon glyphicon-minus minus"></span>' ); }
			else if ( $column_name < $last_data ) 	{ $positive_negative = array( '&#60;', '<span class="glyphicon glyphicon-plus plus"></span>' ); }

			$percentChange = percent_between_two_numbers($column_name, $last_data);
			$column_name 	= substr($column_name, 0, 6);
			$last_data 		= substr($last_data, 0, 6);


			$total_market_cap_html_result .=
				'<td data-value_change="'. $positive_negative[0] .'">
					'. $positive_negative[1] .'
					<div class="calculated_average hidden">'. $column_name .' → </div>
					<div class="latest_num">'. round($percentChange,1) .'%</div>
				</td>';
	}


	// Get Total market cap Volume

	$total_market_cap_avg = $wpdb->get_results( "SELECT AVG(`total_volume_usd`) AS `total_market_cap_avg`
																	FROM `{$wpdb->prefix}crypto_btc_movement`
																	WHERE `datetime` >= '". date('Y-m-d H:i:s', strtotime('-7 days')) ."'
																	ORDER BY `datetime` DESC", ARRAY_A );

	$last_total_market_cap_val = $wpdb->get_results( "SELECT `total_volume_usd` AS `last_total_market_cap_val`
																	FROM `{$wpdb->prefix}crypto_btc_movement`
																	WHERE `datetime` >= '". date('Y-m-d H:i:s', strtotime('-7 days')) ."'
																	ORDER BY `datetime` DESC LIMIT 1", ARRAY_A );

	$column_name 	= $total_market_cap_avg[0]['total_market_cap_avg'];
	$last_data 		= $last_total_market_cap_val[0]['last_total_market_cap_val'];


	if ( isset($column_name) && !empty($column_name) &&
			 isset($last_data) && !empty($last_data) ) {

			$positive_negative = "";
			if ( $column_name == $last_data ) 			{ $positive_negative = array( '=', '<span class="equal">=</span>' ); }
			else if ( $column_name > $last_data ) 	{ $positive_negative = array( '&#62;', '<span class="glyphicon glyphicon-minus minus"></span>' ); }
			else if ( $column_name < $last_data ) 	{ $positive_negative = array( '&#60;', '<span class="glyphicon glyphicon-plus plus"></span>' ); }

			$percentChange = percent_between_two_numbers($column_name, $last_data);
			$column_name 	= substr($column_name, 0, 6);
			$last_data 		= substr($last_data, 0, 6);


			$total_market_cap_volume_html_result .=
				'<td data-value_change="'. $positive_negative[0] .'">
					'. $positive_negative[1] .'
					<div class="calculated_average hidden">'. $column_name .' → </div>
					<div class="latest_num">'. round($percentChange,1) .'%</div>
				</td>';
	}

	return
		'<div class="btc_movement">
			<table class="table.table-bordered">
				<tr>
					<th>BTC dominance</th>
					<th>Total market cap</th>
					<th>CMC Volume</th>
				</tr>
				<tr>
					'. $btc_dominance_html_result .'
					'. $total_market_cap_html_result .'
					'. $total_market_cap_volume_html_result .'
				</tr>
			</table>
		</div>';
}


// Eseménnyel rendelkező coinok
function get_coins_by_events_count($min_event_count = "") {
	global $wpdb;
	$min_event_count = (int) $min_event_count;
	if ( $min_event_count == 0 ) { $min_event_count = 2; }

	$coindar_events = $wpdb->get_results( "SELECT `coin_ID`
																	FROM `{$wpdb->prefix}crypto_coindar_events`
																	WHERE `start_date` >= '". date('Y-m-d H:i:s', strtotime('today midnight')) ."'
																	ORDER BY `start_date` ASC", ARRAY_A );

	$coins_array = array();
	foreach ($coindar_events as $key => $event_data) {
		$coins_array []= (int) $event_data['coin_ID'];
	}

	if ( !empty($coins_array) ) {
		$coins_array = array_count_values($coins_array);

		$coins_array_temp = array();
		foreach ($coins_array as $coin_id => $count) {
			if ( $count >= $min_event_count ) {
				$coins_array_temp []= $coin_id;
			}
		}

		if ( !empty($coins_array_temp) ) {
			asort($coins_array_temp);
			$coins_array_temp = array_values(array_filter(array_unique($coins_array_temp)));

			if ( !empty($coins_array_temp) ) {
				return $coins_array_temp;
			}
		}
	}
	return "";
}



function get_must_sell_and_buy_coins_from_compilation() {
	global $wpdb;

	$crypto_compilation = $wpdb->get_results( "SELECT * FROM `{$wpdb->prefix}crypto_compilation`
																							ORDER BY `datetime` ASC", ARRAY_A );

	// Eseménnyel rendelkező coinok
	$coins_has_got_events = get_coins_by_events_count(2);

	$must_buys = array();
	$buys = array();
	$must_sells = array();
	$sells = array();

	$positive_monitoring_value = 15;
	$negative_monitoring_value = -15;

	foreach ($crypto_compilation as $key => $coin_data) {
		$all_sells_and_buys_datas_evaluation = json_decode($coin_data['all_sells_and_buys_datas_evaluation'], true);


		$exchanges_count = 0;
		if ( !empty($coin_data['bittrex_sells_and_buys_datas_evaluation']) ) 	{ $exchanges_count++; }
		if ( !empty($coin_data['bitfinex_sells_and_buys_datas_evaluation']) ) { $exchanges_count++; }
		if ( !empty($coin_data['hitbtc_sells_and_buys_datas_evaluation']) ) 	{ $exchanges_count++; }
		if ( !empty($coin_data['poloniex_sells_and_buys_datas_evaluation']) ) { $exchanges_count++; }
		if ( !empty($coin_data['bithumb_sells_and_buys_datas_evaluation']) ) 	{ $exchanges_count++; }
		if ( !empty($coin_data['kraken_sells_and_buys_datas_evaluation']) ) 	{ $exchanges_count++; }
		if ( !empty($coin_data['binance_sells_and_buys_datas_evaluation']) ) 	{ $exchanges_count++; }


		if ( isset($all_sells_and_buys_datas_evaluation['positive_negative']) &&
				 (strtotime("now") - $coin_data['creation_date']) >= 47335389 && // older than 18 month
				 in_array($coin_data['coin_ID'], $coins_has_got_events) &&
				 in_array(get_coin_name_by_id($coin_data['coin_ID']), get_option('crypto_coin_popularity_list_winners') ) &&
				 ( $exchanges_count >= 2 )
			) {

				 if ( floatval($coin_data['bittrex_price_change_in_percent']) < 0 &&
							floatval($coin_data['bittrex_price_change_in_percent']) > $negative_monitoring_value &&
						 	$all_sells_and_buys_datas_evaluation['positive_negative'] == "-"
						) {

								$buys []= intval($coin_data['coin_ID']);

 				} else if ( floatval($coin_data['bittrex_price_change_in_percent']) > 0 &&
 										floatval($coin_data['bittrex_price_change_in_percent']) < $positive_monitoring_value &&
 									 	$all_sells_and_buys_datas_evaluation['positive_negative'] == "+"
 									) {

 				 				$sells []= intval($coin_data['coin_ID']);

				} else if ( floatval($coin_data['bittrex_price_change_in_percent']) <= $negative_monitoring_value &&
						 				$all_sells_and_buys_datas_evaluation['positive_negative'] == "-"
					 				) {

			 					$must_buys []= intval($coin_data['coin_ID']);

				} else if ( floatval($coin_data['bittrex_price_change_in_percent']) >= $positive_monitoring_value &&
				 					 	$all_sells_and_buys_datas_evaluation['positive_negative'] == "+"
							 	 ) {

			 				 	$must_sells []= intval($coin_data['coin_ID']);
			 }

		}
	}

	return array(
								'must_buys' => $must_buys,
								'buys' => $buys,

								'must_sells' => $must_sells,
								'sells' => $sells,
							);
}


function get_all_sells_and_buys_datas_evaluation($buys = "", $sells = "") {
	if ( empty($buys) ) 	{ return ""; }
	if ( empty($sells) ) 	{ return ""; }

	$positive_negative = get_sells_and_buys_datas_evaluation(array('percent_change' => $buys), array('percent_change' => $sells));

	$html = '<div class="col-xs-6">
					   <div class="row">
					      <div class="all_total_sells_and_buys buys">
					         <div class="latest_num">'. round($buys,1) .'%</div>
					      </div>
					   </div>
					</div>
					<div class="col-xs-6">
					   <div class="row">
					      <div class="all_total_sells_and_buys sells">
					         <div class="latest_num">'. round($sells,1) .'%</div>
					      </div>
					   </div>
					</div>
					<div class="clearfix"></div>';

	$html = '<div class="all_sells_and_buys_datas_evaluation" data-positive_negative="'. $positive_negative .'" >
						<div class="col-xs-4"></div>
						<div class="col-xs-4">'. $html .'</div>
						<div class="col-xs-4"></div>
						<div class="clearfix"></div>
					</div>';


	return array(
							 'values' => array( $buys, $sells ),
							 'positive_negative' => $positive_negative,
							 'html' => $html,
						 );
}

function get_binance_total_sells_and_buys($coin_ID = "", $order_type = "") {
	if ( empty($coin_ID) ) { return ""; }
	if ( empty($order_type) ) { return ""; }
	global $wpdb;

	if ( strtolower($order_type) == "sell" ) { $order_type = "s"; }
	else { $order_type = "b"; }

	$start_datetime = date('Y-m-d H:i:s', TIME_INTERVAL_TIMESTAMP);

	$binance_market_history = $wpdb->get_results( "SELECT AVG(`total`) AS `btc_total`
																	FROM `{$wpdb->prefix}crypto_binance_market_history`
																	WHERE `coin_ID` = '{$coin_ID}'
																		AND `datetime` >= '{$start_datetime}'
																		AND `ordertype` = '{$order_type}'
																	ORDER BY `datetime` ASC", ARRAY_A );
	$btc_total_avg = $binance_market_history[0]['btc_total'];


	$last_btc_total = $wpdb->get_results( "SELECT `total` AS `last_btc_total`
																	FROM `{$wpdb->prefix}crypto_binance_market_history`
																	WHERE `coin_ID` = '{$coin_ID}'
																		AND `ordertype` = '{$order_type}'
																	ORDER BY `datetime` DESC LIMIT 1", ARRAY_A );
	$last_btc_total = $last_btc_total[0]['last_btc_total'];

	if ( empty($btc_total_avg) ) 		{ return ""; }
	if ( empty($last_btc_total) ) 	{ return ""; }

	$column_name 	= $btc_total_avg;
	$last_data 		= $last_btc_total;

	if ( isset($column_name) && !empty($column_name) &&
			 isset($last_data) && !empty($last_data) ) {

			$positive_negative = "";
			if ( $column_name == $last_data ) 			{ $positive_negative = array( '=', '<span class="equal">=</span>' ); }
			else if ( $column_name > $last_data ) 	{ $positive_negative = array( '>', '<span class="glyphicon glyphicon-minus minus"></span>' ); }
			else if ( $column_name < $last_data ) 	{ $positive_negative = array( '<', '<span class="glyphicon glyphicon-plus plus"></span>' ); }

			$percentChange = percent_between_two_numbers($column_name, $last_data);
			$column_name 	= substr($column_name, 0, 6);
			$last_data 		= substr($last_data, 0, 6);


			 $html =
				 '<div class="binance_total_sells_and_buys">
		 			 '. $positive_negative[1] .'
		 			 <div class="calculated_average hidden">'. $column_name .' → </div>
		 			 <div class="latest_num">'. round($percentChange,1) .'%</div>
		 		 </div>';

			return array(
									 'values' => array( $column_name, $last_data ),
									 'percent_change' => $percentChange,
									 'positive_negative' => $positive_negative[0],
									 'html' => $html,
								 );
	}
	return "";
}

function get_kraken_total_sells_and_buys($coin_ID = "", $order_type = "") {
	if ( empty($coin_ID) ) { return ""; }
	if ( empty($order_type) ) { return ""; }
	global $wpdb;

	if ( strtolower($order_type) == "sell" ) { $order_type = "s"; }
	else { $order_type = "b"; }

	$start_datetime = date('Y-m-d H:i:s', TIME_INTERVAL_TIMESTAMP);

	$kraken_market_history = $wpdb->get_results( "SELECT AVG(`total`) AS `btc_total`
																	FROM `{$wpdb->prefix}crypto_kraken_market_history`
																	WHERE `coin_ID` = '{$coin_ID}'
																		AND `datetime` >= '{$start_datetime}'
																		AND `ordertype` = '{$order_type}'
																	ORDER BY `datetime` ASC", ARRAY_A );
	$btc_total_avg = $kraken_market_history[0]['btc_total'];


	$last_btc_total = $wpdb->get_results( "SELECT `total` AS `last_btc_total`
																	FROM `{$wpdb->prefix}crypto_kraken_market_history`
																	WHERE `coin_ID` = '{$coin_ID}'
																		AND `ordertype` = '{$order_type}'
																	ORDER BY `datetime` DESC LIMIT 1", ARRAY_A );
	$last_btc_total = $last_btc_total[0]['last_btc_total'];

	if ( empty($btc_total_avg) ) 		{ return ""; }
	if ( empty($last_btc_total) ) 	{ return ""; }

	$column_name 	= $btc_total_avg;
	$last_data 		= $last_btc_total;

	if ( isset($column_name) && !empty($column_name) &&
			 isset($last_data) && !empty($last_data) ) {

			$positive_negative = "";
			if ( $column_name == $last_data ) 			{ $positive_negative = array( '=', '<span class="equal">=</span>' ); }
			else if ( $column_name > $last_data ) 	{ $positive_negative = array( '>', '<span class="glyphicon glyphicon-minus minus"></span>' ); }
			else if ( $column_name < $last_data ) 	{ $positive_negative = array( '<', '<span class="glyphicon glyphicon-plus plus"></span>' ); }

			$percentChange = percent_between_two_numbers($column_name, $last_data);
			$column_name 	= substr($column_name, 0, 6);
			$last_data 		= substr($last_data, 0, 6);


			 $html =
				 '<div class="kraken_total_sells_and_buys">
		 			 '. $positive_negative[1] .'
		 			 <div class="calculated_average hidden">'. $column_name .' → </div>
		 			 <div class="latest_num">'. round($percentChange,1) .'%</div>
		 		 </div>';

			return array(
									 'values' => array( $column_name, $last_data ),
									 'percent_change' => $percentChange,
									 'positive_negative' => $positive_negative[0],
									 'html' => $html,
								 );
	}
	return "";
}


function get_cmc_coin_volume($coin_ID = "") {
	if ( empty($coin_ID) ) { return ""; }
	global $wpdb;

	$volume_avg = $wpdb->get_results( "SELECT AVG(`volume_usd_24h`) AS `volume_avg`
																	FROM `{$wpdb->prefix}crypto_coinmarketcap_data`
																	WHERE `coin_ID` = '{$coin_ID}'
																		AND `volume_usd_24h` != ''
																		AND `datetime` >= '". date('Y-m-d H:i:s', strtotime('-7 days')) ."'
																	ORDER BY `datetime` ASC", ARRAY_A );

	$last_volume = $wpdb->get_results( "SELECT `volume_usd_24h` AS `last_volume`
																	FROM `{$wpdb->prefix}crypto_coinmarketcap_data`
																	WHERE `coin_ID` = '{$coin_ID}'
																		AND `volume_usd_24h` != ''
																		AND `datetime` >= '". date('Y-m-d H:i:s', strtotime('-7 days')) ."'
																	ORDER BY `datetime` DESC LIMIT 1", ARRAY_A );

	$column_name 	= $volume_avg[0]['volume_avg'];
	$last_data 		= $last_volume[0]['last_volume'];

	if ( isset($column_name) && !empty($column_name) &&
			 isset($last_data) && !empty($last_data) ) {

			$positive_negative = "";
			if ( $column_name == $last_data ) 			{ $positive_negative = array( '=', '<span class="equal">=</span>' ); }
			else if ( $column_name > $last_data ) 	{ $positive_negative = array( '>', '<span class="glyphicon glyphicon-minus minus"></span>' ); }
			else if ( $column_name < $last_data ) 	{ $positive_negative = array( '<', '<span class="glyphicon glyphicon-plus plus"></span>' ); }

			$percentChange = percent_between_two_numbers($column_name, $last_data);
			$column_name 	= substr($column_name, 0, 6);
			$last_data 		= substr($last_data, 0, 6);


			 $html =
				 '<div class="cmc_coin_volume">
		 			 '. $positive_negative[1] .'
		 			 <div class="calculated_average hidden">'. $column_name .' → </div>
		 			 <div class="latest_num">'. round($percentChange,1) .'%</div>
		 		 </div>';

			return array(
									 'values' => array( $column_name, $last_data ),
									 'percent_change' => $percentChange,
									 'positive_negative' => $positive_negative[0],
									 'html' => $html,
								 );
	}
	return "";
}


function get_bittrex_percentage_distribution_of_the_total_price_diagram($coin_ID = "", $last_price = "") {
	if ( empty($coin_ID) ) { return ""; }
	global $wpdb;
	$result_html = "";

	$bpdottp = $wpdb->get_results( "SELECT `bittrex_percentage_distribution_of_the_total_price`
																		FROM `{$wpdb->prefix}crypto_compilation`
																		WHERE `coin_ID` = '{$coin_ID}'
																		ORDER BY `datetime` DESC LIMIT 1", ARRAY_A );

	if ( isset($bpdottp[0]['bittrex_percentage_distribution_of_the_total_price']) && !empty($bpdottp[0]['bittrex_percentage_distribution_of_the_total_price']) ) {

		$bpdottp_array = json_decode($bpdottp[0]['bittrex_percentage_distribution_of_the_total_price'], true);

		$bpdottp_array_temp = $bpdottp_array;
		array_sort_by_column($bpdottp_array_temp, 'tartomany_end');
		$bpdottp_array_temp = array_reverse($bpdottp_array_temp);
		$max_tartomany_end = $bpdottp_array_temp[0]['tartomany_end'];
		unset($bpdottp_array_temp);

		array_sort_by_column($bpdottp_array, 'tartomany_start');

		$percents = array();
		$prices = array();
		$actual_price = array();
		$list = array();

		foreach ($bpdottp_array as $key => $value) {
			$percents []= $value['how_many_times_in_percent'];
			$prices 	[]= number_format( ($value['tartomany_end'] + $value['tartomany_start']) / 2 ,8);

			if ( !empty($last_price) && ( $value['tartomany_end'] >= floatval($last_price) ) && ( $value['tartomany_start'] < floatval($last_price) ) ) {
				$actual_price []= $last_price;
			} else {
				$actual_price []= "";
			}

			$list []= '<li>'.
									'<div class="percent">'. $value['how_many_times_in_percent'] .'% - <small>'. $value['how_many_times'] .'</small></div>'.
									'<div class="prices"><span>'. number_format($value['tartomany_end'],8) .'</span><span>'. number_format($value['tartomany_start'],8) .'</span></div>'.
								'</li>';
		}


		/*
		$found_key = "";
		foreach ($prices as $key => $price) {
			if ( $price > $last_price ) {
				$found_key = $key - 1;
				break;
			}
		}

		for ($i=0; $i <= $found_key; $i++) {
			if ( $i == $found_key ) {
				$actual_price []= $last_price;
			} else {
				$actual_price []= "";
			}
		}
		*/


		//$list = array_reverse($list);

		if ( !empty($percents) && !empty($prices) && !empty($list) ) {

			$result_html =
				'<div class="col-xs-12">
					<canvas id="f3gc21'. $coin_ID .'"></canvas>
					<script type="text/javascript">
					jQuery(function($) {
						$(window).load(function(){
							var ctx = document.getElementById("f3gc21'. $coin_ID .'").getContext("2d");
							var chart = new Chart(ctx, {
									type: "line",
									data: {
											labels: ['. implode(',', $prices) .'],
											datasets: [{
													steppedLine: false,
													fill: false,
													label: "Highest value: '. number_format($max_tartomany_end,8) .'",
													backgroundColor: "rgba(72, 194, 113, 1)",
													borderColor: "rgba(72, 194, 113, 1)",
													data: ['. implode(',', $percents) .']
											},
											{
													steppedLine: false,
													fill: false,
													label: "Last Price: ",
													backgroundColor: "rgba(255, 0, 0, 1)",
													borderColor: "rgba(255, 0, 0, 1)",
													data: ['. implode(',', $actual_price) .']
											}]
									},
									options: { }
							});
						});
					});
					</script>
				</div>
				<div class="col-xs-12 hidden">
					<ul>'. implode("",$list) .'</ul>
				</div>';
		}
	}

	return '<div class="bittrex_percentage_distribution_of_the_total_price_diagram">'. $result_html .'</div>';
}


function get_total_buys_and_sells_from_market_history($exchange_name = "", $order_type = "") {
	global $wpdb;
	if ( empty($exchange_name) ) { return ""; }
	if ( empty($order_type) ) { return ""; }
	//$start_datetime = date('Y-m-d H:i:s', GET_MARKET_HISTORY_TIMESTAMP);

	$last_datetime = $wpdb->get_results( "SELECT `datetime` AS `last_datetime`
																				FROM `{$wpdb->prefix}crypto_{$exchange_name}_market_history`
																				ORDER BY `datetime` DESC LIMIT 1", ARRAY_A );
	$last_datetime = strtotime($last_datetime[0]['last_datetime']);


	$avg_market_history = $wpdb->get_results( "SELECT AVG(`total`) AS `btc_total`
																	FROM `{$wpdb->prefix}crypto_{$exchange_name}_market_history`
																	WHERE `datetime` >= '". date('Y-m-d H:i:s', $last_datetime - (45 * 60) ) ."'
																		AND `ordertype` = '{$order_type}'
																	ORDER BY `datetime` DESC", ARRAY_A );
	$last_btc_total = $avg_market_history[0]['btc_total'];


	$avg_market_history = $wpdb->get_results( "SELECT AVG(`total`) AS `btc_total`
																	FROM `{$wpdb->prefix}crypto_{$exchange_name}_market_history`
																	WHERE `datetime` < '". date('Y-m-d H:i:s', $last_datetime - (45 * 60) ) ."' AND
																				`datetime` >= '". date('Y-m-d H:i:s', $last_datetime - (240 * 60) ) ."' AND
																				`ordertype` = '{$order_type}'
																	ORDER BY `datetime` DESC", ARRAY_A );
	$btc_total_avg = $avg_market_history[0]['btc_total'];

	if ( empty($btc_total_avg) ) 		{ return ""; }
	if ( empty($last_btc_total) ) 	{ return ""; }

	unset($last_datetime, $avg_market_history);
 	return percent_between_two_numbers(floatval($btc_total_avg), floatval($last_btc_total));
}


// összes eladás lekérése a compilation táblábol
function get_hitbtc_total_sells_from_compilation() {
	global $wpdb;

	$price_changes = $wpdb->get_results( "SELECT `hitbtc_total_sells`
																						FROM `{$wpdb->prefix}crypto_compilation`
																						ORDER BY `datetime` ASC", ARRAY_A );

	$percent_change = array();

	foreach ($price_changes as $key => $value) {
		if ( isset($value['hitbtc_total_sells']) ) {
			$hitbtc_total_sells = json_decode($value['hitbtc_total_sells'], true);

			if ( isset($hitbtc_total_sells['percent_change']) && !empty($hitbtc_total_sells['percent_change']) ) {
				$percent_change []= $hitbtc_total_sells['percent_change'];
			}
		}
	}

	if ( !empty($percent_change) ) {
		return array_sum($percent_change) / count($percent_change);
	}

	return "";
}

// összes vásárlás lekérése a compilation táblábol
function get_hitbtc_total_buys_from_compilation() {
	global $wpdb;

	$price_changes = $wpdb->get_results( "SELECT `hitbtc_total_buys`
																						FROM `{$wpdb->prefix}crypto_compilation`
																						ORDER BY `datetime` ASC", ARRAY_A );

	$percent_change = array();

	foreach ($price_changes as $key => $value) {
		if ( isset($value['hitbtc_total_buys']) ) {
			$hitbtc_total_sells = json_decode($value['hitbtc_total_buys'], true);

			if ( isset($hitbtc_total_sells['percent_change']) && !empty($hitbtc_total_sells['percent_change']) ) {
				$percent_change []= $hitbtc_total_sells['percent_change'];
			}
		}
	}

	if ( !empty($percent_change) ) {
		return array_sum($percent_change) / count($percent_change);
	}

	return "";
}


// összes eladás lekérése a compilation táblábol
function get_bitfinex_total_sells_from_compilation() {
	global $wpdb;

	$price_changes = $wpdb->get_results( "SELECT `bitfinex_total_sells`
																						FROM `{$wpdb->prefix}crypto_compilation`
																						ORDER BY `datetime` ASC", ARRAY_A );

	$percent_change = array();

	foreach ($price_changes as $key => $value) {
		if ( isset($value['bitfinex_total_sells']) ) {
			$bitfinex_total_sells = json_decode($value['bitfinex_total_sells'], true);

			if ( isset($bitfinex_total_sells['percent_change']) && !empty($bitfinex_total_sells['percent_change']) ) {
				$percent_change []= $bitfinex_total_sells['percent_change'];
			}
		}
	}

	if ( !empty($percent_change) ) {
		return array_sum($percent_change) / count($percent_change);
	}

	return "";
}

// összes vásárlás lekérése a compilation táblábol
function get_bitfinex_total_buys_from_compilation() {
	global $wpdb;

	$price_changes = $wpdb->get_results( "SELECT `bitfinex_total_buys`
																						FROM `{$wpdb->prefix}crypto_compilation`
																						ORDER BY `datetime` ASC", ARRAY_A );

	$percent_change = array();

	foreach ($price_changes as $key => $value) {
		if ( isset($value['bitfinex_total_buys']) ) {
			$bitfinex_total_sells = json_decode($value['bitfinex_total_buys'], true);

			if ( isset($bitfinex_total_sells['percent_change']) && !empty($bitfinex_total_sells['percent_change']) ) {
				$percent_change []= $bitfinex_total_sells['percent_change'];
			}
		}
	}

	if ( !empty($percent_change) ) {
		return array_sum($percent_change) / count($percent_change);
	}

	return "";
}


// összes eladás lekérése a compilation táblábol
function get_bittrex_total_sells_from_compilation() {
	global $wpdb;

	$price_changes = $wpdb->get_results( "SELECT `bittrex_total_sells`
																						FROM `{$wpdb->prefix}crypto_compilation`
																						ORDER BY `datetime` ASC", ARRAY_A );

	$percent_change = array();

	foreach ($price_changes as $key => $value) {
		if ( isset($value['bittrex_total_sells']) ) {
			$bittrex_total_sells = json_decode($value['bittrex_total_sells'], true);

			if ( isset($bittrex_total_sells['percent_change']) && !empty($bittrex_total_sells['percent_change']) ) {
				$percent_change []= $bittrex_total_sells['percent_change'];
			}
		}
	}

	if ( !empty($percent_change) ) {
		return array_sum($percent_change) / count($percent_change);
	}

	return "";
}

// összes vásárlás lekérése a compilation táblábol
function get_bittrex_total_buys_from_compilation() {
	global $wpdb;

	$price_changes = $wpdb->get_results( "SELECT `bittrex_total_buys`
																						FROM `{$wpdb->prefix}crypto_compilation`
																						ORDER BY `datetime` ASC", ARRAY_A );

	$percent_change = array();

	foreach ($price_changes as $key => $value) {
		if ( isset($value['bittrex_total_buys']) ) {
			$bittrex_total_sells = json_decode($value['bittrex_total_buys'], true);

			if ( isset($bittrex_total_sells['percent_change']) && !empty($bittrex_total_sells['percent_change']) ) {
				$percent_change []= $bittrex_total_sells['percent_change'];
			}
		}
	}

	if ( !empty($percent_change) ) {
		return array_sum($percent_change) / count($percent_change);
	}

	return "";
}


// melyik van többségben: vásárlások és eladások értékelése
function get_sells_and_buys_datas_evaluation($buys_val = "", $sells_val = "") {
	if ( isset($buys_val['percent_change']) && !empty($buys_val['percent_change']) && isset($sells_val['percent_change']) && !empty($sells_val['percent_change']) ) {

		$buys_val 	= $buys_val['percent_change'];
		$sells_val 	= $sells_val['percent_change'];

		$buys = '+';
		$sells = '+';

		if (strpos($buys, '-') !== false) 	{ $buys = '-'; }
		if (strpos($sells, '-') !== false) 	{ $sells = '-'; }

		$res = '';

		if ( ($buys == '+') && ($sells == '+') && ($buys_val > $sells_val ) ) { $res = '+'; }
		else if ( ($buys == '+') && ($sells == '+') && ($buys_val < $sells_val ) ) { $res = '-'; }

		else if ( ($buys == '-') && ($sells == '-') ) { $res = '-'; }

		else if ( ($buys == '+') && ($sells == '-') && ($buys_val > $sells_val ) ) { $res = '+'; }
		else if ( ($buys == '+') && ($sells == '-') && ($buys_val < $sells_val ) ) { $res = '-'; }

		else if ( ($buys == '-') && ($sells == '+') && ($buys_val > $sells_val ) ) { $res = '+'; }
		else if ( ($buys == '-') && ($sells == '+') && ($buys_val < $sells_val ) ) { $res = '-'; }

		return $res;
	}
	return "";
}


function check_coin_is_banned($coin_name = "") {
	if ( empty($coin_name) ) { return ""; }
	global $wpdb;

	$deleted_shitcoins = $wpdb->get_results( "SELECT `shitcoin_name`
																						FROM `{$wpdb->prefix}crypto_shitcoins`
																						WHERE `shitcoin_name` = '{$coin_name}'
																							AND `deleted` = 1
																							LIMIT 1", ARRAY_A );

	if ( isset($deleted_shitcoins[0]['shitcoin_name']) && !empty($deleted_shitcoins[0]['shitcoin_name']) ) {
		return (boolean) TRUE;
	}
	return (boolean) FALSE;
}


function remove_shitcoins() {
	global $wpdb;

	$shitcoins = $wpdb->get_results( "SELECT DISTINCT `shitcoin_name`, `datetime`
																		FROM `{$wpdb->prefix}crypto_shitcoins`
																		WHERE `deleted` = 0
																		ORDER BY `datetime` DESC", ARRAY_A );

	foreach ($shitcoins as $key => $value) {
		$created_time = strtotime($value['datetime']);
		$coin_name = $value['shitcoin_name'];
		$coin_id = get_coin_id_by_name($coin_name);


		// 14 napja shitcoin --> törlés
		if ( (strtotime(current_time('mysql')) - $created_time) >= (14 * 24) * 60 * 60 ) {

			/*
			// Debug
			if ( $coin_name == 'BCC' ) {
				var_dump($created_time);
				var_dump(strtotime(current_time('mysql')) - $created_time);
				var_dump((14 * 24) * 60 * 60);

				die;
			}
			*/

			$last_rank = $wpdb->get_results( "SELECT `rank`, `datetime`
																				FROM `{$wpdb->prefix}crypto_coinmarketcap_data`
																				WHERE `coin_ID` = $coin_id
																				ORDER BY `datetime` DESC LIMIT 1", ARRAY_A );


			if ( isset($last_rank[0]['rank']) && !empty($last_rank[0]['rank']) ) {
				if ( $coin_id > 0 ) {
					if ( (intval($last_rank[0]['rank']) >= 150) || ((strtotime(current_time('mysql')) - strtotime($last_rank[0]['datetime'])) >= (14 * 24) * 60 * 60) ) {

						$wpdb->delete( $wpdb->prefix .'crypto_bittrex_data', array( 'coin_ID' => $coin_id ), array( '%d' ) );
						$wpdb->delete( $wpdb->prefix .'crypto_coinmarketcap_data', array( 'coin_ID' => $coin_id ), array( '%d' ) );
						$wpdb->delete( $wpdb->prefix .'crypto_compilation', array( 'coin_ID' => $coin_id ), array( '%d' ) );
						$wpdb->delete( $wpdb->prefix .'crypto_mining_data', array( 'coin_ID' => $coin_id ), array( '%d' ) );

						$wpdb->delete( $wpdb->prefix .'crypto_bittrex_market_history', array( 'coin_ID' => $coin_id ), array( '%d' ) );
						$wpdb->delete( $wpdb->prefix .'crypto_bitfinex_market_history', array( 'coin_ID' => $coin_id ), array( '%d' ) );
						$wpdb->delete( $wpdb->prefix .'crypto_hitbtc_market_history', array( 'coin_ID' => $coin_id ), array( '%d' ) );
						$wpdb->delete( $wpdb->prefix .'crypto_poloniex_market_history', array( 'coin_ID' => $coin_id ), array( '%d' ) );
						$wpdb->delete( $wpdb->prefix .'crypto_bithumb_market_history', array( 'coin_ID' => $coin_id ), array( '%d' ) );
						$wpdb->delete( $wpdb->prefix .'crypto_kraken_market_history', array( 'coin_ID' => $coin_id ), array( '%d' ) );
						$wpdb->delete( $wpdb->prefix .'crypto_binance_market_history', array( 'coin_ID' => $coin_id ), array( '%d' ) );

						$wpdb->update(
							$wpdb->prefix .'crypto_shitcoins',
							array( 'deleted' => 1 ),
							array( 'shitcoin_name' => $coin_name ),
							array( '%d' ),
							array( '%d' )
						);

					}
				}
			}
		}
	}


	// Duplikált adatok törlése

	$coins = $wpdb->get_results( "SELECT `ID`, `shitcoin_name`
																			FROM `{$wpdb->prefix}crypto_shitcoins`
																			GROUP BY `shitcoin_name` having count(*) >= 2
																			ORDER BY `shitcoin_name` ASC ", ARRAY_A );
	if ( !empty($coins) ) {
		foreach ($coins as $key => $v) {
			if ( intval($v['ID']) > 0 ) {
				$shitcoin_name = $v['shitcoin_name'];

				$coins_temp = $wpdb->get_results( "SELECT *
																						FROM `{$wpdb->prefix}crypto_shitcoins`
																						WHERE `shitcoin_name` = '{$shitcoin_name}'
																						ORDER BY `datetime` ASC ", ARRAY_A );
				array_pop($coins_temp);

				if ( !empty($coins_temp) ) {
					foreach ($coins_temp as $key2 => $value) {
						$wpdb->delete( $wpdb->prefix .'crypto_shitcoins', array( 'ID' => intval($value['ID']) ), array( '%d' ) );
					}
				}
			}
		}
	}

}

function get_latest_cmc_data_date($coin_ID = "") {
	if ( empty($coin_ID) ) { return ""; }
	global $wpdb;

	$crypto_coinmarketcap_data = $wpdb->get_results( "SELECT `datetime` FROM `{$wpdb->prefix}crypto_coinmarketcap_data`
																							WHERE `coin_ID` = '{$coin_ID}'
																							ORDER BY `datetime` DESC LIMIT 1", ARRAY_A );

	if ( isset($crypto_coinmarketcap_data[0]['datetime']) && !empty($crypto_coinmarketcap_data[0]['datetime']) ) {
		return strtotime($crypto_coinmarketcap_data[0]['datetime']);
	}
	return "";
}


function btc_price_rounding($string = "") {
	$f = substr($string, 0,2);
	$n = intval(substr($string, 2));
	$c = strlen($n);

	if ( $c == 8 ) 				{ $n = $f .round($n,-4); }
	else if ( $c == 7 ) 	{ $n = $f .'0'. round($n,-3); }
	else if ( $c == 6 ) 	{ $n = $f .'00'. round($n,-2); }
	else if ( $c == 5 ) 	{ $n = $f .'000'. round($n,-2); }
	else if ( $c == 4 ) 	{ $n = $f .'0000'. round($n,-1); }
	else if ( $c == 3 ) 	{ $n = $f .'00000'. round($n,-1); }
	else if ( $c == 2 ) 	{ $n = $f .'000000'. round($n,-1); }
	else if ( $c == 1 ) 	{ $n = $string; }

	return number_format($n,8);
}


function get_bithumb_total_sells_and_buys($coin_ID = "", $order_type = "") {
	if ( empty($coin_ID) ) { return ""; }
	if ( empty($order_type) ) { return ""; }
	global $wpdb;

	if ( strtolower($order_type) == "sell" ) { $order_type = "s"; }
	else { $order_type = "b"; }

	$start_datetime = date('Y-m-d H:i:s', TIME_INTERVAL_TIMESTAMP);

	$bithumb_market_history = $wpdb->get_results( "SELECT AVG(`total`) AS `btc_total`
																	FROM `{$wpdb->prefix}crypto_bithumb_market_history`
																	WHERE `coin_ID` = '{$coin_ID}'
																		AND `datetime` >= '{$start_datetime}'
																		AND `ordertype` = '{$order_type}'
																	ORDER BY `datetime` ASC", ARRAY_A );
	$btc_total_avg = $bithumb_market_history[0]['btc_total'];


	$last_btc_total = $wpdb->get_results( "SELECT `total` AS `last_btc_total`
																	FROM `{$wpdb->prefix}crypto_bithumb_market_history`
																	WHERE `coin_ID` = '{$coin_ID}'
																		AND `ordertype` = '{$order_type}'
																	ORDER BY `datetime` DESC LIMIT 1", ARRAY_A );
	$last_btc_total = $last_btc_total[0]['last_btc_total'];

	if ( empty($btc_total_avg) ) 		{ return ""; }
	if ( empty($last_btc_total) ) 	{ return ""; }

	$column_name 	= $btc_total_avg;
	$last_data 		= $last_btc_total;

	if ( isset($column_name) && !empty($column_name) &&
			 isset($last_data) && !empty($last_data) ) {

			$positive_negative = "";
			if ( $column_name == $last_data ) 			{ $positive_negative = array( '=', '<span class="equal">=</span>' ); }
			else if ( $column_name > $last_data ) 	{ $positive_negative = array( '>', '<span class="glyphicon glyphicon-minus minus"></span>' ); }
			else if ( $column_name < $last_data ) 	{ $positive_negative = array( '<', '<span class="glyphicon glyphicon-plus plus"></span>' ); }

			$percentChange = percent_between_two_numbers($column_name, $last_data);
			$column_name 	= substr($column_name, 0, 6);
			$last_data 		= substr($last_data, 0, 6);


			 $html =
				 '<div class="bithumb_total_sells_and_buys">
		 			 '. $positive_negative[1] .'
		 			 <div class="calculated_average hidden">'. $column_name .' → </div>
		 			 <div class="latest_num">'. round($percentChange,1) .'%</div>
		 		 </div>';

			return array(
									 'values' => array( $column_name, $last_data ),
									 'percent_change' => $percentChange,
									 'positive_negative' => $positive_negative[0],
									 'html' => $html,
								 );
	}
	return "";
}


function get_poloniex_total_sells_and_buys($coin_ID = "", $order_type = "") {
	if ( empty($coin_ID) ) { return ""; }
	if ( empty($order_type) ) { return ""; }
	global $wpdb;

	if ( strtolower($order_type) == "sell" ) { $order_type = "s"; }
	else { $order_type = "b"; }

	$start_datetime = date('Y-m-d H:i:s', TIME_INTERVAL_TIMESTAMP);

	$poloniex_market_history = $wpdb->get_results( "SELECT AVG(`total`) AS `btc_total`
																	FROM `{$wpdb->prefix}crypto_poloniex_market_history`
																	WHERE `coin_ID` = '{$coin_ID}'
																		AND `datetime` >= '{$start_datetime}'
																		AND `ordertype` = '{$order_type}'
																	ORDER BY `datetime` ASC", ARRAY_A );
	$btc_total_avg = $poloniex_market_history[0]['btc_total'];


	$last_btc_total = $wpdb->get_results( "SELECT `total` AS `last_btc_total`
																	FROM `{$wpdb->prefix}crypto_poloniex_market_history`
																	WHERE `coin_ID` = '{$coin_ID}'
																		AND `ordertype` = '{$order_type}'
																	ORDER BY `datetime` DESC LIMIT 1", ARRAY_A );
	$last_btc_total = $last_btc_total[0]['last_btc_total'];

	if ( empty($btc_total_avg) ) 		{ return ""; }
	if ( empty($last_btc_total) ) 	{ return ""; }

	$column_name 	= $btc_total_avg;
	$last_data 		= $last_btc_total;

	if ( isset($column_name) && !empty($column_name) &&
			 isset($last_data) && !empty($last_data) ) {

			$positive_negative = "";
			if ( $column_name == $last_data ) 			{ $positive_negative = array( '=', '<span class="equal">=</span>' ); }
			else if ( $column_name > $last_data ) 	{ $positive_negative = array( '>', '<span class="glyphicon glyphicon-minus minus"></span>' ); }
			else if ( $column_name < $last_data ) 	{ $positive_negative = array( '<', '<span class="glyphicon glyphicon-plus plus"></span>' ); }

			$percentChange = percent_between_two_numbers($column_name, $last_data);
			$column_name 	= substr($column_name, 0, 6);
			$last_data 		= substr($last_data, 0, 6);


			 $html =
				 '<div class="poloniex_total_sells_and_buys">
		 			 '. $positive_negative[1] .'
		 			 <div class="calculated_average hidden">'. $column_name .' → </div>
		 			 <div class="latest_num">'. round($percentChange,1) .'%</div>
		 		 </div>';

			return array(
									 'values' => array( $column_name, $last_data ),
									 'percent_change' => $percentChange,
									 'positive_negative' => $positive_negative[0],
									 'html' => $html,
								 );
	}
	return "";
}


function get_hitbtc_total_sells_and_buys($coin_ID = "", $order_type = "") {
	if ( empty($coin_ID) ) { return ""; }
	if ( empty($order_type) ) { return ""; }
	global $wpdb;

	if ( strtolower($order_type) == "sell" ) { $order_type = "s"; }
	else { $order_type = "b"; }

	$start_datetime = date('Y-m-d H:i:s', TIME_INTERVAL_TIMESTAMP);

	$hitbtc_market_history = $wpdb->get_results( "SELECT AVG(`total`) AS `btc_total`
																	FROM `{$wpdb->prefix}crypto_hitbtc_market_history`
																	WHERE `coin_ID` = '{$coin_ID}'
																		AND `datetime` >= '{$start_datetime}'
																		AND `ordertype` = '{$order_type}'
																	ORDER BY `datetime` ASC", ARRAY_A );
	$btc_total_avg = $hitbtc_market_history[0]['btc_total'];


	$last_btc_total = $wpdb->get_results( "SELECT `total` AS `last_btc_total`
																	FROM `{$wpdb->prefix}crypto_hitbtc_market_history`
																	WHERE `coin_ID` = '{$coin_ID}'
																		AND `ordertype` = '{$order_type}'
																	ORDER BY `datetime` DESC LIMIT 1", ARRAY_A );
	$last_btc_total = $last_btc_total[0]['last_btc_total'];

	if ( empty($btc_total_avg) ) 		{ return ""; }
	if ( empty($last_btc_total) ) 	{ return ""; }

	$column_name 	= $btc_total_avg;
	$last_data 		= $last_btc_total;

	if ( isset($column_name) && !empty($column_name) &&
			 isset($last_data) && !empty($last_data) ) {

			$positive_negative = "";
			if ( $column_name == $last_data ) 			{ $positive_negative = array( '=', '<span class="equal">=</span>' ); }
			else if ( $column_name > $last_data ) 	{ $positive_negative = array( '>', '<span class="glyphicon glyphicon-minus minus"></span>' ); }
			else if ( $column_name < $last_data ) 	{ $positive_negative = array( '<', '<span class="glyphicon glyphicon-plus plus"></span>' ); }

			$percentChange = percent_between_two_numbers($column_name, $last_data);
			$column_name 	= substr($column_name, 0, 6);
			$last_data 		= substr($last_data, 0, 6);


			 $html =
				 '<div class="hitbtc_total_sells_and_buys">
		 			 '. $positive_negative[1] .'
		 			 <div class="calculated_average hidden">'. $column_name .' → </div>
		 			 <div class="latest_num">'. round($percentChange,1) .'%</div>
		 		 </div>';

			return array(
									 'values' => array( $column_name, $last_data ),
									 'percent_change' => $percentChange,
									 'positive_negative' => $positive_negative[0],
									 'html' => $html,
								 );
	}
	return "";
}


function get_bitfinex_total_sells_and_buys($coin_ID = "", $order_type = "") {
	if ( empty($coin_ID) ) { return ""; }
	if ( empty($order_type) ) { return ""; }
	global $wpdb;

	if ( strtolower($order_type) == "sell" ) { $order_type = "s"; }
	else { $order_type = "b"; }

	$start_datetime = date('Y-m-d H:i:s', TIME_INTERVAL_TIMESTAMP);

	$bitfinex_market_history = $wpdb->get_results( "SELECT AVG(`total`) AS `btc_total`
																	FROM `{$wpdb->prefix}crypto_bitfinex_market_history`
																	WHERE `coin_ID` = '{$coin_ID}'
																		AND `datetime` >= '{$start_datetime}'
																		AND `ordertype` = '{$order_type}'
																	ORDER BY `datetime` ASC", ARRAY_A );
	$btc_total_avg = $bitfinex_market_history[0]['btc_total'];


	$last_btc_total = $wpdb->get_results( "SELECT `total` AS `last_btc_total`
																	FROM `{$wpdb->prefix}crypto_bitfinex_market_history`
																	WHERE `coin_ID` = '{$coin_ID}'
																		AND `ordertype` = '{$order_type}'
																	ORDER BY `datetime` DESC LIMIT 1", ARRAY_A );
	$last_btc_total = $last_btc_total[0]['last_btc_total'];

	if ( empty($btc_total_avg) ) 		{ return ""; }
	if ( empty($last_btc_total) ) 	{ return ""; }

	$column_name 	= $btc_total_avg;
	$last_data 		= $last_btc_total;

	if ( isset($column_name) && !empty($column_name) &&
			 isset($last_data) && !empty($last_data) ) {

			$positive_negative = "";
			if ( $column_name == $last_data ) 			{ $positive_negative = array( '=', '<span class="equal">=</span>' ); }
			else if ( $column_name > $last_data ) 	{ $positive_negative = array( '>', '<span class="glyphicon glyphicon-minus minus"></span>' ); }
			else if ( $column_name < $last_data ) 	{ $positive_negative = array( '<', '<span class="glyphicon glyphicon-plus plus"></span>' ); }

			$percentChange = percent_between_two_numbers($column_name, $last_data);
			$column_name 	= substr($column_name, 0, 6);
			$last_data 		= substr($last_data, 0, 6);


			 $html =
				 '<div class="bitfinex_total_sells_and_buys">
		 			 '. $positive_negative[1] .'
		 			 <div class="calculated_average hidden">'. $column_name .' → </div>
		 			 <div class="latest_num">'. round($percentChange,1) .'%</div>
		 		 </div>';

			return array(
									 'values' => array( $column_name, $last_data ),
									 'percent_change' => $percentChange,
									 'positive_negative' => $positive_negative[0],
									 'html' => $html,
								 );
	}
	return "";
}


function get_bittrex_total_sells_and_buys($coin_ID = "", $order_type = "") {
	if ( empty($coin_ID) ) { return ""; }
	if ( empty($order_type) ) { return ""; }
	global $wpdb;

	if ( strtolower($order_type) == "sell" ) { $order_type = "s"; }
	else { $order_type = "b"; }

	$start_datetime = date('Y-m-d H:i:s', TIME_INTERVAL_TIMESTAMP);

	$bittrex_market_history = $wpdb->get_results( "SELECT AVG(`total`) AS `btc_total`
																	FROM `{$wpdb->prefix}crypto_bittrex_market_history`
																	WHERE `coin_ID` = '{$coin_ID}'
																		AND `datetime` >= '{$start_datetime}'
																		AND `ordertype` = '{$order_type}'
																	ORDER BY `datetime` ASC", ARRAY_A );
	$btc_total_avg = $bittrex_market_history[0]['btc_total'];


	$last_btc_total = $wpdb->get_results( "SELECT `total` AS `last_btc_total`
																	FROM `{$wpdb->prefix}crypto_bittrex_market_history`
																	WHERE `coin_ID` = '{$coin_ID}'
																		AND `ordertype` = '{$order_type}'
																	ORDER BY `datetime` DESC LIMIT 1", ARRAY_A );
	$last_btc_total = $last_btc_total[0]['last_btc_total'];

	if ( empty($btc_total_avg) ) 		{ return ""; }
	if ( empty($last_btc_total) ) 	{ return ""; }

	$column_name 	= $btc_total_avg;
	$last_data 		= $last_btc_total;

	if ( isset($column_name) && !empty($column_name) &&
			 isset($last_data) && !empty($last_data) ) {

			$positive_negative = "";
			if ( $column_name == $last_data ) 			{ $positive_negative = array( '=', '<span class="equal">=</span>' ); }
			else if ( $column_name > $last_data ) 	{ $positive_negative = array( '>', '<span class="glyphicon glyphicon-minus minus"></span>' ); }
			else if ( $column_name < $last_data ) 	{ $positive_negative = array( '<', '<span class="glyphicon glyphicon-plus plus"></span>' ); }

			$percentChange = percent_between_two_numbers($column_name, $last_data);
			$column_name 	= substr($column_name, 0, 6);
			$last_data 		= substr($last_data, 0, 6);


			 $html =
				 '<div class="bittrex_total_sells_and_buys">
		 			 '. $positive_negative[1] .'
		 			 <div class="calculated_average hidden">'. $column_name .' → </div>
		 			 <div class="latest_num">'. round($percentChange,1) .'%</div>
		 		 </div>';

			return array(
									 'values' => array( $column_name, $last_data ),
									 'percent_change' => $percentChange,
									 'positive_negative' => $positive_negative[0],
									 'html' => $html,
								 );
	}
	return "";
}


function remove_error_log_file() {
	$files = scandir(ABSPATH);
	foreach ($files as $key => $file) {
		if (strpos($file, 'error_log') !== false) {
			unlink(ABSPATH . $file);
			break;
		}
	}
}


function check_coin_is_mineable($coin_ID = "") {
	if ( empty($coin_ID) ) { return ""; }
	global $wpdb;

	$crypto_mining_data = $wpdb->get_results( "SELECT `ID` FROM `{$wpdb->prefix}crypto_mining_data`
																							WHERE `coin_ID` = '{$coin_ID}'
																							LIMIT 1", ARRAY_A );

	if ( isset($crypto_mining_data[0]['ID']) && !empty($crypto_mining_data[0]['ID']) ) {
		return true;
	}
	return false;
}

function check_coin_is_on_bittrex($coin_ID = "") {
	if ( empty($coin_ID) ) { return ""; }
	global $wpdb;

	$crypto_bittrex_data = $wpdb->get_results( "SELECT `ID` FROM `{$wpdb->prefix}crypto_bittrex_data`
																				WHERE `coin_ID` = '{$coin_ID}'
																				LIMIT 1", ARRAY_A );

	if ( isset($crypto_bittrex_data[0]['ID']) && !empty($crypto_bittrex_data[0]['ID']) ) {
		return true;
	}
	return false;
}

function check_is_shitcoin_by_coin_name($coin_name = "") {
	if ( empty($coin_name) ) { return ""; }
	global $wpdb;

	$crypto_shitcoins = $wpdb->get_results( "SELECT `ID` FROM `{$wpdb->prefix}crypto_shitcoins`
																				WHERE `shitcoin_name` = '{$coin_name}'
																				LIMIT 1", ARRAY_A );

	if ( isset($crypto_shitcoins[0]['ID']) && !empty($crypto_shitcoins[0]['ID']) ) {
		return true;
	}
	return false;
}

function get_coin_id_by_name($coin_name = "") {
	if ( empty($coin_name) ) { return ""; }
	global $wpdb;

	$crypto_coin_names = $wpdb->get_results( "SELECT `ID` FROM `{$wpdb->prefix}crypto_coin_names`
																				WHERE `coin_name` = '{$coin_name}'
																				LIMIT 1", ARRAY_A );

	if ( isset($crypto_coin_names[0]['ID']) && !empty($crypto_coin_names[0]['ID']) ) {
		return (int) $crypto_coin_names[0]['ID'];
	}
	return "";
}

function get_coin_name_by_id($coin_ID = "") {
	if ( empty($coin_ID) ) { return ""; }
	global $wpdb;

	$crypto_coin_names = $wpdb->get_results( "SELECT `coin_name` FROM `{$wpdb->prefix}crypto_coin_names`
																				WHERE `ID` = '{$coin_ID}'
																				LIMIT 1", ARRAY_A );

	if ( isset($crypto_coin_names[0]['coin_name']) && !empty($crypto_coin_names[0]['coin_name']) ) {
		return $crypto_coin_names[0]['coin_name'];
	}
	return "";
}


function get_coin_created_date_by_id($coin_ID = "") {
	if ( empty($coin_ID) ) { return ""; }
	global $wpdb;

	$crypto_coin_created_date = $wpdb->get_results( "SELECT `created_date` FROM `{$wpdb->prefix}crypto_coin_names`
																				WHERE `ID` = '{$coin_ID}'
																				LIMIT 1", ARRAY_A );

	if ( isset($crypto_coin_created_date[0]['created_date']) &&
				!empty($crypto_coin_created_date[0]['created_date']) &&
				( $crypto_coin_created_date[0]['created_date'] != "#" )
		) {

		return $crypto_coin_created_date[0]['created_date']; // unix timestamp
	}
	return "";
}


function get_coin_id_by_full_name($coin_name = "") {
	if ( empty($coin_name) ) { return ""; }
	global $wpdb;

	$crypto_coin_id = $wpdb->get_results( "SELECT `ID` FROM `{$wpdb->prefix}crypto_coin_names`
																				WHERE `coin_full_name` = '{$coin_name}'
																				LIMIT 1", ARRAY_A );

	if ( isset($crypto_coin_id[0]['ID']) && !empty($crypto_coin_id[0]['ID']) ) {
		return (int) $crypto_coin_id[0]['ID'];
	}
	return "";
}


function get_coin_full_name_by_id($coin_ID = "") {
	if ( empty($coin_ID) ) { return ""; }
	global $wpdb;

	$crypto_coin_names = $wpdb->get_results( "SELECT `coin_full_name` FROM `{$wpdb->prefix}crypto_coin_names`
																				WHERE `ID` = '{$coin_ID}'
																				LIMIT 1", ARRAY_A );

	if ( isset($crypto_coin_names[0]['coin_full_name']) && !empty($crypto_coin_names[0]['coin_full_name']) ) {
		return $crypto_coin_names[0]['coin_full_name'];
	}
	return "";
}


// Logó URL törlése ha nem elérhető
function delete_a_logo_if_not_available($coin_ID = "", $logo_url = "") {
	if ( empty($coin_ID) ) 	{ return ""; }
	if ( empty($logo_url) ) { return ""; }
	if ( $logo_url == "#" ) { return ""; }
	global $wpdb;

	if ( getimagesize($logo_url) === FALSE ) {

		$wpdb->update(
			$wpdb->prefix .'crypto_coin_names',
			array(
				'logo_url' => "#",
			),
			array( 'ID' => $coin_ID ),
			array( '%s' ),
			array( '%d' )
		);

		return "deleted";
	}

	return "";
}


function get_coin_logo_img($coin_ID = "") {
	if ( empty($coin_ID) ) { return ""; }
	global $wpdb;

	$crypto_coin_logo_url = $wpdb->get_results( "SELECT `logo_url` FROM `{$wpdb->prefix}crypto_coin_names`
																				WHERE `ID` = '{$coin_ID}'
																				LIMIT 1", ARRAY_A );

	if ( isset($crypto_coin_logo_url[0]['logo_url']) && !empty($crypto_coin_logo_url[0]['logo_url']) ) {
		$logo_url = $crypto_coin_logo_url[0]['logo_url'];

		// Logó URL törlése, ha nem elérhető
		if ( delete_a_logo_if_not_available($coin_ID, $logo_url) == 'deleted' ) { return ""; }

	} else {
		// get & update
		$coin_name = get_coin_name_by_id($coin_ID);
		$logo_url = "";

		$coins_data = file_get_contents( 'https://s2.coinmarketcap.com/generated/search/quick_search.json' );
		$coins_data = json_decode($coins_data, true);

		$cmc_id = 0;
		foreach ($coins_data as $key => $val) {
			if ( $val['symbol'] == strtoupper($coin_name) ) {
				$cmc_id = (int) $val['id'];
				break;
			}
		}

		if ( $cmc_id > 0 ) {
			$logo_url = 'https://s2.coinmarketcap.com/static/img/coins/128x128/'. $cmc_id .'.png';
		}

		if ( empty($logo_url) ) {
			$logo_url = "#";
		}

		$wpdb->update(
			$wpdb->prefix .'crypto_coin_names',
			array(
				'logo_url' => $logo_url,
			),
			array( 'ID' => $coin_ID ),
			array( '%s' ),
			array( '%d' )
		);
	}

	if ( !empty($logo_url) && ( $logo_url != "#" ) ) {
		return '<img src="'. $logo_url .'" class="logo" />';
	}
	return "";
}


function get_coin_link($coin_ID = "") {
	if ( empty($coin_ID) ) { return ""; }
	global $wpdb;

	$crypto_coin_names = $wpdb->get_results( "SELECT `link` FROM `{$wpdb->prefix}crypto_coin_names`
																										WHERE `ID` = '{$coin_ID}'
																										LIMIT 1", ARRAY_A );

	if ( isset($crypto_coin_names[0]['link']) && !empty($crypto_coin_names[0]['link']) ) {
		return $crypto_coin_names[0]['link'];
	}
	return "#";
}


function get_creation_date($coin_ID = "") {
	if ( empty($coin_ID) ) { return ""; }
	global $wpdb;

	$crypto_coin_created_date = $wpdb->get_results( "SELECT `created_date` FROM `{$wpdb->prefix}crypto_coin_names`
																				WHERE `ID` = '{$coin_ID}'
																				LIMIT 1", ARRAY_A );

	if ( isset($crypto_coin_created_date[0]['created_date']) && !empty($crypto_coin_created_date[0]['created_date']) ) {
		$created_date = $crypto_coin_created_date[0]['created_date'];
	} else {
		// get & update
		$coin_name = get_coin_name_by_id($coin_ID);
		$created_date = "";

		$bittrex_getmarkets = file_get_contents( 'https://bittrex.com/api/v1.1/public/getmarkets' );
		$bittrex_getmarkets = json_decode($bittrex_getmarkets);

		foreach ($bittrex_getmarkets->result as $key => $coin) {
			if ( ($coin->MarketCurrency == $coin_name) && isset($coin->Created) ) {
				$created_date = strtotime($coin->Created);
				break;
			}
		}

		if ( empty($created_date) ) {
			$created_date = "#";
		}

		$wpdb->update(
			$wpdb->prefix .'crypto_coin_names',
			array(
				'created_date' => $created_date,
			),
			array( 'ID' => $coin_ID ),
			array( '%s' ),
			array( '%d' )
		);
	}

	if ( !empty($created_date) && ( $created_date != "#" ) ) {
		return $created_date;
	}
	return "";
}


// Get coinmarketcap averega USD price
function get_cmc_average_usd_price($coin_ID = "") {
	if ( empty($coin_ID) ) { return ""; }
	global $wpdb;

	$cmc_average_usd = $wpdb->get_results( "SELECT AVG(`price_usd`) AS `cmc_average_usd`
																										FROM `{$wpdb->prefix}crypto_coinmarketcap_data`
																										WHERE `coin_ID` = '{$coin_ID}'
																											AND `datetime` >= '". date('Y-m-d H:i:s', TIME_INTERVAL_TIMESTAMP) ."'
																										ORDER BY `datetime` ASC", ARRAY_A );

	if ( isset($cmc_average_usd[0]['cmc_average_usd']) && !empty($cmc_average_usd[0]['cmc_average_usd']) ) {
		return $cmc_average_usd[0]['cmc_average_usd'];
	}
	return "";
}


// Get coinmarketcap averega BTC price
function get_cmc_average_btc_price($coin_ID = "") {
	if ( empty($coin_ID) ) { return ""; }
	global $wpdb;

	$cmc_average_btc = $wpdb->get_results( "SELECT AVG(`price_btc`) AS `cmc_average_btc`
																										FROM `{$wpdb->prefix}crypto_coinmarketcap_data`
																										WHERE `coin_ID` = '{$coin_ID}'
																											AND `datetime` >= '". date('Y-m-d H:i:s', TIME_INTERVAL_TIMESTAMP) ."'
																										ORDER BY `datetime` ASC", ARRAY_A );

	if ( isset($cmc_average_btc[0]['cmc_average_btc']) && !empty($cmc_average_btc[0]['cmc_average_btc']) ) {
		return number_format($cmc_average_btc[0]['cmc_average_btc'],8);
	}
	return "";
}


function get_last_rank_by_coin_id($coin_ID = "") {
	if ( empty($coin_ID) ) { return ""; }
	global $wpdb;

	$result = $wpdb->get_row( "	SELECT `rank`
															FROM `{$wpdb->prefix}crypto_coinmarketcap_data`
															WHERE `coin_ID` = '{$coin_ID}'
															ORDER BY `datetime` DESC" );

	if ( isset($result->rank) ) {
		if ( intval($result->rank) > 0 ) {
			return (int) $result->rank;
		}
	}

	return "";
}


// return 20 → 25 → 27 → 24 → 23 → 25 → 26 → 24 → 25 → 26
function get_ranks($coin_ID = "") {
	if ( empty($coin_ID) ) { return ""; }
	global $wpdb;

	$ranks = $wpdb->get_results( "SELECT `rank`
																	FROM `{$wpdb->prefix}crypto_coinmarketcap_data`
																	WHERE `coin_ID` = '{$coin_ID}'
																		AND `datetime` >= '". date('Y-m-d H:i:s', TIME_INTERVAL_TIMESTAMP) ."'
																	ORDER BY `datetime` ASC", ARRAY_A );

	$ranks_array = array();
	foreach ($ranks as $key => $rank) {
		$ranks_array []= $rank['rank'];
	}
	$ranks_array = remove_same_values($ranks_array);

	if ( count($ranks_array) <= 10 ) {
		return implode(' → ', $ranks_array);
	} else if ( count($ranks_array) > 10 ) {
		$ranks_array = array_merge(array_slice($ranks_array, 0, 1), array_slice($ranks_array, -9) );
		return implode(' → ', $ranks_array);
	}
	return "";
}


// functions for rating
// return "falling", "equal", "growing"
function check_rank_falls($coin_ID = "") {
	if ( empty($coin_ID) ) { return ""; }
	global $wpdb;

	$rank_average = $wpdb->get_results( "SELECT AVG(`rank`) AS `rank_average`
																										FROM `{$wpdb->prefix}crypto_coinmarketcap_data`
																										WHERE `coin_ID` = '{$coin_ID}'
																											AND `datetime` >= '". date('Y-m-d H:i:s', TIME_INTERVAL_TIMESTAMP) ."'
																										ORDER BY `datetime` ASC", ARRAY_A );

	$last_rank = $wpdb->get_results( "SELECT `rank` AS `last_rank`
																			FROM `{$wpdb->prefix}crypto_coinmarketcap_data`
																			WHERE `coin_ID` = '{$coin_ID}'
																			ORDER BY `datetime` DESC LIMIT 1", ARRAY_A );

	if ( isset($rank_average[0]['rank_average']) && !empty($rank_average[0]['rank_average']) &&
			 isset($last_rank[0]['last_rank']) && !empty($last_rank[0]['last_rank'])
 	 	 ) {

		 if ( intval($rank_average[0]['rank_average']) < intval($last_rank[0]['last_rank']) ) {
			 return "falling";
		 } else if ( intval($rank_average[0]['rank_average']) == intval($last_rank[0]['last_rank']) ) {
			 return "equal";
		 } else if ( intval($rank_average[0]['rank_average']) > intval($last_rank[0]['last_rank']) ) {
			 return "growing";
		 } else {
			 return "";
		 }
	}
	return "";
}


// Deprecated
function get_cmc_average_usd_price_range($coin_ID = "", $time_interval_timestamp = "") {
	if ( empty($coin_ID) ) { return ""; }
	if ( empty($time_interval_timestamp) ) { $time_interval_timestamp = TIME_INTERVAL_TIMESTAMP; }
	global $wpdb;

	$prices = $wpdb->get_results( "SELECT `price_usd`
																	FROM `{$wpdb->prefix}crypto_coinmarketcap_data`
																	WHERE `coin_ID` = '{$coin_ID}'
																		AND `datetime` >= '". date('Y-m-d H:i:s', $time_interval_timestamp) ."'
																	ORDER BY `datetime` ASC", ARRAY_A );

	$min_price = $wpdb->get_results( "SELECT CAST( `price_usd` AS DECIMAL(10,6) ) as `min_price`
																	FROM `{$wpdb->prefix}crypto_coinmarketcap_data`
																	WHERE `coin_ID` = '{$coin_ID}'
																		AND `datetime` >= '". date('Y-m-d H:i:s', $time_interval_timestamp) ."'
																	ORDER BY `min_price` ASC LIMIT 1", ARRAY_A );
	$min_price = $min_price[0]['min_price'];


	$max_price = $wpdb->get_results( "SELECT CAST( `price_usd` AS DECIMAL(10,6) ) as `max_price`
																	FROM `{$wpdb->prefix}crypto_coinmarketcap_data`
																	WHERE `coin_ID` = '{$coin_ID}'
																		AND `datetime` >= '". date('Y-m-d H:i:s', $time_interval_timestamp) ."'
																	ORDER BY `max_price` DESC LIMIT 1", ARRAY_A );
	$max_price = $max_price[0]['max_price'];


	$prices_array = array();
	foreach ($prices as $key => $value) {
		if ( isset($value['price_usd']) && !empty($value['price_usd']) ) {
			$prices_array []= $value['price_usd'];
		}
	}


	if ( !empty($prices_array) ) {
		$oszto = ($max_price - $min_price) / 12;

		$v1_sta = $min_price;
		$v1_end = $min_price + ( $oszto * 1 );

		$v2_sta = $v1_end;
		$v2_end = $min_price + ( $oszto * 2 );

		$v3_sta = $v2_end;
		$v3_end = $min_price + ( $oszto * 3 );

		$v4_sta = $v3_end;
		$v4_end = $min_price + ( $oszto * 4 );

		$v5_sta = $v4_end;
		$v5_end = $min_price + ( $oszto * 5 );

		$v6_sta = $v5_end;
		$v6_end = $min_price + ( $oszto * 6 );

		$v7_sta = $v6_end;
		$v7_end = $min_price + ( $oszto * 7 );

		$v8_sta = $v7_end;
		$v8_end = $min_price + ( $oszto * 8 );

		$v9_sta = $v8_end;
		$v9_end = $min_price + ( $oszto * 9 );

		$v10_sta = $v9_end;
		$v10_end = $min_price + ( $oszto * 10 );

		$v11_sta = $v10_end;
		$v11_end = $min_price + ( $oszto * 11 );

		$v12_sta = $v11_end;
		$v12_end = $max_price;


		$arak_csoportositasa = array();
		foreach ($prices_array as $key => $price) {

			if ( $v12_sta < $price ) {
				if ( $v12_end >= $price ) { $arak_csoportositasa []= 'v12'; }
			}
			else if ( $v11_sta < $price ) {
				if ( $v11_end >= $price ) { $arak_csoportositasa []= 'v11'; }
			}
			else if ( $v10_sta < $price ) {
				if ( $v10_end >= $price ) { $arak_csoportositasa []= 'v10'; }
			}
			else if ( $v9_sta < $price ) {
				if ( $v9_end >= $price ) { $arak_csoportositasa []= 'v9'; }
			}
			else if ( $v8_sta < $price ) {
				if ( $v8_end >= $price ) { $arak_csoportositasa []= 'v8'; }
			}
			else if ( $v7_sta < $price ) {
				if ( $v7_end >= $price ) { $arak_csoportositasa []= 'v7'; }
			}
			else if ( $v6_sta < $price ) {
				if ( $v6_end >= $price ) { $arak_csoportositasa []= 'v6'; }
			}
			else if ( $v5_sta < $price ) {
				if ( $v5_end >= $price ) { $arak_csoportositasa []= 'v5'; }
			}
			else if ( $v4_sta < $price ) {
				if ( $v4_end >= $price ) { $arak_csoportositasa []= 'v4'; }
			}
			else if ( $v3_sta < $price ) {
				if ( $v3_end >= $price ) { $arak_csoportositasa []= 'v3'; }
			}
			else if ( $v2_sta < $price ) {
				if ( $v2_end >= $price ) { $arak_csoportositasa []= 'v2'; }
			}
			else if ( $v1_sta < $price ) {
				if ( $v1_end >= $price ) { $arak_csoportositasa []= 'v1'; }
			}
		}

		$arak_csoportositasa = array_count_values($arak_csoportositasa);
		arsort($arak_csoportositasa);

		$winner = "";
		foreach ($arak_csoportositasa as $key => $value) {
			$winner = $key;
			break;
		}

		$tartomany_start = "";
		$tartomany_end = "";

				 if ( $winner == 'v1' ) { $tartomany_start = $v1_sta; $tartomany_end = $v1_end; }
		else if ( $winner == 'v2' ) { $tartomany_start = $v2_sta; $tartomany_end = $v2_end; }
		else if ( $winner == 'v3' ) { $tartomany_start = $v3_sta; $tartomany_end = $v3_end; }
		else if ( $winner == 'v4' ) { $tartomany_start = $v4_sta; $tartomany_end = $v4_end; }
		else if ( $winner == 'v5' ) { $tartomany_start = $v5_sta; $tartomany_end = $v5_end; }
		else if ( $winner == 'v6' ) { $tartomany_start = $v6_sta; $tartomany_end = $v6_end; }
		else if ( $winner == 'v7' ) { $tartomany_start = $v7_sta; $tartomany_end = $v7_end; }
		else if ( $winner == 'v8' ) { $tartomany_start = $v8_sta; $tartomany_end = $v8_end; }
		else if ( $winner == 'v9' ) { $tartomany_start = $v9_sta; $tartomany_end = $v9_end; }
		else if ( $winner == 'v10' ){ $tartomany_start = $v10_sta; $tartomany_end = $v10_end; }
		else if ( $winner == 'v11' ){ $tartomany_start = $v11_sta; $tartomany_end = $v11_end; }
		else if ( $winner == 'v12' ){ $tartomany_start = $v12_sta; $tartomany_end = $v12_end; }


		if ( isset($tartomany_start) && !empty($tartomany_start) ) {
			return array(
										'tartomany_start' => $tartomany_start,
										'tartomany_end' 	=> $tartomany_end,
										'html' 						=> $tartomany_end .'<br>'. $tartomany_start,
									);
		}
	}
	return "";
}


// Deprecated
function get_cmc_average_btc_price_range($coin_ID = "") {
	if ( empty($coin_ID) ) { return ""; }
	global $wpdb;

	$prices = $wpdb->get_results( "SELECT `price_btc`
																	FROM `{$wpdb->prefix}crypto_coinmarketcap_data`
																	WHERE `coin_ID` = '{$coin_ID}'
																		AND `datetime` >= '". date('Y-m-d H:i:s', TIME_INTERVAL_TIMESTAMP) ."'
																	ORDER BY `datetime` ASC", ARRAY_A );

	$min_price = $wpdb->get_results( "SELECT CAST( `price_btc` AS DECIMAL(10,6) ) as `min_price`
																	FROM `{$wpdb->prefix}crypto_coinmarketcap_data`
																	WHERE `coin_ID` = '{$coin_ID}'
																		AND `datetime` >= '". date('Y-m-d H:i:s', TIME_INTERVAL_TIMESTAMP) ."'
																	ORDER BY `min_price` ASC LIMIT 1", ARRAY_A );
	$min_price = number_format($min_price[0]['min_price'],8);


	$max_price = $wpdb->get_results( "SELECT CAST( `price_btc` AS DECIMAL(10,6) ) as `max_price`
																	FROM `{$wpdb->prefix}crypto_coinmarketcap_data`
																	WHERE `coin_ID` = '{$coin_ID}'
																		AND `datetime` >= '". date('Y-m-d H:i:s', TIME_INTERVAL_TIMESTAMP) ."'
																	ORDER BY `max_price` DESC LIMIT 1", ARRAY_A );
	$max_price = number_format($max_price[0]['max_price'],8);


	$prices_array = array();
	foreach ($prices as $key => $value) {
		if ( isset($value['price_btc']) && !empty($value['price_btc']) ) {
			$prices_array []= floatval(number_format($value['price_btc'],10));
		}
	}

	if ( !empty($prices_array) ) {

		$oszto = ($max_price - $min_price) / 12;

		$v1_sta = $min_price;
		$v1_end = $min_price + ( $oszto * 1 );

		$v2_sta = $v1_end;
		$v2_end = $min_price + ( $oszto * 2 );

		$v3_sta = $v2_end;
		$v3_end = $min_price + ( $oszto * 3 );

		$v4_sta = $v3_end;
		$v4_end = $min_price + ( $oszto * 4 );

		$v5_sta = $v4_end;
		$v5_end = $min_price + ( $oszto * 5 );

		$v6_sta = $v5_end;
		$v6_end = $min_price + ( $oszto * 6 );

		$v7_sta = $v6_end;
		$v7_end = $min_price + ( $oszto * 7 );

		$v8_sta = $v7_end;
		$v8_end = $min_price + ( $oszto * 8 );

		$v9_sta = $v8_end;
		$v9_end = $min_price + ( $oszto * 9 );

		$v10_sta = $v9_end;
		$v10_end = $min_price + ( $oszto * 10 );

		$v11_sta = $v10_end;
		$v11_end = $min_price + ( $oszto * 11 );

		$v12_sta = $v11_end;
		$v12_end = $max_price;


		$arak_csoportositasa = array();
		foreach ($prices_array as $key => $price) {

			if ( $v12_sta < $price ) {
				if ( $v12_end >= $price ) { $arak_csoportositasa []= 'v12'; }
			}
			else if ( $v11_sta < $price ) {
				if ( $v11_end >= $price ) { $arak_csoportositasa []= 'v11'; }
			}
			else if ( $v10_sta < $price ) {
				if ( $v10_end >= $price ) { $arak_csoportositasa []= 'v10'; }
			}
			else if ( $v9_sta < $price ) {
				if ( $v9_end >= $price ) { $arak_csoportositasa []= 'v9'; }
			}
			else if ( $v8_sta < $price ) {
				if ( $v8_end >= $price ) { $arak_csoportositasa []= 'v8'; }
			}
			else if ( $v7_sta < $price ) {
				if ( $v7_end >= $price ) { $arak_csoportositasa []= 'v7'; }
			}
			else if ( $v6_sta < $price ) {
				if ( $v6_end >= $price ) { $arak_csoportositasa []= 'v6'; }
			}
			else if ( $v5_sta < $price ) {
				if ( $v5_end >= $price ) { $arak_csoportositasa []= 'v5'; }
			}
			else if ( $v4_sta < $price ) {
				if ( $v4_end >= $price ) { $arak_csoportositasa []= 'v4'; }
			}
			else if ( $v3_sta < $price ) {
				if ( $v3_end >= $price ) { $arak_csoportositasa []= 'v3'; }
			}
			else if ( $v2_sta < $price ) {
				if ( $v2_end >= $price ) { $arak_csoportositasa []= 'v2'; }
			}
			else if ( $v1_sta < $price ) {
				if ( $v1_end >= $price ) { $arak_csoportositasa []= 'v1'; }
			}
		}
		$arak_csoportositasa = array_count_values($arak_csoportositasa);
		arsort($arak_csoportositasa);

		$winner = "";
		foreach ($arak_csoportositasa as $key => $value) {
			$winner = $key;
			break;
		}

		$tartomany_start = "";
		$tartomany_end = "";

				 if ( $winner == 'v1' ) { $tartomany_start = $v1_sta; $tartomany_end = $v1_end; }
		else if ( $winner == 'v2' ) { $tartomany_start = $v2_sta; $tartomany_end = $v2_end; }
		else if ( $winner == 'v3' ) { $tartomany_start = $v3_sta; $tartomany_end = $v3_end; }
		else if ( $winner == 'v4' ) { $tartomany_start = $v4_sta; $tartomany_end = $v4_end; }
		else if ( $winner == 'v5' ) { $tartomany_start = $v5_sta; $tartomany_end = $v5_end; }
		else if ( $winner == 'v6' ) { $tartomany_start = $v6_sta; $tartomany_end = $v6_end; }
		else if ( $winner == 'v7' ) { $tartomany_start = $v7_sta; $tartomany_end = $v7_end; }
		else if ( $winner == 'v8' ) { $tartomany_start = $v8_sta; $tartomany_end = $v8_end; }
		else if ( $winner == 'v9' ) { $tartomany_start = $v9_sta; $tartomany_end = $v9_end; }
		else if ( $winner == 'v10' ){ $tartomany_start = $v10_sta; $tartomany_end = $v10_end; }
		else if ( $winner == 'v11' ){ $tartomany_start = $v11_sta; $tartomany_end = $v11_end; }
		else if ( $winner == 'v12' ){ $tartomany_start = $v12_sta; $tartomany_end = $v12_end; }


		if ( isset($tartomany_start) && !empty($tartomany_start) ) {
			return array(
										'tartomany_start' => number_format($tartomany_start,8),
										'tartomany_end' 	=> number_format($tartomany_end,8),
										'html' 						=> number_format($tartomany_end,8) .'<br>'. number_format($tartomany_start,8),
									);
		}
	}
	return "";
}


// Get oldest coinmarketcap data (from last TIME_INTERVAL_TIMESTAMP interval)
function get_oldest_cmc_data($coin_ID = "") {
	if ( empty($coin_ID) ) { return ""; }
	global $wpdb;

	$datetime = $wpdb->get_results( "SELECT `datetime`
																	FROM `{$wpdb->prefix}crypto_coinmarketcap_data`
																	WHERE `coin_ID` = '{$coin_ID}'
																		AND `datetime` >= '". date('Y-m-d H:i:s', TIME_INTERVAL_TIMESTAMP) ."'
																	ORDER BY `datetime` ASC LIMIT 1", ARRAY_A );

	if ( isset($datetime[0]['datetime']) && !empty($datetime[0]['datetime']) ) {
		return strtotime($datetime[0]['datetime']);
	}
}


function array_sort_by_column(&$arr = "", $col = "", $dir = SORT_ASC) {
	$sort_col = array();
	foreach ($arr as $key=> $row) {
	    $sort_col[$key] = $row[$col];
	}

	array_multisort($sort_col, $dir, $arr);
}


function get_bittrex_last_price($coin_ID = "") {
	if ( empty($coin_ID) ) { return ""; }
	global $wpdb;

	$last_price = $wpdb->get_results( "SELECT `close` as `last_price`
																	FROM `{$wpdb->prefix}crypto_bittrex_data`
																	WHERE `coin_ID` = '{$coin_ID}'
																	ORDER BY `datetime` DESC LIMIT 1", ARRAY_A );

	if ( isset($last_price[0]['last_price']) && !empty($last_price[0]['last_price']) ) {
		return $last_price[0]['last_price'];
	}
	return "";
}


function get_bittrex_av_btc_price($coin_ID = "", $interval = TIME_INTERVAL_TIMESTAMP) {
	if ( empty($coin_ID) ) { return ""; }
	global $wpdb;

	$bittrex_btc_price = $wpdb->get_results( "SELECT AVG(`close`) AS `btc_price`
																										FROM `{$wpdb->prefix}crypto_bittrex_data`
																										WHERE `coin_ID` = '{$coin_ID}'
																											AND `datetime` >= '". date('Y-m-d H:i:s', $interval) ."'
																										ORDER BY `datetime` ASC", ARRAY_A );

	if ( isset($bittrex_btc_price[0]['btc_price']) && !empty($bittrex_btc_price[0]['btc_price']) ) {
		return number_format($bittrex_btc_price[0]['btc_price'],8);
	}
	return "";
}


function get_cmc_average_usd_price_range_extended($coin_ID = "") {
	if ( empty($coin_ID) ) { return ""; }
	global $wpdb;
	$start_datetime = date('Y-m-d H:i:s', TIME_INTERVAL_TIMESTAMP);

	$prices = $wpdb->get_results( "SELECT `price_usd` as `price_usd`
																	FROM `{$wpdb->prefix}crypto_coinmarketcap_data`
																	WHERE `coin_ID` = '{$coin_ID}'
																		AND `datetime` >= '{$start_datetime}'
																	ORDER BY `datetime` ASC", ARRAY_A );

	$min_price = $wpdb->get_results( "SELECT `price_usd` as `min_price`
																	FROM `{$wpdb->prefix}crypto_coinmarketcap_data`
																	WHERE `coin_ID` = '{$coin_ID}'
																		AND `datetime` >= '{$start_datetime}'
																	ORDER BY `price_usd` ASC LIMIT 1", ARRAY_A );
	$min_price = $min_price[0]['min_price'];


	$max_price = $wpdb->get_results( "SELECT `price_usd` as `max_price`
																	FROM `{$wpdb->prefix}crypto_coinmarketcap_data`
																	WHERE `coin_ID` = '{$coin_ID}'
																		AND `datetime` >= '{$start_datetime}'
																	ORDER BY `price_usd` DESC LIMIT 1", ARRAY_A );
	$max_price = $max_price[0]['max_price'];


	$prices_array = array();
	foreach ($prices as $key => $value) {
		if ( isset($value['price_usd']) && !empty($value['price_usd']) ) {
			$prices_array []= (float) $value['price_usd'];
		}
	}

	if ( !empty($prices_array) ) {

		$oszto = ($max_price - $min_price) / 12;

		$v1_sta = $min_price;
		$v1_end = $min_price + ( $oszto * 1 );

		$v2_sta = $v1_end;
		$v2_end = $min_price + ( $oszto * 2 );

		$v3_sta = $v2_end;
		$v3_end = $min_price + ( $oszto * 3 );

		$v4_sta = $v3_end;
		$v4_end = $min_price + ( $oszto * 4 );

		$v5_sta = $v4_end;
		$v5_end = $min_price + ( $oszto * 5 );

		$v6_sta = $v5_end;
		$v6_end = $min_price + ( $oszto * 6 );

		$v7_sta = $v6_end;
		$v7_end = $min_price + ( $oszto * 7 );

		$v8_sta = $v7_end;
		$v8_end = $min_price + ( $oszto * 8 );

		$v9_sta = $v8_end;
		$v9_end = $min_price + ( $oszto * 9 );

		$v10_sta = $v9_end;
		$v10_end = $min_price + ( $oszto * 10 );

		$v11_sta = $v10_end;
		$v11_end = $min_price + ( $oszto * 11 );

		$v12_sta = $v11_end;
		$v12_end = $max_price;

		$arak_csoportositasa = array();
		foreach ($prices_array as $key => $price) {

			if ( $v12_sta < $price ) {
				if ( $v12_end >= $price ) { $arak_csoportositasa []= 'v12'; }
			}
			else if ( $v11_sta < $price ) {
				if ( $v11_end >= $price ) { $arak_csoportositasa []= 'v11'; }
			}
			else if ( $v10_sta < $price ) {
				if ( $v10_end >= $price ) { $arak_csoportositasa []= 'v10'; }
			}
			else if ( $v9_sta < $price ) {
				if ( $v9_end >= $price ) { $arak_csoportositasa []= 'v9'; }
			}
			else if ( $v8_sta < $price ) {
				if ( $v8_end >= $price ) { $arak_csoportositasa []= 'v8'; }
			}
			else if ( $v7_sta < $price ) {
				if ( $v7_end >= $price ) { $arak_csoportositasa []= 'v7'; }
			}
			else if ( $v6_sta < $price ) {
				if ( $v6_end >= $price ) { $arak_csoportositasa []= 'v6'; }
			}
			else if ( $v5_sta < $price ) {
				if ( $v5_end >= $price ) { $arak_csoportositasa []= 'v5'; }
			}
			else if ( $v4_sta < $price ) {
				if ( $v4_end >= $price ) { $arak_csoportositasa []= 'v4'; }
			}
			else if ( $v3_sta < $price ) {
				if ( $v3_end >= $price ) { $arak_csoportositasa []= 'v3'; }
			}
			else if ( $v2_sta < $price ) {
				if ( $v2_end >= $price ) { $arak_csoportositasa []= 'v2'; }
			}
			else if ( $v1_sta < $price ) {
				if ( $v1_end >= $price ) { $arak_csoportositasa []= 'v1'; }
			}
		}
		$arak_csoportositasa = array_count_values($arak_csoportositasa);
		arsort($arak_csoportositasa);

		$winner = "";
		foreach ($arak_csoportositasa as $key => $value) {
			$winner = $key;
			break;
		}

		$tartomany_start = "";
		$tartomany_end = "";

				 if ( $winner == 'v1' ) { $tartomany_start = $v1_sta; $tartomany_end = $v1_end; }
		else if ( $winner == 'v2' ) { $tartomany_start = $v2_sta; $tartomany_end = $v2_end; }
		else if ( $winner == 'v3' ) { $tartomany_start = $v3_sta; $tartomany_end = $v3_end; }
		else if ( $winner == 'v4' ) { $tartomany_start = $v4_sta; $tartomany_end = $v4_end; }
		else if ( $winner == 'v5' ) { $tartomany_start = $v5_sta; $tartomany_end = $v5_end; }
		else if ( $winner == 'v6' ) { $tartomany_start = $v6_sta; $tartomany_end = $v6_end; }
		else if ( $winner == 'v7' ) { $tartomany_start = $v7_sta; $tartomany_end = $v7_end; }
		else if ( $winner == 'v8' ) { $tartomany_start = $v8_sta; $tartomany_end = $v8_end; }
		else if ( $winner == 'v9' ) { $tartomany_start = $v9_sta; $tartomany_end = $v9_end; }
		else if ( $winner == 'v10' ){ $tartomany_start = $v10_sta; $tartomany_end = $v10_end; }
		else if ( $winner == 'v11' ){ $tartomany_start = $v11_sta; $tartomany_end = $v11_end; }
		else if ( $winner == 'v12' ){ $tartomany_start = $v12_sta; $tartomany_end = $v12_end; }


		// Árcsoportok százalékos számítása
		$percentage_distribution_of_the_total_price = "";
		if ( !empty($arak_csoportositasa) ) {
			$max_value = "";
			foreach ($arak_csoportositasa as $key => $value) { $max_value = $value; break; }

			$percentage_distribution_of_the_total_price = array();
			foreach ($arak_csoportositasa as $key => $value) {

						if (  $key == 'v1' ) { $ts = $v1_sta; $te = $v1_end; }
				else if ( $key == 'v2' ) { $ts = $v2_sta; $te = $v2_end; }
				else if ( $key == 'v3' ) { $ts = $v3_sta; $te = $v3_end; }
				else if ( $key == 'v4' ) { $ts = $v4_sta; $te = $v4_end; }
				else if ( $key == 'v5' ) { $ts = $v5_sta; $te = $v5_end; }
				else if ( $key == 'v6' ) { $ts = $v6_sta; $te = $v6_end; }
				else if ( $key == 'v7' ) { $ts = $v7_sta; $te = $v7_end; }
				else if ( $key == 'v8' ) { $ts = $v8_sta; $te = $v8_end; }
				else if ( $key == 'v9' ) { $ts = $v9_sta; $te = $v9_end; }
				else if ( $key == 'v10' ){ $ts = $v10_sta; $te = $v10_end; }
				else if ( $key == 'v11' ){ $ts = $v11_sta; $te = $v11_end; }
				else if ( $key == 'v12' ){ $ts = $v12_sta; $te = $v12_end; }

				$percent = round( ($value * 100) / $max_value , 2);
				if ( $value == $max_value ) { $percent = 100; }

				$percentage_distribution_of_the_total_price []= array(
																															'tartomany_start' => $ts,
																															'tartomany_end' => $te,
																															'how_many_times' => $value,
																															'how_many_times_in_percent' => $percent,
																														 );
			}
		}
		// ---

		if ( isset($tartomany_start) && !empty($tartomany_start) ) {
			return array(
										'tartomany_start' => $tartomany_start,
										'tartomany_end' 	=> $tartomany_end,
										'html' 						=> $tartomany_end .'<br>'. $tartomany_start,
										'percentage_distribution_of_the_total_price' => $percentage_distribution_of_the_total_price,
									);
		}
	}
	return "";
}


function get_bittrex_average_btc_price_range($coin_ID = "") {
	if ( empty($coin_ID) ) { return ""; }
	global $wpdb;
	$start_datetime = date('Y-m-d H:i:s', TIME_INTERVAL_TIMESTAMP);

	$prices = $wpdb->get_results( "SELECT `close` as `price_btc`
																	FROM `{$wpdb->prefix}crypto_bittrex_data`
																	WHERE `coin_ID` = '{$coin_ID}'
																		AND `datetime` >= '{$start_datetime}'
																	ORDER BY `datetime` ASC", ARRAY_A );

	$min_price = $wpdb->get_results( "SELECT `low` as `min_price`
																	FROM `{$wpdb->prefix}crypto_bittrex_data`
																	WHERE `coin_ID` = '{$coin_ID}'
																		AND `datetime` >= '{$start_datetime}'
																	ORDER BY `low` ASC LIMIT 1", ARRAY_A );
	$min_price = number_format($min_price[0]['min_price'],8);


	$max_price = $wpdb->get_results( "SELECT `high` as `max_price`
																	FROM `{$wpdb->prefix}crypto_bittrex_data`
																	WHERE `coin_ID` = '{$coin_ID}'
																		AND `datetime` >= '{$start_datetime}'
																	ORDER BY `high` DESC LIMIT 1", ARRAY_A );

	$max_price = number_format($max_price[0]['max_price'],8);

	$prices_array = array();
	foreach ($prices as $key => $value) {
		if ( isset($value['price_btc']) && !empty($value['price_btc']) ) {
			$prices_array []= floatval(number_format($value['price_btc'],10));
		}
	}

	if ( !empty($prices_array) ) {

		$oszto = ($max_price - $min_price) / 12;

		$v1_sta = $min_price;
		$v1_end = $min_price + ( $oszto * 1 );

		$v2_sta = $v1_end;
		$v2_end = $min_price + ( $oszto * 2 );

		$v3_sta = $v2_end;
		$v3_end = $min_price + ( $oszto * 3 );

		$v4_sta = $v3_end;
		$v4_end = $min_price + ( $oszto * 4 );

		$v5_sta = $v4_end;
		$v5_end = $min_price + ( $oszto * 5 );

		$v6_sta = $v5_end;
		$v6_end = $min_price + ( $oszto * 6 );

		$v7_sta = $v6_end;
		$v7_end = $min_price + ( $oszto * 7 );

		$v8_sta = $v7_end;
		$v8_end = $min_price + ( $oszto * 8 );

		$v9_sta = $v8_end;
		$v9_end = $min_price + ( $oszto * 9 );

		$v10_sta = $v9_end;
		$v10_end = $min_price + ( $oszto * 10 );

		$v11_sta = $v10_end;
		$v11_end = $min_price + ( $oszto * 11 );

		$v12_sta = $v11_end;
		$v12_end = $max_price;

		$arak_csoportositasa = array();
		foreach ($prices_array as $key => $price) {

			if ( $v12_sta < $price ) {
				if ( $v12_end >= $price ) { $arak_csoportositasa []= 'v12'; }
			}
			else if ( $v11_sta < $price ) {
				if ( $v11_end >= $price ) { $arak_csoportositasa []= 'v11'; }
			}
			else if ( $v10_sta < $price ) {
				if ( $v10_end >= $price ) { $arak_csoportositasa []= 'v10'; }
			}
			else if ( $v9_sta < $price ) {
				if ( $v9_end >= $price ) { $arak_csoportositasa []= 'v9'; }
			}
			else if ( $v8_sta < $price ) {
				if ( $v8_end >= $price ) { $arak_csoportositasa []= 'v8'; }
			}
			else if ( $v7_sta < $price ) {
				if ( $v7_end >= $price ) { $arak_csoportositasa []= 'v7'; }
			}
			else if ( $v6_sta < $price ) {
				if ( $v6_end >= $price ) { $arak_csoportositasa []= 'v6'; }
			}
			else if ( $v5_sta < $price ) {
				if ( $v5_end >= $price ) { $arak_csoportositasa []= 'v5'; }
			}
			else if ( $v4_sta < $price ) {
				if ( $v4_end >= $price ) { $arak_csoportositasa []= 'v4'; }
			}
			else if ( $v3_sta < $price ) {
				if ( $v3_end >= $price ) { $arak_csoportositasa []= 'v3'; }
			}
			else if ( $v2_sta < $price ) {
				if ( $v2_end >= $price ) { $arak_csoportositasa []= 'v2'; }
			}
			else if ( $v1_sta < $price ) {
				if ( $v1_end >= $price ) { $arak_csoportositasa []= 'v1'; }
			}
		}
		$arak_csoportositasa = array_count_values($arak_csoportositasa);
		arsort($arak_csoportositasa);

		$winner = "";
		foreach ($arak_csoportositasa as $key => $value) {
			$winner = $key;
			break;
		}

		$tartomany_start = "";
		$tartomany_end = "";

				 if ( $winner == 'v1' ) { $tartomany_start = $v1_sta; $tartomany_end = $v1_end; }
		else if ( $winner == 'v2' ) { $tartomany_start = $v2_sta; $tartomany_end = $v2_end; }
		else if ( $winner == 'v3' ) { $tartomany_start = $v3_sta; $tartomany_end = $v3_end; }
		else if ( $winner == 'v4' ) { $tartomany_start = $v4_sta; $tartomany_end = $v4_end; }
		else if ( $winner == 'v5' ) { $tartomany_start = $v5_sta; $tartomany_end = $v5_end; }
		else if ( $winner == 'v6' ) { $tartomany_start = $v6_sta; $tartomany_end = $v6_end; }
		else if ( $winner == 'v7' ) { $tartomany_start = $v7_sta; $tartomany_end = $v7_end; }
		else if ( $winner == 'v8' ) { $tartomany_start = $v8_sta; $tartomany_end = $v8_end; }
		else if ( $winner == 'v9' ) { $tartomany_start = $v9_sta; $tartomany_end = $v9_end; }
		else if ( $winner == 'v10' ){ $tartomany_start = $v10_sta; $tartomany_end = $v10_end; }
		else if ( $winner == 'v11' ){ $tartomany_start = $v11_sta; $tartomany_end = $v11_end; }
		else if ( $winner == 'v12' ){ $tartomany_start = $v12_sta; $tartomany_end = $v12_end; }


		// Árcsoportok százalékos számítása
		$percentage_distribution_of_the_total_price = "";
		if ( !empty($arak_csoportositasa) ) {
			$max_value = "";
			foreach ($arak_csoportositasa as $key => $value) { $max_value = $value; break; }

			$percentage_distribution_of_the_total_price = array();
			foreach ($arak_csoportositasa as $key => $value) {

						if (  $key == 'v1' ) { $ts = $v1_sta; $te = $v1_end; }
				else if ( $key == 'v2' ) { $ts = $v2_sta; $te = $v2_end; }
				else if ( $key == 'v3' ) { $ts = $v3_sta; $te = $v3_end; }
				else if ( $key == 'v4' ) { $ts = $v4_sta; $te = $v4_end; }
				else if ( $key == 'v5' ) { $ts = $v5_sta; $te = $v5_end; }
				else if ( $key == 'v6' ) { $ts = $v6_sta; $te = $v6_end; }
				else if ( $key == 'v7' ) { $ts = $v7_sta; $te = $v7_end; }
				else if ( $key == 'v8' ) { $ts = $v8_sta; $te = $v8_end; }
				else if ( $key == 'v9' ) { $ts = $v9_sta; $te = $v9_end; }
				else if ( $key == 'v10' ){ $ts = $v10_sta; $te = $v10_end; }
				else if ( $key == 'v11' ){ $ts = $v11_sta; $te = $v11_end; }
				else if ( $key == 'v12' ){ $ts = $v12_sta; $te = $v12_end; }

				$percent = round( ($value * 100) / $max_value , 2);
				if ( $value == $max_value ) { $percent = 100; }

				$percentage_distribution_of_the_total_price []= array(
																															'tartomany_start' => $ts,
																															'tartomany_end' => $te,
																															'how_many_times' => $value,
																															'how_many_times_in_percent' => $percent,
																														 );
			}
		}
		// ---

		if ( isset($tartomany_start) && !empty($tartomany_start) ) {
			return array(
										'tartomany_start' => number_format($tartomany_start,8),
										'tartomany_end' 	=> number_format($tartomany_end,8),
										'html' 						=> number_format($tartomany_end,8) .'<br>'. number_format($tartomany_start,8),
										'percentage_distribution_of_the_total_price' => $percentage_distribution_of_the_total_price,
									);
		}
	}
	return "";
}


function btc_price_range($prices_array = "") {
	if ( empty($prices_array) ) { return ""; }

	$prices_array_temp = $prices_array;
	asort($prices_array_temp);
	$min = number_format($prices_array_temp[0],10);
	$max = number_format(end($prices_array_temp),10);

	$oszto = ($max - $min) / 10;

	$v1 = $min + ( $oszto * 1 );
	$v2 = $min + ( $oszto * 2 );
	$v3 = $min + ( $oszto * 3 );
	$v4 = $min + ( $oszto * 4 );
	$v5 = $min + ( $oszto * 5 );
	$v6 = $min + ( $oszto * 6 );
	$v7 = $min + ( $oszto * 7 );
	$v8 = $min + ( $oszto * 8 );
	$v9 = $min + ( $oszto * 9 );
	$v10 = $min + ( $oszto * 10 );

	$count = array();
	foreach ($prices_array as $key => $price) {
		$price = floatval($price);

		if 			( ($v1 <= $price) && ($v2 > $price) ) { $count []= '1'; }
		else if ( ($v2 <= $price) && ($v3 > $price) ) { $count []= '2'; }
		else if ( ($v3 <= $price) && ($v4 > $price) ) { $count []= '3'; }
		else if ( ($v4 <= $price) && ($v5 > $price) ) { $count []= '4'; }
		else if ( ($v5 <= $price) && ($v6 > $price) ) { $count []= '5'; }
		else if ( ($v6 <= $price) && ($v7 > $price) ) { $count []= '6'; }
		else if ( ($v7 <= $price) && ($v8 > $price) ) { $count []= '7'; }
		else if ( ($v8 <= $price) && ($v9 > $price) ) { $count []= '8'; }
		else if ( ($v9 <= $price) && ($v10 > $price) ) { $count []= '9'; }
	}

	$count = array_count_values($count);
	asort($count);
	$count = array_values($count);
	$winner = array_pop(array_keys($count));

	if 			( $winner == 1 ) { return array( $v1, $v2 ); }
	else if ( $winner == 2 ) { return array( $v2, $v3 ); }
	else if ( $winner == 3 ) { return array( $v3, $v4 ); }
	else if ( $winner == 4 ) { return array( $v4, $v5 ); }
	else if ( $winner == 5 ) { return array( $v5, $v6 ); }
	else if ( $winner == 6 ) { return array( $v6, $v7 ); }
	else if ( $winner == 7 ) { return array( $v7, $v8 ); }
	else if ( $winner == 8 ) { return array( $v8, $v9 ); }
	else if ( $winner == 9 ) { return array( $v9, $v10 ); }
	else { return ""; }
}


function get_evaluated_data($coin_ID = "") {
	if ( empty($coin_ID) ) { return ""; }
	global $wpdb;

	$evaluated_data = $wpdb->get_results( "SELECT COUNT(`ID`) AS `evaluated_data`
																	FROM `{$wpdb->prefix}crypto_coinmarketcap_data`
																	WHERE `coin_ID` = '{$coin_ID}'
																		AND `datetime` >= '". date('Y-m-d H:i:s', TIME_INTERVAL_TIMESTAMP) ."'
																	ORDER BY `datetime` ASC", ARRAY_A );

	if ( isset($evaluated_data[0]['evaluated_data']) && !empty($evaluated_data[0]['evaluated_data']) ) {
		return intval($evaluated_data[0]['evaluated_data']);
	}
	return "";
}


function get_bittrex_evaluated_data($coin_ID = "") {
	if ( empty($coin_ID) ) { return ""; }
	global $wpdb;

	$evaluated_data = $wpdb->get_results( "SELECT COUNT(`ID`) AS `evaluated_data`
																	FROM `{$wpdb->prefix}crypto_bittrex_data`
																	WHERE `coin_ID` = '{$coin_ID}'
																		AND `datetime` >= '". date('Y-m-d H:i:s', TIME_INTERVAL_TIMESTAMP) ."'
																	ORDER BY `datetime` ASC", ARRAY_A );

	if ( isset($evaluated_data[0]['evaluated_data']) && !empty($evaluated_data[0]['evaluated_data']) ) {
		return intval($evaluated_data[0]['evaluated_data']);
	}
	return "";
}


function get_mining_data($coin_ID = "", $db_column_name = "", $db_column_name2 = "") {
	if ( empty($coin_ID) ) { return ""; }
	if ( empty($db_column_name) ) { return ""; }
	global $wpdb;

	$mining_data = $wpdb->get_results( "SELECT AVG(`{$db_column_name}`) AS `column_name`
																	FROM `{$wpdb->prefix}crypto_mining_data`
																	WHERE `coin_ID` = '{$coin_ID}'
																		AND `datetime` < '". date('Y-m-d H:i:s', strtotime('-6 hours')) ."'
																		AND `datetime` >= '". date('Y-m-d H:i:s', TIME_INTERVAL_TIMESTAMP) ."'
																	ORDER BY `datetime` ASC", ARRAY_A );

	if ( !empty($db_column_name2) ) {
		$mining_data2 = $wpdb->get_results( "SELECT `{$db_column_name2}` AS `last_data`
																		FROM `{$wpdb->prefix}crypto_mining_data`
																		WHERE `coin_ID` = '{$coin_ID}'
																			AND `datetime` >= '". date('Y-m-d H:i:s', TIME_INTERVAL_TIMESTAMP) ."'
																		ORDER BY `datetime` DESC LIMIT 1", ARRAY_A );
	} else {
		$mining_data2 = $wpdb->get_results( "SELECT `{$db_column_name}` AS `last_data`
																		FROM `{$wpdb->prefix}crypto_mining_data`
																		WHERE `coin_ID` = '{$coin_ID}'
																			AND `datetime` >= '". date('Y-m-d H:i:s', TIME_INTERVAL_TIMESTAMP) ."'
																		ORDER BY `datetime` DESC LIMIT 1", ARRAY_A );
	}

	$column_name 	= $mining_data[0]['column_name'];
	$last_data 		= $mining_data2[0]['last_data'];

	if ( isset($column_name) && !empty($column_name) &&
			 isset($last_data) && !empty($last_data) ) {

			$positive_negative = "";
			if ( $column_name == $last_data ) 			{ $positive_negative = array( '=', '<span class="equal">=</span>' ); }
			else if ( $column_name > $last_data ) 	{ $positive_negative = array( '>', '<span class="glyphicon glyphicon-minus minus"></span>' ); }
			else if ( $column_name < $last_data ) 	{ $positive_negative = array( '<', '<span class="glyphicon glyphicon-plus plus"></span>' ); }

			$percentChange = percent_between_two_numbers($column_name, $last_data);
			$column_name 	= substr($column_name, 0, 6);
			$last_data 		= substr($last_data, 0, 6);


			 $html =
				 '<div class="calculating_mining_data">
		 			 '. $positive_negative[1] .'
		 			 <div class="calculated_average hidden">'. $column_name .' → </div>
		 			 <div class="latest_num">'. round($percentChange,1) .'%</div>
		 		 </div>';

			return array(
									 'values' => array( $column_name, $last_data ),
									 'percent_change' => $percentChange,
									 'positive_negative' => $positive_negative[0],
									 'html' => $html,
								 );
	}
	return "";
}


function percent_between_two_numbers($old_value = "", $new_value = "") {
	if ( empty($old_value) ) { return ""; }
	if ( empty($new_value) ) { return ""; }

	return (($new_value - $old_value) / $old_value) * 100;
	//return (1 - $old_value / $new_value) * 100;
}


function get_rating_info($coin_ID = "", $market = "", $last_price_in_btc = "", $tartomany_start = "", $tartomany_end = "") {
	if ( empty($coin_ID) ) { return ""; }
	if ( empty($last_price_in_btc) ) { return ""; }
	if ( empty($tartomany_start) ) { return ""; }
	if ( empty($tartomany_end) ) { return ""; }
	global $wpdb;
	$megeri_array = "";
	$db_table_name = $wpdb->prefix. 'crypto_coinmarketcap_data';

	if ( $market == 'bittrex' ) {
		$atlag = get_bittrex_av_btc_price($coin_ID);
	} else {
		$atlag = get_cmc_average_btc_price($coin_ID);
	}


	$percentageChange = 15; // <-- 15% -al csökkentse az árat
	$originalNumber = $tartomany_end;
	$numberToAdd = ($originalNumber / 100) * $percentageChange;
	$newNumber = $originalNumber - $numberToAdd;

	if ( ($last_price_in_btc < $tartomany_end) && ($last_price_in_btc > $newNumber) ) {
		$megeri_array = array( "decline" => "false", "status" => 'good_price', "msg" => "Jó áron van!" );
	}
	if ( $last_price_in_btc <= $newNumber ) {
		$megeri_array = array( "decline" => "false", "status" => 'must_buy', "msg" => "Venni kell!" );
	}
	if ( $last_price_in_btc >= $tartomany_end ) {
		$megeri_array = array( "decline" => "false", "status" => 'expensive', "msg" => "Drága" );
	}


	// rank esés
	if ( $market == 'cmc' ) {
		$ranks_avg = $wpdb->get_results( "SELECT AVG(`rank`) AS `rank_avg`
																		FROM `{$db_table_name}`
																		WHERE `coin_ID` = '{$coin_ID}'
																			AND `datetime` >= '". date('Y-m-d H:i:s', TIME_INTERVAL_TIMESTAMP) ."'
																		ORDER BY `datetime` ASC", ARRAY_A );

		$last_rank = $wpdb->get_results( "SELECT `rank` AS `last_rank`
																			FROM `{$db_table_name}`
																			WHERE `coin_ID` = '{$coin_ID}'
																			ORDER BY `datetime` DESC LIMIT 1", ARRAY_A );

		if ( isset($ranks_avg[0]['rank_avg']) && !empty($ranks_avg[0]['rank_avg']) &&
				 isset($last_rank[0]['last_rank']) && !empty($last_rank[0]['last_rank'])
			 ) {

			if ( $ranks_avg[0]['rank_avg'] < $last_rank[0]['last_rank'] ) {
				$megeri_array = array( "decline" => "true", "status" => $megeri_array['status'], "msg" => "Esik a rank" );
			}
		}
	}

	return $megeri_array;
}


function get_mining_rating_info($coin_ID = "") {
	if ( empty($coin_ID) ) { return ""; }
	global $wpdb;

	$mining_block_time 					= get_mining_data($coin_ID, 'block_time');
	$mining_difficult						= get_mining_data($coin_ID, 'difficult24', 'difficult');
	$mining_nethash							= get_mining_data($coin_ID, 'nethash');

	$mining_estimated_rewards 	= get_mining_data($coin_ID, 'estimated_rewards24', 'estimated_rewards');
	$mining_btc_revenue 				= get_mining_data($coin_ID, 'btc_revenue24', 'btc_revenue');
	$mining_profitability 			= get_mining_data($coin_ID, 'profitability24', 'profitability');

	$m = $mining_nethash['percent_change'] - $mining_difficult['percent_change'] - $mining_block_time['percent_change'];
	$p = $mining_estimated_rewards['percent_change'] + $mining_btc_revenue['percent_change'] + $mining_profitability['percent_change'];

	if ( !empty($m) && !empty($p) ) {
		return array(
									"mining_rate" => $m,
									"profit_rate" => $p,
									"html" => 'Mining: <b>'. round($m,1) .'%</b><br>Profit: <b>'. round($p,1) .'%</b>',
		 						);
	}
	return "";
}


function get_price_change_in_percent($last_btc_price = "", $tartomany_max = "") {
	if ( empty($last_btc_price) ) { return ""; }
	if ( empty($tartomany_max) ) { return ""; }

	$percent = percent_between_two_numbers($tartomany_max, $last_btc_price);

	if ( !empty($percent) ) {
		return round($percent,1) .'%';
	}
	return "";
}


function encode_arr($data = "") {
    return base64_encode(serialize($data));
}

function decode_arr($data = "") {
    return unserialize(base64_decode($data));
}
