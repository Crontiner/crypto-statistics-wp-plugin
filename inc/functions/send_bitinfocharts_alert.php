<?php
// Jelez ha egy nagy befektetés vagy eladás történt a top walletek közül

function get_bitinfocharts_alert_buy_amount($coin_ID = "") {
	if ( $coin_ID > 0 ) {  }
	else { return ""; }
	global $wpdb;

	$avg_changed_amount = $wpdb->get_results( "SELECT `wd`.`changed_amount` AS `avg_changed_amount`
																			FROM `{$wpdb->prefix}crypto_bitinfocharts_rich_wallets_datas` AS `wd`
																			LEFT JOIN `{$wpdb->prefix}crypto_bitinfocharts_rich_wallets` AS `w`
																						ON `wd`.`wallet_ID` = `w`.`ID`
																			WHERE `w`.`coin_ID` = {$coin_ID} AND
																						`wd`.`changed_amount` > 0
																			ORDER BY `wd`.`changed_amount` DESC LIMIT 200 ", ARRAY_A );

	if ( !empty($avg_changed_amount) ) {
		$avg_changed_amount_temp = array();
		foreach ($avg_changed_amount as $key => $val) {
			$avg_changed_amount_temp []= $val['avg_changed_amount'];
		}
		unset($avg_changed_amount);
		$avg_changed_amount_temp = array_sum($avg_changed_amount_temp) / count($avg_changed_amount_temp);
		if ( $avg_changed_amount_temp > 0 ) {
			return (int) $avg_changed_amount_temp;
		}
	}
	return "";
}


function get_bitinfocharts_alert_sell_amount($coin_ID = "") {
	if ( $coin_ID > 0 ) {  }
	else { return ""; }
	global $wpdb;

	$avg_changed_amount = $wpdb->get_results( "SELECT `wd`.`changed_amount` AS `avg_changed_amount`
																					FROM `{$wpdb->prefix}crypto_bitinfocharts_rich_wallets_datas` AS `wd`
																					LEFT JOIN `{$wpdb->prefix}crypto_bitinfocharts_rich_wallets` AS `w`
																								ON `wd`.`wallet_ID` = `w`.`ID`
																					WHERE `w`.`coin_ID` = {$coin_ID} AND
																								`wd`.`changed_amount` IS NOT NULL AND
																								`wd`.`changed_amount` LIKE '-%'
																					ORDER BY `wd`.`changed_amount` ASC LIMIT 200 ", ARRAY_A );

	if ( !empty($avg_changed_amount) ) {
		$avg_changed_amount_temp = array();
		foreach ($avg_changed_amount as $key => $val) {
			$avg_changed_amount_temp []= $val['avg_changed_amount'];
		}
		unset($avg_changed_amount);
		$avg_changed_amount_temp = abs(array_sum($avg_changed_amount_temp) / count($avg_changed_amount_temp));
		if ( $avg_changed_amount_temp > 0 ) {
			return (int) $avg_changed_amount_temp;
		}
	}
	return "";
}


function send_bitinfocharts_wallet_alert() {
	global $wpdb;
	$big_amount_changes = array();

	// kiszámolja a pénzmozgásokat
	calculate_change_amounts();
	calculate_change_amounts();
	calculate_change_amounts();


	$last_updated_datetime = $wpdb->get_results( "	SELECT `last_updated_datetime`
																							FROM `{$wpdb->prefix}crypto_bitinfocharts_rich_wallets`
																							ORDER BY `last_updated_datetime` DESC
																							LIMIT 1", ARRAY_A );
	if ( isset($last_updated_datetime[0]['last_updated_datetime']) ) {
		$last_updated_datetime = strtotime($last_updated_datetime[0]['last_updated_datetime']);
		$start_timestamp = $last_updated_datetime - 26100; // -7 hour 15 min


		$coins_data = array(
													'BTC' => array(
																					'coin_id' => BTC_ID,
																					'alert_buy_amount' 	=> get_bitinfocharts_alert_buy_amount(BTC_ID),
																					'alert_sell_amount' => get_bitinfocharts_alert_sell_amount(BTC_ID),
																				),
													'DASH' => array(
																					'coin_id' => DASH_ID,
																					'alert_buy_amount' 	=> get_bitinfocharts_alert_buy_amount(DASH_ID),
																					'alert_sell_amount' => get_bitinfocharts_alert_sell_amount(DASH_ID),
																				),
													'LTC' => array(
																					'coin_id' => LTC_ID,
																					'alert_buy_amount' 	=> get_bitinfocharts_alert_buy_amount(LTC_ID),
																					'alert_sell_amount' => get_bitinfocharts_alert_sell_amount(LTC_ID),
																				),
													'VTC' => array(
																					'coin_id' => VTC_ID,
																					'alert_buy_amount' 	=> get_bitinfocharts_alert_buy_amount(VTC_ID),
																					'alert_sell_amount' => get_bitinfocharts_alert_sell_amount(VTC_ID),
																				),
												);


		foreach ($coins_data as $coin_name => $coin_data) {
			$coin_ID = (int) $coin_data['coin_id'];
			$big_price_buy = "";
			$big_price_sell = "";


			// Big Buy

			$max_changed_amount = $wpdb->get_results( "	SELECT `wd`.`changed_amount` AS `max_changed_amount`
																									FROM `{$wpdb->prefix}crypto_bitinfocharts_rich_wallets_datas` AS `wd`
																									LEFT JOIN `{$wpdb->prefix}crypto_bitinfocharts_rich_wallets` AS `w`
																												ON `wd`.`wallet_ID` = `w`.`ID`
																									WHERE `w`.`coin_ID` = {$coin_ID} AND
																												`wd`.`changed_amount` > 0 AND
																												`wd`.`datetime` > '". date('Y-m-d H:i:s', $start_timestamp) ."'
																									ORDER BY `wd`.`changed_amount` DESC LIMIT 1 ", ARRAY_A );
			$max_changed_amount = abs($max_changed_amount[0]['max_changed_amount']);

			if ( $max_changed_amount >= $coin_data['alert_buy_amount'] ) {
				$big_price_buy = $max_changed_amount;
			}


			// Big Sell

			$max_changed_amount = $wpdb->get_results( "	SELECT `wd`.`changed_amount` AS `max_changed_amount`
																									FROM `{$wpdb->prefix}crypto_bitinfocharts_rich_wallets_datas` AS `wd`
																									LEFT JOIN `{$wpdb->prefix}crypto_bitinfocharts_rich_wallets` AS `w`
																												ON `wd`.`wallet_ID` = `w`.`ID`
																									WHERE `w`.`coin_ID` = {$coin_ID} AND
																												`wd`.`changed_amount` < 0 AND
																												`wd`.`datetime` > '". date('Y-m-d H:i:s', $start_timestamp) ."'
																									ORDER BY `wd`.`changed_amount` ASC LIMIT 1 ", ARRAY_A );
			$max_changed_amount = abs($max_changed_amount[0]['max_changed_amount']);


			if ( $max_changed_amount >= $coin_data['alert_sell_amount'] ) {
				$big_price_sell = $max_changed_amount;
			}


			$big_amount_changes []= array(
																		'buy' 	=> $big_price_buy,
																		'sell' 	=> $big_price_sell,
																		'coin' 	=> $coin_ID,
																	);
		}
	}


	if ( !empty($big_amount_changes) ) {
		$alert_msg = "";
		foreach ($big_amount_changes as $key => $val) {

			$coin_name = "";
			if ( isset($val['coin']) && !empty($val['coin']) ) {
				$coin_name = get_coin_name_by_id($val['coin']);
			}


			if ( isset($val['buy']) && !empty($val['buy']) ) {
				$alert_msg .= $coin_name .' - Big Buy: '. $val['buy'] .'<br>';
			}
			if ( isset($val['sell']) && !empty($val['sell']) ) {
				$alert_msg .= $coin_name .' - Big Sell: '. $val['sell'] .'<br>';
			}

			if ( !empty($alert_msg) ) {
				$alert_msg .= '<br>';
			}
		}

		if ( !empty($alert_msg) ) {
			crypto_send_message($alert_msg, 'Big money movement alert!', 'big_money_movement');
		}
	}

	return "";
}
