<?php

function add_poloniex_market_history($coin_id) {
	global $wpdb;
	if ( empty($coin_id) ) { return ""; }
	$coin_name = get_coin_name_by_id($coin_id);
	if ( empty($coin_name) ) { return ""; }


	// Remove old datas
	$wpdb->query(
		$wpdb->prepare(
			"DELETE FROM `{$wpdb->prefix}crypto_poloniex_market_history`
				WHERE `datetime` < %s
			",
				date('Y-m-d H:i:s', REMOVE_MARKET_HISTORY_TIMESTAMP)
			)
	);
	// ---


	$poloniex_datas = file_get_contents( 'https://poloniex.com/public?command=returnTradeHistory&currencyPair=BTC_'. strtoupper($coin_name) .'&start='. strtotime("-7 minutes") );
	$poloniex_datas = json_decode($poloniex_datas, true);

	$buys_array = array();
	$sells_array = array();

	if ( isset($poloniex_datas[0]['date']) && !empty($poloniex_datas[0]['date']) ) {
		foreach ($poloniex_datas as $key => $value) {

			if ( isset($value['type']) ) {
				if ( $value['type'] == 'sell' ) {

					if ( isset($value['rate']) ) 		{ $sells_array['rate'][]= $value['rate']; } // BTC price: 1 (ETH) in BTC (actual trade price)
					if ( isset($value['amount']) ) 		{ $sells_array['amount'][]= $value['amount']; } // altcoin (ETH) price -- not BTC!

					if ( isset($value['rate']) &&
							 isset($value['amount']) ) 		{ $sells_array['total'][]= $value['total']; } // BTC price

				}
				else if ( $value['type'] == 'buy' ) {

					if ( isset($value['rate']) ) 		{ $buys_array['rate'][]= $value['rate']; }
					if ( isset($value['amount']) ) 		{ $buys_array['amount'][]= $value['amount']; }

					if ( isset($value['rate']) &&
							 isset($value['amount']) ) 		{ $buys_array['total'][]= $value['total']; } // BTC price
				}
			}

		}
	}


	if ( 	is_array($sells_array['amount']) && !empty($sells_array['amount']) &&
				is_array($sells_array['total']) && !empty($sells_array['total']) &&
				is_array($buys_array['amount']) && !empty($buys_array['amount']) &&
				is_array($buys_array['total']) && !empty($buys_array['total']) ) {


		//$sells_array['rate'] 	= number_format(array_sum($sells_array['rate']) / count($sells_array['rate']),8);
		$sells_array['amount'] 	= array_sum($sells_array['amount']);
		$sells_array['total'] 	= array_sum($sells_array['total']);

		//$buys_array['rate'] 		= number_format(array_sum($buys_array['rate']) / count($buys_array['rate']),8);
		$buys_array['amount'] 	= array_sum($buys_array['amount']);
		$buys_array['total'] 		= array_sum($buys_array['total']);


		if ( !empty($sells_array) ) {
			$wpdb->insert(
					$wpdb->prefix .'crypto_poloniex_market_history',
					array(
						'coin_ID' => $coin_id,
						'quantity' => $sells_array['amount'],
						'total' => $sells_array['total'],
						'ordertype' => 's',
					),
					array(
						'%d','%s','%s','%s',
					)
			);
		}

		if ( !empty($buys_array) ) {
			$wpdb->insert(
					$wpdb->prefix .'crypto_poloniex_market_history',
					array(
						'coin_ID' => $coin_id,
						'quantity' => $buys_array['amount'],
						'total' => $buys_array['total'],
						'ordertype' => 'b',
					),
					array(
						'%d','%s','%s','%s',
					)
			);
		}
	}

}
