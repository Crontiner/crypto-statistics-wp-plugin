<?php

function add_btc_movement_info() {
	global $wpdb;


	$cmc_info = file_get_contents( 'https://pro-api.coinmarketcap.com/v1/global-metrics/quotes/latest?CMC_PRO_API_KEY='. CMC_PRO_API_KEY .'&convert=BTC' );
	$cmc_info = json_decode($cmc_info, true);

	$cmc_info2 = file_get_contents( 'https://pro-api.coinmarketcap.com/v1/global-metrics/quotes/latest?CMC_PRO_API_KEY='. CMC_PRO_API_KEY .'&convert=USD' );
	$cmc_info2 = json_decode($cmc_info2, true);


	$bitcoin_percentage_of_market_cap = "";
	$active_cryptocurrencies = "";
	$total_volume_usd = "";
	$active_markets = "";
	$total_market_cap_by_available_supply_usd = "";

	if ( isset($cmc_info['data']['btc_dominance']) ) 											{ $bitcoin_percentage_of_market_cap = $cmc_info['data']['btc_dominance']; }
	if ( isset($cmc_info['data']['active_cryptocurrencies']) ) 						{ $active_cryptocurrencies = $cmc_info['data']['active_cryptocurrencies']; }
	if ( isset($cmc_info2['data']['quote']['USD']['total_volume_24h']) ) 	{ $total_volume_usd = $cmc_info2['data']['quote']['USD']['total_volume_24h']; }
	if ( isset($cmc_info['data']['active_market_pairs']) ) 								{ $active_markets = $cmc_info['data']['active_market_pairs']; }
	if ( isset($cmc_info2['data']['quote']['USD']['total_market_cap']) ) 	{ $total_market_cap_by_available_supply_usd = $cmc_info2['data']['quote']['USD']['total_market_cap']; }
	if ( isset($cmc_info['data']['quote']['BTC']['total_market_cap']) ) 	{ $total_market_cap_btc = $cmc_info['data']['quote']['BTC']['total_market_cap']; }


	$wpdb->insert(
			$wpdb->prefix .'crypto_btc_movement',
			array(
				'bitcoin_percentage_of_market_cap' => $bitcoin_percentage_of_market_cap,
				'active_cryptocurrencies' => $active_cryptocurrencies,
				'total_volume_usd' => $total_volume_usd,
				'active_markets' => $active_markets,
				'total_market_cap_by_available_supply_usd' => $total_market_cap_by_available_supply_usd,
				'total_market_cap_btc' => $total_market_cap_btc,
			),
			array(
				'%s','%s','%s','%s','%s','%s',
			)
	);

}
