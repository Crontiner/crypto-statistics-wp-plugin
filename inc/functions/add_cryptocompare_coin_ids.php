<?php

// lekéri a cryptocompare oldalról a coin ID-kat

function add_cryptocompare_coin_ids() {
	global $wpdb;

	$crypto_coin_names = $wpdb->get_results( "SELECT `coin_name` FROM `{$wpdb->prefix}crypto_coin_names`
																								 WHERE `cryptocompare_coin_ID` IS NULL ", ARRAY_A );
	$crypto_coin_names_array = array();
	foreach ($crypto_coin_names as $key => $coin_data) {
		$crypto_coin_names_array []= $coin_data['coin_name'];
	}


	if ( !empty($crypto_coin_names_array) ) {

		$coinlist = file_get_contents( 'https://www.cryptocompare.com/api/data/coinlist/' );
		$coinlist = json_decode($coinlist, true);

		$coins_array = array();

		if ( isset($coinlist['Data']['BTC']['Id']) ) {
			foreach ($coinlist['Data'] as $key => $coin_data) {
				if ( isset($coin_data['Symbol']) && isset($coin_data['Id']) ) {
					$coins_array [$coin_data['Symbol']]= (int) $coin_data['Id'];
				}
			}
		}

		if ( !empty($coins_array) ) {
			foreach ($coins_array as $coin_symbol => $coin_cryptocompare_id) {
				if ( in_array($coin_symbol, $crypto_coin_names_array) && ($coin_cryptocompare_id > 0) ) {

					$wpdb->update(
						$wpdb->prefix .'crypto_coin_names',
						array(
							'cryptocompare_coin_ID' => $coin_cryptocompare_id,
						),
						array( 'coin_name' => $coin_symbol ),
						array( '%d' ),
						array( '%s' )
					);

				}
			}
		}
	}

	return "";
}
