<?php

function add_bitfinex_market_history($coin_id) {
	global $wpdb;
	if ( empty($coin_id) ) { return ""; }
	$coin_name = get_coin_name_by_id($coin_id);
	if ( empty($coin_name) ) { return ""; }


	// Remove old datas
	$wpdb->query(
		$wpdb->prepare(
			"DELETE FROM `{$wpdb->prefix}crypto_bitfinex_market_history`
				WHERE `datetime` < %s
			",
				date('Y-m-d H:i:s', REMOVE_MARKET_HISTORY_TIMESTAMP)
			)
	);
	// ---


	$bitfinex_datas = file_get_contents( 'https://api.bitfinex.com/v1/trades/'. strtoupper($coin_name) .'BTC' );
	$bitfinex_datas = json_decode($bitfinex_datas, true);

	$buys_array = array();
	$sells_array = array();

	if ( isset($bitfinex_datas[0]['timestamp']) && !empty($bitfinex_datas[0]['timestamp']) ) {
		foreach ($bitfinex_datas as $key => $value) {

			if ( isset($value['type']) ) {
				if ( $value['type'] == 'sell' ) {

					if ( isset($value['price']) ) 		{ $sells_array['price'][]= $value['price']; } // BTC price: 1 (ETH) in BTC (actual trade price)
					if ( isset($value['amount']) ) 		{ $sells_array['amount'][]= $value['amount']; } // altcoin (ETH) price -- not BTC!

					if ( isset($value['price']) &&
							 isset($value['amount']) ) 		{ $sells_array['total'][]= number_format(floatval($value['amount']) * floatval($value['price']),8); } // BTC price

				}
				else if ( $value['type'] == 'buy' ) {

					if ( isset($value['price']) ) 		{ $buys_array['price'][]= $value['price']; }
					if ( isset($value['amount']) ) 		{ $buys_array['amount'][]= $value['amount']; }

					if ( isset($value['price']) &&
							 isset($value['amount']) ) 		{ $buys_array['total'][]= number_format(floatval($value['amount']) * floatval($value['price']),8); } // BTC price
				}
			}

		}
	}


	if ( 	is_array($sells_array['amount']) && !empty($sells_array['amount']) &&
				is_array($sells_array['total']) && !empty($sells_array['total']) &&
				is_array($buys_array['amount']) && !empty($buys_array['amount']) &&
				is_array($buys_array['total']) && !empty($buys_array['total']) ) {


		//$sells_array['price'] 	= number_format(array_sum($sells_array['price']) / count($sells_array['price']),8);
		$sells_array['amount'] 	= array_sum($sells_array['amount']);
		$sells_array['total'] 	= array_sum($sells_array['total']);

		//$buys_array['price'] 		= number_format(array_sum($buys_array['price']) / count($buys_array['price']),8);
		$buys_array['amount'] 	= array_sum($buys_array['amount']);
		$buys_array['total'] 		= array_sum($buys_array['total']);


		if ( !empty($sells_array) ) {
			$wpdb->insert(
					$wpdb->prefix .'crypto_bitfinex_market_history',
					array(
						'coin_ID' => $coin_id,
						'quantity' => $sells_array['amount'],
						'total' => $sells_array['total'],
						'ordertype' => 's',
					),
					array(
						'%d','%s','%s','%s',
					)
			);
		}

		if ( !empty($buys_array) ) {
			$wpdb->insert(
					$wpdb->prefix .'crypto_bitfinex_market_history',
					array(
						'coin_ID' => $coin_id,
						'quantity' => $buys_array['amount'],
						'total' => $buys_array['total'],
						'ordertype' => 'b',
					),
					array(
						'%d','%s','%s','%s',
					)
			);
		}
	}

}
