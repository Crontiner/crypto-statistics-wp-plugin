<?php

function add_hitbtc_market_history($coin_id) {
	global $wpdb;
	if ( empty($coin_id) ) { return ""; }
	$coin_name = get_coin_name_by_id($coin_id);
	if ( empty($coin_name) ) { return ""; }


	// Remove old datas
	$wpdb->query(
		$wpdb->prepare(
			"DELETE FROM `{$wpdb->prefix}crypto_hitbtc_market_history`
				WHERE `datetime` < %s
			",
				date('Y-m-d H:i:s', REMOVE_MARKET_HISTORY_TIMESTAMP)
			)
	);
	// ---


	$hitbtc_datas = file_get_contents( 'https://api.hitbtc.com/api/2/public/trades/'. strtoupper($coin_name) .'BTC?sort=DESC');
	$hitbtc_datas = json_decode($hitbtc_datas, true);

	$buys_array = array();
	$sells_array = array();

	if ( isset($hitbtc_datas[0]['timestamp']) && !empty($hitbtc_datas[0]['timestamp']) ) {
		foreach ($hitbtc_datas as $key => $value) {

			if ( isset($value['side']) ) {
				if ( $value['side'] == 'sell' ) {

					if ( isset($value['quantity']) ) 		{ $sells_array['quantity'][]= $value['quantity']; } // altcoin (ETH) price - not BTC!
					if ( isset($value['price']) ) 			{ $sells_array['price'][]= $value['price']; } // BTC price: 1 (ETH) in BTC (actual trade price)

					if ( isset($value['price']) &&
							 isset($value['quantity']) ) 		{ $sells_array['total'][]= number_format(floatval($value['quantity']) * floatval($value['price']),8); } // BTC price
				}
				else if ( $value['side'] == 'buy' ) {

					if ( isset($value['quantity']) ) 		{ $buys_array['quantity'][]= $value['quantity']; }
					if ( isset($value['price']) ) 			{ $buys_array['price'][]= $value['price']; }

					if ( isset($value['price']) &&
							 isset($value['quantity']) ) 		{ $buys_array['total'][]= number_format(floatval($value['quantity']) * floatval($value['price']),8); } // BTC price
				}
			}

		}
	}



	if ( 	is_array($sells_array['quantity']) && !empty($sells_array['quantity']) &&
				is_array($sells_array['total']) && !empty($sells_array['total']) &&
				is_array($buys_array['quantity']) && !empty($buys_array['quantity']) &&
				is_array($buys_array['total']) && !empty($buys_array['total']) ) {


		$sells_array['quantity'] 	= array_sum($sells_array['quantity']);
		//$sells_array['price'] 		= number_format(array_sum($sells_array['price']) / count($sells_array['price']),8);
		$sells_array['total'] 		= array_sum($sells_array['total']);

		$buys_array['quantity'] 	= array_sum($buys_array['quantity']);
		//$buys_array['price'] 			= number_format(array_sum($buys_array['price']) / count($buys_array['price']),8);
		$buys_array['total'] 			= array_sum($buys_array['total']);


		if ( !empty($sells_array) ) {
			$wpdb->insert(
					$wpdb->prefix .'crypto_hitbtc_market_history',
					array(
						'coin_ID' => $coin_id,
						'quantity' => $sells_array['quantity'],
						'total' => $sells_array['total'],
						'ordertype' => 's',
					),
					array(
						'%d','%s','%s','%s',
					)
			);
		}

		if ( !empty($buys_array) ) {
			$wpdb->insert(
					$wpdb->prefix .'crypto_hitbtc_market_history',
					array(
						'coin_ID' => $coin_id,
						'quantity' => $buys_array['quantity'],
						'total' => $buys_array['total'],
						'ordertype' => 'b',
					),
					array(
						'%d','%s','%s','%s',
					)
			);
		}
	}

}
