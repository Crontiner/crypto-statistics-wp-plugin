<?php
function add_coindar_events() {
	global $wpdb;

	$coindar_actual_month_datas = file_get_contents( 'https://coindar.org/api/v1/events?year='. date('Y') .'&month='. date('m') );
	$coindar_actual_month_datas = json_decode($coindar_actual_month_datas, true);

	$coindar_next_month_datas = file_get_contents( 'https://coindar.org/api/v1/events?year='. date('Y', strtotime("+1 month")) .'&month='. date('m', strtotime("+1 month")) );
	$coindar_next_month_datas = json_decode($coindar_next_month_datas, true);

	$coindar_next_next_month_datas = file_get_contents( 'https://coindar.org/api/v1/events?year='. date('Y', strtotime("+2 month")) .'&month='. date('m', strtotime("+2 month")) );
	$coindar_next_next_month_datas = json_decode($coindar_next_next_month_datas, true);

	$coindar_next_next_next_month_datas = file_get_contents( 'https://coindar.org/api/v1/events?year='. date('Y', strtotime("+3 month")) .'&month='. date('m', strtotime("+3 month")) );
	$coindar_next_next_next_month_datas = json_decode($coindar_next_next_next_month_datas, true);

	$coindar_next_next_next_next_month_datas = file_get_contents( 'https://coindar.org/api/v1/events?year='. date('Y', strtotime("+4 month")) .'&month='. date('m', strtotime("+4 month")) );
	$coindar_next_next_next_next_month_datas = json_decode($coindar_next_next_next_next_month_datas, true);


	$bittrex_coins_temp = array();
	$bittrex_coins = $wpdb->get_results( "SELECT DISTINCT `coin_ID` FROM `{$wpdb->prefix}crypto_bittrex_data`", ARRAY_A );
	foreach ($bittrex_coins as $key => $value) {
		$bittrex_coins_temp []= get_coin_name_by_id($value['coin_ID']);
	}
	$bittrex_coins_temp []= get_coin_name_by_id(BCH_ID);
	$bittrex_coins_temp []= get_coin_name_by_id(BTC_ID);

	$bittrex_coins = $bittrex_coins_temp;
	unset($bittrex_coins_temp);
	$bittrex_coins = array_values(array_filter($bittrex_coins));


	$coindar_datas = array_merge($coindar_actual_month_datas, $coindar_next_month_datas, $coindar_next_next_month_datas, $coindar_next_next_next_month_datas, $coindar_next_next_next_next_month_datas);


	if ( isset($coindar_datas[0]['coin_symbol']) ) {

		// Remove old datas
		$wpdb->query(
			$wpdb->prepare(
				"DELETE FROM `{$wpdb->prefix}crypto_coindar_events`
					WHERE `start_date` >= %s
				",
					date('Y-m-d H:i:s', strtotime(date('Y-m')))
				)
		);
		// ---


		foreach ($coindar_datas as $key => $event_data) {
			$caption 				= $event_data['caption'];
			$proof 					= $event_data['proof'];
			$public_date 		= $event_data['public_date'];
			$start_date 		= $event_data['start_date'];
			$end_date 			= $event_data['end_date'];
			$coin_symbol 		= $event_data['coin_symbol'];

			if ( !empty($start_date) ) {
				$start_date = date('Y-m-d H:i:s', strtotime($start_date));
			}
			if ( !empty($end_date) ) {
				$end_date = date('Y-m-d H:i:s', strtotime($end_date));
			}

			if ( in_array($coin_symbol, $bittrex_coins) ) {
				$coin_id = (int) get_coin_id_by_name($coin_symbol);

				if ( $coin_id > 0 ) {
					$wpdb->insert(
						$wpdb->prefix .'crypto_coindar_events',
						array(
							'coin_ID' => $coin_id,
							'caption' => $caption,
							'proof' => $proof,
							'public_date' => $public_date,
							'start_date' => $start_date,
							'end_date' => $end_date,
						),
						array(
							'%d','%s','%s','%s','%s','%s'
						)
					);
				}

			}
		}
	}
}
