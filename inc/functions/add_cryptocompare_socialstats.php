<?php

function add_cryptocompare_socialstats() {
	global $wpdb;

	$crypto_coin_names = $wpdb->get_results( "SELECT `ID`, `coin_name`, `cryptocompare_coin_ID` FROM `{$wpdb->prefix}crypto_coin_names`
																								 WHERE `cryptocompare_coin_ID` IS NOT NULL ", ARRAY_A );
	$crypto_coin_names_array = array();
	foreach ($crypto_coin_names as $key => $coin_data) {
		if ( check_coin_is_banned($coin_data['coin_name']) === FALSE ) {
			$crypto_coin_names_array [$coin_data['ID']]= (int) $coin_data['cryptocompare_coin_ID'];
		}
	}

	if ( !empty($crypto_coin_names_array) ) {
		foreach ($crypto_coin_names_array as $coin_id => $cryptocompare_coin_ID) {

			$socialstats = file_get_contents( 'https://www.cryptocompare.com/api/data/socialstats/?id='. $cryptocompare_coin_ID );
			$socialstats = json_decode($socialstats, true);


			$cryptopian_followers = "";
			$cryptopian_posts = "";
			$cryptopian_comments = "";

			$twitter_link = "";
			$twitter_followers = "";
			$twitter_favourites = "";

			$reddit_posts_per_day = "";
			$reddit_comments_per_day = "";
			$reddit_link = "";
			$reddit_subscribers = "";

			$facebook_likes = "";
			$facebook_link = "";

			$code_repo_created_at = "";
			$code_repo_last_update = "";
			$code_repo_forks = "";
			$code_repo_url = "";
			$code_repo_closed_issues = "";
			$code_repo_closed_pull_issues = "";
			$code_repo_last_push = "";
			$code_repo_language = "";


			if ( isset($socialstats['Data']['CryptoCompare']['Followers']) ) { $cryptopian_followers = $socialstats['Data']['CryptoCompare']['Followers']; }
			if ( isset($socialstats['Data']['CryptoCompare']['Posts']) ) { $cryptopian_posts = $socialstats['Data']['CryptoCompare']['Posts']; }
			if ( isset($socialstats['Data']['CryptoCompare']['Comments']) ) { $cryptopian_comments = $socialstats['Data']['CryptoCompare']['Comments']; }

			if ( isset($socialstats['Data']['Twitter']['link']) ) { $twitter_link = $socialstats['Data']['Twitter']['link']; }
			if ( isset($socialstats['Data']['Twitter']['followers']) ) { $twitter_followers = $socialstats['Data']['Twitter']['followers']; }
			if ( isset($socialstats['Data']['Twitter']['favourites']) ) { $twitter_favourites = $socialstats['Data']['Twitter']['favourites']; }

			if ( isset($socialstats['Data']['Reddit']['posts_per_day']) ) { $reddit_posts_per_day = $socialstats['Data']['Reddit']['posts_per_day']; }
			if ( isset($socialstats['Data']['Reddit']['comments_per_day']) ) { $reddit_comments_per_day = $socialstats['Data']['Reddit']['comments_per_day']; }
			if ( isset($socialstats['Data']['Reddit']['link']) ) { $reddit_link = $socialstats['Data']['Reddit']['link']; }
			if ( isset($socialstats['Data']['Reddit']['subscribers']) ) { $reddit_subscribers = $socialstats['Data']['Reddit']['subscribers']; }

			if ( isset($socialstats['Data']['Facebook']['likes']) ) { $facebook_likes = $socialstats['Data']['Facebook']['likes']; }
			if ( isset($socialstats['Data']['Facebook']['link']) ) { $facebook_link = $socialstats['Data']['Facebook']['link']; }

			if ( isset($socialstats['Data']['CodeRepository']['List'][0]['created_at']) ) { $code_repo_created_at = $socialstats['Data']['CodeRepository']['List'][0]['created_at']; }
			if ( isset($socialstats['Data']['CodeRepository']['List'][0]['last_update']) ) { $code_repo_last_update = $socialstats['Data']['CodeRepository']['List'][0]['last_update']; }
			if ( isset($socialstats['Data']['CodeRepository']['List'][0]['forks']) ) { $code_repo_forks = $socialstats['Data']['CodeRepository']['List'][0]['forks']; }
			if ( isset($socialstats['Data']['CodeRepository']['List'][0]['url']) ) { $code_repo_url = $socialstats['Data']['CodeRepository']['List'][0]['url']; }
			if ( isset($socialstats['Data']['CodeRepository']['List'][0]['closed_issues']) ) { $code_repo_closed_issues = $socialstats['Data']['CodeRepository']['List'][0]['closed_issues']; }
			if ( isset($socialstats['Data']['CodeRepository']['List'][0]['closed_pull_issues']) ) { $code_repo_closed_pull_issues = $socialstats['Data']['CodeRepository']['List'][0]['closed_pull_issues']; }
			if ( isset($socialstats['Data']['CodeRepository']['List'][0]['last_push']) ) { $code_repo_last_push = $socialstats['Data']['CodeRepository']['List'][0]['last_push']; }
			if ( isset($socialstats['Data']['CodeRepository']['List'][0]['language']) ) { $code_repo_language = $socialstats['Data']['CodeRepository']['List'][0]['language']; }


			$wpdb->insert(
				$wpdb->prefix .'crypto_cryptocompare_socialstats',
				array(
					'coin_id' => $coin_id,
					'cryptopian_followers' => $cryptopian_followers,
					'cryptopian_posts' => $cryptopian_posts,
					'cryptopian_comments' => $cryptopian_comments,
					'twitter_link' => $twitter_link,
					'twitter_followers' => $twitter_followers,
					'twitter_favourites' => $twitter_favourites,
					'reddit_posts_per_day' => $reddit_posts_per_day,
					'reddit_comments_per_day' => $reddit_comments_per_day,
					'reddit_link' => $reddit_link,
					'reddit_subscribers' => $reddit_subscribers,
					'facebook_likes' => $facebook_likes,
					'facebook_link' => $facebook_link,
					'code_repo_created_at' => $code_repo_created_at,
					'code_repo_last_update' => $code_repo_last_update,
					'code_repo_forks' => $code_repo_forks,
					'code_repo_url' => $code_repo_url,
					'code_repo_closed_issues' => $code_repo_closed_issues,
					'code_repo_closed_pull_issues' => $code_repo_closed_pull_issues,
					'code_repo_last_push' => $code_repo_last_push,
					'code_repo_language' => $code_repo_language,
				),
				array(
					'%d','%s','%s','%s','%s','%s','%s','%s','%s','%s',
					'%s','%s','%s','%s','%s','%s','%s','%s','%s','%s',
					'%s',
				)
			);

		}

		// Delete old unnecessary links
		/*
		$wpdb->query(
			$wpdb->prepare(
				" UPDATE `{$wpdb->prefix}crypto_cryptocompare_socialstats`
					SET `twitter_link` = %s,
							`reddit_link` = %s,
							`facebook_link` = %s,
							`code_repo_url` = %s,
							`code_repo_language` = %s
					WHERE `datetime` < %s
				",
				"", "", "", "", "", date('Y-m-d H:i:s', DELETE_CRYPTOCOMPARE_SOCIALSTATS_OLD_UNNECESSARY_DATA)
			)
		);
		*/

		// Remove old datas
		$wpdb->query(
			$wpdb->prepare(
				"DELETE FROM `{$wpdb->prefix}crypto_cryptocompare_socialstats`
					WHERE `datetime` < %s
				",
					date('Y-m-d H:i:s', DELETE_CRYPTOCOMPARE_SOCIALSTATS_OLD_UNNECESSARY_DATA)
				)
		);
		// ---


	}

	return "";
}
