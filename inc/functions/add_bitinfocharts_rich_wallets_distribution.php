<?php

function add_bitinfocharts_rich_wallets_distribution() {
	global $wpdb;
	$coins_data_array = array();
	$balance_types = get_option('crypto_rich_wallets_distribution_balance_types');

	if ( empty($balance_types) ) { $balance_types = array(); }


	$urls_array = array(
									'BTC' => 'https://bitinfocharts.com/top-100-richest-bitcoin-addresses.html',
									'BCH' => 'https://bitinfocharts.com/top-100-richest-bitcoin%20cash-addresses.html',
									'LTC' => 'https://bitinfocharts.com/top-100-richest-litecoin-addresses.html',
									'DASH' => 'https://bitinfocharts.com/top-100-richest-dash-addresses.html',
									'VTC' => 'https://bitinfocharts.com/top-100-richest-vertcoin-addresses.html',
								);


	foreach ($urls_array as $coin_name => $url) {
		$coin_ID = get_coin_id_by_name($coin_name);
		$site_data = file_get_contents($url);
		preg_match_all("%\<table((?s).*?)</table>%", $site_data, $matches);

		if ( (intval($coin_ID) > 0) && isset($matches[0][0]) && !empty($matches[0][0]) ) {
			$table_html = $matches[0][0];
			preg_match_all('#<tr[^>]*>(.*?)</tr>#s', $table_html, $table_rows);

			if ( isset($table_rows[0]) && !empty($table_rows[0]) ) {
				$table_rows = $table_rows[0];

				if ( is_array($table_rows) && !empty($table_rows) ) {
					foreach ($table_rows as $tr_key => $tr_html) {
						if ( $tr_key > 0 ) {
							preg_match_all('#<td[^>]*>(.*?)</td>#s', $tr_html, $table_row_datas);

							if ( isset($table_row_datas[0][0]) && !empty($table_row_datas[0][0]) ) {
								$table_row_datas = $table_row_datas[0];

								foreach ($table_row_datas as $key2 => $table_row_data) {
									preg_match_all('/<td[^>]*data-val="(.*?)">(.*?)<\/td>/', $table_row_data, $attr, PREG_PATTERN_ORDER);

									if ( isset($attr[1][0]) && !empty(isset($attr[1][0])) ) {
										if ( $key2 == 1 ) { $coins_data_array[$coin_ID][$tr_key]['addresses'] = floatval($attr[1][0]); }
										else if ( $key2 == 2 ) { $coins_data_array[$coin_ID][$tr_key]['addresses_in_percent'] = floatval($attr[1][0]); }
										else if ( $key2 == 3 ) { $coins_data_array[$coin_ID][$tr_key]['coins'] = floatval($attr[1][0]); }
										//else if ( $key2 == 4 ) { $coins_data_array[$coin_ID][$tr_key]['usd'] = floatval($attr[1][0]); }
										else if ( $key2 == 5 ) { $coins_data_array[$coin_ID][$tr_key]['coins_in_percent'] = floatval($attr[1][0]); }
									} else {
										if ( $key2 == 0 ) { $coins_data_array[$coin_ID][$tr_key]['balance'] = strip_tags($table_row_data); }
									}

								}

							}
						}
					}
				}

			}
		}
	}


	if ( is_array($coins_data_array) && !empty($coins_data_array) ) {
		foreach ($coins_data_array as $coin_ID => $wallets_datas_array) {
			if ( (intval($coin_ID) > 0) && !empty($wallets_datas_array) ) {
				foreach ($wallets_datas_array as $key => $data) {

					$balance = "";
					$addresses = "";
					$addresses_in_percent = "";
					$coins = "";
					$coins_in_percent = "";

					if ( isset($data['balance']) ) { $balance = $data['balance']; }
					if ( isset($data['addresses']) ) { $addresses = $data['addresses']; }
					if ( isset($data['addresses_in_percent']) ) { $addresses_in_percent = $data['addresses_in_percent']; }
					if ( isset($data['coins']) ) { $coins = $data['coins']; }
					if ( isset($data['coins_in_percent']) ) { $coins_in_percent = $data['coins_in_percent']; }


					// get balance type ID
					if ( !empty($balance) ) {
						$new_balance_type = TRUE;

						foreach ($balance_types as $key => $balance_type) {
							if ( $balance == $balance_type ) {
								$balance = $key;
								$new_balance_type = FALSE;
								break;
							}
						}

						if ( $new_balance_type === TRUE ) {
							$balance_types []= $balance;
							update_option( 'crypto_rich_wallets_distribution_balance_types', $balance_types, false );
							$balance_types = get_option('crypto_rich_wallets_distribution_balance_types');

							foreach ($balance_types as $key => $balance_type) {
								if ( $balance == $balance_type ) { $balance = $key; }
							}
						}
					}


					$wpdb->insert(
							$wpdb->prefix .'crypto_rich_wallets_distribution',
							array(
								'coin_ID' => $coin_ID,
								'balance_type' => $balance,
								'addresses' => $addresses,
								'addresses_in_percent' => $addresses_in_percent,
								'coins' => $coins,
								'coins_in_percent' => $coins_in_percent,
							),
							array(
								'%d','%d','%s','%s','%s','%s',
							)
					);

				}
			}
		}
	}
}
