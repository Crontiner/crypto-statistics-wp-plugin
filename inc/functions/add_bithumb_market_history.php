<?php

function add_bithumb_market_history($coin_id) {
	global $wpdb;
	if ( empty($coin_id) ) { return ""; }
	$coin_name = get_coin_name_by_id($coin_id);
	if ( empty($coin_name) ) { return ""; }


	// Remove old datas
	$wpdb->query(
		$wpdb->prepare(
			"DELETE FROM `{$wpdb->prefix}crypto_bithumb_market_history`
				WHERE `datetime` < %s
			",
				date('Y-m-d H:i:s', REMOVE_MARKET_HISTORY_TIMESTAMP)
			)
	);
	// ---


	$bithumb_datas = file_get_contents( 'https://api.bithumb.com/public/transaction_history/'. strtoupper($coin_name) .'?count=100' );
	$bithumb_datas = json_decode($bithumb_datas, true);

	$buys_array = array();
	$sells_array = array();

	if ( isset($bithumb_datas['data']) && !empty($bithumb_datas['data']) ) {
		foreach ($bithumb_datas['data'] as $key => $value) {

			if ( isset($value['type']) ) {
				if ( $value['type'] == 'bid' ) { // Sell: bid

					if ( isset($value['total']) ) 				{ $sells_array['total'][]= number_format($value['total'] / 10000000,8); } // BTC price: 1 (ETH) in BTC (actual trade price)
					if ( isset($value['units_traded']) ) 	{ $sells_array['amount'][]= $value['units_traded']; } // altcoin (ETH) price -- not BTC!

				}
				else if ( $value['type'] == 'ask' ) { // Buy: ask

					if ( isset($value['total']) ) 				{ $buys_array['total'][]= number_format($value['total'] / 10000000,8); }
					if ( isset($value['units_traded']) ) 	{ $buys_array['amount'][]= $value['units_traded']; }

				}
			}

		}
	}


	if ( 	is_array($sells_array['amount']) && !empty($sells_array['amount']) &&
				is_array($sells_array['total']) && !empty($sells_array['total']) &&
				is_array($buys_array['amount']) && !empty($buys_array['amount']) &&
				is_array($buys_array['total']) && !empty($buys_array['total']) ) {


		$sells_array['amount'] 	= array_sum($sells_array['amount']);
		$sells_array['total'] 	= array_sum($sells_array['total']);

		$buys_array['amount'] 	= array_sum($buys_array['amount']);
		$buys_array['total'] 		= array_sum($buys_array['total']);


		if ( !empty($sells_array) ) {
			$wpdb->insert(
					$wpdb->prefix .'crypto_bithumb_market_history',
					array(
						'coin_ID' => $coin_id,
						'quantity' => $sells_array['amount'],
						'total' => $sells_array['total'],
						'ordertype' => 's',
					),
					array(
						'%d','%s','%s','%s',
					)
			);
		}

		if ( !empty($buys_array) ) {
			$wpdb->insert(
					$wpdb->prefix .'crypto_bithumb_market_history',
					array(
						'coin_ID' => $coin_id,
						'quantity' => $buys_array['amount'],
						'total' => $buys_array['total'],
						'ordertype' => 'b',
					),
					array(
						'%d','%s','%s','%s',
					)
			);
		}
	}

}
