<?php

function calculate_change_amounts() {
	global $wpdb;

	$wallets_IDs = $wpdb->get_results( "SELECT `wallet_ID`
																	FROM `{$wpdb->prefix}crypto_bitinfocharts_rich_wallets_datas`
																	WHERE `changed_amount` IS NULL OR `changed_amount` = ''
																	GROUP BY `wallet_ID` having count(*) >= 2
																	ORDER BY `wallet_ID` ASC
																	LIMIT 1000
																", ARRAY_A );

	$wallets_IDs_temp = array();
	foreach ($wallets_IDs as $key => $value) {
		$wallets_IDs_temp []= (int) $value['wallet_ID'];
	}
	$wallets_IDs = array_values(array_filter(array_unique($wallets_IDs_temp)));
	unset($wallets_IDs_temp);


	if ( !empty($wallets_IDs) ) {
		foreach ($wallets_IDs as $key => $wallet_ID) {

			$wallet_amounts = $wpdb->get_results("SELECT *
																			FROM `{$wpdb->prefix}crypto_bitinfocharts_rich_wallets_datas`
																			WHERE `wallet_ID` = '". $wallet_ID ."'
																			ORDER BY `datetime` ASC
																		", ARRAY_A );

			foreach ($wallet_amounts as $key => $wallet_data_array) {
				if ( $key > 0 ) {

					$prev_amount = $wallet_amounts[$key - 1]['actual_amount'];
					$actual_amount = $wallet_data_array['actual_amount'];

					$n1 = (float) $prev_amount;
					$n2 = (float) $actual_amount;

					if ( $n1 > $n2 ) {
				    $n3 = '-'. number_format( $n1 - $n2, 8 );
					} else if ( $n1 < $n2 ) {
				    $n3 = number_format( $n2 - $n1, 8 );
					} else {
				    $n3 = 0;
					}

					$n3 = (string) $n3;
					$n3 = str_replace(',','',$n3);

					$wallet_amounts[$key]['changed_amount'] = $n3;
				}
			}

			foreach ($wallet_amounts as $key => $wallet_data_array) {
				$wpdb->update(
					$wpdb->prefix .'crypto_bitinfocharts_rich_wallets_datas',
					array( 'changed_amount' => $wallet_data_array['changed_amount'] ),
					array( 'ID' => $wallet_data_array['ID'] ),
					array( '%s' ),
					array( '%d' )
				);
			}

		}
	}
}


function get_rich_wallets_data($link, $coin_symbol) {
	$final_array_temp = array();

	$bitinfocharts_html = file_get_contents( $link );
	preg_match_all('#<tr[^>]*>(.*?)</tr>#s', $bitinfocharts_html, $matches);

	foreach ($matches as $key => $value) {
		foreach ($matches[$key] as $key2 => $html) {
			if (strpos($html, '/address/') !== false) {

				$html = str_replace('<tr>', '', $html);
				$html = str_replace('</tr>', '', $html);
				$html = explode('<td', $html);

				$address = substr($html[2], strpos($html[2], '/address/'));
				$address = substr($address, 0, strpos($address, '</a>'));
				$address = substr($address, strpos($address, '">') + 2);

				$btc_amount = substr($html[3], strpos($html[3], 'data-val="'));
				$btc_amount = substr($btc_amount, 0, strpos($btc_amount, $coin_symbol));
				$btc_amount = substr($btc_amount, 0, strpos($btc_amount, '">') + 2);
				$btc_amount = str_replace('data-val="', '', $btc_amount);
				$btc_amount = (float) str_replace('">', '', $btc_amount);

				$last_in_timestamp = substr($html[6], strpos($html[6], 'data-val="'));
				$last_in_timestamp = substr($last_in_timestamp, 0, strpos($last_in_timestamp, 'UTC'));
				$last_in_timestamp = substr($last_in_timestamp, 0, strpos($last_in_timestamp, '">') + 2);
				$last_in_timestamp = str_replace('data-val="', '', $last_in_timestamp);
				$last_in_timestamp = (int) str_replace('">', '', $last_in_timestamp);

				$last_out_timestamp = substr($html[9], strpos($html[9], 'data-val="'));
				$last_out_timestamp = substr($last_out_timestamp, 0, strpos($last_out_timestamp, 'UTC'));
				$last_out_timestamp = substr($last_out_timestamp, 0, strpos($last_out_timestamp, '">') + 2);
				$last_out_timestamp = str_replace('data-val="', '', $last_out_timestamp);
				$last_out_timestamp = (int) str_replace('">', '', $last_out_timestamp);

				if ( !empty($address) && !empty($btc_amount) ) {
					$final_array_temp[$address] = array(
																								'amount' => $btc_amount,
																								'last_in_timestamp' => $last_in_timestamp,
																								'last_out_timestamp' => $last_out_timestamp,
																							);
				}
			}
		}
	}

	return $final_array_temp;
}


function add_bitinfocharts_rich_wallets_datas() {
	global $wpdb;
	$links = array();
	$final_array = array();

	for ($i=1; $i <= 40; $i++) {
		if ( $i == 1 ) {
			$link = 'https://bitinfocharts.com/top-100-richest-bitcoin-addresses.html';
		} else {
			$link = 'https://bitinfocharts.com/top-100-richest-bitcoin-addresses-'. $i .'.html';
		}
		$links['BTC'][] = $link;
	}

	for ($i=1; $i <= 40; $i++) {
		if ( $i == 1 ) {
			$link = 'https://bitinfocharts.com/top-100-richest-dash-addresses.html';
		} else {
			$link = 'https://bitinfocharts.com/top-100-richest-dash-addresses-'. $i .'.html';
		}
		$links['DASH'][] = $link;
	}

	for ($i=1; $i <= 40; $i++) {
		if ( $i == 1 ) {
			$link = 'https://bitinfocharts.com/top-100-richest-litecoin-addresses.html';
		} else {
			$link = 'https://bitinfocharts.com/top-100-richest-litecoin-addresses-'. $i .'.html';
		}
		$links['LTC'][] = $link;
	}

	for ($i=1; $i <= 40; $i++) {
		if ( $i == 1 ) {
			$link = 'https://bitinfocharts.com/top-100-richest-vertcoin-addresses.html';
		} else {
			$link = 'https://bitinfocharts.com/top-100-richest-vertcoin-addresses-'. $i .'.html';
		}
		$links['VTC'][] = $link;
	}


	foreach ($links['BTC'] as $key => $bitinfocharts_link) {
		$datas = get_rich_wallets_data($bitinfocharts_link, 'BTC');
		if ( !empty($datas) ) {
			$final_array['BTC'][]= $datas;
		}
	}
	$final_array_temp = array();
	foreach ($final_array['BTC'] as $key => $wallets_data_array) {
		foreach ($final_array['BTC'][$key] as $wallet_address => $wallet_data_array) {
			$final_array_temp [$wallet_address]= $wallet_data_array;
		}
	}
	$final_array['BTC'] = $final_array_temp;
	unset($final_array_temp);


	foreach ($links['DASH'] as $key => $bitinfocharts_link) {
		$datas = get_rich_wallets_data($bitinfocharts_link, 'DASH');
		if ( !empty($datas) ) {
			$final_array['DASH'][]= $datas;
		}
	}
	$final_array_temp = array();
	foreach ($final_array['DASH'] as $key => $wallets_data_array) {
		foreach ($final_array['DASH'][$key] as $wallet_address => $wallet_data_array) {
			$final_array_temp [$wallet_address]= $wallet_data_array;
		}
	}
	$final_array['DASH'] = $final_array_temp;
	unset($final_array_temp);


	foreach ($links['LTC'] as $key => $bitinfocharts_link) {
		$datas = get_rich_wallets_data($bitinfocharts_link, 'LTC');
		if ( !empty($datas) ) {
			$final_array['LTC'][]= $datas;
		}
	}
	$final_array_temp = array();
	foreach ($final_array['LTC'] as $key => $wallets_data_array) {
		foreach ($final_array['LTC'][$key] as $wallet_address => $wallet_data_array) {
			$final_array_temp [$wallet_address]= $wallet_data_array;
		}
	}
	$final_array['LTC'] = $final_array_temp;
	unset($final_array_temp);


	foreach ($links['VTC'] as $key => $bitinfocharts_link) {
		$datas = get_rich_wallets_data($bitinfocharts_link, 'VTC');
		if ( !empty($datas) ) {
			$final_array['VTC'][]= $datas;
		}
	}
	$final_array_temp = array();
	foreach ($final_array['VTC'] as $key => $wallets_data_array) {
		foreach ($final_array['VTC'][$key] as $wallet_address => $wallet_data_array) {
			$final_array_temp [$wallet_address]= $wallet_data_array;
		}
	}
	$final_array['VTC'] = $final_array_temp;
	unset($final_array_temp);
	unset($links);


	$BTC_ID = get_coin_id_by_name('BTC');
	$DASH_ID = get_coin_id_by_name('DASH');
	$LTC_ID = get_coin_id_by_name('LTC');
	$VTC_ID = get_coin_id_by_name('VTC');

	$coins_array = array(
												'BTC' 	=> $BTC_ID,
												'DASH' 	=> $DASH_ID,
												'LTC' 	=> $LTC_ID,
												'VTC' 	=> $VTC_ID,
											);

	foreach ($coins_array as $coin_symbol => $coin_ID) {

		// insert walletts datas

		foreach ($final_array[$coin_symbol] as $wallet_address => $wallet_addresses_array) {

			$wallet_id = $wpdb->get_results( "SELECT `ID` FROM `{$wpdb->prefix}crypto_bitinfocharts_rich_wallets`
																								WHERE `wallet_address` = '". $wallet_address ."' LIMIT 1 ", ARRAY_A );
			$wallet_id = (int) $wallet_id[0]['ID'];


			if ( $wallet_id == 0 ) {
				$wpdb->insert(
					$wpdb->prefix .'crypto_bitinfocharts_rich_wallets',
					array(
						'wallet_address' => $wallet_address,
						'coin_ID' => $coin_ID,
						'last_updated_datetime' => current_time('mysql'),
					),
					array('%s','%d','%s')
				);
				$wallet_id = (int) $wpdb->insert_id;
			}

			if ( $wallet_id > 0 ) {

				$wpdb->update(
					$wpdb->prefix .'crypto_bitinfocharts_rich_wallets',
					array( 'last_updated_datetime' => current_time('mysql') ),
					array( 'ID' => $wallet_id ),
					array( '%s' ),
					array( '%d' )
				);

				$last_in_timestamp = $wallet_addresses_array['last_in_timestamp'];
				$last_out_timestamp = $wallet_addresses_array['last_out_timestamp'];

				if ( $last_in_timestamp > 0 ) 	{ $last_in_timestamp = date('Y-m-d H:i:s', $last_in_timestamp); }
				else { $last_in_timestamp = ""; }
				if ( $last_out_timestamp > 0 ) { $last_out_timestamp = date('Y-m-d H:i:s', $last_out_timestamp); }
				else { $last_out_timestamp = ""; }

				if ( strtotime($last_in_timestamp) > strtotime($last_out_timestamp) ) { $datetime = $last_in_timestamp; }
				else if ( strtotime($last_in_timestamp) < strtotime($last_out_timestamp) ) { $datetime = $last_out_timestamp; }
				else if ( strtotime($last_in_timestamp) == strtotime($last_out_timestamp) ) { $datetime = $last_out_timestamp; }
				else { $datetime = ""; }

				$wallet_data_id = $wpdb->get_results( "SELECT `ID`
																					FROM `{$wpdb->prefix}crypto_bitinfocharts_rich_wallets_datas`
																					WHERE `wallet_ID` = '". $wallet_id ."' AND
																								`actual_amount` = '". strval($wallet_addresses_array['amount']) ."' AND
																								`datetime` = '". $datetime ."'
																					LIMIT 1 ", ARRAY_A );
				$wallet_data_id = (int) $wallet_data_id[0];


				if ( $wallet_data_id == 0 ) {

					if ( strtotime($last_in_timestamp) > strtotime($last_out_timestamp) ) { $datetime = $last_in_timestamp; }
					else if ( strtotime($last_in_timestamp) < strtotime($last_out_timestamp) ) { $datetime = $last_out_timestamp; }
					else if ( strtotime($last_in_timestamp) == strtotime($last_out_timestamp) ) { $datetime = $last_out_timestamp; }
					else { $datetime = ""; }

					$wpdb->insert(
						$wpdb->prefix .'crypto_bitinfocharts_rich_wallets_datas',
						array(
							'wallet_ID' => $wallet_id,
							'actual_amount' => strval($wallet_addresses_array['amount']),
							'datetime' => $datetime,
						),
						array('%d','%s','%s')
					);
				}
			}
		}


		// Kieset walletok törlése
		$useless_wallets = $wpdb->get_results( "	SELECT `ID`
																							FROM `{$wpdb->prefix}crypto_bitinfocharts_rich_wallets`
																							WHERE `last_updated_datetime` <= '". date('Y-m-d H:i:s', strtotime("-4 days")) ."' OR `last_updated_datetime` IS NULL
																							ORDER BY `ID` ASC LIMIT 1000", ARRAY_A );

		if ( !empty($useless_wallets) ) {
			foreach ($useless_wallets as $key => $value) {
				if ( intval($value['ID']) > 0 ) {

					$wpdb->delete( $wpdb->prefix .'crypto_bitinfocharts_rich_wallets',
													array( 'ID' => intval($value['ID']) ), array( '%d' ));

					$wpdb->delete( $wpdb->prefix .'crypto_bitinfocharts_rich_wallets_datas',
													array( 'wallet_ID' => intval($value['ID']) ), array( '%d' ));
				}
			}
		}


		// kiszámolja a pénzmozgásokat
		//calculate_change_amounts($coin_ID);
		calculate_change_amounts();
		calculate_change_amounts();
		calculate_change_amounts();
	}


	unset($final_array);
}
