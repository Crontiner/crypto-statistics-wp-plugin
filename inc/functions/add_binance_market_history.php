<?php

	// https://github.com/binance-exchange/binance-official-api-docs/blob/master/rest-api.md

function add_binance_market_history($coin_id) {
	global $wpdb;
	if ( empty($coin_id) ) { return ""; }
	$coin_name = get_coin_name_by_id($coin_id);
	if ( empty($coin_name) ) { return ""; }


	// Remove old datas
	$wpdb->query(
		$wpdb->prepare(
			"DELETE FROM `{$wpdb->prefix}crypto_binance_market_history`
				WHERE `datetime` < %s
			",
				date('Y-m-d H:i:s', REMOVE_MARKET_HISTORY_TIMESTAMP)
			)
	);
	// ---


	$binance_datas = file_get_contents( 'https://api.binance.com/api/v1/aggTrades?symbol='. strtoupper($coin_name) .'BTC&startTime='. strtotime("-8 minutes") .'000&endTime='. strtotime("now") .'000' );
	$binance_datas = json_decode($binance_datas, true);


	$buys_array = array();
	$sells_array = array();

	if ( isset($binance_datas[0]['T']) && !empty($binance_datas[0]['T']) ) {
		foreach ($binance_datas as $key => $value) {

			if ( isset($value['m']) ) {
				if ( $value['m'] == FALSE ) {

					if ( isset($value['p']) ) 		{ $sells_array['price'][]= $value['p']; } // BTC price: 1 (ETH) in BTC (actual trade price)
					if ( isset($value['q']) ) 		{ $sells_array['amount'][]= $value['q']; } // altcoin (ETH) price -- not BTC!

					if ( isset($value['p']) &&
							 isset($value['q']) ) 		{ $sells_array['total'][]= number_format(floatval($value['q']) * floatval($value['p']),8); } // BTC price

				}
				else if ( $value['m'] == TRUE ) {

					if ( isset($value['p']) ) 		{ $buys_array['price'][]= $value['p']; }
					if ( isset($value['q']) ) 		{ $buys_array['amount'][]= $value['q']; }

					if ( isset($value['p']) &&
							 isset($value['q']) ) 		{ $buys_array['total'][]= number_format(floatval($value['q']) * floatval($value['p']),8); } // BTC price
				}
			}

		}
	}


	//$sells_array['price'] 	= number_format(array_sum($sells_array['price']) / count($sells_array['price']),8);
	$sells_array['amount'] 	= array_sum($sells_array['amount']);
	$sells_array['total'] 	= array_sum($sells_array['total']);

	//$buys_array['price'] 		= number_format(array_sum($buys_array['price']) / count($buys_array['price']),8);
	$buys_array['amount'] 	= array_sum($buys_array['amount']);
	$buys_array['total'] 		= array_sum($buys_array['total']);


	if ( !empty($sells_array) ) {
		$wpdb->insert(
				$wpdb->prefix .'crypto_binance_market_history',
				array(
					'coin_ID' => $coin_id,
					'quantity' => $sells_array['amount'],
					'total' => $sells_array['total'],
					'ordertype' => 's',
				),
				array(
					'%d','%s','%s','%s',
				)
		);
	}

	if ( !empty($buys_array) ) {
		$wpdb->insert(
				$wpdb->prefix .'crypto_binance_market_history',
				array(
					'coin_ID' => $coin_id,
					'quantity' => $buys_array['amount'],
					'total' => $buys_array['total'],
					'ordertype' => 'b',
				),
				array(
					'%d','%s','%s','%s',
				)
		);
	}

}
