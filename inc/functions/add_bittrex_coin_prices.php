<?php

function add_bittrex_coin_prices_v2($coin_name) {
	global $wpdb;
	if ( empty($coin_name) ) { return ""; }

	if ( $coin_name == 'BCH' ) {
		$bittrex_datas = file_get_contents( 'https://bittrex.com/Api/v2.0/pub/market/GetTicks?marketName=BTC-BCC&tickInterval=hour&_='. strtotime("-4 month") );
	} else {
		$bittrex_datas = file_get_contents( 'https://bittrex.com/Api/v2.0/pub/market/GetTicks?marketName=BTC-'. $coin_name .'&tickInterval=hour&_='. strtotime("-4 month") );
	}


	$bittrex_datas = json_decode($bittrex_datas);

	$coin_id = get_coin_id_by_name($coin_name);

	if ( ( $coin_id > 0 ) && isset($bittrex_datas->result) && !empty($bittrex_datas->result) ) {


		// lekéri a kiv. coin összes dátumát
		$filled_rows = $wpdb->get_results( "SELECT `datetime` FROM `{$wpdb->prefix}crypto_bittrex_data`
																					WHERE `coin_ID` = '{$coin_id}'
																					ORDER BY `datetime` ASC", ARRAY_A );

		$filled_datetimes = array();
		foreach ($filled_rows as $key => $row) {
			if ( isset($row['datetime']) && !empty($row['datetime']) ) {
				$filled_datetimes []= date('Y-m-d H:30', strtotime($row['datetime']));
			}
		}

		foreach ($bittrex_datas->result as $key => $hourly_data) {
			$bittrex_timestamp = strtotime($hourly_data->T);
			$year = date('Y', $bittrex_timestamp);
			$month = date('m', $bittrex_timestamp);
			$day = date('d', $bittrex_timestamp);
			$hour = date('G', $bittrex_timestamp);

			//if ( ($hour % 2) == 0 ) {
				if ( in_array(date('Y-m-d H:30', $bittrex_timestamp), $filled_datetimes) ) {
					//var_dump('meglévő: '. date('Y-m-d H:30', $bittrex_timestamp));
				} else {
					//var_dump('új: '. date('Y-m-d H:30', $bittrex_timestamp));

					$open 				= "";
					$high 				= "";
					$low 					= "";
					$close 				= "";
					$volume_24h 	= "";
					$base_volume 	= "";
					$datetime			= current_time('mysql');

					if ( isset($hourly_data->O) ) {
						$open 				= number_format($hourly_data->O, 8);
					}
					if ( isset($hourly_data->H) ) {
						$high 				= number_format($hourly_data->H, 8);
					}
					if ( isset($hourly_data->L) ) {
						$low 					= number_format($hourly_data->L, 8);
					}
					if ( isset($hourly_data->C) ) {
						$close 				= number_format($hourly_data->C, 8);
					}
					if ( isset($hourly_data->V) ) {
						$volume_24h 	= $hourly_data->V;
					}
					if ( isset($hourly_data->BV) ) {
						$base_volume 	= number_format($hourly_data->BV, 8);
					}
					if ( isset($hourly_data->T) ) {
						$datetime = date('Y-m-d H:i:s', $bittrex_timestamp);
					}


					$wpdb->insert(
							$wpdb->prefix .'crypto_bittrex_data',
							array(
								'coin_ID' => $coin_id,
								'open' => $open,
								'high' => $high,
								'low' => $low,
								'close' => $close,
								'volume_24h' => $volume_24h,
								'base_volume' => $base_volume,
								'datetime' => $datetime,
							),
							array(
								'%d','%s','%s','%s','%s','%s','%s','%s'
							)
					);

				}
			//}
		}
	}
}
