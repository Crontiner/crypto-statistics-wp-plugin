<?php

function add_technical_analysis_datas() {
	$technical_analysis = file_get_contents( 'https://crypto3bh21.000webhostapp.com/?get_technical_analysis&anticache='. strtolower(wp_generate_password(8, false, false)) );

	if ( !empty($technical_analysis) ) {
		global $wpdb;
		$technical_analysis = json_decode(base64_decode($technical_analysis), true);

		if ( !empty($technical_analysis) ) {
			foreach ($technical_analysis as $key => $v) {

				if ( intval($v['cID']) > 0 ) {
					$markets_data = $wpdb->get_results( " SELECT *
																								FROM `cryp_crypto_technical_analysis`
																								WHERE `coin_ID` = ". $v['cID'] ." AND
																											`type` = '". $v['type'] ."' AND
																											`datetime` = '". $v['time'] ."'
																								LIMIT 1 ", ARRAY_A );

					if ( empty($markets_data) ) {
						$wpdb->insert(
								$wpdb->prefix .'crypto_technical_analysis',
								array(
									'coin_ID' => intval($v['cID']),
									'type' => $v['type'],
									'technical_indic_summary' => (int) $v['ti_sum'],
									'technical_indic_buy' => (int) $v['tib'],
									'technical_indic_sell' => (int) $v['tis'],
									'moving_avg_buy' => (int) $v['mab'],
									'moving_avg_sell' => (int) $v['mas'],
									'datetime' => $v['time'],
								),
								array('%d','%s','%d','%d','%d','%d','%d','%s',)
						);
					}
				}
			}

			// Remove old datas
			$wpdb->query(
				$wpdb->prepare(
					"DELETE FROM `{$wpdb->prefix}crypto_technical_analysis`
						WHERE `datetime` < %s
					",
						date('Y-m-d H:i:s', strtotime('-72 hours'))
					)
			);
			// ---

		}
	}
}
