<?php
// Hozzáadja a hiányzó értékeket a "cryp_crypto_coin_names" táblához

function add_cmc_missing_values_for_crypto_coin_names_db_table() {
	global $wpdb;

	$crypto_coin_names_data = $wpdb->get_results( "SELECT `coin_name` FROM `{$wpdb->prefix}crypto_coin_names`
																						WHERE `coin_full_name` = '' OR
																									`logo_url` = '' OR
																									`logo_url` = '#' OR
																									`link` = ''
																						", ARRAY_A );

	if ( isset($crypto_coin_names_data[0]) ) {

		// Coin nevek összegyűjtése
		$crypto_coin_names_array = array();
		foreach ($crypto_coin_names_data as $key => $coin_data) {
			$crypto_coin_names_array []= $coin_data['coin_name'];
		}
		$crypto_coin_names_array = array_values(array_filter(array_unique($crypto_coin_names_array)));


		if ( !empty($crypto_coin_names_array) ) {

			// CMC adatok lekérése
			$html_content 				= file_get_contents( 'https://coinmarketcap.com/' );
			$html_content_page2 	= file_get_contents( 'https://coinmarketcap.com/2' );
			$html_content .= $html_content_page2;

			preg_match_all('/<td class=\"no\-wrap currency\-name">(.*?)<\/td>/s',$html_content,$estimates);

			$coins = array();
			foreach ($estimates[0] as $key => $td_html_content) {

				$img_src 				= getTextBetween('src="','"',$td_html_content);
				$coin_full_name = getTextBetween('alt="','"',$td_html_content);
				$link 					= getTextBetween('href="','"',$td_html_content);

				$coin_symbol 		= getTextBetween('<span class="currency-symbol">','</span>',$td_html_content);
				$coin_symbol 		= getTextBetween('">','</a>',$coin_symbol);


				$img_src = str_replace('16x16','200x200', $img_src);
				if ( !empty($link) ) {
					$link = 'https://coinmarketcap.com'. $link;
				}

				$coins []= array( 'coin_symbol' => $coin_symbol,
													'coin_full_name' => $coin_full_name,
													'img_src' => $img_src,
													'link' => $link,
												);
			}

			foreach ($coins as $key => $coin_data) {
				$coin_symbol = $coin_data['coin_symbol'];
				if ( in_array($coin_symbol, $crypto_coin_names_array) && ( check_coin_is_banned($coin_symbol) === FALSE ) ) {

					$selected_coin_data = $wpdb->get_results( "SELECT * FROM `{$wpdb->prefix}crypto_coin_names`
																											WHERE `coin_name` = '{$coin_symbol}' LIMIT 1", ARRAY_A );

					// Update: coin full name

					if (  !empty($coin_data['coin_full_name']) &&
								isset($selected_coin_data[0]['coin_full_name']) &&
								empty($selected_coin_data[0]['coin_full_name']) ) {

							$wpdb->update(
								$wpdb->prefix .'crypto_coin_names',
								array(
									'coin_full_name' => $coin_data['coin_full_name'],
								),
								array( 'ID' => $selected_coin_data[0]['ID'] ),
								array( '%s' ),
								array( '%d' )
							);
					}


					// Update: logo URL

					if (  !empty($coin_data['img_src']) &&
								isset($selected_coin_data[0]['logo_url']) &&
								(empty($selected_coin_data[0]['logo_url']) || $selected_coin_data[0]['logo_url'] == "#" ) ) {

							$wpdb->update(
								$wpdb->prefix .'crypto_coin_names',
								array(
									'logo_url' => $coin_data['img_src'],
								),
								array( 'ID' => $selected_coin_data[0]['ID'] ),
								array( '%s' ),
								array( '%d' )
							);
					}


					// Update: CMC link

					if (  !empty($coin_data['link']) &&
								isset($selected_coin_data[0]['link']) &&
								empty($selected_coin_data[0]['link']) ) {

							$wpdb->update(
								$wpdb->prefix .'crypto_coin_names',
								array(
									'link' => $coin_data['link'],
								),
								array( 'ID' => $selected_coin_data[0]['ID'] ),
								array( '%s' ),
								array( '%d' )
							);
					}

				}
			}

		}
	}
}
