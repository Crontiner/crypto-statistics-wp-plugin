<?php

function add_kraken_market_history($coin_id) {
	global $wpdb;
	if ( empty($coin_id) ) { return ""; }
	$coin_name = get_coin_name_by_id($coin_id);
	if ( empty($coin_name) ) { return ""; }


	// Remove old datas
	$wpdb->query(
		$wpdb->prepare(
			"DELETE FROM `{$wpdb->prefix}crypto_kraken_market_history`
				WHERE `datetime` < %s
			",
				date('Y-m-d H:i:s', REMOVE_MARKET_HISTORY_TIMESTAMP)
			)
	);
	// ---


	$kraken_datas = file_get_contents( 'https://api.kraken.com/0/public/Trades?pair=X'. strtoupper($coin_name) .'XXBT&since='. strtotime("-7 minutes") .'000000000' );
	$kraken_datas = json_decode($kraken_datas, true);

	$buys_array = array();
	$sells_array = array();


	if ( isset($kraken_datas['result']['X'. strtoupper($coin_name) .'XXBT']) && !empty($kraken_datas['result']['X'. strtoupper($coin_name) .'XXBT']) ) {
		foreach ($kraken_datas['result']['X'. strtoupper($coin_name) .'XXBT'] as $key => $value) {

			if ( isset($value[3]) ) {
				if ( $value[3] == 's' ) {

					if ( isset($value[1]) ) 		{ $sells_array['quantity'][]= $value[1]; } // altcoin (ETH) price - not BTC!

					if ( isset($value[0]) &&
							 isset($value[1]) ) 		{ $sells_array['total'][]= number_format(floatval($value[1]) * floatval($value[0]),8); } // BTC price

				}
				else if ( $value[3] == 'b' ) {

					if ( isset($value[1]) ) 		{ $buys_array['quantity'][]= $value[1]; }

					if ( isset($value[0]) &&
							 isset($value[1]) ) 		{ $buys_array['total'][]= number_format(floatval($value[1]) * floatval($value[0]),8); } // BTC price

				}
			}

		}
	}


	if ( 	is_array($sells_array['quantity']) && !empty($sells_array['quantity']) &&
				is_array($sells_array['total']) && !empty($sells_array['total']) &&
				is_array($buys_array['quantity']) && !empty($buys_array['quantity']) &&
				is_array($buys_array['total']) && !empty($buys_array['total']) ) {


		$sells_array['amount'] 	= array_sum($sells_array['quantity']);
		$sells_array['total'] 	= array_sum($sells_array['total']);

		$buys_array['amount'] 	= array_sum($buys_array['quantity']);
		$buys_array['total'] 		= array_sum($buys_array['total']);


		if ( !empty($sells_array) ) {
			$wpdb->insert(
					$wpdb->prefix .'crypto_kraken_market_history',
					array(
						'coin_ID' => $coin_id,
						'quantity' => $sells_array['amount'],
						'total' => $sells_array['total'],
						'ordertype' => 's',
					),
					array(
						'%d','%s','%s','%s',
					)
			);
		}

		if ( !empty($buys_array) ) {
			$wpdb->insert(
					$wpdb->prefix .'crypto_kraken_market_history',
					array(
						'coin_ID' => $coin_id,
						'quantity' => $buys_array['amount'],
						'total' => $buys_array['total'],
						'ordertype' => 'b',
					),
					array(
						'%d','%s','%s','%s',
					)
			);
		}
	}
}
