<?php

function high_profit_warning() {
	own_investments_sc_function('update');

	$own_investments = get_option('crypto_own_investments');
	$high_profit_warning = array();

	if ( !empty($own_investments) ) {
		$own_investments = json_decode($own_investments, true);

		if ( isset($own_investments['investments']) ) {
			foreach ($own_investments['investments'] as $key => $value) {

				if ( floatval($value['profit_in_percent']) >= 50 ) { // <--- min 50% profit
					$high_profit_warning []= '<li><a href="'. get_edit_post_link($value['post_id']) .'">'. get_coin_name_by_id($value['coin_id']) .' - '. $value['profit_in_percent'] .'%</a></li>';
				}
			}
		}
	}

	$msg = "";
	if ( !empty($high_profit_warning) ) {
		$msg .=
						'<h4>Befektetések amik meghaladják az 50%-os profitot:</h4>
						 <ul>'. implode('', $high_profit_warning) .'</ul><br>';
	}

	if ( !empty($msg) ) {
		crypto_send_message($msg, 'Magas profitú befektetések', 'high_profitable_investments');
	}
}

// Figyelmeztetés küldése ha árnövekedés vagy csökkenés várható
function price_changes_are_expected() {
	global $wpdb;

	$compilation_datas = $wpdb->get_results( "SELECT `ID`, `coin_ID`, `all_sells_and_buys_datas_evaluation`
																					FROM `{$wpdb->prefix}crypto_compilation` ", ARRAY_A );

	$rise_is_expected_array = array();
	$reduction_is_expected_array = array();
	$site_url = get_permalink( PAGE_OSSZEALLITASOK );

	foreach ($compilation_datas as $k => $v) {
		$row_ID = (int) $v['ID'];
		$coin_ID = (int) $v['coin_ID'];
		$last_update_timestamp = strtotime($v['datetime']);

		if ( (intval($row_ID) > 0) && (intval($coin_ID) > 0) && (intval($last_update_timestamp) > 0) ) {

			$volume = get_cmc_coin_volume($coin_ID);
			if ( isset($volume['percent_change']) ) { $volume_percent_change = $volume['percent_change']; }
			else { $volume_percent_change = 0; }

			$buys = 0;
			$sells = 0;

			if ( (strtotime("NOW") - intval($last_update_timestamp)) >= (1 * 60 * 60) ) { // max 1 órában frissűlt adatok

				if ( isset($v['all_sells_and_buys_datas_evaluation']) ) {
					$v['all_sells_and_buys_datas_evaluation'] = json_decode($v['all_sells_and_buys_datas_evaluation'], true);
					if ( isset($v['all_sells_and_buys_datas_evaluation']['values']) ) {
						$v['all_sells_and_buys_datas_evaluation'] = $v['all_sells_and_buys_datas_evaluation']['values'];

						$buys = (float) $v['all_sells_and_buys_datas_evaluation'][0];
						$sells = (float) $v['all_sells_and_buys_datas_evaluation'][1];
					}
				}

				if ( ($buys >= 150) && ($sells <= 0) && ($volume_percent_change >= 20) ) {
					// sok a vétel, emelkedni fog az ár

					$rise_is_expected_array []= $coin_ID;

				} else if ( ($sells >= 150) && ($buys <= 0) && ($volume_percent_change <= -20) ) {
					// sok az eladás, csökkenni fog az ár

					$reduction_is_expected_array []= $coin_ID;
				}
			}
		}
	}

	$rise_is_expected_array = array_values(array_filter(array_unique($rise_is_expected_array)));
	$reduction_is_expected_array = array_values(array_filter(array_unique($reduction_is_expected_array)));

	$rise_is_expected = array();
	if ( !empty($rise_is_expected_array) ) {
		foreach ($rise_is_expected_array as $key => $coin_ID) {
			$coin_name = get_coin_name_by_id($coin_ID);
			$rise_is_expected []= '<li><a href="'. $site_url .'#'. strtolower($coin_name) .'">'. $coin_name .'</a></li>';
		}
	}

	$reduction_is_expected = array();
	if ( !empty($reduction_is_expected_array) ) {
		foreach ($reduction_is_expected_array as $key => $coin_ID) {
			$coin_name = get_coin_name_by_id($coin_ID);
			$reduction_is_expected []= '<li><a href="'. $site_url .'#'. strtolower($coin_name) .'">'. $coin_name .'</a></li>';
		}
	}

	$msg = "";
	if ( !empty($rise_is_expected) ) {
		$msg .=
						'<h4>Árnövekedés várható:</h4>
						 <ul>'. implode('', $rise_is_expected) .'</ul><br>';
	}

	if ( !empty($reduction_is_expected) ) {
		$msg .=
						'<h4>Árcsökkenés várható:</h4>
						 <ul>'. implode('', $reduction_is_expected) .'</ul><br>';
	}

	if ( !empty($msg) ) {
		crypto_send_message($msg, 'Árváltozás várható', 'price_changes_are_expected');
	}
}

function fav_coin_price_alert() {
	global $wpdb;

	$fav_coins_array = json_decode(FAV_COINS_ARRAY);
	$fav_coins = array();
	foreach ($fav_coins_array as $key => $value) {
		$fav_coins []= '"'. $value .'"';
	}

	$alert = array();

	$crypto_compilation = $wpdb->get_results( "SELECT `{$wpdb->prefix}crypto_compilation`.`coin_ID`, `{$wpdb->prefix}crypto_compilation`.`bittrex_price_change_in_percent` AS `coin_percent_change`
																		FROM `{$wpdb->prefix}crypto_compilation`
																		LEFT JOIN `{$wpdb->prefix}crypto_coin_names` ON `{$wpdb->prefix}crypto_compilation`.`coin_ID` = `{$wpdb->prefix}crypto_coin_names`.`ID`
																		WHERE `{$wpdb->prefix}crypto_coin_names`.`coin_name` IN (". implode(',', $fav_coins) .")
																		ORDER BY `{$wpdb->prefix}crypto_compilation`.`datetime` DESC", ARRAY_A );

	if ( empty($crypto_compilation) ) { return ""; }

	foreach ($crypto_compilation as $key => $coin_data) {
		if ( floatval($coin_data['coin_percent_change']) < -15 ) {

			// vásárlás
			$alert['buys'][] = get_coin_name_by_id((int) $coin_data['coin_ID']);

		} else if ( floatval($coin_data['coin_percent_change']) > 15 ) {

			// eladás (csak akkor küldi ki, ha van ilyen befektetés)
			if ( get_investment_id( get_coin_name_by_id((int) $coin_data['coin_ID']) ) > 0 ) {
				$alert['sells'][] = get_coin_name_by_id((int) $coin_data['coin_ID']);
			}

		}
	}

	$msg = "";
	$site_url = get_site_url();

	if ( isset($alert['buys']) ) {
		$msg .= '<h4>Must Buys:</h4><ul>';

		foreach ($alert['buys'] as $key => $coin_name) {
			$msg .= '<li><a href="'. $site_url .'/#'. strtolower($coin_name) .'">'. $coin_name .'</a></li>';
		}
		$msg .= '</ul>';
	}
	if ( isset($alert['sells']) ) {
		$msg .= '<h4>Must Sells:</h4><ul>';
		foreach ($alert['sells'] as $key => $coin_name) {
			$msg .= '<li><a href="'. $site_url .'/#'. strtolower($coin_name) .'">'. $coin_name .'</a></li>';
		}
		$msg .= '</ul>';
	}

	crypto_send_message($msg, 'fav coin price alert', 'fav_coin_price_alert');
}



// Ha nagyot esnek vagy nőnek az árak akkor küld egy figyelmeztető emailt

function check_market_status() {
	$last_sell_buy_days_data 	= (float) get_option( 'crypto_last_sell_buy_days_data' ); // cmc százalékos esés/emelkedés arány (2 óránként frissül)
	$buy_or_sell 							= get_option( 'crypto_buy_or_sell_'. date('Y-m') ); // cmc top 100 állapot (30 percenként frissül)

	if ( isset($_GET['devmode']) ) {
		//var_dump($last_sell_buy_days_data);  die;
	}


	if ( !empty($buy_or_sell) ) {
		$last_buy_or_sell_data = end($buy_or_sell);
		unset($buy_or_sell);

		if ( ($last_sell_buy_days_data <= -15) &&
				 ($last_buy_or_sell_data[0] == "-") &&
				 ($last_buy_or_sell_data['neg'] >= 98) ) {

					 // Vásárlás
					$res = get_must_sell_and_buy_coins_from_compilation();
					$must_buys = array();
					$buys = array();
					$msg = "";
					$site_url = get_permalink( PAGE_OSSZEALLITASOK );

					if ( !empty($res['must_buys']) ) {
						foreach ($res['must_buys'] as $key => $coin_ID) {
							$coin_name = get_coin_name_by_id($coin_ID);
							$must_buys []= '<li><a href="'. $site_url .'#'. strtolower($coin_name) .'">'. $coin_name .'</a></li>';
						}
					}
					if ( !empty($res['buys']) ) {
						foreach ($res['buys'] as $key => $coin_ID) {
							$coin_name = get_coin_name_by_id($coin_ID);
							$buys []= '<li><a href="'. $site_url .'#'. strtolower($coin_name) .'">'. $coin_name .'</a></li>';
						}
					}

					if ( !empty($must_buys) ) {
						$msg .=
										'<h4>Must Buys:</h4>
										 <ul>'. implode('', $must_buys) .'</ul><br>';
					}

					if ( !empty($buys) ) {
						$msg .=
										'<h4>Buys:</h4>
										 <ul>'. implode('', $buys) .'</ul><br>';
					}

					crypto_send_message($msg, 'market status', 'check_market_status');

		} else if ( ($last_sell_buy_days_data >= 15) &&
				 ($last_buy_or_sell_data[0] == "+") &&
				 ($last_buy_or_sell_data['pos'] >= 98) ) {

					 // Eladás

					$res = get_must_sell_and_buy_coins_from_compilation();
					$must_sells = array();
					$sells = array();
					$msg = "";
					$site_url = get_permalink( PAGE_OSSZEALLITASOK );

					if ( !empty($res['must_sells']) ) {
						foreach ($res['must_sells'] as $key => $coin_ID) {
							$coin_name = get_coin_name_by_id($coin_ID);
							$must_sells []= '<li><a href="'. $site_url .'#'. strtolower($coin_name) .'">'. $coin_name .'</a></li>';
						}
					}
					if ( !empty($res['sells']) ) {
						foreach ($res['sells'] as $key => $coin_ID) {
							$coin_name = get_coin_name_by_id($coin_ID);

							// eladás (csak akkor küldi ki, ha van ilyen befektetés)
							if ( get_investment_id($coin_name) > 0 ) {
								$sells []= '<li><a href="'. $site_url .'#'. strtolower($coin_name) .'">'. $coin_name .'</a></li>';
							}
						}
					}

					if ( !empty($must_sells) ) {
						$msg .=
										'<h4>Must Sells:</h4>
										 <ul>'. implode('', $must_sells) .'</ul><br>';
					}

					if ( !empty($sells) ) {
						$msg .=
										'<h4>Sells:</h4>
										 <ul>'. implode('', $sells) .'</ul><br>';
					}

					if ( !empty($msg) ) {
						crypto_send_message($msg, 'market status', 'check_market_status');
					}
		}
	}
}


function crypto_send_message($msg = "", $subject = "", $type = "", $next_message_after_this_time = "") {
	if ( empty($msg) ) { return ""; }
	if ( !empty($subject) ) { $subject = " - ". $subject; }
	if ( empty($type) ) { $type = 'normal'; } // 'normal' || 'error'
	if ( empty($next_message_after_this_time) ) { $next_message_after_this_time = 21600; } // 6 hour in sec
	$send_email = FALSE;


	$option_data = get_option( 'crypto_sent_message_'. $type );
	if ( isset($option_data['timestamp']) ) {
		if ( (strtotime(current_time('mysql')) - $option_data['timestamp']) >= $option_data['next_message_after_this_time']) {
			$send_email = TRUE;
		}
	} else {
		$send_email = TRUE;
	}


	if ( $send_email === TRUE ) {
		$site_url = get_site_url();
		$subject = str_replace('http://', '', $site_url). $subject;

		$headers = array( 'Content-Type: text/html; charset=UTF-8',
											'From: '. get_bloginfo('name') .' <noreply@crypto.paulovics.hu>',
										);
		wp_mail( get_option('admin_email') , $subject, $msg, $headers );


		$option_data = array(	'subject' => $subject,
													'msg' => $msg,
													'next_message_after_this_time' => $next_message_after_this_time,
													'timestamp' => strtotime(current_time('mysql'))
												);

		update_option( 'crypto_sent_message_'. $type, $option_data, false );
	}
}
