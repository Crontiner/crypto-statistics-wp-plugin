<?php

function add_bittrex_market_history($coin_id) {
	global $wpdb;
	if ( empty($coin_id) ) { return ""; }
	$coin_name = get_coin_name_by_id($coin_id);
	if ( empty($coin_name) ) { return ""; }

	// Remove old datas
	$wpdb->query(
		$wpdb->prepare(
			"DELETE FROM `{$wpdb->prefix}crypto_bittrex_market_history`
				WHERE `datetime` < %s
			",
				date('Y-m-d H:i:s', REMOVE_MARKET_HISTORY_TIMESTAMP)
			)
	);
	// ---

	if ( $coin_name == 'BCH' ) {
		$bittrex_datas = file_get_contents( 'https://bittrex.com/api/v1.1/public/getmarkethistory?market=BTC-BCC' );
	} else {
		$bittrex_datas = file_get_contents( 'https://bittrex.com/api/v1.1/public/getmarkethistory?market=BTC-'. strtoupper($coin_name));
	}

	$bittrex_datas = json_decode($bittrex_datas, true);

	$buys_array = array();
	$sells_array = array();

	if ( isset($bittrex_datas['result']) && !empty($bittrex_datas['result']) ) {
		foreach ($bittrex_datas['result'] as $key => $value) {

			if ( isset($value['OrderType']) ) {
				if ( $value['OrderType'] == 'SELL' ) {

					if ( isset($value['Quantity']) ) 		{ $sells_array['quantity'][]= $value['Quantity']; } // altcoin (ETH) price - not BTC!
					if ( isset($value['Price']) ) 			{ $sells_array['price'][]= $value['Price']; } // BTC price: 1 (ETH) in BTC (actual trade price)
					if ( isset($value['Total']) ) 			{ $sells_array['total'][]= $value['Total']; } // BTC price

				}
				else if ( $value['OrderType'] == 'BUY' ) {

					if ( isset($value['Quantity']) ) 		{ $buys_array['quantity'][]= $value['Quantity']; }
					if ( isset($value['Price']) ) 			{ $buys_array['price'][]= $value['Price']; }
					if ( isset($value['Total']) ) 			{ $buys_array['total'][]= $value['Total']; }

				}
			}

		}

		if ( !empty($sells_array) || !empty($buys_array) ) {

			$sells_array['quantity'] 	= array_sum($sells_array['quantity']);
			//$sells_array['price'] 		= number_format(array_sum($sells_array['price']) / count($sells_array['price']),8);
			$sells_array['total'] 		= array_sum($sells_array['total']);

			$buys_array['quantity'] 	= array_sum($buys_array['quantity']);
			//$buys_array['price'] 			= number_format(array_sum($buys_array['price']) / count($buys_array['price']),8);
			$buys_array['total'] 			= array_sum($buys_array['total']);


			if ( !empty($sells_array) ) {
				$wpdb->insert(
						$wpdb->prefix .'crypto_bittrex_market_history',
						array(
							'coin_ID' => $coin_id,
							'quantity' => $sells_array['quantity'],
							'total' => $sells_array['total'],
							'ordertype' => 's',
						),
						array(
							'%d','%s','%s','%s',
						)
				);
			}

			if ( !empty($buys_array) ) {
				$wpdb->insert(
						$wpdb->prefix .'crypto_bittrex_market_history',
						array(
							'coin_ID' => $coin_id,
							'quantity' => $buys_array['quantity'],
							'total' => $buys_array['total'],
							'ordertype' => 'b',
						),
						array(
							'%d','%s','%s','%s',
						)
				);
			}
		}

	}
}
