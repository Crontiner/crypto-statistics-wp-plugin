<?php

function set_cryp_markets_book_orders() {
	$markets_book_orders = file_get_contents( 'http://crypto3bh21.000webhostapp.com/?get_markets_book_orders=f8c3b22ed4a29ca0f0a7453b5c57b5e3&anticache='. wp_generate_password(8, false, false) );

	if ( !empty($markets_book_orders) ) {
		$markets_book_orders = json_decode(base64_decode($markets_book_orders), true);

		if ( isset($markets_book_orders[0]['buys']) ) {
			update_option('cryp_markets_book_orders', $markets_book_orders, false);
		}
	}
	return "";
}

function get_cryp_markets_book_orders() {
	$markets_book_orders = get_option('cryp_markets_book_orders');

	if ( isset($markets_book_orders[0]['buys']) ) {
		if ( (strtotime('NOW') - $markets_book_orders[0]['timestamp']) < 3600 ) {
			$markets_book_orders_temp = array();
			foreach ($markets_book_orders as $key => $val) {
				$markets_book_orders_temp [$val['coin_name']]= $val;
			}
			unset($markets_book_orders);
			return $markets_book_orders_temp;
		}
	}
	return "";
}

function get_cryp_markets_book_orders_diagram($coin_name = "") {
	if ( empty($coin_name) ) { return ""; }

	/*
	if ( empty($price_format) ) { $price_format = 'BTC'; }
	else { $price_format = strtoupper($price_format); }
	*/

	$markets_book_orders = get_cryp_markets_book_orders();
	$markets_book_orders = $markets_book_orders[$coin_name];
	$id = wp_generate_password(8,false,false);

	if ( isset($markets_book_orders['buys']) ) {
		$buys_array = $markets_book_orders['buys'];
		$sells_array = $markets_book_orders['sells'];

		if ( $coin_name == 'BTC' ) {
			$current_price = get_cmc_usd_price(get_coin_id_by_name($coin_name));
		} else {
			$current_price = get_bittrex_last_price(get_coin_id_by_name($coin_name));
		}


		unset($markets_book_orders['buys'], $markets_book_orders['sells']);

		$between_point = 0;
		$between_point_max = 0;
		$between_point_min = 0;
		if ( $buys_array['part10']['amount'] > $sells_array['part1']['amount'] ) {
			$between_point_max = $buys_array['part10']['amount'];
			$between_point_min = $sells_array['part1']['amount'];
		} else if ( $buys_array['part10']['amount'] < $sells_array['part1']['amount'] ) {
			$between_point_min = $buys_array['part10']['amount'];
			$between_point_max = $sells_array['part1']['amount'];
		}
		if ( $between_point_max != 0 ) {
			$between_point = (($between_point_max - $between_point_min) / 2) + $between_point_min;
		}

		array_sort_by_column($buys_array, 'min_price');
		array_sort_by_column($sells_array, 'min_price');

		$buys = array();
		$sells = array();
		$labels = array();
		foreach ($buys_array as $key => $val) {
			$buys []= '"'. $val['amount'] .'",';
			$labels []= '"'. $val['min_price'] .' - '. $val['max_price'] .'",';
			$sells []= '"0",';
		}

		$buys []= '"'. $between_point .'",';
		$sells []= '"'. $between_point .'",';
		//$labels []= '"'. $markets_book_orders['current_price'] .'",';
		$labels []= '"'. $current_price .'",';

		foreach ($sells_array as $key => $val) {
			$sells []= '"'. $val['amount'] .'",';
			$labels []= '"'. $val['min_price'] .' - '. $val['max_price'] .'",';
			$buys []= '"0",';
		}

		$result_html =
			'<canvas id="n3jk21'. $id .'"></canvas>
				<script type="text/javascript">
				jQuery(function($) {
					$(window).load(function(){
						var ctx = document.getElementById("n3jk21'. $id .'").getContext("2d");
						var chart = new Chart(ctx, {
								type: "line",
								data: {
										labels: ['. join($labels) .'],
										datasets: [{
												steppedLine: false,
												fill: true,
												label: "'. round($markets_book_orders['all_buys_amount_in_percent'],1) .'%",
												backgroundColor: "rgba(72, 194, 113, 1)",
												borderColor: "rgba(72, 194, 113, 1)",
												data: ['. join($buys) .'],
												steppedLine: false,
										},
										{
												steppedLine: false,
												fill: true,
												label: "'. round($markets_book_orders['all_sells_amount_in_percent'],1) .'%",
												backgroundColor: "rgba(255, 0, 0, 1)",
												borderColor: "rgba(255, 0, 0, 1)",
												data: ['. join($sells) .'],
												steppedLine: false,
										}]
								},
								options: { }
						});
					});
				});
				</script>';

				$max_buy_price = 0; // <-- melyik az az ár amelyiken a legtöbbet fognak venni vagyis a támasz
				$max_sell_price = 0;

				array_sort_by_column($buys_array, 'amount');
				$buys_array = array_reverse($buys_array);
				array_sort_by_column($sells_array, 'amount');
				$sells_array = array_reverse($sells_array);

				foreach ($buys_array as $key => $val) 	{ $max_buy_price = $val['max_price']; break; }
				foreach ($sells_array as $key => $val) 	{ $max_sell_price = $val['min_price']; break; }

				$between_low_and_current_percent = round(percent_between_two_numbers($current_price, $max_buy_price),1);
				$between_high_and_current_percent = round(percent_between_two_numbers($current_price, $max_sell_price),1);

				return 	'<div class="cryp_markets_book_orders_diagram">'.
									'<h4 class="sc_title">Order Book <small>- utolsó lekérés: <b>'. timeAgo($markets_book_orders['timestamp']) .'</b></small> | <small><b>'. $max_buy_price .'</b> ↔ ('. $between_low_and_current_percent .'%) ↔ <u>'. $current_price .'</u> ↔ ('. $between_high_and_current_percent .'%) ↔ <b>'. $max_sell_price .'</b></small></h4>'.
									$result_html.
								'</div>';
	}
	return "";
}
