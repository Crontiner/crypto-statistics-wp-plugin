<?php
/**
 * Plugin Name: Crypto
 * Plugin URI:
 * Description:
 * Version: 1.0
 * Author:
 * Author URI:
 */

// Buy/Sell (ask, bid)


/*****************/
/*    Globals    */
/*****************/

date_default_timezone_set("Europe/Budapest");

define( 'BTC_ID', 2 );
define( 'BCH_ID', 4 );
define( 'ETH_ID', 3 );
define( 'LTC_ID', 6 );
define( 'VTC_ID', 1 );
define( 'XMR_ID', 12 );
define( 'DASH_ID', 9 );
define( 'XRP_ID', 5 );

define( 'DAILY_SALES_AND_PURCHASE_INTERVAL_TIMESTAMP', strtotime(date('Y-m-01 00:00:00', strtotime("-3 month"))) ); // Mennyire régi adatokig legyen lekérve
define( 'TIME_INTERVAL_TIMESTAMP', strtotime("-2 month") ); // Mennyire régi adatokig legyen lekérve
define( 'LONG_TERM_TIME_INTERVAL_TIMESTAMP', strtotime("-94 days") ); // Mennyire régi adatokig legyen lekérve
define( 'REMOVE_MARKET_HISTORY_TIMESTAMP', strtotime('-36 hours') ); // Ennél régebbi adatok törölve lesznek a market historyból
define( 'GET_MARKET_HISTORY_TIMESTAMP', strtotime('-36 hours') ); // Ennyi lesz a legrégebbi számítandó adat
define( 'REMOVE_OLD_COMPILATION_DATAS_TIMESTAMP', strtotime('-3 hours') ); // Ennél régebbi adatok törölve lesznek
define( 'EGYSZERRE_ERTEKELENDO_COINOK', 6 );
define( 'FAV_COINS_ARRAY', json_encode(array( 'NEO', 'QTUM', 'LTC', 'ETH', 'XMR', 'XRP', 'DASH', 'BCH', 'ZEC', 'BTG', 'ETC', 'SC', 'XEM' )) );
define( 'SELL_BUY_MONTH_CACHE_REFRESH_TIME', 4 * 60 * 60 ); // 4 hours
define( 'MINING_DATA_DIAGRAMS_CACHE_REFRESH_TIME', 5 * 60 * 60 ); // 5 hours
define( 'DELETE_CRYPTOCOMPARE_SOCIALSTATS_OLD_UNNECESSARY_DATA', strtotime(current_time('mysql')) - 6 * 60 * 60 ); // - 6 hours

define( 'MTHUMB_URL', plugin_dir_url( __FILE__ ) .'lib/mthumb/mthumb.php?src=' );
define( 'PLUGIN_DIR_PATH', plugin_dir_path( __FILE__ ) );

define( 'PAGE_OSSZEALLITASOK', 42 );
define( 'PAGE_NEPSZERUSEG', 55 );
define( 'PAGE_BANYASZATI_ADATOK', 159 );

define( 'secret_ID', get_option('crypto_secred_id') );
define( 'CMC_PRO_API_KEY', get_option('crypto_cmc_pro_api_key') );

/**/


/*********************/
/*    Includes       */
/*********************/

$included_files_array = array(

		// Functions.php
		'inc/functions.php',
		'inc/functions_v2.php',


		// FUNCTIONS/...
		'inc/functions/add_binance_market_history.php',
		'inc/functions/add_bittrex_coin_prices.php',
		'inc/functions/add_btc_movement_info.php',
		'inc/functions/add_bittrex_market_history.php',
		'inc/functions/add_bitfinex_market_history.php',
		'inc/functions/add_hitbtc_market_history.php',
		'inc/functions/add_poloniex_market_history.php',
		'inc/functions/add_bithumb_market_history.php',
		'inc/functions/add_kraken_market_history.php',
		'inc/functions/send_message.php',
		'inc/functions/add_coindar_events.php',
		'inc/functions/add_cmc_missing_values.php',
		'inc/functions/add_cryptocompare_coin_ids.php',
		'inc/functions/add_cryptocompare_socialstats.php',
		'inc/functions/add_bitinfocharts_rich_wallets_datas.php',
		'inc/functions/add_technical_analysis_datas.php',
		'inc/functions/communicate_000webhost_server.php',
		'inc/functions/cryp_markets_book_orders.php',
		'inc/functions/send_bitinfocharts_alert.php',
		'inc/functions/add_bitinfocharts_rich_wallets_distribution.php',


		// CRON
		'inc/cron/get_crypto_coins_info.php',
		'inc/cron/create_crypto_statistic.php',


		// SHORTCODES
		'inc/shortcodes/get_weather_datas.php',
		'inc/shortcodes/crypto_statistic.php',
		'inc/shortcodes/crypto_statistic_v2.php',
		'inc/shortcodes/mining_data_diagrams.php',
		'inc/shortcodes/sell_buy_month.php',
		'inc/shortcodes/sell_buy_days.php',
		'inc/shortcodes/shitcoins_list.php',
		'inc/shortcodes/daily_sales_and_purchase.php',
		'inc/shortcodes/crypto_statistic_diagram.php',
		'inc/shortcodes/sells_and_buys_datas_evaluation.php',
		'inc/shortcodes/must_sell_and_buy_coins_from_compilation.php',
		'inc/shortcodes/coindar_events_list.php',
		'inc/shortcodes/test_sc.php',
		'inc/shortcodes/coin_popularity_list.php',
		'inc/shortcodes/compare_coin_prices.php',
		'inc/shortcodes/own_investments.php',
		'inc/shortcodes/rich_wallets_month_diagram.php',
		'inc/shortcodes/rich_wallets_daily_diagram.php',
		'inc/shortcodes/rich_wallets_amount_movements.php',
		'inc/shortcodes/rich_separated_wallets_amount_movements.php',
		'inc/shortcodes/own_investments_round_diagram.php',
		'inc/shortcodes/befektetes_kalkulator.php',
		'inc/shortcodes/mining_data_comparison.php',
		'inc/shortcodes/osszeallitasok_usd_alapjan.php',
		'inc/shortcodes/market_changes_graph.php',
		'inc/shortcodes/one_btc_mining_cost.php',
		'inc/shortcodes/coin_values_compare_in_percent.php',
		'inc/shortcodes/cmc_yearly_average_diagram.php',
		'inc/shortcodes/rich_wallets_distribution.php',


		// LIB
		'lib/Mobile-Detect-2.8.30/Mobile_Detect.php',
		'lib/tiny-html-minifier/tiny-html-minifier.php',
	);

foreach ($included_files_array as $key => $file) {
	if ( file_exists(PLUGIN_DIR_PATH .$file) ) { include PLUGIN_DIR_PATH .$file; }
}

/**/



/*********************/
/*    CRON UPDATE    */
/*********************/

add_filter('wp_head', 'crypto_cron_update_function');
function crypto_cron_update_function() {
	global $wpdb;

	// 5 percenként lefut
	if ( isset($_GET['create_crypto_statistic_cron_update']) && ( $_GET['create_crypto_statistic_cron_update'] == secret_ID ) ) {
		// Új adatok lekérésekor szüneteltetés

		// 2x fut le 1 órában
		if ( its_time_to_run(array(15, 45), 5) ) {
			set_cryp_markets_book_orders('update');
		}

		if ( !its_time_to_run(array(15)) ) {

			create_crypto_statistic_function("");
			remove_error_log_file();
			check_market_status();
			fav_coin_price_alert();
			price_changes_are_expected();
			high_profit_warning();

			if ( its_time_to_run(array(5, 20, 35, 50), 5) ) {
				add_technical_analysis_datas();
			}
		}

	}

	if ( isset($_GET['crypto_statistic_manual_update']) ) {
		create_crypto_statistic_function("manual_update");
	}

	// 2 óránként fut le
	if ( isset($_GET['get_crypto_coins_info_cron_update']) && ( $_GET['get_crypto_coins_info_cron_update'] == secret_ID ) ) {
		get_crypto_coins_info_cron_function();
		sell_buy_days_function("update");
		add_bitinfocharts_rich_wallets_distribution();
	}

	// 30 percenként fut le
	if ( isset($_GET['update_buy_or_sell_data']) && ( $_GET['update_buy_or_sell_data'] == secret_ID ) ) {
		check_buy_or_sell('update');
	}

	// 10 percenként lefut
	if ( isset($_GET['get_bittrex_market_history']) && ( $_GET['get_bittrex_market_history'] == secret_ID ) ) {

		$cmc_coins = $wpdb->get_results( "SELECT DISTINCT `coin_ID` FROM `{$wpdb->prefix}crypto_coinmarketcap_data`", ARRAY_A );
		foreach ($cmc_coins as $key => $value) {
			if ( $value['coin_ID'] != BTC_ID ) {

				if ( its_time_to_run(array(10, 30, 50), 5) ) {

					add_bittrex_market_history($value['coin_ID']);
					add_bitfinex_market_history($value['coin_ID']);
					add_hitbtc_market_history($value['coin_ID']);
					add_poloniex_market_history($value['coin_ID']);

				} else {
					add_bithumb_market_history($value['coin_ID']);
					add_kraken_market_history($value['coin_ID']);
					add_binance_market_history($value['coin_ID']);
				}

			}
		}

	}


	// 2x fut le 1 nap
	if ( isset($_GET['remove_shitcoins']) && ( $_GET['remove_shitcoins'] == secret_ID ) ) {
		remove_shitcoins();
		add_coindar_events();
		add_cmc_missing_values_for_crypto_coin_names_db_table();
		clear_cmc_avg_price_range_cache();
	}


	// 7 óránként fut le
	if ( isset($_GET['update_bitinfocharts_rich_wallets_datas']) && ( $_GET['update_bitinfocharts_rich_wallets_datas'] == secret_ID ) ) {
		add_bitinfocharts_rich_wallets_datas();
		send_bitinfocharts_wallet_alert();
	}


	// 6 óránként fut le
	if ( isset($_GET['update_cryptocompare_socialstats']) && ( $_GET['update_cryptocompare_socialstats'] == secret_ID ) ) {
		add_cryptocompare_coin_ids();
		add_cryptocompare_socialstats();
		coin_popularity_list_sc_function('update');
		mining_data_comparison_sc_function('update');
	}


	// hogy üres html fájlt mentsen le a cron
	if ( isset($_GET['create_crypto_statistic_cron_update']) ||
			 isset($_GET['get_crypto_coins_info_cron_update']) ||
			 isset($_GET['update_buy_or_sell_data']) ||
			 isset($_GET['get_bittrex_market_history']) ||
			 isset($_GET['remove_shitcoins']) ||
			 isset($_GET['update_bitinfocharts_rich_wallets_datas']) ||
			 isset($_GET['update_cryptocompare_socialstats'])
		 ) { die; }
}

/**/



/********************************/
/*         CSS & jQuery         */
/********************************/

add_filter('wp_footer', 'add_to_footer_function');
function add_to_footer_function() {
	global $post;
	wp_register_style ('googlefonts', 'https://fonts.googleapis.com/css?family=Roboto+Condensed:300,400,700&amp;subset=latin-ext', array(),'2','all');
	wp_enqueue_style( 'googlefonts');

	wp_enqueue_style( 'style_css', plugin_dir_url( __FILE__ ).'css/style.css' );

	//wp_enqueue_script( 'chart_js', 'https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.1/Chart.min.js', array( 'jquery' ));
	wp_enqueue_script( 'chart_js', plugin_dir_url( __FILE__ ).'js/Chart.min.js', array( 'jquery' )); // <--- 2.7.2

	if ( is_page(array(PAGE_BANYASZATI_ADATOK, PAGE_NEPSZERUSEG)) ) {
		wp_enqueue_script( 'tablesort_js', plugin_dir_url( __FILE__ ).'lib/tablesort-gh-pages/dist/tablesort.min.js', array( 'jquery' ));
		wp_enqueue_script( 'tablesort_numbers_js', plugin_dir_url( __FILE__ ).'lib/tablesort-gh-pages/dist/sorts/tablesort.number.min.js', array( 'jquery' ));
		//wp_enqueue_script( 'tablesort_date_js', plugin_dir_url( __FILE__ ).'lib/tablesort-gh-pages/dist/sorts/tablesort.date.min.js', array( 'jquery' ));
	}

	wp_enqueue_script( 'site_js', plugin_dir_url( __FILE__ ).'js/site.js', array( 'jquery' ));
	wp_localize_script( 'site_js', 'crypt', array( 'ajaxurl' => admin_url( 'admin-ajax.php' )));
}

 //add_action('admin_enqueue_scripts', 'crypt_enqueue_scripts_admin');
 function crypt_enqueue_scripts_admin () {
 	/* CSS */
 	wp_enqueue_style('crypt_admin_css', plugin_dir_url( __FILE__ ).'css/admin.css');

 	/* JS */
 	wp_enqueue_script( 'crypt_admin_js', plugin_dir_url( __FILE__ ) . 'js/admin.js' );
 }
 /**/


/***********************/
/*    CPT létrehozás   */
/***********************/

add_action( 'init', 'crypto_custom_post_types' );
function crypto_custom_post_types() {

	$labels = array(
			'name'                  => 'Befektetések',
			'singular_name'         => 'Befektetések',
			'add_new'               => 'Új befektetés hozzáadása',
			'add_new_item'          => 'Új befektetés hozzáadása',
			'edit_item'             => 'Befektetés szerkesztése',
			'new_item'              => 'Új befektetés',
			'view_item'             => 'Befektetés megtekintése',
			'search_items'          => 'Befektetés keresése',
			'not_found'             => 'Nincs ilyen befektetés',
			'not_found_in_trash'    => 'Nincs befektetés a lomtárban',
			'parent_item_colon'     => ''
	);
	$args = array(
			'labels' => $labels,
			'public' => true,
			'show_in_nav_menus' => true,
			'has_archive' => true,
			'menu_icon' => 'dashicons-chart-line',
			'supports' => array('title')
	);
	register_post_type('befektetesek_cpt', $args);

}

/**/


/***************/
/*   METABOX   */
/***************/

add_action( 'add_meta_boxes', 'crypto_custom_meta_box' );
function crypto_custom_meta_box($post) {
	add_meta_box('befektetesi_adatok_metabox', 'Adatok', 'befektetesi_adatok_metabox_function', 'befektetesek_cpt', 'normal' , 'default');
}

function befektetesi_adatok_metabox_function($post) {
    $coin_symbol_meta = get_post_meta($post->ID, 'coin_symbol_meta', true);
    $btc_exchange_rate_meta = get_post_meta($post->ID, 'btc_exchange_rate_meta', true);
    $amount_meta = get_post_meta($post->ID, 'amount_meta', true);

    echo "
        <table>
            <tr>
                <th>Coin Symbol: </th>
                <td><input type='text' style='width: 100%;' name='coin_symbol_meta' value='".$coin_symbol_meta."' /></td>
            </tr>
						<tr>
                <th>BTC árfolyam: </th>
                <td><input type='text' style='width: 100%;' name='btc_exchange_rate_meta' value='".$btc_exchange_rate_meta."' /></td>
            </tr>
            <tr>
                <th>Összeg (nem BTC): </th>
                <td><input type='text' style='width: 100%;' name='amount_meta' value='".$amount_meta."' /></td>
            </tr>
        </table>";
}

add_action('save_post', 'crypto_save_custom_meta_box');
function crypto_save_custom_meta_box($post_id) {
	if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE)
		return;
	global $post;

	if ($_POST['post_type'] == "befektetesek_cpt") {
		update_post_meta($post->ID, "coin_symbol_meta", $_POST['coin_symbol_meta']);
		update_post_meta($post->ID, "btc_exchange_rate_meta", $_POST['btc_exchange_rate_meta']);
		update_post_meta($post->ID, "amount_meta", $_POST['amount_meta']);
	}
}

/**/


/********************/
/*    Add Filters   */
/********************/

add_filter('show_admin_bar', '__return_false');


add_action( 'wp_footer', 'crypto_wp_footer_filter', 100 );
function crypto_wp_footer_filter() {
	$used_mem = memory_get_usage();
	$used_mem = ($used_mem / 1024) / 1024;
	$used_mem = round($used_mem,2);

	echo '<div class="crypto_wp_footer_filter">
					<p>used memory: '. $used_mem .'Mb</p>
				</div>';
}


// shortkódok html kimenetének tömörítése
add_filter('do_shortcode_tag', 'crypto_shortcode_after_filter', 10, 2);
function crypto_shortcode_after_filter( $output, $tag ) {

	$enable_shortcodes_array = array(

		// Front page
		'daily_sales_and_purchase',
		'must_sell_and_buy_coins_from_compilation',
		'coindar_events_list',
		'sells_and_buys_datas_evaluation',
		'shitcoins_list',
		'sell_buy_month',
		'sell_buy_days',

		// Statistics subpages
		'market_changes_graph',
		'get_weather_datas',
		'crypto_statistic_diagram',
		'compare_coin_prices',
		'coin_values_compare_in_percent',
		'cmc_yearly_average_diagram',

		// Saját befektetések
		'own_investments',
		'own_investments_round_diagram',

		// Rich wallets
		'rich_wallets_amount_movements',
		'rich_wallets_month_diagram',
		'rich_wallets_daily_diagram',
		'rich_separated_wallets_amount_movements',
		'rich_wallets_distribution_diagram',

		//'crypto_statistic_v2',
		'coin_popularity_list',
		'market_changes_graph',
		'osszeallitasok_usd_alapjan',
		'befektetes_kalkulator',
		'mining_data_diagrams',
		'mining_data_comparison',
	);

	if ( in_array($tag, $enable_shortcodes_array) ) {
		$output = str_replace('=">"', '="&#62;"', $output);
		$output = str_replace("='>'", "='&#62;'", $output);
		$output = str_replace('="<"', '="&#60;"', $output);
		$output = str_replace("='<'", "='&#60;'", $output);

		return TinyMinify::html($output, $options = [
							'collapse_whitespace' => false,
							'collapse_json_lt' => false,
						]);
	}
	return $output;
}

/**/


/********************/
/*    Add Actions   */
/********************/

/* W3C cache

	// Scheduled Action Hook
	function w3_flush_cache( ) {
		$w3_plugin_totalcache->flush_all();
	}

	// Schedule Cron Job Event
	add_action( 'wp', 'crypto_w3tc_cache_flush' );
	function crypto_w3tc_cache_flush() {
		if ( !wp_next_scheduled( 'w3_flush_cache' ) ) {
			wp_schedule_event( current_time( 'timestamp' ), 'daily', 'w3_flush_cache' );
		}
	}

/* /W3C cache */


add_action( 'init', 'crypto_login_protect' );
function crypto_login_protect() {
	if ( is_user_logged_in() ||
			isset($_GET['create_crypto_statistic_cron_update']) ||
			isset($_GET['get_crypto_coins_info_cron_update']) ||
			isset($_GET['update_buy_or_sell_data']) ||
			isset($_GET['get_bittrex_market_history']) ||
			isset($_GET['remove_shitcoins']) ||
			isset($_GET['update_bitinfocharts_rich_wallets_datas']) ||
			isset($_GET['update_cryptocompare_socialstats']) ||
			isset($_GET['login_code'])
	 ) { /* ok */ }
	else {
		die;
	}
}


// Add specific CSS class by filter
add_filter( 'body_class', 'crypto_body_class_function' );
function crypto_body_class_function( $classes ) {
	$detect = new Mobile_Detect;

	if ( $detect->isMobile() ) {
		$classes[] = 'view_on_mobile_screen';
	}

	return $classes;
}


remove_action('wp_head', 'print_emoji_detection_script', 7);
remove_action('wp_print_styles', 'print_emoji_styles');

/**/


/*******************************/
/*          AJAX               */
/*******************************/


add_action( 'wp_ajax_can_i_update_the_page', 'crypt_can_i_update_the_page' );
add_action( 'wp_ajax_nopriv_can_i_update_the_page', 'crypt_can_i_update_the_page' );
function crypt_can_i_update_the_page() {
	$last_compilation_update = (int) $_POST['last_compilation_update'];
	$result = "";

	if ( $last_compilation_update > 0 ) {
		global $wpdb;

		$latest_compilation_update = $wpdb->get_results( "SELECT `datetime` FROM `{$wpdb->prefix}crypto_compilation`
																											ORDER BY `datetime` DESC LIMIT 1", ARRAY_A );

		if ( strtotime($latest_compilation_update[0]['datetime']) > $last_compilation_update ) {
			if ( (strtotime(current_time('mysql')) - strtotime($latest_compilation_update[0]['datetime'])) >= (3 * 60) ) { // 3 perc után frissít

				echo json_encode(array('refresh' => 'true', 'hash' => $_POST['hash']));
				exit;
			}
		}
	}

	echo json_encode(array('refresh' => 'false', 'hash' => $_POST['hash']));
	exit;
}

/**/
