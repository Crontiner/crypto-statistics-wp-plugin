jQuery(function($) {
	var b = $('body');
	var w= b.innerWidth();

	if (w < 1850) {
		$('main.main-container .container').first().addClass('col-xs-12').removeClass('container');
	}

	function getMinute(t) {
	   var a = new Date(t * 1000);
	    var today = new Date();
	    var yesterday = new Date(Date.now() - 86400000);
	    var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
	    var year = a.getFullYear();
	    var month = months[a.getMonth()];
	    var date = a.getDate();
	    var hour = a.getHours();
	    var min = a.getMinutes();
			return min;
	    if (a.setHours(0,0,0,0) == today.setHours(0,0,0,0))
	        return 'today, ' + hour + ':' + min;
	    else if (a.setHours(0,0,0,0) == yesterday.setHours(0,0,0,0))
	        return 'yesterday, ' + hour + ':' + min;
	    else if (year == today.getFullYear())
	        return date + ' ' + month + ', ' + hour + ':' + min;
	    else
	        return date + ' ' + month + ' ' + year + ', ' + hour + ':' + min;
	}

	/*
	setInterval(function () {

		if (!document.hidden) {
			var timeStamp = Math.floor(Date.now() / 1000);
			var last_compilation_update = $('input#last_compilation_update').val();

			//console.log( getMinute(timeStamp) );

			if ( (getMinute(timeStamp) > 14) && (getMinute(timeStamp) < 20) ) { }
			else {
				if ( ( (timeStamp + 3600) - last_compilation_update ) >= 660 ) {
					//location.reload();
				}
			}
		} else {
			//console.dir( 'bezárva' );
		}

	}, 1000);
	*/


	setInterval(function () {

		if (!document.hidden) {

			// Search fresh data and page refresh

			var ajax_hash = Math.floor((Math.random() * 99999) + 1);
			var last_compilation_update = $('input#last_compilation_update').val();

			if ( last_compilation_update > 0 ) {
				jQuery.post(
					crypt.ajaxurl,
					{
						last_compilation_update : last_compilation_update,
						hash : ajax_hash,
						action : 'can_i_update_the_page'
					},
					function(data) {
							if (data.hash == ajax_hash) {
								if ( data.refresh == 'true' ) {
									location.reload();
								}
							}
					},'json'
				);
			}

		} else {
			//console.dir( 'bezárva' );
		}

	}, 5000);



	$('button.daily_sales_and_purchase_conditions_btn').click(function(e){
		e.preventDefault();
		$(this).closest('.row').find('ul').toggleClass('show_details');
	});


	$('table#crypto_statistic_v2_sc tr.more_info').each(function(){
		var $this = $(this);
		$this.prev().addClass('more_info_is_filled');
	});


	$('table#crypto_statistic_v2_sc tr.more_info').each(function(){
		var $this = $(this);
		var bittrex_datas = $this.find('.bittrex-datas table td').first().html();
		var miner_datas = $this.find('.miner-datas table td').first().html();
		var buys_and_sells = $this.find('.actual_buys_and_sells table td').first().html();

		if ( miner_datas == "" ) {
			$this.find('.miner-datas').remove();
		}
	});


	/*
	$('table#crypto_statistic_v2_sc tr.more_info .bittrex-datas').each(function(){
		var $this = $(this);
		var buys = $this.find('.bittrex_total_sells_and_buys').first().find('.latest_num').html();
		var sells = $this.find('.bittrex_total_sells_and_buys').last().find('.latest_num').html();

		if ( (buys != undefined) && (sells != undefined) ) {
			var buys_val = buys.replace('%', '');
			var sells_val = sells.replace('%', '');

			buys = '+';
			sells = '+';

			if ( buys_val.indexOf('-') >= 0 ) 	{ buys = '-'; }
			if ( sells_val.indexOf('-') >= 0 ) 	{ sells = '-'; }

			buys_val = parseFloat(buys_val);
			sells_val = parseFloat(sells_val);

			var res = '';

			if ( (buys == '+') && (sells == '+') && (buys_val > sells_val ) ) { res = '+'; }
			else if ( (buys == '+') && (sells == '+') && (buys_val < sells_val ) ) { res = '-'; }

			else if ( (buys == '-') && (sells == '-') ) { res = '-'; }

			else if ( (buys == '+') && (sells == '-') && (buys_val > sells_val ) ) { res = '+'; }
			else if ( (buys == '+') && (sells == '-') && (buys_val < sells_val ) ) { res = '-'; }

			else if ( (buys == '-') && (sells == '+') && (buys_val > sells_val ) ) { res = '+'; }
			else if ( (buys == '-') && (sells == '+') && (buys_val < sells_val ) ) { res = '-'; }

			$this.find('.bittrex_total_sells_and_buys').closest('td').attr('data-bittrex_total_sells_and_buys_res', res);
		}
	});
	*/

	if ( $('#coin_popularity_table').length ) {
		new Tablesort(document.getElementById('coin_popularity_table'), {
		  descending: true
		});
	}

	if ( $('#mining_data_comparison_table').length ) {
		new Tablesort(document.getElementById('mining_data_comparison_table'), {
		  descending: true
		});
	}

});
